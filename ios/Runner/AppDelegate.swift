import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GeneratedPluginRegistrant.register(with: self)
    let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
    let pushTrigger = FlutterMethodChannel(name: "com.orbi.tutor/pushTrigger", binaryMessenger: controller.binaryMessenger)
    pushTrigger.setMethodCallHandler({
              (call: FlutterMethodCall, result: FlutterResult) -> Void in
              switch call.method {
                  case "stopKiller":
                      result(true)
                      break
                  case "startKiller":
                      result(true)
                      break
                  default:
                      result(FlutterMethodNotImplemented)
                      break
              }
    })
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
