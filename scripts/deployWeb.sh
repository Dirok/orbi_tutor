fvm flutter clean
fvm flutter pub get
fvm flutter build web --web-renderer canvaskit --release --verbose
cd ./build/web || exit

gcloud auth activate-service-account --key-file "$FOCUSED_GCLOUD_CREDENTIALS"
gsutil -m rm -r gs://web.focusednow.io/
gsutil -m cp -r . gs://web.focusednow.io