importScripts("https://www.gstatic.com/firebasejs/8.6.1/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.6.1/firebase-messaging.js");

const firebaseConfig = {
  apiKey: "AIzaSyDaZdxGniyJbB-j8OUfYGZhI_MTJQws8FA",
  authDomain: "focusednow.firebaseapp.com",
  projectId: "focusednow",
  storageBucket: "focusednow.appspot.com",
  messagingSenderId: "276649359177",
  appId: "1:276649359177:web:dbcf0dc00ebc212347cc98",
  measurementId: "G-E79D3V0D2D"
};
 firebase.initializeApp(firebaseConfig);
  const messaging = firebase.messaging();

  messaging.onBackgroundMessage(function(payload) {
    console.log('Received background message ', payload);

    const notificationTitle = payload.notification.title;
    const notificationOptions = {
      body: payload.notification.body,
    };

    self.registration.showNotification(notificationTitle,
      notificationOptions);
  });