import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.exec
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2019_2.triggers.vcs
import jetbrains.buildServer.configs.kotlin.v2019_2.vcs.GitVcsRoot

/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2021.1"

project {

    vcsRoot(TeamCitySettings)

    buildType(BuildAndroidFirebase)
    buildType(DeployWeb)
    buildType(BuildIOsTestflight)
    buildType(Build)
    buildType(BuildIOsFirebase)
}

object Build : BuildType({
    name = "Build Google Internal"

    params {
        param("FLUTTER", "2.0.3")
        param("GIT_BRANCH", "master")
        param("ORBI_PROJECT", "D:/work/orbi_tutor")
    }

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        script {
            name = "Flutter build apk"
            workingDir = "%ORBI_PROJECT%"
            scriptContent = """
                cd %ORBI_PROJECT%
                git clean -fdx
                git pull
                git checkout -b teamcity origin/teamcity
                git branch -d %GIT_BRANCH%
                git checkout %GIT_BRANCH%
                Xcopy fastlane\notes.txt assets\translations\
                cd
                fvm flutter build appbundle
            """.trimIndent()
        }
        script {
            name = "Execute fastlane"
            executionMode = BuildStep.ExecutionMode.RUN_ON_FAILURE
            workingDir = "%ORBI_PROJECT%"
            scriptContent = "bundle exec fastlane googleInternal"
        }
    }

    triggers {
        vcs {
        }
    }
})

object BuildAndroidFirebase : BuildType({
    name = "Build Android Firebase"

    params {
        param("FLUTTER", "2.0.3")
        param("GIT_BRANCH", "master")
        param("ORBI_PROJECT", "D:/work/orbi_tutor")
    }

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        script {
            name = "Flutter build apk"
            workingDir = "%ORBI_PROJECT%"
            scriptContent = """
                cd %ORBI_PROJECT%
                git clean -fdx
                git pull
                git checkout -b teamcity origin/teamcity
                git branch -d %GIT_BRANCH%
                git checkout %GIT_BRANCH%
                Xcopy fastlane\notes.txt assets\translations\
                cd
                fvm flutter build apk
            """.trimIndent()
        }
        script {
            name = "Execute fastlane"
            executionMode = BuildStep.ExecutionMode.RUN_ON_FAILURE
            workingDir = "%ORBI_PROJECT%"
            scriptContent = "bundle exec fastlane firebase"
        }
    }

    triggers {
        vcs {
        }
    }

    failureConditions {
        nonZeroExitCode = false
    }

    requirements {
        contains("env.OS", "Windows_NT")
    }
})

object BuildIOsFirebase : BuildType({
    name = "Build iOs Firebase"

    enablePersonalBuilds = false
    type = BuildTypeSettings.Type.DEPLOYMENT
    maxRunningBuilds = 1

    params {
        param("FLUTTER", "2.0.3")
        param("GIT_BRANCH", "master")
        param("ORBI_PROJECT", "~/Developer/focused_now_tc_auto/orbi_tutor/")
    }

    vcs {
        root(DslContext.settingsRoot)

        cleanCheckout = true
    }

    steps {
        script {
            name = "Flutter build ipa"
            scriptContent = """
                #cd %ORBI_PROJECT%
                #git clean -fdx
                #git pull
                #git checkout -b teamcity
                #git branch -d %GIT_BRANCH%
                #git checkout %GIT_BRANCH%
                cp fastlane/notes.txt assets/translations/
                fvm flutter pub get
                fvm flutter build ios --release
            """.trimIndent()
        }
        script {
            name = "Execute fastlane"
            executionMode = BuildStep.ExecutionMode.RUN_ON_FAILURE
            scriptContent = """
                #cd %ORBI_PROJECT%
                #pwd
                #ls build/ios/iphoneos/
                bundle update
                bundle exec fastlane ios firebase
            """.trimIndent()
        }
    }

    triggers {
        vcs {
            enabled = false
        }
    }

    requirements {
        contains("teamcity.agent.jvm.os.name", "Mac OS X")
    }
})

object BuildIOsTestflight : BuildType({
    name = "Build iOs Testflight"

    params {
        param("FLUTTER", "2.0.3")
        param("GIT_BRANCH", "master")
        param("ORBI_PROJECT", "~/Developer/focused_now_tc_auto/orbi_tutor/")
    }

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        script {
            name = "Flutter build ipa"
            workingDir = "%ORBI_PROJECT%"
            scriptContent = """
                cd %ORBI_PROJECT%
                git clean -fdx
                git pull
                git checkout -b teamcity origin/teamcity
                git branch -d %GIT_BRANCH%
                git checkout %GIT_BRANCH%
                cp fastlane/notes.txt assets/translations/
                fvm flutter build apk
            """.trimIndent()
        }
        script {
            name = "Execute fastlane"
            executionMode = BuildStep.ExecutionMode.RUN_ON_FAILURE
            workingDir = "%ORBI_PROJECT%"
            scriptContent = """
                cd %ORBI_PROJECT%
                pwd
                ls build/ios/iphoneos/
                bundle exec fastlane ios testfly
            """.trimIndent()
        }
    }

    triggers {
        vcs {
        }
    }

    requirements {
        contains("teamcity.agent.jvm.os.name", "Mac OS X")
    }
})

object DeployWeb : BuildType({
    name = "Deploy Web"

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        exec {
            path = "scripts/deployWeb.sh"
            param("script.content", """
                fvm flutter clean
                fvm flutter pub get
                fvm flutter build web --web-renderer canvaskit --release --verbose
                cd ./build/web || exit
                
                gcloud auth activate-service-account --key-file "${'$'}FOCUSED_GCLOUD_CREDENTIALS"
                gsutil -m rm -r gs://web.focusednow.io/
                gsutil -m cp -r . gs://web.focusednow.io
            """.trimIndent())
        }
    }

    triggers {
        vcs {
        }
    }

    requirements {
        exists("env.FOCUSED_GCLOUD_CREDENTIALS")
    }
})

object TeamCitySettings : GitVcsRoot({
    name = "TeamCitySettings"
    url = "git@bitbucket.org:orbi360/teamcitysettings.git"
    branch = "master"
    authMethod = uploadedKey {
        uploadedKey = "teamcity_orbimac"
    }
})
