import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

void main() {
  const debugUrl = 'https://debug.imfocused.io';
  const email = 'teacher13@demo.ru';
  const password = '123456';

  group('Storage module', () {
    test('Should try save photo and throw exception for not mobile saving', () async {
      final file = File('test_resources/test_photo.jpg');
      var result = false;
      try {
        result = await StorageService().saveImageToGallery(file.path);
      } catch (e) {
        expect(e.runtimeType, CastError);
      }
      expect(result, false);
    });
  });

  group('Report module', () {
    setUp(() {
      Get.put(ConnectivityService());
    });
    test('Send report request', () async {
      final model = ReportViewModel();

      model.email.value = email;
      model.issue.value = password;
      final result = await model.trySendRequest();
      expect(result, true);
    });
  });

  group('Login module', () {
    final server = ServerEnvironment.custom(debugUrl);
    final _mainServer = Get.find<MainServerApi>();

    setUpAll(() async {
      Get.lazyPut(() => ConnectivityService());
      Get.lazyPut(() => UriLinkService());
      Get.lazyPut(() => PushService(), fenix: true);
      Get.lazyPut(() => ImageServiceMobile(), fenix: true);
      Get.lazyPut(() => ConfigService(), fenix: true);
      Get.lazyPut<LectureStorageService>(() => LectureStorageServiceMobile());
      Get.lazyPut(() => ServerUpdatesService(), fenix: true);
      Get.lazyPut(() => UserDataService(), fenix: true);
      Get.lazyPut(() => LectureDataService(), fenix: true);
      Get.lazyPut(() => AuthorizationService(), fenix: true);
      Get.lazyPut(() => MainServerApi(Settings.server.url), fenix: true);

      await Settings.load();
    });

    test('Send email & password request', () async {
      final model = LoginViewModel();

      await model.switchServer(server);
      model.email.value = email;
      model.password.value = password;

      model.listenEvents(
        expectAsync1((event) {
          expect(event is AuthorizationSuccessEvent, true);
        }),
      );

      model.signInWithEmail();
    });

    test('Send email request', () async {
      final model = LoginViewModel();

      await model.switchServer(server);
      model.email.value = email;
      model.password.value = '';

      model.listenEvents(
        expectAsync1((event) {
          expect(event is AuthorizationFailedEvent, true);
        }),
      );

      model.signInWithEmail();
    });

    test('Send password request', () async {
      final model = LoginViewModel();

      await model.switchServer(server);
      model.email.value = '';
      model.password.value = password;

      model.listenEvents(
        expectAsync1((event) {
          expect(event is AuthorizationFailedEvent, true);
        }),
      );

      model.signInWithEmail();
    });

    // Вот это пример объединения тестов в 1 тест
    group('Integrity login test', () {
      test('Test login', () async {
        final model = LoginViewModel();

        await model.switchServer(server);

        model.email.value = '';
        model.password.value = password;

        final firstListener = model.listenEvents(
          expectAsync1(
            (event) {
              expect(event is AuthorizationFailedEvent, true);
            },
            count: 2,
          ),
        );

        model.signInWithEmail();

        await Future.delayed(Duration(seconds: 4));

        model.email.value = email;
        model.password.value = '';

        model.signInWithEmail();

        await Future.delayed(Duration(seconds: 4));

        model.email.value = email;
        model.password.value = password;

        firstListener?.cancel();
        model.listenEvents(
          expectAsync1(
            (event) {
              expect(event is AuthorizationSuccessEvent, true);
            },
          ),
        );

        model.signInWithEmail();
      });
    });
  });
}
