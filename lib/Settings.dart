import 'package:bson/bson.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/get_utils.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tutor/focused.dart';

class Settings {
  static const String _serverKey = 'server';
  static const String _serverTypeKey = 'server_type';
  static const String _nameForLogsKey = 'name_for_logs';
  static const String _cameraResolutionKey = 'camera_resolution';
  static const String _paperWidthKey = 'paper_width';
  static const String _paperHeightKey = 'paper_height';
  static const String _cameraVerticalMirroringKey = 'camera_vertical_mirroring';
  static const String _paperRecognitionKey = 'paper_recognition';
  static const String _saveImagesKey = 'save_images';
  static const String _lastShownChangelog = 'last_shown_changelog';
  static const String _cameraDelayKey = 'camera_delay';
  static const String _stylusModeKey = 'stylus_mode';
  static const String _localizationKey = 'localization';
  static const String _countryKey = 'country';
  static const String _deviceIdKey = 'device_id';

  static final List<ServerEnvironment> serverTypeAddress = [
    ServerEnvironment.release(),
    ServerEnvironment.custom(),
  ];

  static const Duration httpsRequestTimeout = Duration(seconds: 10);
  static const Duration httpsFileRequestTimeout = Duration(seconds: 30);

  static const double whiteboardWidthFactor = 16.0;
  static const double whiteboardHeightFactor = 9.0;
  static get whiteboardAspectRatio => whiteboardWidthFactor / whiteboardHeightFactor;
  
  static const String _lecturesFolderName = 'Lectures';
  static const String _downloadsFolderName = 'Downloads';
  
  static String tmpFolderPath;
  static String downloadsFolderPath;
  static String lecturesFolderPath;

  /// Notification ID for persistent notification - same as in android native part
  static const int persistentNotificationId = 112231;

  /// Image quality after compression in percents - 0..100
  static const int imageCompressionQuality = 30;
  /// Image size in bytes when need to compress image
  static const int minImageSizeToCompress = 300000;

  static const allowedNumOfActiveLectures = 2;

  static SharedPreferences _preferences;

  static ServerEnvironment get server =>
      ServerEnvironment.fromString(_preferences?.getString(_serverKey));
  static set server(ServerEnvironment value) =>
      _preferences?.setString(_serverKey, value.toString());

  static ObjectId get deviceId {
    final deviceId = _preferences?.getString(_deviceIdKey);
    if (deviceId == null) return null;
    return ObjectId.fromHexString(deviceId);
  }
  static set deviceId(ObjectId value) => _preferences?.setString(_deviceIdKey, value.toHexString());

  static String get emailForLogs => _preferences?.getString(_nameForLogsKey);
  static set emailForLogs(String value) => _preferences?.setString(_nameForLogsKey, value);

  static CameraResolution get cameraResolution => CameraResolution.values[_preferences?.getInt(_cameraResolutionKey) ?? 2];
  static set cameraResolution(CameraResolution value) => _preferences?.setInt(_cameraResolutionKey, value.index);

  static double get cameraDelaySeconds => _preferences?.getDouble(_cameraDelayKey) ?? 1.0;
  static set cameraDelaySeconds(double value) => _preferences?.setDouble(_cameraDelayKey, value);

  static int get paperWidth => _preferences?.getInt(_paperWidthKey) ?? 297;

  static int get paperHeight => _preferences?.getInt(_paperHeightKey) ?? 210;

  static bool get isCameraVerticalMirroringEnabled => _preferences?.getBool(_cameraVerticalMirroringKey) ?? true;
  static set isCameraVerticalMirroringEnabled(bool value) => _preferences?.setBool(_cameraVerticalMirroringKey, value);

  static bool get isPaperRecognitionEnabled => _preferences?.getBool(_paperRecognitionKey) ?? true;
  static set isPaperRecognitionEnabled(bool value) => _preferences?.setBool(_paperRecognitionKey, value);

  static bool get isNeedToSaveImages => _preferences?.getBool(_saveImagesKey) ?? false;
  static set isNeedToSaveImages(bool value) => _preferences?.setBool(_saveImagesKey, value);

  static int get lastShownChangelog => _preferences?.getInt(_lastShownChangelog) ?? 0;
  static set lastShownChangelog(int value) => _preferences?.setInt(_lastShownChangelog, value);

  static bool _isStylusModeEnabled;
  static bool get isStylusModeEnabled => _isStylusModeEnabled ??= _preferences?.getBool(_stylusModeKey) ?? false;
  static set isStylusModeEnabled(bool value) {
    _isStylusModeEnabled = value;
    _preferences?.setBool(_stylusModeKey, value);
  }

  static String get localization => _preferences?.getString(_localizationKey);
  static set localization(String value) => _preferences?.setString(_localizationKey, value);

  static String get country => _preferences?.getString(_countryKey);
  static set country(String value) => _preferences?.setString(_countryKey, value);

  static load() async {
    _preferences = await SharedPreferences.getInstance();

    if (Settings.deviceId == null)
      Settings.deviceId = ObjectId();

    await migrateSettings();

    if (GetPlatform.isMobile) {
      final tmpFolder = await getTemporaryDirectory();
      tmpFolderPath = tmpFolder.path;
    } else {
      tmpFolderPath = ".";
    }

    String dataFolderPath;
    if (GetPlatform.isMobile) {
      final dataFolder = GetPlatform.isIOS
          ? await getLibraryDirectory()
          : await getExternalStorageDirectory();
      dataFolderPath = dataFolder.path;
    } else {
      dataFolderPath = ".";
    }

    downloadsFolderPath = join(dataFolderPath, _downloadsFolderName);
    lecturesFolderPath = join(dataFolderPath, _lecturesFolderName);

    cameraDelaySeconds = 1.0;
  }

  static Future<void> migrateSettings() async {
    // Migrate serverType from int to string 0.5.1 -> 0.5.2+
    try {
      Log.info('Getting server $server to test its type');
    } catch (e) {
      Log.warning('Migration of server');
      await _preferences?.remove(_serverTypeKey);
      server = serverTypeAddress.first;
    }
  }
}

enum CameraResolution {
  /// 720p (1280x720)
  high,

  /// 1080p (1920x1080)
  veryHigh,

  /// 2160p (3840x2160)
  ultraHigh,
}

enum PaperFormat {
  A4,
  A5,
  A6
}
