import 'package:bson/bson.dart';
import 'package:flutter/foundation.dart';
import 'package:tutor/focused.dart';

// Users

class GetOnlineListCommand extends WebSocketCommand {
  GetOnlineListCommand() : super('GetOnlineList');
}

class GetUserStateListCommand extends WebSocketCommand {
  GetUserStateListCommand() : super('GetUsersState');
}

class GetUserStateCommand extends WebSocketCommand {
  GetUserStateCommand(ObjectId userId)
      : super('GetUserState', data: {'UserID': userId.toHexString()});
}

class SetHandStateCommand extends WebSocketCommand {
  SetHandStateCommand(bool isRaised)
      : super('SetHandState', data: {'HandState': isRaised});
}

class SetUserHandStateCommand extends WebSocketCommand {
  SetUserHandStateCommand(ObjectId userId, bool isRaised)
      : super('SetUserHandState', data: {'UserID': userId.toHexString(), 'HandState': isRaised});
}

class SetAllowSpeakCommand extends WebSocketCommand {
  SetAllowSpeakCommand(ObjectId userId, bool value)
      : super('SetAllowSpeak', data: {'UserID': userId.toHexString(), 'AllowSpeak': value});
}

class SetMicEnabledCommand extends WebSocketCommand {
  SetMicEnabledCommand(bool value)
      : super('SetMicEnabled', data: {'MicEnabled': value});
}

// Whiteboards

class InsertWhiteboardCommand extends WebSocketCommand {
  InsertWhiteboardCommand(int index, Whiteboard wb)
      : super('InsertBoard', data: {
        'ID' : wb.id, 'Name': wb.name,'UserID': wb.ownerId, 'Index': index});
}

class SetCurrentWhiteboardCommand extends WebSocketCommand{
  SetCurrentWhiteboardCommand(ObjectId whiteboardId)
      : super('SetCurrentUserBoard', data: {'ID': whiteboardId.toHexString()});
}

class SetMainWhiteboardCommand extends WebSocketCommand{
  SetMainWhiteboardCommand(ObjectId whiteboardId)
      : super('SetMainBoard', data: {'BoardID': whiteboardId.toHexString()});
}

class GetMainWhiteboardId extends WebSocketCommand {
  GetMainWhiteboardId() : super('GetMainBoardID');
}

class GetWhiteboardCommand extends WebSocketCommand {
  GetWhiteboardCommand(ObjectId whiteboardId)
      : super('GetBoardInfo', data: {'BoardID': whiteboardId.toHexString()});
}

class GetUserWhiteboardsCommand extends WebSocketCommand {
  GetUserWhiteboardsCommand(ObjectId userId)
      : super('GetUserBoards', data: {'UserId': userId.toHexString()});
}

class SubscribeWhiteboardCommand extends WebSocketCommand {
  SubscribeWhiteboardCommand(ObjectId whiteboardId)
      : super('Subscribe', data: {'BoardID': whiteboardId.toHexString()});
}

class UnsubscribeWhiteboardCommand extends WebSocketCommand {
  UnsubscribeWhiteboardCommand(ObjectId whiteboardId)
      : super('Unsubscribe', data: {'BoardID': whiteboardId.toHexString()});
}

// Image

class GetWhiteboardImageCommand extends WebSocketCommand {
  GetWhiteboardImageCommand(String imageName)
      : super('GetImage', data: {'ImageID': imageName});
}

/// Command to insert image on whiteboard
///
/// [imageName] - name of the image file saved (originally [ImageID] but as [Name] is not used it should be written)
///
/// [boardId] - id of the board to insert to
///
/// [type] - type of the data: png, jpg, pdf, etc.
///
/// [setActive] - if this parameter is true it will send event that image is added to everyone
class AddImageCommand extends WebSocketCommand {
  AddImageCommand(ObjectId whiteboardId, ImageInfo image, {@required bool setActive})
      : super('InsertImageToBoard', data: {'ImageID': image.name,
          'BoardID': whiteboardId.toHexString(), 'Name': image.name,
          'Type': image.type.asString, 'SetActive': setActive});
}

// Pin

class SetPinStateCommand extends WebSocketCommand {
  SetPinStateCommand(bool enabled) : super('SetPinMode', data: {'PinMode' : enabled});
}

class GetPinStateCommand extends WebSocketCommand {
  GetPinStateCommand() : super('GetPinMode');
}

// Drawing

class DrawingCreatePathCommand extends WebSocketCommand {
  DrawingCreatePathCommand(ObjectId whiteboardId, int color, double strokeWidth, ScreenSize screenSize)
      : super('CreateDrawingPath', data: {
    'BoardID': whiteboardId.toHexString(),
    'Color': color,
    'StrokeWidth': strokeWidth,
    'ScreenSize': screenSize,
  });
}

class DrawingAddPointCommand extends WebSocketCommand {
  DrawingAddPointCommand(ObjectId whiteboardId, DrawingPoint point)
      : super('AddDrawingPoint', data: {'BoardID': whiteboardId.toHexString(), 'Point': point});
}

class DrawingUndoCommand extends WebSocketCommand {
  DrawingUndoCommand(ObjectId whiteboardId)
      : super('UndoDrawingPath', data: {'BoardID': whiteboardId.toHexString()});
}

class DrawingEraseCommand extends WebSocketCommand  {
  DrawingEraseCommand(ObjectId whiteboardId)
      : super('EraseDrawing', data: {'BoardID': whiteboardId.toHexString()});
}

class GetAllDrawingPathsCommand extends WebSocketCommand {
  GetAllDrawingPathsCommand(ObjectId whiteboardId)
      : super('GetAllDrawingPaths', data: {'BoardID': whiteboardId.toHexString()});
}

class UploadDrawingCommand extends WebSocketCommand {
  UploadDrawingCommand(Whiteboard wb)
      : super('ResetDrawingPaths', data: {'BoardID': wb.id, 'PathList': wb.drawingSet.serialDrawingData.toList()});
}