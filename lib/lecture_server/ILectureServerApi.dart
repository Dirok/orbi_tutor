import 'dart:async';

import 'package:bson/bson.dart';
import 'package:tutor/focused.dart';

typedef void OnImageReceived(ImageMessage msg);
typedef void OnUserWhiteboardInserted(Whiteboard whiteboard, int index);
typedef void OnWhiteboardImageChanged(ObjectId whiteboardId, String imageName);
typedef void OnDrawingPathAdded(ObjectId whiteboardId, DrawingData drawingData, ObjectId userId);
typedef void OnDrawingPointAdded(ObjectId whiteboardId, DrawingPoint point, ObjectId userId);
typedef void OnLastDrawingPathRemoved(ObjectId whiteboardId, ObjectId userId);
typedef void OnDrawingCleaned(ObjectId whiteboardId);
typedef void OnUserConnected(ObjectId userId);
typedef void OnUserDisconnected(ObjectId userId);
typedef void OnUserHandStateChanged(ObjectId userId, bool isRaised);
typedef void OnUserMicStateChanged(ObjectId userId, bool isEnabled);
typedef void OnMainWhiteboardChanged(MainWhiteboardInfo info);
typedef void OnPinStateChanged(bool isPinned);
typedef void OnAllowSpeakChanged(bool allowSpeak);
typedef void OnLectureFinished();
typedef void OnLectureCheckingMode();

typedef void OnConnectionClosed();

abstract class ILectureServerApi {
  OnImageReceived imageReceived;
  OnUserWhiteboardInserted userWhiteboardInserted;
  OnWhiteboardImageChanged whiteboardImageChanged;
  OnDrawingPathAdded drawingPathAdded;
  OnDrawingPointAdded drawingPointAdded;
  OnLastDrawingPathRemoved lastDrawingPathRemoved;
  OnDrawingCleaned drawingCleared;
  OnUserConnected userConnected;
  OnUserDisconnected userDisconnected;
  OnUserHandStateChanged userHandStateChanged;
  OnUserMicStateChanged userMicStateChanged;
  OnMainWhiteboardChanged mainWhiteboardChanged;
  OnPinStateChanged pinStateChanged;
  OnAllowSpeakChanged allowSpeakChanged;
  OnLectureFinished lectureFinished;
  OnLectureCheckingMode lectureCheckingMode;

  OnConnectionClosed connectionClosed;

  bool get isConnected;
  bool get isDisconnected;

  Future<bool> connect(Lecture lecture, {String jwt, ObjectId userId});

  Future<void> close();

  Future<bool> getPinState();
  Future<MainWhiteboardInfo> getMainWhiteboardInfo();
  Future<void> sendCommonWhiteboardSet(WhiteboardSet whiteboardSet);
  Future<void> sendInsertWhiteboard(int index, Whiteboard whiteboard, ObjectId whiteboardSetId);
  Future<void> sendSetCurrentWhiteboard(ObjectId whiteboardId, ObjectId whiteboardSetId);
  Future<UserState> getUserState(ObjectId userId);
  Future<WhiteboardSet> getCommonWhiteboardSet();
  Future<WhiteboardSet> getUserWhiteboardsInfo(ObjectId userId);
  Future<void> subscribeWhiteboard(ObjectId whiteboardId);
  Future<void> unsubscribeWhiteboard(ObjectId whiteboardId);
  Future<Whiteboard> loadWhiteboardInfo(ObjectId whiteboardId);
  Future<List<ObjectId>> getOnlineUserList();
  Future<void> setMainWhiteboard(ObjectId whiteboardId);
  Future<Map<ObjectId, UserState>> getStudentStates();
  Future<bool> sendStudentHandState(ObjectId studentId, bool isHandRaised);
  Future<bool> sendStudentAllowSpeak(ObjectId studentId, bool allow);
  Future<bool> sendPinState(bool enabled);
  Future<bool> setHandState(bool isHandRaised);
  Future<bool> setMicState(bool isEnabled);
  Future<DrawingSet> getAllDrawingData(Whiteboard whiteboard);

  bool requestWhiteboardImage(ObjectId whiteboardId, ImageInfo imageInfo);
  void uploadDrawing(Whiteboard wb);
  void createDrawingPath(ObjectId whiteboardId, DrawingData drawingData);
  void addDrawingPoint(ObjectId whiteboardId, DrawingPoint point, ObjectId drawingId);
  void addDrawingPath(ObjectId whiteboardId, DrawingData drawingData);
  void undoDrawing(ObjectId whiteboardId, ObjectId drawingId);
  void clearDrawing(ObjectId whiteboardId);

  Future<SendDataResult> uploadImage(String token, ImageInfo image, ObjectId lectureId, ObjectId whiteboardId);
  Future<ImageInfo> downloadImage(String token, ObjectId lectureId, String filename, String filePath);
}

class ImageMessage {
  final ObjectId whiteboardId;
  final ImageInfo imageInfo;

  ImageMessage(this.whiteboardId, this.imageInfo);
}

enum SendDataResult {
  success,
  fail,
  tooLarge,
  noData
}