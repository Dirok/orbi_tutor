import 'package:bson/bson.dart';

abstract class WebSocketCommand {
  final ObjectId transactionId = ObjectId();
  final String commandName;
  final Map<String, dynamic> data;

  WebSocketCommand(this.commandName, {this.data});

  Map<String, dynamic> toJson() =>
      {
        'TransactionID': transactionId.toHexString(),
        'Command': commandName,
        'Data': data ?? ''
      };
}