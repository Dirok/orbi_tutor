import 'dart:convert';

import 'package:bson/bson.dart';
import 'package:flutter/foundation.dart';
import 'package:tutor/focused.dart';

enum ActionType {
  unknown,
  whiteboardInserted,
  whiteboardChanged,
  imageAdded,
  drawingAdded,
  drawingCreated,
  drawingPoint,
  drawingUndo,
  drawingClear,
  userStateAdded,
  userStateChanged,
  userMicStateChanged,
  userHandStateChanged,
  userAllowSpeakChanged
}

extension ActionTypeEx on ActionType {
  String get asString => describeEnum(this);
}

class ActionTypeHelper {
  static final Map<String, ActionType> _strMap =
  Map<String, ActionType>.fromIterable(
      ActionType.values, key: (v) => describeEnum(v), value: (v) => v);

  static ActionType fromString(String value) =>
      value != null && value != '' && _strMap.containsKey(value) ? _strMap[value] : ActionType.unknown;
}

class Action {
  ObjectId whiteboardSetId;
  ObjectId whiteboardId;
  ObjectId userId;
  ActionType actionType;

  Action.fromJson(Map<String, dynamic> json) :
        whiteboardSetId = json.containsKey(RealmKeys.whiteboardSetId)
            ? ObjectId.fromHexString(json[RealmKeys.whiteboardSetId]) : null,
        whiteboardId = json.containsKey(RealmKeys.whiteboardId)
            ? ObjectId.fromHexString(json[RealmKeys.whiteboardId]) : null,
        userId = json.containsKey(RealmKeys.userId)
            ? ObjectId.fromHexString(json[RealmKeys.userId]) : null,
        actionType = ActionTypeHelper.fromString(json[RealmKeys.actionType]);

  factory Action.fromRealmJson(Map<String, dynamic> json) {
    final actionType = ActionTypeHelper.fromString(json[RealmKeys.actionType]);
    switch (actionType) {
      case ActionType.whiteboardInserted:
        return WhiteboardInsertedAction.fromJson(json);
      case ActionType.imageAdded:
        return ImageAddedAction.fromJson(json);
      case ActionType.drawingCreated:
      case ActionType.drawingUndo:
      case ActionType.drawingClear:
        return DrawingAction.fromJson(json);
      case ActionType.drawingPoint:
        return DrawingPointAction.fromJson(json);
      case ActionType.drawingAdded:
        return DrawingAdded.fromJson(json);
      case ActionType.userStateAdded:
      case ActionType.userStateChanged:
        return UserStateAction.fromJson(json);
      case ActionType.userMicStateChanged:
      case ActionType.userHandStateChanged:
      case ActionType.userAllowSpeakChanged:
        return UserStateParameterChangedAction<bool>.fromJson(json);
      default:
        return Action.fromJson(json);
    }
  }
}

class UserStateAction extends Action {
  final UserState userState;

  UserStateAction.fromJson(Map<String, dynamic> json)
      : userState = UserState.fromRealmJson(json[RealmKeys.userState]),
        super.fromJson(json);
}

class UserStateParameterChangedAction<T> extends Action {
  final T value;

  UserStateParameterChangedAction.fromJson(Map<String, dynamic> json)
      : value = json[RealmKeys.value] as T,
        super.fromJson(json);
}

class WhiteboardInsertedAction extends Action {
  final Whiteboard whiteboard;
  final int position;

  WhiteboardInsertedAction.fromJson(Map<String, dynamic> json)
      : whiteboard = Whiteboard.fromRealmJson(
      Map<String, dynamic>.from(json[RealmKeys.whiteboard])),
        position = json[RealmKeys.position],
        super.fromJson(json);
}

class ImageAddedAction extends Action {
  final ImageInfo image;

  ImageAddedAction.fromJson(Map<String, dynamic> json)
      : image = ImageInfo.fromRealmJson(
      Map<String, dynamic>.from(json[RealmKeys.image])),
        super.fromJson(json);
}

class DrawingAdded extends Action {
  final DrawingData drawing;

  DrawingAdded.fromJson(Map<String, dynamic> json)
      : drawing = DrawingData.fromRealmJson(
      Map<String, dynamic>.from(json[RealmKeys.drawing])),
        super.fromJson(json);
}

class DrawingAction extends Action {
  final ObjectId drawingId;

  DrawingAction.fromJson(Map<String, dynamic> json)
      : drawingId = ObjectId.fromHexString(json[RealmKeys.drawingId]),
        super.fromJson(json);
}

class DrawingPointAction extends DrawingAction {
  final DrawingPoint point;

  DrawingPointAction.fromJson(Map<String, dynamic> json)
      : point = DrawingPoint.fromRealmJson(jsonDecode(json[RealmKeys.point])),
        super.fromJson(json);
}