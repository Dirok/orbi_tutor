import 'package:flutter/foundation.dart';

enum WebSocketDataType {
  unknown,
  file,
  image
}

class WebSocketDataTypeHelper {
  static final Map<String, WebSocketDataType> _strMap =
  Map<String, WebSocketDataType>.fromIterable(
      WebSocketDataType.values, key: (v) => describeEnum(v), value: (v) => v);

  static WebSocketDataType fromString(String value) =>
      _strMap.containsKey(value) ? _strMap[value] : WebSocketDataType.unknown;
}

extension WebSocketDataTypeEx on WebSocketDataType {
  String get asString => describeEnum(this);
}

abstract class WebSocketDataDescription {
  Map<String, dynamic> toJson();
}