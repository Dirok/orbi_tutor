import 'dart:async';
import 'dart:collection';

import 'package:bson/bson.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class LectureServerClassicApi implements ILectureServerApi {
  final _s3Server = Get.find<S3ServerApi>();
  final LectureServerTextChannel _textServer = LectureServerTextChannel();
  final _imageService = Get.find<ImageService>();

  final HashSet<ObjectId> _subscribedWhiteboardIds = HashSet();

  Map<String, Function(Map<String, dynamic>)> _eventMap;

  OnImageReceived imageReceived;
  OnUserWhiteboardInserted userWhiteboardInserted;
  OnWhiteboardImageChanged whiteboardImageChanged;
  OnDrawingPathAdded drawingPathAdded;
  OnDrawingPointAdded drawingPointAdded;
  OnLastDrawingPathRemoved lastDrawingPathRemoved;
  OnDrawingCleaned drawingCleared;
  OnUserConnected userConnected;
  OnUserDisconnected userDisconnected;
  OnUserHandStateChanged userHandStateChanged;
  OnUserMicStateChanged userMicStateChanged;
  OnMainWhiteboardChanged mainWhiteboardChanged;
  OnPinStateChanged pinStateChanged;
  OnAllowSpeakChanged allowSpeakChanged;
  OnLectureFinished lectureFinished;
  OnLectureCheckingMode lectureCheckingMode;

  OnConnectionClosed connectionClosed;

  bool get isConnected => _textServer.isConnected;
  bool get isDisconnected => !isConnected;

  LectureServerClassicApi() {
    _eventMap = {
      UserWhiteboardInsertedMessage.messageType: _onUserWhiteboardInserted,
      WhiteboardImageChangedMessage.messageType: _onWhiteboardImageChanged,
      DrawingCreatePathMessage.messageType: _onDrawingPathAdded,
      DrawingAddPointMessage.messageType: _onDrawingPointAdded,
      DrawingUndoMessage.messageType: _onLastDrawingPathRemoved,
      DrawingEraseMessage.messageType: _onDrawingCleaned,
      UserConnectedMessage.messageType: _onUserConnected,
      UserDisconnectedMessage.messageType: _onUserDisconnected,
      HandStateChangedMessage.messageType: _onUserHandStateChanged,
      MicStateChangedMessage.messageType: _onUserMicStateChanged,
      MainWhiteboardChangedMessage.messageType: _onMainWhiteboardChanged,
      PinStateChangedMessage.messageType: _onPinStateChanged,
      AllowSpeakChangedMessage.messageType: _onAllowSpeakChanged,
      LectureFinishedMessage.messageType: _onLectureFinished,
      LectureCheckingModeMessage.messageType: _onLectureCheckingMode
    };

    _textServer.messageReceived = _onMessageReceived;

    _textServer.connectionClosed = _onConnectionClosed;
    _textServer.connectionError = _onConnectionClosed;
  }

  Future<bool> connect(Lecture lecture, {String jwt, ObjectId userId}) {
    var isTextServerConnected = _textServer.initConnection(lecture.server, lecture.key);
    return Future.value(isTextServerConnected);
  }

  _onConnectionClosed() => connectionClosed?.call();

  Future<void> close() async {
    Log.info('close');

    _textServer.messageReceived = null;
    _textServer.connectionClosed = null;
    _textServer.connectionError = null;

    await _textServer.close();
  }

  // data

  Future<SendDataResult> uploadImage(String token, ImageInfo image, ObjectId lectureId,
      ObjectId whiteboardId) async {
    final data = _imageService.getImageBytesFromLocalStorage(image);
    final filename = image.name;

    if (data == null || filename == null) {
      Log.error('${data == null ? 'Data' : 'Name'} is null');
      return SendDataResult.noData;
    }

    final parameters = 'lesson=${lectureId.toHexString()}&filename=$filename';
    final uploadResult = await _s3Server.upload(token, data, parameters, filename);

    if (uploadResult.isSuccess) {
      final isImageAdded = await _addImageInfo(whiteboardId, image);
      return isImageAdded ? SendDataResult.success : SendDataResult.fail;
    } else {
      Log.error('Image uploading error');
      return SendDataResult.fail;
    }
  }

  Future<bool> _addImageInfo(ObjectId whiteboardId, ImageInfo image) async {
    final result = await _textServer.sendCommand(
        AddImageCommand(whiteboardId, image, setActive: true));

    if (result == null) {
      Log.error('Add image info request error');
      return false;
    }

    var msg = _try(() => WhiteboardImageChangedMessage.fromJson(result.data));
    return msg != null;
  }

  Future<ImageInfo> downloadImage(
      String token, ObjectId lectureId, String filename, String filePath) async {
    final parameters = 'lesson=${lectureId.toHexString()}&filename=$filename';
    final result = await _s3Server.download(token, parameters, filename);
    if (!result.isSuccess || result.payload == null) return null;

    final image = ImageInfo(filename, ImageType.Picture, ObjectId(), folderPath: filePath);

    final isSuccess = await _imageService.saveImageBytesToLocalStorage(image, result.payload);
    if (!isSuccess) return null;

    return image;
  }

  // text

  _onMessageReceived(WebSocketMessage msg) {
    if (_eventMap.containsKey(msg.messageType))
      _eventMap[msg.messageType].call(msg.data);
    else
      Log.warning('Unknown message type ${msg.messageType}');
  }

  // events

  T _try<T>(T Function() func) {
    try {
      return func?.call();
    } catch (e) {
      Log.error(e);
      return null;
    }
  }

  _onUserWhiteboardInserted(Map<String, dynamic> json) async {
    var msg = _try(() => UserWhiteboardInsertedMessage.fromJson(json));
    if (msg == null) return;

    final wb = await loadWhiteboardInfo(msg.whiteboardId);
    if (wb != null)
      userWhiteboardInserted?.call(wb, msg.index);
  }

  _onWhiteboardImageChanged(Map<String, dynamic> json) {
    var msg = _try(() => WhiteboardImageChangedMessage.fromJson(json));
    if (msg != null)
      whiteboardImageChanged?.call(msg.whiteboardId, msg.imageName);
  }

  _onDrawingPathAdded(Map<String, dynamic> json) {
    var msg = _try(() => DrawingCreatePathMessage.fromJson(json));
    if (msg != null)
      drawingPathAdded?.call(msg.whiteboardId, msg.drawingData, msg.userId);
  }

  _onDrawingPointAdded(Map<String, dynamic> json) {
    var msg = _try(() => DrawingAddPointMessage.fromJson(json));
    if (msg != null)
      drawingPointAdded?.call(msg.whiteboardId, msg.point, msg.userId);
  }

  _onLastDrawingPathRemoved(Map<String, dynamic> json) {
    var msg = _try(() => DrawingUndoMessage.fromJson(json));
    if (msg != null)
      lastDrawingPathRemoved?.call(msg.whiteboardId, msg.userId);
  }

  _onDrawingCleaned(Map<String, dynamic> json) {
    var msg = _try(() => DrawingEraseMessage.fromJson(json));
    if (msg != null)
      drawingCleared?.call(msg.whiteboardId);
  }

  _onUserConnected(Map<String, dynamic> json) {
    var msg = _try(() => UserConnectedMessage.fromJson(json));
    if (msg != null)
      userConnected?.call(msg.userId);
  }

  _onUserDisconnected(Map<String, dynamic> json) {
    var msg = _try(() => UserDisconnectedMessage.fromJson(json));
    if (msg != null)
      userDisconnected?.call(msg.userId);
  }

  _onUserHandStateChanged(Map<String, dynamic> json) {
    var msg = _try(() => HandStateChangedMessage.fromJson(json));
    if (msg != null)
      userHandStateChanged?.call(msg.userId, msg.isRaised);
  }

  _onUserMicStateChanged(Map<String, dynamic> json) {
    var msg = _try(() => MicStateChangedMessage.fromJson(json));
    if (msg != null)
      userMicStateChanged?.call(msg.userId, msg.isEnabled);
  }

  _onMainWhiteboardChanged(Map<String, dynamic> json) {
    var msg = _try(() => MainWhiteboardChangedMessage.fromJson(json));
    if (msg == null) return;

    final info = MainWhiteboardInfo(
      id: msg.id,
      ownerId: msg.ownerId,
      containingSequenceLength: msg.containingSequence.length,
      index: msg.index,
    );
    mainWhiteboardChanged?.call(info);
  }

  _onPinStateChanged(Map<String, dynamic> json) {
    var msg = _try(() => PinStateChangedMessage.fromJson(json));
    if (msg != null)
      pinStateChanged?.call(msg.isPinned);
  }

  _onAllowSpeakChanged(Map<String, dynamic> json) {
    var msg = _try(() => AllowSpeakChangedMessage.fromJson(json));
    if (msg != null)
      allowSpeakChanged?.call(msg.allowSpeak);
  }

  _onLectureFinished(Map<String, dynamic> json) => lectureFinished?.call();

  _onLectureCheckingMode(Map<String, dynamic> json) => lectureCheckingMode?.call();

  // commands

  Future<bool> getPinState() async {
    var result = await _textServer.sendCommand(GetPinStateCommand());
    if (result == null) throw CommandFailedException();
    var msg = _try(() => PinStateMessage.fromJson(result.data));
    if (msg == null) throw CommandFailedException();
    return msg.isPinned;
  }

  Future<MainWhiteboardInfo> getMainWhiteboardInfo() async {
    var result = await _textServer.sendCommand(GetMainWhiteboardId());
    if (result == null) throw CommandFailedException();
    var msg = _try(() => MainWhiteboardMessage.fromJson(result.data));
    if (msg == null) throw CommandFailedException();

    final info = MainWhiteboardInfo(id: msg.id, ownerId: msg.ownerId,
      containingSequenceLength: msg.containingSequence.length, index: msg.index);
    return info;
  }

  @override
  Future<void> sendCommonWhiteboardSet(WhiteboardSet whiteboardSet) {
    throw UnimplementedError();
  }

  Future<void> sendInsertWhiteboard(int index, Whiteboard wb, ObjectId whiteboardSetId) async {
    var result = await _textServer.sendCommand(InsertWhiteboardCommand(index, wb));
    if (result == null) throw CommandFailedException();
    // var msg = WhiteboardInsertedMessage.fromJson(result.data);
  }

  Future<void> sendSetCurrentWhiteboard(ObjectId whiteboardId, ObjectId whiteboardSetId) async {
    var result = await _textServer.sendCommand(SetCurrentWhiteboardCommand(whiteboardId));
    if (result == null) throw CommandFailedException();
    // var msg = CurrentWhiteboardChangedMessage.fromJson(result.data);
  }

  Future<UserState> getUserState(ObjectId userId) async {
    var result = await _textServer.sendCommand(GetUserStateCommand(userId));
    if (result == null) throw CommandFailedException();
    var msg = _try(() => UserStateMessage.fromJson(result.data));
    if (msg == null) throw CommandFailedException();
    return msg.userState;
  }

  Future<WhiteboardSet> getCommonWhiteboardSet() {
    throw UnimplementedError();
  }

  Future<WhiteboardSet> getUserWhiteboardsInfo(ObjectId userId) async {
    var result = await _textServer.sendCommand(GetUserWhiteboardsCommand(userId));
    if (result == null) throw CommandFailedException();
    var msg = _try(() => UserWhiteboardsMessage.fromJson(result.data));
    if (msg == null) throw CommandFailedException();
    return msg.whiteboardSet;
  }

  Future<void> subscribeWhiteboard(ObjectId whiteboardId) async {
    if (_subscribedWhiteboardIds.contains(whiteboardId)) return; // TODO check
    var result = await _textServer.sendCommand(SubscribeWhiteboardCommand(whiteboardId));
    if (result == null) throw CommandFailedException();
    _subscribedWhiteboardIds.add(whiteboardId);
  }

  Future<void> unsubscribeWhiteboard(ObjectId whiteboardId) async {
    if (!_subscribedWhiteboardIds.contains(whiteboardId)) return;
    var result = await _textServer.sendCommand(UnsubscribeWhiteboardCommand(whiteboardId));
    if (result == null) throw CommandFailedException();
    _subscribedWhiteboardIds.remove(whiteboardId);
  }

  Future<Whiteboard> loadWhiteboardInfo(ObjectId whiteboardId) async {
    var result = await _textServer.sendCommand(GetWhiteboardCommand(whiteboardId));
    if (result == null) throw CommandFailedException();
    var msg = _try(() => WhiteboardInfoMessage.fromJson(result.data));
    if (msg == null) throw CommandFailedException();
    return msg.whiteboard;
  }

  Future<List<ObjectId>> getOnlineUserList() async {
    var result = await _textServer.sendCommand(GetOnlineListCommand());
    if (result == null) throw CommandFailedException();
    var msg = _try(() => OnlineUsersMessage.fromJson(result.data));
    if (msg == null) throw CommandFailedException();
    return msg.userIdList;
  }

  Future<void> setMainWhiteboard(ObjectId whiteboardId) async {
    var result = await _textServer.sendCommand(SetMainWhiteboardCommand(whiteboardId));
    if (result == null) throw CommandFailedException();
    // var msg = MainWhiteboardChangedMessage.fromJson(result.data);
  }

  Future<Map<ObjectId, UserState>> getStudentStates() async {
    var result = await _textServer.sendCommand(GetUserStateListCommand());
    if (result == null) throw CommandFailedException();
    var msg = _try(() => UserStateListMessage.fromJson(result.data));
    if (msg == null) throw CommandFailedException();
    return msg.userStateMap;
  }

  Future<bool> sendStudentHandState(ObjectId studentId, bool isHandRaised) async {
    var result = await _textServer.sendCommand(SetUserHandStateCommand(studentId, isHandRaised));
    if (result == null) throw CommandFailedException();
    var msg = _try(() => HandStateChangedMessage.fromJson(result.data));
    return msg.isRaised;
  }

  Future<bool> sendStudentAllowSpeak(ObjectId studentId, bool allow) async {
    var result = await _textServer.sendCommand(SetAllowSpeakCommand(studentId, allow));
    if (result == null) throw CommandFailedException();
    var msg = _try(() => AllowSpeakChangedMessage.fromJson(result.data));
    return msg.allowSpeak;
  }

  Future<bool> sendPinState(bool enabled) async {
    var result = await _textServer.sendCommand(SetPinStateCommand(enabled));
    if (result == null) throw CommandFailedException();
    var msg = _try(() => PinStateMessage.fromJson(result.data));
    if (msg == null) throw CommandFailedException();
    return msg.isPinned;
  }

  Future<bool> setHandState(bool isHandRaised) async {
    var result = await _textServer.sendCommand(SetHandStateCommand(isHandRaised));
    if (result == null) throw CommandFailedException();
    var msg = _try(() => HandStateChangedMessage.fromJson(result.data));
    if (msg == null) throw CommandFailedException();
    return msg.isRaised;
  }

  Future<bool> setMicState(bool isEnabled) async {
    var result = await _textServer.sendCommand(SetMicEnabledCommand(isEnabled));
    if (result == null) throw CommandFailedException();
    var msg = _try(() => MicStateChangedMessage.fromJson(result.data));
    if (msg == null) throw CommandFailedException();
    return msg.isEnabled;
  }

  Future<DrawingSet> getAllDrawingData(Whiteboard whiteboard) async {
    var result = await _textServer.sendCommand(GetAllDrawingPathsCommand(whiteboard.id));
    if (result == null) throw CommandFailedException();
    var msg = _try(() => GetAllDrawingPathsMessage.fromJson(result.data));
    if (msg == null) throw CommandFailedException();
    return msg.drawingSet;
  }

  // awaitless commands

  bool requestWhiteboardImage(ObjectId whiteboardId, ImageInfo imageInfo) =>
      _textServer.sendCommandAwaitless(GetWhiteboardImageCommand(imageInfo.name));

  void uploadDrawing(Whiteboard wb) =>
      _textServer.sendCommandAwaitless(UploadDrawingCommand(wb));

  void createDrawingPath(ObjectId whiteboardId, DrawingData drawingData) =>
      _textServer.sendCommandAwaitless(DrawingCreatePathCommand(whiteboardId,
          drawingData.paint.color.value, drawingData.paint.strokeWidth, drawingData.surfaceSize));

  void addDrawingPoint(ObjectId whiteboardId, DrawingPoint point, ObjectId drawingId) =>
      _textServer.sendCommandAwaitless(DrawingAddPointCommand(whiteboardId, point));

  void addDrawingPath(ObjectId whiteboardId, DrawingData drawingData) { } // for realm only

  void undoDrawing(ObjectId whiteboardId, ObjectId drawingId) =>
      _textServer.sendCommandAwaitless(DrawingUndoCommand(whiteboardId));

  void clearDrawing(ObjectId whiteboardId) =>
      _textServer.sendCommandAwaitless(DrawingEraseCommand(whiteboardId));
}

class ImageDescription implements WebSocketDataDescription {
  final String fileName;
  final String type;
  final ObjectId whiteboardId;

  ImageDescription(this.fileName, this.type, this.whiteboardId);

  @override
  Map<String, dynamic> toJson() => {
      'ID': fileName,
      'Name': fileName,
      'SetActive': true,
      'Type': type,
      'BoardID': whiteboardId.toHexString()
    };
}

class CommandFailedException implements Exception {}