import 'dart:typed_data';

class WebSocketDataMessage {
  final String imageId;
  final String fileName;
  final String whiteboardId;
  final String senderId;
  final bool isActive;

  Uint8List data;

  WebSocketDataMessage.fromJson(Map<String, dynamic> json)
      : imageId = json['ID'],
        fileName = json['Name'],
        isActive = json['SetActive'],
        senderId = json['SenderID'],
        whiteboardId = json['BoardID'];
}