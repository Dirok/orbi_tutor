import 'package:flutter/foundation.dart';

enum LectureServerType {
  unknown,
  lectureServer,
  realm
}

extension LectureServerTypeEx on LectureServerType {
  String get asString => describeEnum(this);
}

class LectureServerTypeHelper {
  static final Map<String, LectureServerType> _strMap =
  Map<String, LectureServerType>.fromIterable(
      LectureServerType.values, key: (v) => describeEnum(v), value: (v) => v);

  static LectureServerType fromString(String value) =>
      value != null && value != '' && _strMap.containsKey(value) ? _strMap[value] : LectureServerType.unknown;
}