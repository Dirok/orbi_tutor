import 'package:tutor/focused.dart';

abstract class LectureServerWS {
  static const String _protocol = 'wss';
  static const String _port = '8888';

  final ServerWebSocket webSocket = ServerWebSocket();

  void Function() connectionClosed;
  void Function() connectionError;

  static const String _path = 'ws/text';

  bool get isConnected => webSocket.isConnected;

  LectureServerWS() {
    webSocket.connectionClosed = onConnectionClosed;
    webSocket.connectionError = onConnectionError;
  }

  onConnectionClosed() => connectionClosed?.call();
  onConnectionError() => connectionError?.call();

  bool initConnection(String address, String key) {
    final url = '$_protocol://$address:$_port/$_path/$key';

    Log.info('initConnection to $url');
    var isConnected = webSocket.connect(url, key);
    if (isConnected) webSocket.onReceiveCallback = onReceived;
    return isConnected;
  }

  void onReceived(dynamic data);

  Future<void> stopConnection() {
    webSocket.onReceiveCallback = null;
    return webSocket.disconnect();
  }

  Future<void> close() {
    Log.info('close');

    webSocket.connectionClosed = null;
    webSocket.connectionError = null;

    return stopConnection();
  }
}