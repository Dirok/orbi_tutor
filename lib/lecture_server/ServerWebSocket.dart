import 'dart:typed_data';

import 'package:tutor/focused.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class ServerWebSocket {
  static const Duration _pingInterval = Duration(seconds: 15);

  WebSocketChannel _channel;

  void Function(dynamic) onReceiveCallback;

  bool _isConnected = false;
  bool get isConnected => _isConnected;

  void Function() connectionClosed;
  void Function() connectionError;

  bool connect(String url, String key) {
    try {
      _channel = WebSocketChannelFactory.connect(url, pingInterval: _pingInterval);
      _channel.stream.listen((data) {
        _onReceptionOfMessageFromServer(data);
      }, onDone: () {
        Log.info("stream closed");
        _isConnected = false;
        connectionClosed?.call();
      }, onError: (error) {
        Log.error('stream error: ' + error.toString());
        _isConnected = false;
        connectionError?.call();
      });
      _isConnected = true;
    } catch(e) {
      Log.error(e.toString());
      _isConnected = false;
      connectionClosed?.call();
    }
    return _isConnected;
  }

  Future<void> disconnect() async {
    await _channel?.sink?.close();
    _isConnected = false;
  }

  bool send(String message) {
    if (!_isConnected || _channel == null || _channel.sink == null) return false;
    _channel.sink.add(message);
    return true;
  }

  bool sendData(Uint8List data) {
    if (!_isConnected || _channel == null || _channel.sink == null) return false;
    _channel.sink.add(data);
    return true;
  }

  void _onReceptionOfMessageFromServer(dynamic data) {
    _isConnected = true;
    onReceiveCallback?.call(data);
  }
}
