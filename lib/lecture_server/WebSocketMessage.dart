import 'package:bson/bson.dart';

class WebSocketMessage {
  final ObjectId transactionId;
  final String messageType;
  final Map<String, dynamic> data;

  WebSocketMessage.fromJson(Map<String, dynamic> json) :
        transactionId = json.containsKey('TransactionID')
            ? ObjectId.fromHexString(json['TransactionID']) : null,
        messageType = json['MessageType'],
        data = json['Data'];
}