import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tutor/focused.dart';

// TODO remove when rest will moved to main server
class LectureServerRest {
  static const String _protocol = 'https';
  static const String _port = '8888';

  final String _url;

  LectureServerRest(String address) : _url = '$_protocol://$address:$_port';

  Future<bool> stopLecture(String lectureId, String key) async {
    try {
      Map<String, String> headers = {
        "Content-type": "application/json",
        'Authorization': 'LessonAuth $key'
      };

      final uri = Uri.parse('$_url/lesson/stop');
      final response = await http.get(uri, headers: headers);

      var responseBody = json.decode(utf8.decode(response.bodyBytes));
      if (response.statusCode == 200) {
        Log.info('Stop lecture request success: ' + responseBody["message"]);
        return true;
      } else {
        Log.info('Stop lecture request error: ' + responseBody["message"]);
        return false;
      }
    } catch (e) {
      Log.error('Stop lecture request error', exception: e);
      return false;
    }
  }

  Future<bool> setLectureCheckingMode(String lectureId, String userId, String key) async {
    try {
      Map<String, String> headers = {
        "Content-type": "application/json",
        'Authorization': 'LessonAuth $key'
      };

      final uri = Uri.parse('$_url/lesson/kick_students');
      final response = await http.get(uri, headers: headers);

      var responseBody = json.decode(utf8.decode(response.bodyBytes));
      if (response.statusCode == 200) {
        Log.info('Checking mode request success: ' + responseBody["message"]);
        return true;
      } else {
        Log.info('Checking mode request error: ' + responseBody["message"]);
        return false;
      }
    } catch (e) {
      Log.error('Checking mode request error', exception: e);
      return false;
    }
  }
}