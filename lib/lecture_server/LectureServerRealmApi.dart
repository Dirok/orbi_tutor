import 'dart:async';
import 'dart:convert';

import 'package:bson/bson.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/services.dart';
import 'package:mongo_realm/mongo_realm.dart';
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

class LectureServerRealmApi implements ILectureServerApi {
  final _lectureSubscriptions = <StreamSubscription>[];

  Map<ActionType, void Function(Action)> _actionsMap;

  final _app = RealmApp();

  RealmLecture _realmLecture;

  Lecture _lecture;
  ObjectId _userId;

  OnImageReceived imageReceived;
  OnUserWhiteboardInserted userWhiteboardInserted;
  OnWhiteboardImageChanged whiteboardImageChanged;
  OnDrawingPathAdded drawingPathAdded;
  OnDrawingPointAdded drawingPointAdded;
  OnLastDrawingPathRemoved lastDrawingPathRemoved;
  OnDrawingCleaned drawingCleared;
  OnUserConnected userConnected;
  OnUserDisconnected userDisconnected;
  OnUserHandStateChanged userHandStateChanged;
  OnUserMicStateChanged userMicStateChanged;
  OnMainWhiteboardChanged mainWhiteboardChanged;
  OnPinStateChanged pinStateChanged;
  OnAllowSpeakChanged allowSpeakChanged;
  OnLectureFinished lectureFinished;
  OnLectureCheckingMode lectureCheckingMode;

  OnConnectionClosed connectionClosed;

  // TODO: implement isConnected
  bool _isConnected = false;
  bool get isConnected => _isConnected;
  bool get isDisconnected => !isConnected;

  Future<bool> _auth(String token) async {
    try {
      _isConnected = await _app.login(Credentials.jwt(token));
      return _isConnected;
    } on PlatformException catch (e) {
      Log.error('Realm auth error', exception: e);
      return false;
    }
  }

  @override
  Future<bool> connect(Lecture lecture, {String jwt, ObjectId userId}) async {
    if (jwt == null) {
      Log.error('Failed to connect: jwt is null');
      return false;
    }

    _lecture = lecture;
    _userId = userId;

    if (!await _auth(jwt)) return false;

    _realmLecture = await _app.openRealmLecture(lecture.id.toHexString());

    _subscribeToActions();

    return true;
  }

  @override
  Future<void> close() async {
    _lectureSubscriptions..forEach((s) => s.cancel())..clear();
  }

  void _subscribeToActions() {
    _actionsMap = {
      ActionType.whiteboardInserted: (a) => _onWhiteboardInserted(a),
      ActionType.whiteboardChanged: (a) => _onCurrentWhiteboardChanged(a),
      ActionType.imageAdded: (a) => _onImageAdded(a),
      ActionType.drawingAdded: (a) => _onDrawingAdded(a),
      ActionType.drawingCreated: (a) => _onDrawingCreate(a),
      ActionType.drawingPoint: (a) => _onDrawingPoint(a),
      ActionType.drawingUndo: (a) => _onDrawingUndo(a),
      ActionType.drawingClear: (a) => _onDrawingClear(a),
      ActionType.userMicStateChanged: (a) => _onUserMicStateChanged(a),
      ActionType.userHandStateChanged: (a) => _onUserHandStateChanged(a),
      ActionType.userAllowSpeakChanged: (a) => _onUserAllowSpeakChanged(a)
    };

    _lectureSubscriptions.add(_realmLecture.actionsStream.listen(
            (event) => _onActionAdded(Map<String, dynamic>.from(event))));
  }

  @override
  void createDrawingPath(ObjectId whiteboardId, DrawingData drawingData) {
    return; // disabled until the realm sync gets faster

    final data = drawingData.toRealmJson();
    data[RealmKeys.userId] = _userId.toHexString();
    data[RealmKeys.whiteboardId] = whiteboardId.toHexString();

    try {
      _realmLecture.createDrawingPath(data);
      Log.info('Drawing ${drawingData.id} created');
    } catch (e) {
      Log.error('Create drawing ${drawingData.id} failed', exception: e);
    }
  }

  @override
  void addDrawingPoint(ObjectId whiteboardId, DrawingPoint point, ObjectId drawingId) {
    return; // disabled until the realm sync gets faster

    final data = {
      RealmKeys.userId: _userId.toHexString(),
      RealmKeys.whiteboardId: whiteboardId.toHexString(),
      RealmKeys.drawingId: drawingId.toHexString(),
      RealmKeys.point: jsonEncode(point.toRealmJson())
    };

    try {
      _realmLecture.addDrawingPoint(data);
      Log.info('Point $point added to drawing $drawingId');
    } catch(e) {
      Log.error('Adding point $point to drawing $drawingId failed', exception: e);
    }
  }

  @override
  void addDrawingPath(ObjectId whiteboardId, DrawingData drawingData) {
    final data = drawingData.toRealmJson();
    data[RealmKeys.userId] = _userId.toHexString();
    data[RealmKeys.whiteboardId] = whiteboardId.toHexString();

    try {
      _realmLecture.createDrawingPath(data);
      Log.info('Drawing ${drawingData.id} created');
    } catch (e) {
      Log.error('Create drawing ${drawingData.id} failed', exception: e);
    }
  }

  @override
  void clearDrawing(ObjectId whiteboardId) {
    // TODO: implement clearDrawing
    throw UnimplementedError();
  }

  @override
  Future<DrawingSet> getAllDrawingData(Whiteboard whiteboard) async {
    try {
      return _loadDrawingSet(whiteboard.drawingIds);
    } catch (e) {
      Log.error('Getting all drawings for whiteboard ${whiteboard.id} failed', exception: e);
      return null;
    }
  }

  Future<DrawingSet> _loadDrawingSet(List<ObjectId> drawingIds) async {
    final drawingsByUserId = <ObjectId, List<DrawingData>>{};
    final sequence = <DrawingIndex>[];

    for (final drawingId in drawingIds) {
      final data = {
        RealmKeys.drawingId: drawingId.toHexString()
      };

      final json = await _realmLecture.getDrawing(data);
      if (json == null) {
        Log.error('Drawing $drawingId not found');
        continue;
      }

      final drawingData = DrawingData.fromRealmJson(json);
      if (drawingData == null) continue;

      final userId = ObjectId.fromHexString(json[RealmKeys.userId]);

      if (!drawingsByUserId.containsKey(userId))
        drawingsByUserId[userId] = [];

      final userDrawingsMap = drawingsByUserId[userId];
      userDrawingsMap.add(drawingData);

      final index = userDrawingsMap.length - 1;
      
      sequence.add(DrawingIndex(index, userId));
    }

    return DrawingSet(drawingIds, sequence, drawingsByUserId);
  }

  @override
  Future<MainWhiteboardInfo> getMainWhiteboardInfo() {
  // TODO: implement getMainWhiteboardId
    throw UnimplementedError();
  }

  @override
  Future<List<ObjectId>> getOnlineUserList() {
    // TODO: implement getOnlineUserList
    throw UnimplementedError();
  }

  @override
  Future<bool> getPinState() {
    // TODO: implement getPinState
    throw UnimplementedError();
  }

  @override
  Future<Map<ObjectId, UserState>> getStudentStates() async {
    try {
      final jsonList = await _realmLecture.getUserStates({});
      if (jsonList == null) {
        Log.error('UserStates not found');
        return null;
      }

      final userStatesMap = <ObjectId, UserState>{};
      for (final json in jsonList) {
        final userState = UserState.fromRealmJson(json);
        userStatesMap[userState.userid] = userState;
      }

      Log.info('All UserStates loaded');
      return userStatesMap;
    } catch (e) {
      Log.error('Getting all UserStates failed', exception: e);
      return null;
    }
  }

  @override
  Future<UserState> getUserState(ObjectId userId) async {
    final data = {RealmKeys.userId: userId.toHexString()};

    try {
      final json = await _realmLecture.getUserState(data);
      if (json == null) {
        Log.error('UserState for $userId not found');
        return null;
      }

      final userState = UserState.fromRealmJson(json);
      Log.info('UserState for $userId loaded');
      return userState;
    } catch (e) {
      Log.error('Getting UserState for $userId failed', exception: e);
      return null;
    }
  }

  @override
  Future<WhiteboardSet> getCommonWhiteboardSet() async {
    try {
      var json = await _realmLecture.getCommonWhiteboardSet({});
      var whiteboardSet = WhiteboardSet.fromRealmJson(json);

      var whiteboards = <ObjectId, Whiteboard>{};

      for (var id in whiteboardSet.whiteboardIds) {
        var wb = await loadWhiteboardInfo(id);
        if (wb != null)
          whiteboards[id] = wb;
      }

      whiteboardSet.whiteboardMap = whiteboards;

      return whiteboardSet;
    } catch (e) {
      Log.error('Getting common whiteboard set id for Lecture ${_lecture.id} failed', exception: e);
      return null;
    }
  }

  @override
  Future<WhiteboardSet> getUserWhiteboardsInfo(ObjectId userId) async {
    throw UnimplementedError();
  }

  @override
  Future<Whiteboard> loadWhiteboardInfo(ObjectId whiteboardId) async {
    final data = {RealmKeys.whiteboardId: whiteboardId.toHexString()};

    try {
      final json = await _realmLecture.getWhiteboardInfo(data);
      if (json == null) {
        Log.info('Whiteboard loading failed: $whiteboardId not found');
        return null;
      }

      final wb = Whiteboard.fromRealmJson(json);
      Log.info('Whiteboard ${wb.id} loaded');

      await _loadImages(wb);
      Log.info('Images loaded for ${wb.id}');

      return wb;
    } catch (e) {
      Log.error('Whiteboard $whiteboardId loading failed', exception: e);
      return null;
    }
  }

  Future<void> _loadImages(Whiteboard wb) async {
    final images = <ImageInfo>[];
    for (final id in wb.imageIds) {
      final image = await _getImageInfo(id);
      if (image != null)
        images.add(image);
    }
    wb.images = images;
  }

  @override
  bool requestWhiteboardImage(ObjectId whiteboardId, ImageInfo imageInfo) {
    _downloadImage(imageInfo).then((isSuccess) {
      if (isSuccess)
        imageReceived?.call(ImageMessage(whiteboardId, imageInfo));
    });
    return true;
  }

  @override
  Future<void> sendCommonWhiteboardSet(WhiteboardSet whiteboardSet) async {
    var data = whiteboardSet.toRealmJson();
    try {
        await _realmLecture.setCommonWhiteboardSet(data);
        Log.info('Whiteboard set ${whiteboardSet.id} added');
      } catch (e) {
        Log.error('Whiteboard set ${whiteboardSet.id} adding failed', exception: e);
      }
  }

  @override
  Future<void> sendInsertWhiteboard(int position, Whiteboard whiteboard, ObjectId whiteboardSetId) async {
    var data = whiteboard.toRealmJson();
    data[RealmKeys.userId] = _userId.toHexString();
    data[RealmKeys.whiteboardSetId] = whiteboardSetId.toHexString();
    data[RealmKeys.position] = position;

    try {
      await _realmLecture.insertWhiteboard(data);
      Log.info('Whiteboard ${whiteboard.id} inserted with position $position');
      return true;
    } catch (e) {
      Log.error('Whiteboard ${whiteboard.id} insertion failed', exception: e);
      return false;
    }
  }

  @override
  Future<bool> sendPinState(bool enabled) {
    // TODO: implement sendPinState
    throw UnimplementedError();
  }

  @override
  Future<void> sendSetCurrentWhiteboard(ObjectId whiteboardId, ObjectId whiteboardSetId) async {
    var data = {
      RealmKeys.userId: _userId.toHexString(),
      RealmKeys.whiteboardSetId: whiteboardSetId.toHexString(),
      RealmKeys.whiteboardId: whiteboardId.toHexString(),
    };

    try {
      await _realmLecture.changeCurrentWhiteboard(data);
      Log.info('Whiteboard $whiteboardId set as current in whiteboard set $whiteboardSetId');
    } catch (e) {
      Log.error('Whiteboard $whiteboardId set as current failed', exception: e);
    }
  }

  @override
  Future<bool> sendStudentAllowSpeak(ObjectId studentId, bool allow) async {
    final data = {
      RealmKeys.userId: studentId.toHexString(),
      RealmKeys.value: allow
    };

    try {
      await _realmLecture.setUserAllowSpeak(data);
      Log.info('AllowSpeak changed to $allow for user $studentId');
      return true;
    } catch (e) {
      Log.error('Failed to change AllowSpeak to $allow', exception: e);
      return false;
    }
  }

  @override
  Future<bool> sendStudentHandState(ObjectId studentId, bool isHandRaised) async {
    final data = {
      RealmKeys.userId: studentId.toHexString(),
      RealmKeys.value: isHandRaised
    };

    try {
      await _realmLecture.setUserHandState(data);
      Log.info('Hand state changed to $isHandRaised for user $studentId');
      return true;
    } catch (e) {
      Log.error('Failed to change hand state to $isHandRaised for user $studentId', exception: e);
      return false;
    }
  }

  @override
  Future<bool> setHandState(bool isHandRaised) async {
    final data = {
      RealmKeys.userId: _userId.toHexString(),
      RealmKeys.value: isHandRaised
    };

    try {
      await _realmLecture.setUserHandState(data);
      Log.info('Hand state changed to $isHandRaised');
      return true;
    } catch (e) {
      Log.error('Failed to change hand state to $isHandRaised', exception: e);
      return false;
    }
  }

  @override
  Future<void> setMainWhiteboard(ObjectId whiteboardId) async {
    // TODO: implement setMainWhiteboard
    throw UnimplementedError();
  }

  @override
  Future<bool> setMicState(bool isEnabled) async {
    final data = {
      RealmKeys.userId: _userId.toHexString(),
      RealmKeys.value: isEnabled
    };

    try {
      await _realmLecture.setUserMicState(data);
      Log.info('Mic state changed to $isEnabled');
      return true;
    } catch (e) {
      Log.error('Failed to change mic state to $isEnabled', exception: e);
      return false;
    }
  }

  @override
  Future<void> subscribeWhiteboard(ObjectId whiteboardId) async {
    final data = {RealmKeys.whiteboardId: whiteboardId.toHexString()};
    try {
      await _realmLecture.watchWhiteboard(data);
      Log.info('Subscribed to whiteboard $whiteboardId');
    } catch (e) {
      Log.error('Subscribe to whiteboard $whiteboardId failed', exception: e);
      return;
    }
  }

  @override
  Future<void> unsubscribeWhiteboard(ObjectId whiteboardId) async {
    final data = {RealmKeys.whiteboardId: whiteboardId.toHexString()};
    try {
      await _realmLecture.unwatchWhiteboard(data);
      Log.info('Unsubscribed to whiteboard $whiteboardId');
    } catch (e) {
      Log.error('Unsubscribe to whiteboard $whiteboardId failed', exception: e);
      return;
    }
  }

  void _onActionAdded(Map<String, dynamic> data) async {
    final action = Action.fromRealmJson(data);
    if (_actionsMap.containsKey(action.actionType))
      _actionsMap[action.actionType]?.call(action);
  }

  void _onWhiteboardInserted(WhiteboardInsertedAction action) {
      Log.info('Whiteboard ${action.whiteboardId} inserted by user ${action.userId}');
      userWhiteboardInserted?.call(action.whiteboard, action.position);
  }

  void _onCurrentWhiteboardChanged(Action action) {
      Log.info('Current whiteboard changed to ${action.whiteboardId}');
      mainWhiteboardChanged?.call(MainWhiteboardInfo(id: action.whiteboardId));
  }

  void _onImageAdded(ImageAddedAction action) async {
    Log.info('Image ${action.image.id} added to whiteboard ${action.whiteboardId} by user ${action.userId}');

    final isSuccess = await _downloadImage(action.image);
    if (!isSuccess) return;

    imageReceived?.call(ImageMessage(action.whiteboardId, action.image));
  }

  Future<ImageInfo> _getImageInfo(ObjectId id) async {
    final data = {RealmKeys.imageId: id.toHexString()};

    try {
      final json = await _realmLecture.getImageInfo(data);
      if (json == null) {
        Log.info('ImageInfo loading failed: $id not found');
        return null;
      }

      final imageInfo = ImageInfo.fromRealmJson(json);

      Log.info('ImageInfo $id loaded');

      return imageInfo;
    } catch (e) {
      Log.error('ImageInfo $id loading failed', exception: e);
      return null;
    }
  }

  Future<bool> _downloadImage(ImageInfo imageInfo) async {
    imageInfo.folderPath = _lecture.imagesFolderPath;
    // TODO move to service
    final storage = FirebaseStorage.instance;
    final ref = storage.refFromURL(imageInfo.url);

    try {
      dynamic imageFile = File(imageInfo.path);
      await ref.writeToFile(imageFile);
      return true;
    } on FirebaseException catch (e) {
      Log.error('Image download failed', exception: e);
      return false;
    }
  }

  void _onDrawingAdded(DrawingAdded action) async {
    Log.info('Drawing ${action.drawing.id} created by user ${action.userId} from whiteboard ${action.whiteboardId}');
    drawingPathAdded?.call(action.whiteboardId, action.drawing, action.userId);
  }

  void _onDrawingCreate(DrawingAction action) async {
    Log.info('Drawing ${action.drawingId} created by user ${action.userId} from whiteboard ${action.whiteboardId}');
    var drawing = await _getDrawing(action.drawingId);
    if (drawing != null)
      drawingPathAdded?.call(action.whiteboardId, drawing, action.userId);
  }

  void _onDrawingPoint(DrawingPointAction action) {
    Log.info('Drawing point ${action.point} added to drawing ${action.drawingId}'
        ' by user ${action.userId} from whiteboard ${action.whiteboardId}');
    drawingPointAdded?.call(action.whiteboardId, action.point, action.userId);
  }

  void _onDrawingUndo(DrawingAction action) {
    Log.info('Drawing ${action.drawingId} removed by user ${action.userId} from whiteboard ${action.whiteboardId}');
    lastDrawingPathRemoved?.call(action.whiteboardId, action.userId);
  }

  void _onDrawingClear(Action action) {
    Log.info('Whiteboard ${action.whiteboardId} cleared by user ${action.userId}');
    drawingCleared?.call(action.whiteboardId);
  }

  void _onUserMicStateChanged(UserStateParameterChangedAction<bool> action) {
    Log.info('User MicState changed to ${action.value} by user ${action.userId}');
    userMicStateChanged?.call(action.userId, action.value);
  }

  void _onUserHandStateChanged(UserStateParameterChangedAction<bool> action) {
    Log.info('User HandState changed to ${action.value} by user ${action.userId}');
    userHandStateChanged?.call(action.userId, action.value);
  }

  void _onUserAllowSpeakChanged(UserStateParameterChangedAction<bool> action) {
    if (action.userId != _userId) return;
    Log.info('User AllowSpeak changed to ${action.value} by user ${action.userId}');
    allowSpeakChanged?.call(action.value);
  }

  Future<DrawingData> _getDrawing(ObjectId drawingId) async {
    final data = {RealmKeys.drawingId: drawingId.toHexString()};

    try {
      final json = await _realmLecture.getDrawing(data);
      if (json == null) {
        Log.error('Getting drawing failed: $drawingId not found');
        return null;
      }
      final drawing = DrawingData.fromRealmJson(json);
      Log.info('Got drawing $drawingId');
      return drawing;
    } catch (e) {
      Log.error('Getting drawing $drawingId failed', exception: e);
      return null;
    }
  }

  @override
  void undoDrawing(ObjectId whiteboardId, ObjectId drawingId) async {
    final data = {
      RealmKeys.userId: _userId.toHexString(),
      RealmKeys.whiteboardId: whiteboardId.toHexString(),
      RealmKeys.drawingId: drawingId.toHexString(),
    };

    try {
      await _realmLecture.drawingUndo(data);
      Log.info('Drawing $drawingId removed from whiteboard $whiteboardId');
    } catch (e) {
      Log.error('Drawing $drawingId removing from whiteboard $whiteboardId failed', exception: e);
    }
  }

  @override
  Future<bool> uploadDrawing(Whiteboard wb) async {
    throw UnimplementedError();
  }

  @override
  Future<SendDataResult> uploadImage(String token, ImageInfo image, ObjectId lectureId, ObjectId whiteboardId) async {
    //TODO move to service
    final storage = FirebaseStorage.instance;
    final ref = storage.ref('/${_lecture.id.toHexString()}/images').child(image.name);

    try {
      dynamic imageFile = File(image.path);
      await ref.putFile(imageFile);
    } on FirebaseException catch (e) {
      Log.error('Image upload failed', exception: e);
      return SendDataResult.fail;
    }

    image.url = await ref.getDownloadURL();

    await _addImageInfo(image, whiteboardId);

    return SendDataResult.success;
  }

  Future<bool> _addImageInfo(ImageInfo image, ObjectId whiteboardId) async {
    final data = image.toRealmJson();
    data[RealmKeys.whiteboardId] = whiteboardId.toHexString();

    try {
      final isSuccess = await _realmLecture.addImageInfo(data);
      if (!isSuccess) {
        Log.info('Add image ${image.id} to whiteboard $whiteboardId failed');
        return false;
      }
      Log.info('Image ${image.id} added to whiteboard $whiteboardId');
    } catch (e) {
      Log.error('Add image ${image.id} to whiteboard $whiteboardId failed', exception: e);
    }

    return true;
  }

  @override
  Future<ImageInfo> downloadImage(
      String token, ObjectId lectureId, String filename, String filePath) {
    // TODO: is realm moving to S3 proxy too?
    throw UnimplementedError();
  }
}