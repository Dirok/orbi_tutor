import 'package:bson/bson.dart';
import 'package:tutor/focused.dart';

// Users

class OnlineUsersMessage {
  static const String messageType = 'OnlineList';
  final List<ObjectId> userIdList;
  OnlineUsersMessage.fromJson(Map<String, dynamic> json) :
    userIdList = (json['OnlineIDs'] as List).map((id) => ObjectId.fromHexString(id)).toList();
}

class UserConnectedMessage {
  static const String messageType = 'UserConnected';
  final ObjectId userId;
  UserConnectedMessage.fromJson(Map<String, dynamic> json) :
        userId = ObjectId.fromHexString(json['UserID']);
}

class UserDisconnectedMessage {
  static const String messageType = 'UserDisconnected';
  final ObjectId userId;
  UserDisconnectedMessage.fromJson(Map<String, dynamic> json) :
        userId = ObjectId.fromHexString(json['UserID']);
}

class UserStateListMessage {
  static const String messageType = 'UsersState';
  final Map<ObjectId, UserState> userStateMap;
  UserStateListMessage.fromJson(Map<String, dynamic> json) :
        userStateMap = (json['Users'] as Map).map((id, state) =>
            MapEntry(ObjectId.fromHexString(id), UserState.fromJson(state)));
}

class UserStateMessage {
  static const String messageType = 'UserState';
  final ObjectId userId;
  final UserState userState;
  UserStateMessage.fromJson(Map<String, dynamic> json) :
        userId = ObjectId.fromHexString(json['UserID']),
        userState = UserState.fromJson(json);
}

class HandStateChangedMessage {
  static const String messageType = 'HandStateChanged';
  final ObjectId userId;
  final bool isRaised;
  HandStateChangedMessage.fromJson(Map<String, dynamic> json) :
        userId = ObjectId.fromHexString(json['UserID']),
        isRaised = json['HandState'];
}

class AllowSpeakChangedMessage {
  static const String messageType = 'AllowSpeakChanged';
  final ObjectId userId;
  final bool allowSpeak;
  AllowSpeakChangedMessage.fromJson(Map<String, dynamic> json) :
        userId = ObjectId.fromHexString(json['UserID']),
        allowSpeak = json['AllowSpeak'];
}

class MicStateChangedMessage {
  static const String messageType = 'MicEnabledChanged';
  final ObjectId userId;
  final bool isEnabled;
  MicStateChangedMessage.fromJson(Map<String, dynamic> json) :
        userId = ObjectId.fromHexString(json['UserID']),
        isEnabled = json['MicEnabled'];
}

// Whiteboards

class WhiteboardInsertedMessage {
  static const String messageType = 'BoardInserted';
  final ObjectId whiteboardId;
  final ObjectId ownerId;
  final int index;
  WhiteboardInsertedMessage.fromJson(Map<String, dynamic> json) :
        whiteboardId = ObjectId.fromHexString(json['Board']),
        ownerId = ObjectId.fromHexString(json['OwnerID']),
        index = json['Index'];
}

class UserWhiteboardInsertedMessage {
  static const String messageType = 'UserBoardInserted';
  final ObjectId ownerId;
  final ObjectId whiteboardId;
  final int index;
  UserWhiteboardInsertedMessage.fromJson(Map<String, dynamic> json) :
        ownerId = ObjectId.fromHexString(json['OwnerID']),
        whiteboardId = ObjectId.fromHexString(json['Board']),
        index = json['Index'];
}

class CurrentWhiteboardChangedMessage {
  static const String messageType = 'CurrentUserBoardChanged';
  final ObjectId whiteboardId;
  CurrentWhiteboardChangedMessage.fromJson(Map<String, dynamic> json) :
    whiteboardId = ObjectId.fromHexString(json['Board']);
}

class MainWhiteboardMessage {
  static const String messageType = 'MainBoard';
  final ObjectId id;
  final ObjectId ownerId;
  final List<ObjectId> containingSequence;
  final int index;

  MainWhiteboardMessage.fromJson(Map<String, dynamic> json)
      : id = ObjectId.fromHexString(json['MainBoardID']),
        ownerId = ObjectId.fromHexString(json['MainBoardOwnerID']),
        containingSequence = (json['BoardsSequenceOfMainBoardOwner'] as List)
            .map((id) => ObjectId.fromHexString(id)).toList(),
        index = json['MainBoardIndexInSequence'];
}

class MainWhiteboardChangedMessage {
  static const String messageType = 'MainBoardChanged';
  final ObjectId id;
  final ObjectId ownerId;
  final List<ObjectId> containingSequence;
  final int index;

  MainWhiteboardChangedMessage.fromJson(Map<String, dynamic> json)
      : id = ObjectId.fromHexString(json['MainBoardID']),
        ownerId = ObjectId.fromHexString(json['MainBoardOwnerID']),
        containingSequence = (json['BoardsSequenceOfMainBoardOwner'] as List)
            .map((id) => ObjectId.fromHexString(id)).toList(),
        index = json['MainBoardIndexInSequence'];
}

class WhiteboardInfoMessage {
  static const String messageType = 'BoardInfo';
  final Whiteboard whiteboard;
  WhiteboardInfoMessage.fromJson(Map<String, dynamic> json) :
    whiteboard = Whiteboard.fromJson(json);
}

class UserWhiteboardsMessage {
  static const String messageType = 'UserBoards';
  final WhiteboardSet whiteboardSet;
  UserWhiteboardsMessage.fromJson(Map<String, dynamic> json) :
        whiteboardSet = WhiteboardSet.fromJson(json);
}

class SubscribeWhiteboardMessage {
  static const String messageType = 'SubscribeToBoard';
  final ObjectId whiteboardId;
  SubscribeWhiteboardMessage.fromJson(Map<String, dynamic> json) :
    whiteboardId = ObjectId.fromHexString(json['Board']);
}

class UnsubscribeWhiteboardMessage {
  static const String messageType = 'UnsubscribeToBoard';
  final ObjectId whiteboardId;
  UnsubscribeWhiteboardMessage.fromJson(Map<String, dynamic> json) :
    whiteboardId = ObjectId.fromHexString(json['Board']);
}

// Image

class WhiteboardImageChangedMessage {
  static const String messageType = 'ChangeImage';
  final ObjectId whiteboardId;
  final String imageName;
  WhiteboardImageChangedMessage.fromJson(Map<String, dynamic> json) :
    whiteboardId = ObjectId.fromHexString(json['BoardID']),
    imageName = json['ImageID'];
}

// Pin

class PinStateMessage {
  static const String messageType = 'PinMode';
  final bool isPinned;
  PinStateMessage.fromJson(Map<String, dynamic> json) :
    isPinned = json['PinMode'];
}

class PinStateChangedMessage {
  static const String messageType = 'PinModeChanged';
  final bool isPinned;
  PinStateChangedMessage.fromJson(Map<String, dynamic> json) :
    isPinned = json['PinMode'];
}

// Drawing

class DrawingCreatePathMessage {
  static const String messageType = 'PathCreation';
  final DrawingData drawingData;
  final ObjectId whiteboardId;
  final ObjectId userId;
  DrawingCreatePathMessage.fromJson(Map<String, dynamic> json) :
    drawingData = DrawingData.fromJson(json),
    whiteboardId = ObjectId.fromHexString(json['BoardID']),
    userId = ObjectId.fromHexString(json['UserID']);
}

class DrawingAddPointMessage {
  static const String messageType = 'DrawingPoint';
  final DrawingPoint point;
  final ObjectId whiteboardId;
  final ObjectId userId;
  DrawingAddPointMessage.fromJson(Map<String, dynamic> json) :
    point = DrawingPoint.fromJson(json['Point']),
    whiteboardId = ObjectId.fromHexString(json['BoardID']),
    userId = ObjectId.fromHexString(json['UserID']);
}

class DrawingUndoMessage {
  static const String messageType = 'DrawingPathUndo';
  final ObjectId whiteboardId;
  final ObjectId userId;
  DrawingUndoMessage.fromJson(Map<String, dynamic> json) :
    whiteboardId = ObjectId.fromHexString(json['BoardID']),
    userId = ObjectId.fromHexString(json['UserID']);
}

class DrawingEraseMessage {
  static const String messageType = 'EraseDrawing';
  final ObjectId whiteboardId;
  final ObjectId userId;
  DrawingEraseMessage.fromJson(Map<String, dynamic> json) :
    whiteboardId = ObjectId.fromHexString(json['BoardID']),
    userId = ObjectId.fromHexString(json['UserID']);
}

class GetAllDrawingPathsMessage {
  static const String messageType = 'AllDrawingPaths';
  final DrawingSet drawingSet;
  GetAllDrawingPathsMessage.fromJson(Map<String, dynamic> json) :
        drawingSet = DrawingSet.fromJson(json);
}

// Other

class LectureFinishedMessage {
  static const String messageType = 'LessonFinished';
}

class LectureCheckingModeMessage {
  static const String messageType = 'GoesToOnlyTeacherMode';
}