class RealmKeys {
  static const whiteboardSetId = 'whiteboardSetId';
  static const whiteboardId = 'whiteboardId';
  static const drawingId = 'drawingId';
  static const userId = 'userId';
  static const point = 'point';
  static const position = 'position';
  static const imageId = 'imageId';
  static const value = 'value';
  static const userState = 'userState';
  static const whiteboard = 'whiteboard';
  static const image = 'image';
  static const drawing = 'drawing';
  static const actionType = 'actionType';
}