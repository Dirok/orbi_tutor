import 'dart:async';
import 'dart:convert';

import 'package:bson/bson.dart';
import 'package:tutor/focused.dart';

typedef void OnMessageReceived(WebSocketMessage msg);

class LectureServerTextChannel extends LectureServerWS {
  static const Duration _commandTimeout = Duration(seconds: 15);

  final JsonEncoder _encoder = JsonEncoder();
  final JsonDecoder _decoder = JsonDecoder();

  final Map<ObjectId, Completer<WebSocketMessage>> _commandCompleterMap = Map();

  OnMessageReceived messageReceived;

  @override
  void onReceived(serverMessage) {
    var messageText = utf8.decode(serverMessage);

    Log.info('receive $messageText');

    var json = _decoder.convert(messageText);

    WebSocketMessage msg;

    try {
      msg = WebSocketMessage.fromJson(json);
    } catch(e) {
      Log.error('Message structure does not match WebSocketMessage');
      return;
    }

    // response for command
    if (_commandCompleterMap.containsKey(msg.transactionId)) {
      var completer = _commandCompleterMap[msg.transactionId];
      completer.complete(msg);
    } else { // event
      messageReceived?.call(msg);
    }
  }

  bool sendCommandAwaitless(WebSocketCommand command) {
    var json = _encoder.convert(command);

    var isSent = webSocket.send(json);
    if (isSent)
      Log.info('command ${command.commandName} sent with data: ${command.data}');
    else
      Log.error('command ${command.commandName} failed: not sent');

    return isSent;
  }

  Future<WebSocketMessage> sendCommand(WebSocketCommand command) async {
    var completer = Completer<WebSocketMessage>();
    _commandCompleterMap[command.transactionId] = completer;

    var isSent = webSocket.send(_encoder.convert(command));
    if (!isSent) {
      _commandCompleterMap.remove(command.transactionId);
      Log.error('command ${command.commandName} failed: not sent');
      return null;
    }

    Log.info('command ${command.commandName} sent with data: ${command.data} and transaction id: ${command.transactionId}');

    WebSocketMessage msg;

    try {
      msg = await completer.future.timeout(_commandTimeout);
      if (msg == null)
        Log.error('command ${command.commandName} failed: response is null');
    } on TimeoutException catch (e) {
      Log.error('command ${command.commandName} filed: timeout expired', exception: e);
    }

    _commandCompleterMap.remove(command.transactionId);

    return msg;
  }
}