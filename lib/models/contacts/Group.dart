import 'package:bson/bson.dart';
import 'package:tutor/focused.dart';

class Group implements IContact {
  final ObjectId id;
  final String name;
  final User owner;
  final GroupPolicy policy;
  final List<IContact> members = [];

  @override
  Iterable<ObjectId> get membersIds => members.map((user) => user.id);

  @override
  String get fullName => name;

  List<String> get membersNames => members.map((user) => user.fullName).toList(); // temp todo del

  Group(this.id, this.name, this.owner, this.policy);

  Group.fromJson(Map<String, dynamic> json)
      : id = ObjectId.fromHexString(json['id']),
        name = json['name'],
        owner = User.fromJson(json['owner']),
        policy = GroupPolicyHelper.fromString(json['policy']);

  Map<String, dynamic> toJson() => {
    'id' : id.toHexString(),
    'name' : name,
    'owner': owner.toJson(),
    'policy' : policy.asString,
  };

  void addMembers(List<IContact> newMembers) {
    members.addAll(newMembers);
  }

  void assignMembers(List<IContact> newMembers) {
    members.clear();
    addMembers(newMembers);
  }

  @override
  bool operator ==(Object other) => identical(this, other) ||
      (other is Group && id == other.id && owner == other.owner &&
          name == other.name && policy == other.policy);

  @override
  int get hashCode => id.hashCode;
}