import 'package:bson/bson.dart';

abstract class IContact {
  final ObjectId id;
  String get fullName;
  Iterable<ObjectId> get membersIds;

  IContact(this.id);
}
