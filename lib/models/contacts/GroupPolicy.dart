import 'package:flutter/foundation.dart';

enum GroupPolicy {
  by_invite,
  opened,
  unknown
}

class GroupPolicyHelper {
  static final Map<String, GroupPolicy> _strMap =
  Map<String, GroupPolicy>.fromIterable(
      GroupPolicy.values, key: (v) => describeEnum(v), value: (v) => v);

  static GroupPolicy fromString(String value) =>
      _strMap.containsKey(value) ? _strMap[value] : GroupPolicy.unknown;
}

extension GroupPolicyEx on GroupPolicy {
  String get asString => describeEnum(this);
}