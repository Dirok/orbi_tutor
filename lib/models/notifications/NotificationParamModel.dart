import 'package:intl/intl.dart';
import 'package:tutor/focused.dart';

class NotificationParamModel {
  NotificationParam type;
  var value;
  var format;

  NotificationParamModel.fromJson(Map<String, dynamic> value) {
    this.value = value['value'];
    this.type = NotificationParamHelper.fromString(value['type']);
    if (value.containsKey('format'))
      this.format = value['format'];
  }

  String formattedValue() {
    switch (type) {
      case NotificationParam.Integer:
      case NotificationParam.String:
        return value.toString();
        break;
      case NotificationParam.DateTime:
        final localTime = DateTime.parse(value).toLocal();
        final timeFormatter = DateFormat(format);
        return timeFormatter.format(localTime);
        break;
    }
    return '';
  }
}
