import 'dart:convert';

import 'package:bson/bson.dart';
import 'package:tutor/focused.dart';

class NotificationModel {
  final _paramRegex = RegExp(r'(\$[0-9]*)');

  Map<String, String> _title;
  Map<String, String> _body;
  Map<String, NotificationParamModel> _params;

  NotificationType type;
  ObjectId lectureId;

  NotificationModel.fromJson(Map<String, dynamic> jsonInput) {
    try {
      _title = Map.from(json.decode(jsonInput['title']));
      _body = Map.from(json.decode(jsonInput['body']));
      final allParams = _paramRegex.allMatches(_body.values.first);
      _params = Map();
      allParams.forEach((element) {
        final paramName = element.group(0);
        _params[paramName] =
            NotificationParamModel.fromJson(Map.from(json.decode(jsonInput[paramName])));
      });

      if (jsonInput.containsKey('lessonID')) // TODO replace by lectureId in other side
        lectureId = ObjectId.fromHexString(jsonInput['lessonID']);
      if (jsonInput.containsKey('eventType'))
        type = NotificationTypeHelper.fromString(jsonInput['eventType']);
    } catch (e) {
      Log.error('Error while parsing notification object', exception: e);
    }
  }

  String formattedTitle(String _localization) => _applyParams(_title[_localization]);

  String formattedBody(String _localization) => _applyParams(_body[_localization]);

  String _applyParams(String input) {
    _params.forEach((key, value) {
      input = input.replaceAll(key, value.formattedValue());
    });

    return input;
  }
}
