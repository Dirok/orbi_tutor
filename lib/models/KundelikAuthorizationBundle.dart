import 'package:flutter/foundation.dart';

enum KundelikAuthorizationScopes {
  CommonInfo,           /// personal data
  ContactInfo,          /// contact information
  FriendsAndRelatives,  /// a list of friends and relatives
  EducationalInfo,      /// information about school and academic performance
  SocialInfo,           /// list of groups and events
  Files,                /// user files
  Wall,                 /// wall
  Messages              /// private messages
}
extension KundelikAuthorizationScopesEx on KundelikAuthorizationScopes {
  String get asString => describeEnum(this);
}

enum KundelikAuthorizationGrantType {
  AuthorizationCode,
  RefreshToken
}
extension KundelikAuthorizationGrantTypeEx on KundelikAuthorizationGrantType {
  String get asString => describeEnum(this);
}

class KundelikAuthorizationBundle {
  final String accessToken;         /// token id
  final int expiresIn;              /// token expiring time
  final String expiresInStr;
  final String refreshToken;        /// token for refresh
  final String scope;               /// list of access rights, separated by commas
  final int userId;                 /// user id
  final String userIdStr;

  KundelikAuthorizationBundle(this.accessToken, this.expiresIn, this.expiresInStr,
      this.refreshToken, this.scope, this.userId, this.userIdStr);

  KundelikAuthorizationBundle.fromJson(Map<String, dynamic> json)
      : accessToken = json['accessToken'],
        expiresIn = json['expiresIn'],
        expiresInStr = json['expiresInStr'],
        refreshToken = json['refreshToken'],
        scope = json['scope'],
        userId = json['userId'],
        userIdStr = json['userIdStr'];
}