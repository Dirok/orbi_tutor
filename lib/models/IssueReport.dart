import 'package:universal_io/io.dart';

class IssueReport {
  final String contacts;
  final String issueDescription;
  final List<File> files;

  IssueReport(this.contacts, this.issueDescription, this.files);
}
