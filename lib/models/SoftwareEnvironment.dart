class SoftwareEnvironment {
  final String appVersion;
  final String device;
  final String platform;

  SoftwareEnvironment(this.appVersion, this.device, this.platform);

  String get systemInfo => 'iamfocused\n'
                         + 'App version: ' + appVersion + '\n'
                         + 'Device: '      + device     + '\n'
                         + 'Platform: '    + platform;
}