import 'package:bson/bson.dart';
import 'package:tutor/focused.dart';

class DrawingSet {
  final List<ObjectId> drawingIds;
  final List<DrawingIndex> sequence;
  final Map<ObjectId, List<DrawingData>> drawingsByUserId;

  Iterable<DrawingData> get serialDrawingData sync* {
    for (final drawingIndex in sequence) {
      if (!drawingsByUserId.containsKey(drawingIndex.userId)) continue;
      final drawingDataList = drawingsByUserId[drawingIndex.userId];
      if (drawingIndex.index < drawingDataList.length)
        yield drawingDataList[drawingIndex.index];
    }
  }

  get length => sequence.length;

  DrawingSet(this.drawingIds, this.sequence, this.drawingsByUserId);

  DrawingSet.empty() : drawingIds = [], sequence = [], drawingsByUserId = {};

  void addDrawingPath(DrawingData drawingData, ObjectId userId) {
    if (!drawingsByUserId.containsKey(userId))
      drawingsByUserId[userId] = [];

    var userDrawingDataList = drawingsByUserId[userId];
    var index = userDrawingDataList.length;
    userDrawingDataList.insert(index, drawingData);
    sequence.add(DrawingIndex(index, userId));
    drawingIds.add(drawingData.id);
  }

  ObjectId addDrawingPoint(DrawingPoint point, ObjectId userId) {
    var drawingData = drawingsByUserId[userId].last;
    drawingData.addPoint(point);
    return drawingData.id;
  }

  ObjectId removeLastDrawingPath(ObjectId userId) {
    if (sequence.length == 0) return null;
    if (!drawingsByUserId.containsKey(userId)) return null;
    var drawingDataList = drawingsByUserId[userId];
    if (drawingDataList.length == 0) return null;
    var index = sequence.lastIndexWhere((di) => di.userId == userId);
    if (index == -1) return null;
    var drawingIndex = sequence.removeAt(index);
    if (drawingIndex.index >= drawingDataList.length) return null;
    var drawingData = drawingDataList.removeAt(drawingIndex.index);
    if (drawingIds.contains(drawingData.id))
      drawingIds.remove(drawingData.id);
    return drawingData.id;
  }

  void clear() {
    drawingsByUserId.clear();
    sequence.clear();
    drawingIds.clear();
  }

  DrawingSet.fromJson(Map<String, dynamic> json) :
        drawingIds = [], // STUB
        sequence = json['Sequence'] != null
            ? (json['Sequence'] as List).map((v) => DrawingIndex.fromJson(v)).toList() : [],
        drawingsByUserId = (json['Paths'] as Map).map((id, v) =>
            MapEntry(ObjectId.fromHexString(id), (v['PathList'] as List).map((d) => DrawingData.fromJson(d)).toList()));

  Map<String, dynamic> toJson() =>
      {
        'Sequence': sequence,
        'Paths': drawingsByUserId.map((id, drawingData) =>
            MapEntry(id.toHexString(), Map.fromEntries([MapEntry('PathList', drawingData)])))
      };
}

class DrawingIndex implements Comparable<DrawingIndex> {
  final int index;
  final ObjectId userId;

  DrawingIndex(this.index, this.userId);

  DrawingIndex.fromJson(Map<String, dynamic> json) :
        index = json['Index'],
        userId = ObjectId.fromHexString(json['UserID']);

  Map<String, dynamic> toJson() =>
      {
        'Index': index,
        'UserID': userId.toHexString()
      };

  @override
  int compareTo(DrawingIndex other) => index.compareTo(other.index);
}