import 'package:flutter/foundation.dart';

enum ImageType {
  Recognized,
  Unrecognized,
  Photo,
  Picture
}

extension ImageTypeEx on ImageType {
  String get asString => describeEnum(this);
}

class ImageTypeHelper {
  static final Map<String, ImageType> _strMap =
  Map<String, ImageType>.fromIterable(
      ImageType.values, key: (v) => describeEnum(v), value: (v) => v);

  static ImageType fromString(String value) =>
      value != null && value != '' && _strMap.containsKey(value) ? _strMap[value] : ImageType.Picture;
}