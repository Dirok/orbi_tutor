import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';

class DrawingPoint extends Point<double> {
  DrawingPoint(double x, double y) : super(x, y);
  DrawingPoint.fromOffset(Offset offset) : super(offset.dx, offset.dy);

  DrawingPoint.fromJson(Map<String, dynamic> json) : super(json['X']+.0, json['Y']+.0);
  Map<String, dynamic> toJson() => {'X': x, 'Y': y};

  DrawingPoint.fromRealmJson(Map<String, dynamic> json) : super(json['x']+.0, json['y']+.0);
  Map<String, dynamic> toRealmJson() => {'x': x, 'y': y};
}

extension DrawingPointEx on DrawingPoint {
  DrawingPoint convertPointToScreen(ScreenSize fromScreen, ScreenSize toScreen) {
    var scale = toScreen.height / fromScreen.height;
    return DrawingPoint(scale * x, scale * y);
  }
}

class ScreenSize {
  double width;
  double height;

  ScreenSize(this.width, this.height);

  ScreenSize.fromSize(Size size) : width = size.width, height = size.height;

  ScreenSize.fromJson(Map<String, dynamic> json)
      : width = json['Width']+.0,
        height = json['Height']+.0;

  Map<String, dynamic> toJson() => {'Width': width, 'Height': height};

  @override
  String toString() => '{Width: $width, Height: $height}';

  factory ScreenSize.fromRealmJson(Map<String, dynamic> json) =>
      ScreenSize(json['width'] + .0, json['height'] + .0);

  Map<String, dynamic> toRealmJson() => {'width': width, 'height': height};
}