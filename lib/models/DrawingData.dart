import 'dart:convert';
import 'dart:ui';

import 'package:bson/bson.dart';
import 'package:flutter/material.dart';
import 'package:tutor/focused.dart';

ScreenSize selfScreenSize;
ScreenSize selfSurfaceSize;

class DrawingData {
  final ObjectId id;
  ScreenSize surfaceSize;
  List<DrawingPoint> points = [];
  Path path;
  Paint paint;

  DrawingData._internal(this.id, this.paint, this.surfaceSize, this.points, this.path);

  DrawingData(Color color, double strokeWidth, this.surfaceSize) : id = ObjectId() {
    paint = Paint()
      ..strokeCap = StrokeCap.round
      ..isAntiAlias = true
      ..color = color
      ..strokeWidth = strokeWidth
      ..style = PaintingStyle.stroke
      ..strokeJoin = StrokeJoin.round;
  }

  addPoint(DrawingPoint drawingPoint) {
    points.add(drawingPoint);

    var p = surfaceSize == selfSurfaceSize ? drawingPoint
        : drawingPoint.convertPointToScreen(surfaceSize, selfSurfaceSize);

    path ??= Path()..moveTo(p.x, p.y);
    path.lineTo(p.x, p.y);
  }

//  collapsePoints() {
//    if (points.length < 3) return;
//
//    var newPoints = points.sublist(0, 3);
//
//    for (var i = 3; i < points.length; i++) {
//      var p = points[i];
//
//      var n = newPoints.length - 1;
//      var p0 = newPoints[n - 1] - newPoints[n - 2];
//      var p1 = newPoints[n] - newPoints[n - 1];
//      var p2 = p - newPoints[n];
//
//
//      var delta = (p2.dy - p1.dy) * (p1.dx - p0.dx) - (p1.dy - p0.dy) * (p2.dx - p1.dx);
//
////      var p1 = newPoints[n - 2];
////      var p2 = newPoints[n - 1];
////
////      var delta = (p1.dy - p2.dy) * p.dx + (p2.dx - p1.dx) * p.dy + (p1.dx * p2.dy - p2.dx * p1.dy);
//
//      if (delta <= 0.000000001) newPoints[newPoints.length - 1] = p;
//      else newPoints.add(p);
//    }
//
//    points = newPoints;
//  }

  static double _scaleStrokeWidth(double strokeWidth, ScreenSize fromSurfaceSize) {
    var scale = selfSurfaceSize.height / fromSurfaceSize.height;
    return strokeWidth * scale;
  }

  static Path _pathFromPoints(List<DrawingPoint> points, ScreenSize surfaceSize) {
    if (points == null || points.length == 0) return null;

    var path = Path();
    var p0 = points[0].convertPointToScreen(surfaceSize, selfSurfaceSize);
    path.moveTo(p0.x, p0.y);

    for (var point in points) {
      var p = point.convertPointToScreen(surfaceSize, selfSurfaceSize);
      path.lineTo(p.x, p.y);
    }

    return path;
  }

  DrawingData.fromJson(Map<String, dynamic> json) : id = ObjectId() {
    surfaceSize = ScreenSize.fromJson(json['ScreenSize']);
    paint = Paint()
      ..color = Color(json['Color'])
      ..strokeWidth = _scaleStrokeWidth(json['StrokeWidth']+.0, surfaceSize)
      ..strokeCap = StrokeCap.round
      ..isAntiAlias = true
      ..style = PaintingStyle.stroke
      ..strokeJoin = StrokeJoin.round;

    points = json['Points'] != null ? (json['Points'] as List).map((p) => DrawingPoint.fromJson(p)).toList() : [];
    path = _pathFromPoints(points, surfaceSize);
  }

  Map<String, dynamic> toJson() => {
    'Color' : paint.color.value,
    'StrokeWidth' : paint.strokeWidth,
    'ScreenSize' : surfaceSize,
    'Points' : points,
  };

  @override
  String toString() => '{Color: ${paint.color.value}, StrokeWidth: ${paint.strokeWidth}, ScreenSize: $surfaceSize, Points: $points }';

  factory DrawingData.fromRealmJson(Map<String, dynamic> json) {
    var id = ObjectId.fromHexString(json['_id']);
    var surfaceSize = ScreenSize.fromRealmJson(jsonDecode(json['screenSize']));
    var paint = Paint()
      ..color = Color(json['color'])
      ..strokeWidth = _scaleStrokeWidth(json['strokeWidth']+.0, surfaceSize)
      ..strokeCap = StrokeCap.round
      ..isAntiAlias = true
      ..style = PaintingStyle.stroke
      ..strokeJoin = StrokeJoin.round;

    var points = json['points'] != null ? (json['points'] as List)
        .map((p) => DrawingPoint.fromRealmJson(jsonDecode(p))).toList() : [];
    var path = _pathFromPoints(points, surfaceSize);

    return DrawingData._internal(id, paint, surfaceSize, points, path);
  }

  Map<String, dynamic> toRealmJson() => {
    '_id' : id.toHexString(),
    'color' : paint.color.value,
    'strokeWidth' : paint.strokeWidth,
    'screenSize' : jsonEncode(surfaceSize.toRealmJson()),
    'points' : points.map((p) => jsonEncode(p.toRealmJson())).toList(growable: false)
  };
}