import 'dart:async';

import 'package:get/get.dart';
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

class UnpackingException implements Exception {
  final String path;

  const UnpackingException(this.path);
}


class Download<TStateHolder extends DownloadStateHolder> {
  final Uri url;
  final String path;
  final String extractPath;
  final Duration connectionTimeout;
  final TStateHolder _stateHolder;

  TStateHolder get currentState => _stateHolder;

  final _requestSubscription = Rx<StreamSubscription>(null);

  Download(this._stateHolder, this.url, this.path, this.extractPath,
      this.connectionTimeout);
}

extension DownloadExtension on Download {

  Rx<DownloadState> get stateX => _stateHolder.downloadState;
  DownloadState get state => _stateHolder.downloadState.value;
}

extension DownloadLogicExtension on Download {

  _setState(DownloadState state, {Object error}) {
    Log.info("download: ${_stateHolder.downloadState()} ->  $state");
    _stateHolder.downloadState.value = state;
    if (error != null) {
      final ex =
      error is Exception ? error : Exception("Unknown error: $error");
      _stateHolder.downloadError.value = ex;
    }
  }

  _startDownload() async {
    onExtraction(bool extracted) {
      if (extracted) {
        _setState(DownloadState.downloaded);
      } else {
        _setState(DownloadState.downloadFailed,
            error: UnpackingException(path.toString()));
      }
    }

    onExtractionFailure(Object e) {
      _setState(DownloadState.downloadFailed, error: e);
    }

    _setState(DownloadState.initializing);

    try {
      final client = new HttpClient()..connectionTimeout = connectionTimeout;

      final request = await client.getUrl(url);

      request.headers
          .add(HttpHeaders.contentTypeHeader, "application/octet-stream");

      var response = await request.close();

      final hashes = response.headers['x-goog-hash'];
      final cloudHash = hashes
          .firstWhere((element) => element.contains('md5='), orElse: () => '')
          .replaceFirst('md5=', '');

      int byteCount = 0;
      int totalBytes = response.contentLength;

      File file = File(path);
      var raf = file.openSync(mode: FileMode.write);

      Log.info('downloading start total $totalBytes link $url');

      _setState(DownloadState.downloading);

      _requestSubscription.value = response.listen(
            (data) {
          Log.debug(
              'download... chunk ${data.length} ... downloaded $byteCount ');
          byteCount += data.length;
          raf.writeFromSync(data);

          _stateHolder.downloadProgress.value = byteCount / totalBytes;
        },
        onDone: () {
          raf.closeSync();

          HashHelper.calculateMd5(path).then((value) {
            if (cloudHash == value) {
              _setState(DownloadState.unpacking);
              Log.info('hashes matched successfully - $cloudHash');
              FileService()
                  .extractZip(path, extractPath)
                  .then(onExtraction, onError: onExtractionFailure);
            } else {
              final hashException = StorageException(
                  'hashes: cloud - $cloudHash and local - $value');
              _setState(DownloadState.downloadFailed, error: hashException);
              file.deleteSync();
            }
          });
        },
        onError: (e) {
          Log.error('downloading error');
          raf.closeSync();
          file.deleteSync();

          _setState(DownloadState.downloadFailed, error: e);
        },
        cancelOnError: true,
      );
    } catch (e) {
      Log.error(e.toString());
      _setState(DownloadState.downloadFailed, error: e);
      rethrow;
    }
  }

  /// Start throws an error to the stream when hashes doesn't match
  start() async {
    if (!state.canBeStarted) {
      Log.info("Can not start download when it is in $state state");
      return;
    }

    _startDownload();
  }

  // void pause() async {
  //   assert(canBePaused);
  //   if (!canBePaused) return;
  //
  //   _requestSubscription.value.pause();
  //   _setState(DownloadState.downloadingPaused);
  // }
  //
  // void resume() async {
  //   assert(canBePaused);
  //   if (!canBePaused) return;
  //
  //   _requestSubscription.value.resume();
  //   _setState(DownloadState.downloading);
  // }

  cancel() async {
    if (state != DownloadState.downloading) {
      Log.info("Can not cancel download when it is in $state state");
      return;
    }

    _setState(DownloadState.downloadCancelled);

    var subscription = _requestSubscription.value;
    if (subscription == null) {
      Log.info("request subscription is null on download cancel");
      return;
    }

    subscription
        .cancel()
        .then((value) => {Log.info("download has been cancelled")});

    // FIXME: проверить надо ли удалять уже скаченные куски

    _requestSubscription.value = null;
  }
}