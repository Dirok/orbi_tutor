import 'dart:convert';

import 'package:bson/bson.dart';
import 'package:path/path.dart' show join;
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

class Whiteboard {
  static const String _ext = '.dat';

  final ObjectId id;
  final ObjectId ownerId;

  final String name;

  var images = <ImageInfo>[];

  DrawingSet drawingSet = DrawingSet.empty();

  ImageInfo get image => images.isNotEmpty ? images.last : null;

  var imageIds = <ObjectId>[];
  var drawingIds = <ObjectId>[];

  Whiteboard(this.ownerId): id = ObjectId(), name = '';

  void addDrawingPath(DrawingData drawingData, ObjectId userId) =>
      drawingSet.addDrawingPath(drawingData, userId);

  ObjectId addDrawingPoint(DrawingPoint point, ObjectId userId) =>
      drawingSet.addDrawingPoint(point, userId);

  ObjectId removeLastDrawingPath(ObjectId userId) =>
      drawingSet.removeLastDrawingPath(userId);

  clearDrawing() => drawingSet.clear();

  Future<void> saveDrawing(String folderPath) async {
    var filePath = join(folderPath, id.toHexString() + _ext);
    var file = File(filePath);
    if (!await file.exists()) await file.create(recursive: true);

    var str = JsonEncoder().convert(drawingSet);
    await file.writeAsString(str, flush: true);
  }

  Future<void> loadDrawing(String folderPath) async {
    var filePath = join(folderPath, id.toHexString() + _ext);
    var file = File(filePath);
    if (!await file.exists()) return null;

    var str = await file.readAsString();
    var json = JsonDecoder().convert(str);

    try {
      drawingSet = DrawingSet.fromJson(json);
    } catch (e) {
       print(e);
    }
  }

  void setImageFolderPath(String imagesFolderPath) => image?.folderPath = imagesFolderPath;

  Whiteboard.fromJson(Map<String, dynamic> json) :
        id = ObjectId.fromHexString(json['ID']),
        ownerId = ObjectId.fromHexString(json['OwnerID']),
        name = json['Name'],
        images = _parseImages(json);

  static List<ImageInfo> _parseImages(Map<String, dynamic> json) {
    final imageName = json['CurrentImage'] as String;
    if (imageName == null || imageName.isEmpty) return <ImageInfo>[];

    final imageType = ImageTypeHelper.fromString(json['ImageType']);
    final ownerId = ObjectId.fromHexString(json['OwnerID']);

    return [ImageInfo(imageName, imageType, ownerId)];
  }

  Map<String, dynamic> toJson() =>
      {
        'ID': id.toHexString(),
        'OwnerID': ownerId.toHexString(),
        'Name': name,
        'CurrentImage': image?.name,
        'ImageType': image?.type?.asString
      };

  Whiteboard.fromRealmJson(Map<String, dynamic> json) :
        id = ObjectId.fromHexString(json['_id']),
        ownerId = ObjectId.fromHexString(json['ownerId']),
        name = json['name'],
        imageIds = (json['images'] as List).map((id) => ObjectId.fromHexString(id)).toList(),
        drawingIds = (json['drawings'] as List).map((id) => ObjectId.fromHexString(id)).toList();

  Map<String, dynamic> toRealmJson() => {
    '_id': id.toHexString(),
    'ownerId': ownerId.toHexString(),
    'name': name,
    'images': images.map((i) => i.id.toHexString()).toList(),
    'drawings': drawingSet.drawingIds.map((id) => id.toHexString()).toList()
  };
}