import 'dart:core';

import 'package:tutor/focused.dart';

class LectureListFilter {
  static final _defaultMinDate = DateTime(2020, 1, 1);
  static final _defaultMaxDate = DateTime(DateTime.now().year + 1, DateTime.now().month, 1);

  final fromDateTimeFilter = Filter<DateTime>(_defaultMinDate);
  final toDateTimeFilter = Filter<DateTime>(_defaultMaxDate);

  final ownersFilterList = <ToggledFilter<User>>[];
  final groupsFilterList = <ToggledFilter<Group>>[];
  final subjectsFilterList = <ToggledFilter<Subject>>[];

  List<Group> get accountGroups => appSession.account.groups;
  List<Subject> get userSubjects => appSession.account.userSubjects;

  LectureListFilter() {
    final account = appSession?.account;
    if (account == null) return;

    ownersFilterList.addAll(account.groups?.map((g) => ToggledFilter(g.owner, g.owner.shortName))?.toList());
    groupsFilterList.addAll(account.groups?.map((g) => ToggledFilter(g, g.name))?.toList());
    subjectsFilterList.addAll(account.userSubjects?.map((s) => ToggledFilter(s, s.name))?.toList());
  }

  void updateAllFilters() {
    updateOwnersFilterList();
    updateGroupsFilterList();
    updateSubjectsFilterList();
  }

  void updateOwnersFilterList() {
    ownersFilterList.removeWhere((f) => !accountGroups.any((g) => g.owner.shortName == f.name));

    for (var g in accountGroups) {
      if (subjectsFilterList.any((f) => f.name == g.owner.shortName)) continue;
      ownersFilterList.add(ToggledFilter(g.owner, g.owner.shortName));
    }
  }

  void updateGroupsFilterList() {
    groupsFilterList.removeWhere((f) => !accountGroups.any((g) => g.name == f.name));

    for (var g in accountGroups) {
      if (groupsFilterList.any((f) => f.name == g.name)) continue;
      groupsFilterList.add(ToggledFilter(g, g.name));
    }
  }

  void updateSubjectsFilterList() {
    subjectsFilterList.removeWhere((f) => !userSubjects.any((s) => s.name == f.name));

    for (var s in userSubjects) {
      if (subjectsFilterList.any((f) => f.name == s.name)) continue;
      subjectsFilterList.add(ToggledFilter(s, s.name));
    }
  }
}