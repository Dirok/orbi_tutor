import 'dart:convert';

import 'package:bson/bson.dart';
import 'package:get/get.dart';
import 'package:path/path.dart' show join;
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

class Lecture with DownloadStateHolder {
  final ObjectId id;
  final String subjectName;
  final User owner;
  final String description;
  final DateTime startDateTime;
  final LecturePolicy policy;
  final LectureServerType serverType;

  LectureState state = LectureState.planned;
  Rx<Location> dataLocation = Location.nowhere.obs;
  String server;
  String key;

  String s3server;

  List<User> participants = [];

  Lecture.fromJson(Map<String, dynamic> json)
      : id = ObjectId.fromHexString(json['id']),
        subjectName = json['subject'],
        owner = User.fromJson(json['owner']),
        description = json['description'],
        startDateTime = DateTime.parse(json['datetime']).toLocal(),
        policy = LecturePolicyHelper.fromString(json['policy']),
        state = LectureStateHelper.fromString(json['state']),
        serverType = LectureServerTypeHelper.fromString(json['lectureServerType']);
        // server = json['server'],
        // key = json['key'];

  static Future<Lecture> fromStorage(String lectureFolder) async {
    final dataFilePath = LectureDataExtension.getDataPathFromFolder(lectureFolder);
    final str = await File(dataFilePath).readAsString();
    final json = JsonDecoder().convert(str);
    final lecture = Lecture.fromJson(json)
    ..dataLocation.value = Location.local;
    return lecture;
  }
}

extension LectureExtension on Lecture {
  static Map<String, Lecture> listToMap(List<Lecture> list) =>
      Map.fromIterable(list, key: (e) => e.id, value: (e) => e);

  bool get isPlanned => this.state == LectureState.planned;
  bool get isActive => this.state == LectureState.active;
  bool get isOnTeacherCheck => this.state == LectureState.checking;
  bool get isProcessingOnServer => this.state == LectureState.passed;
  bool get isPassed => this.state == LectureState.completed;
  bool get isUnknown => this.state == LectureState.unknown;
}

extension LectureAccessRightsExtension on Lecture {
  bool get currentUserIsLectureOwner => this.owner == appSession.account.user;
  bool get currentUserIsLectureParticipant => !currentUserIsLectureOwner;
}

extension LectureDataExtension on Lecture {
  static const String _fileName = 'Lecture.dat';
  static const String _whiteboardsFolderName = 'FinalLectureState';
  static const String _imagesFolderName = 'Images';

  String get folderPath => join(Settings.lecturesFolderPath, id.toHexString());
  String get whiteboardsFolderPath => join(folderPath, _whiteboardsFolderName);
  String get imagesFolderPath => join(folderPath, _imagesFolderName);
  String get dataFilePath => getDataPathFromFolder(folderPath);

  String get archivePath => join(Settings.downloadsFolderPath, id.toHexString() + Constants.archiveExt);

  static String getDataPathFromFolder(String folder) => join(folder, _fileName);
}


extension LectureLocationExtension on Lecture {
  addDataLocation(Location locationToAdd) =>
      this.dataLocation.value = this.dataLocation().addLocationFlag(locationToAdd);

  removeDataLocation(Location locationToRemove) =>
      this.dataLocation.value = this.dataLocation().removeLocationFlag(locationToRemove);

  hasDataLocation(Location location) {
    switch (this.dataLocation()) {
      case Location.nowhere: return false;
      case Location.local: return location == Location.local;
      case Location.server: return location == Location.server;
      case Location.everywhere: return true;
    }
  }
}