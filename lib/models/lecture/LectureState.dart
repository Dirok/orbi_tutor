import 'package:flutter/foundation.dart';

enum LectureState {
  active,
  checking,
  planned,
  passed,
  completed,
  unknown
}

class LectureStateHelper {
  static final Map<String, LectureState> _strMap =
  Map<String, LectureState>.fromIterable(
      LectureState.values, key: (v) => describeEnum(v), value: (v) => v);

  static LectureState fromString(String value) =>
      _strMap.containsKey(value) ? _strMap[value] : LectureState.unknown;
}

extension LectureStateEx on LectureState {
  String get asString => describeEnum(this);
}