import 'package:flutter/foundation.dart';

enum LecturePolicy {
  by_invite,
  opened,
  unknown
}

class LecturePolicyHelper {
  static final Map<String, LecturePolicy> _strMap =
  Map<String, LecturePolicy>.fromIterable(
      LecturePolicy.values, key: (v) => describeEnum(v), value: (v) => v);

  static LecturePolicy fromString(String value) =>
      _strMap.containsKey(value) ? _strMap[value] : LecturePolicy.unknown;
}

extension LecturePolicyEx on LecturePolicy {
  String get asString => describeEnum(this);
}