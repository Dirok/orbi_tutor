class Filter<T> {
  final T defaultValue;
  T value;

  Filter(this.defaultValue) : value = defaultValue;

  void reset() => value = defaultValue;
}

class ToggledFilter<TItem> extends Filter<bool> {
  final TItem item;
  final String name;

  ToggledFilter(this.item, this.name, {bool defaultValue = true}) : super(defaultValue);
}