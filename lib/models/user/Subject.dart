import 'package:flutter/foundation.dart';

enum SubjectType {
  predef,
  custom,
  unknown
}

class SubjectTypeHelper {
  static final Map<String, SubjectType> _strMap =
  Map<String, SubjectType>.fromIterable(
      SubjectType.values, key: (v) => describeEnum(v), value: (v) => v);

  static SubjectType fromString(String value) =>
      _strMap.containsKey(value) ? _strMap[value] : SubjectType.unknown;
}

extension SubjectTypeEx on SubjectType {
  String get asString => describeEnum(this);
}

class Subject {
  final String id;
  final String name;
  final SubjectType type;

  Subject.custom(this.id) : name = id, type = SubjectType.custom;

  Subject.unknown() : id = '', name = '', type = SubjectType.unknown;

  Subject.fromJson(Map<String, dynamic> json)
      : id = json['defName'],
        name = json['localization'],
        type = SubjectTypeHelper.fromString(json['type']);

  Map<String, dynamic> toJson() => {
    'defName' : id,
    'localization' : name,
    'type' : type.asString
  };

  @override
  bool operator ==(Object other) => identical(this, other)
      || (other is Subject && id == other.id && name == other.name && type == other.type);

  @override
  int get hashCode => id.hashCode;
}

extension SubjectExtension on Subject {
  static Map<String, Subject> listToMap(List<Subject> list) =>
      Map.fromIterable(list, key: (e) => e.id, value: (e) => e);

  bool get isCustom => this.type == SubjectType.custom;
  bool get isPredefined => this.type == SubjectType.predef;
  bool get isUnknown => this.type == SubjectType.unknown;
}