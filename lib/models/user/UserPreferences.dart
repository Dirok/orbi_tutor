import 'dart:convert';
import 'dart:ui';

import 'package:tutor/focused.dart';

class UserPreferences {
  Color _pickerColor;
  double _strokeWidth;
  DateTime _filterFromDateTime;
  DateTime _filterToDateTime;
  bool _isNeedToSaveImages;
  bool _isStylusModeEnabled;
  bool _isOwnersExpanded;
  bool _isGroupsExpanded;
  bool _isSubjectsExpanded;
  RecentQueue<Subject> _recentSearchedSubjectList = RecentQueue(10);

  Color get pickerColor => _pickerColor;
  set pickerColor(Color value) {
    _pickerColor = value;
    appSession?.setAccountPref();
  }

  double get strokeWidth => _strokeWidth;
  set strokeWidth(double value) {
    _strokeWidth = value;
    appSession?.setAccountPref();
  }

  DateTime get filterFromDateTime => _filterFromDateTime;
  set filterFromDateTime(DateTime value) {
    _filterFromDateTime = value;
    appSession?.setAccountPref();
  }

  DateTime get filterToDateTime => _filterToDateTime;
  set filterToDateTime(DateTime value) {
    _filterToDateTime = value;
    appSession?.setAccountPref();
  }

  bool get isNeedToSaveImages => _isNeedToSaveImages;
  set isNeedToSaveImages(bool value) {
    _isNeedToSaveImages = value;
    appSession?.setAccountPref();
  }

  bool get isStylusModeEnabled => _isStylusModeEnabled;
  set isStylusModeEnabled(bool value) {
    _isStylusModeEnabled = value;
    appSession?.setAccountPref();
  }

  bool get isOwnersExpanded => _isOwnersExpanded;
  set isOwnersExpanded(bool value) {
    _isOwnersExpanded = value;
    appSession?.setAccountPref();
  }

  bool get isGroupsExpanded => _isGroupsExpanded;
  set isGroupsExpanded(bool value) {
    _isGroupsExpanded = value;
    appSession?.setAccountPref();
  }

  bool get isSubjectsExpanded => _isSubjectsExpanded;
  set isSubjectsExpanded(bool value) {
    _isSubjectsExpanded = value;
    appSession?.setAccountPref();
  }

  RecentQueue<Subject> get recentSearchedSubjectList => _recentSearchedSubjectList;
  set recentSearchedSubjectList(RecentQueue<Subject> list) {
    _recentSearchedSubjectList = list;
    appSession?.setAccountPref();
  }

  UserPreferences();

  UserPreferences.fromJson(Map<String, dynamic> json) {
    final jsonPickerColor = json['picker_color'];
    pickerColor = jsonPickerColor != null ? Color(jsonPickerColor) : null;

    strokeWidth = json['stroke_width'];

    final jsonFilterFromDateTime = json['filter_date_from'];
    filterFromDateTime = jsonFilterFromDateTime != null
      ? DateTime.fromMillisecondsSinceEpoch(jsonFilterFromDateTime)
      : null;

    final jsonFilterToDateTime = json['filter_date_to'];
    filterToDateTime = jsonFilterToDateTime != null
      ? DateTime.fromMillisecondsSinceEpoch(jsonFilterToDateTime)
      : null;
    isNeedToSaveImages = json['save_images'];
    isStylusModeEnabled = json['stylus_mode_enabled'];
    isOwnersExpanded = json['owners_expanded'] ?? false;
    isGroupsExpanded = json['groups_expanded'] ?? false;
    isSubjectsExpanded = json['subjects_expanded'] ?? false;

    try {
      final jsonRecentSearchedSubject = json['recent_searched_subject'];
      if (jsonRecentSearchedSubject != null) {
        recentSearchedSubjectList.clear();
        jsonDecode(jsonRecentSearchedSubject).forEach((element) {
          recentSearchedSubjectList.add(Subject.fromJson(element));
        });
      }
    } catch(e) {
      Log.error("Error in decode json from recent_searched_subject - $e");
    }
  }

  Map<String, dynamic> toJson() => {
    'picker_color' : pickerColor?.value,
    'stroke_width' : strokeWidth,
    'filter_date_from' : filterFromDateTime?.millisecondsSinceEpoch,
    'filter_date_to' : filterToDateTime?.millisecondsSinceEpoch,
    'save_images' : isNeedToSaveImages,
    'stylus_mode_enabled' : isStylusModeEnabled,
    'owners_expanded' : isOwnersExpanded,
    'groups_expanded' : isGroupsExpanded,
    'subjects_expanded' : isSubjectsExpanded,
    'recent_searched_subject' : '[' + recentSearchedSubjectList.fold('', (prev, elem) =>
      "$prev ${prev != '' ? ',' : ''} ${jsonEncode(elem.toJson())}") + ']',
  };
}