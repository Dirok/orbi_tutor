import 'package:bson/bson.dart';

class UserState {
  ObjectId userid;

  final bool isHandRaised;
  final bool allowSpeak;
  final bool isMicEnabled;
  final int whiteboardsCount;

  bool get hasWhiteboards => whiteboardsCount > 0;

  UserState.fromJson(Map<String, dynamic> json)
      : isHandRaised = json['HandState'],
        allowSpeak = json['AllowSpeak'],
        isMicEnabled = json['MicEnabled'],
        whiteboardsCount = json['BoardsCount'] as int;

  UserState.fromRealmJson(Map<String, dynamic> json)
      : userid = ObjectId.fromHexString(json['userId']),
        isHandRaised = json['isHandRaised'],
        allowSpeak = json['isSpeakAllowed'],
        isMicEnabled = json['isMicEnabled'],
        whiteboardsCount = json['whiteboardsCount'] as int;
}