import 'package:bson/bson.dart';
import 'package:tutor/focused.dart';

class User implements IContact {
  final ObjectId id;
  final String name;
  final String surname;
  final String patronymic;

  @override
  Iterable<ObjectId> get membersIds => [id];

  @override
  String get fullName => '$surname $name $patronymic';

  String get surnameName => '$surname $name';

  String get shortName {
    String shortName = '';
    if (name.length > 0)
      shortName += '${name[0].toUpperCase()}.';
    if (patronymic.length > 0)
      shortName += ' ${patronymic[0].toUpperCase()}.';
    shortName += ' $surname';
    return shortName;
  }

  User(this.id, this.name, this.surname, this.patronymic);

  User.fromJson(Map<String, dynamic> json)
      : id = ObjectId.fromHexString(json['id']),
        name = json['name'],
        surname = json['surname'],
        patronymic = json['patronymic'];

  Map<String, dynamic> toJson() => {
    'id' : id.toHexString(),
    'name' : name,
    'surname' : surname,
    'patronymic' : patronymic,
  };

  @override
  bool operator ==(Object other) => identical(this, other)
      || (other is User && id == other.id && name == other.name
          && surname == other.surname && patronymic == other.patronymic);

  @override
  int get hashCode => id.hashCode;
}