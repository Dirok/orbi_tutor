class UserHashChanges {
  final int emptyHash = -1;  // should be other than at server (0)

  int accountHash;
  int groupsHash;
  int lecturesHash;
  int subjectsHash;

  UserHashChanges({this.accountHash = -1, this.groupsHash = -1, this.lecturesHash = -1, this.subjectsHash = -1});

  void initUserHashes() {
    accountHash = emptyHash;
    groupsHash = emptyHash;
    lecturesHash = emptyHash;
    subjectsHash = emptyHash;
  }

  UserHashChanges.fromJson(Map<String, dynamic> json)
      : accountHash = json['accountHash'],
        groupsHash = json['groupsHash'],
        lecturesHash = json['lecturesHash'],
        subjectsHash = json['subjectsHash'];

  Map<String, dynamic> toJson() => {
    'accountHash' : accountHash,
    'groupsHash' : groupsHash,
    'lecturesHash' : lecturesHash,
    'subjectsHash' : subjectsHash
  };

  @override
  bool operator ==(Object other) => identical(this, other)
      || (other is UserHashChanges
          && accountHash == other.accountHash
          && groupsHash == other.groupsHash
          && lecturesHash == other.lecturesHash
          && subjectsHash == other.subjectsHash);

  @override
  int get hashCode {
    return accountHash.hashCode
    ^ groupsHash.hashCode
    ^ lecturesHash.hashCode
    ^ subjectsHash.hashCode;
  }
}