import 'package:bson/bson.dart';
import 'package:tutor/focused.dart';

class Account {
  final String description;
  final String email;
  final UserPermissions permissions;
  final UserPreferences preferences;
  final int countryCode;

  User user;
  ObjectId get id => user.id;
  
  UserHashChanges hashes = UserHashChanges();
  // todo [another task] del this after filter model will use data from services only
  List<Group> groups = [];
  List<Subject> predefinedSubjects = [];
  List<Subject> userSubjects = [];

  // todo [another task] transit this when user customization will be saving
  RecentQueue<ContactViewModel> recentSearchedParticipantList = RecentQueue(10);

  bool get hasSchoolLicense => permissions != null
      && (permissions.hasTeacherLicense || permissions.hasStudentLicense);

  bool get hasCreationPermissions => permissions != null
      && (permissions.hasTeacherLicense || permissions.hasPersonalLicense);

  Account.fromJson(Map<String, dynamic> json)
      : user = User.fromJson(json.containsKey('user') ? json['user'] : json),
        description = json['description'],
        email = json['email'],
        permissions = json.containsKey('permissions')
            ? UserPermissions.fromJson(json['permissions'])
            : UserPermissions.fromLicenses(json),
        preferences = json.containsKey('preferences')
            ? UserPreferences.fromJson(json['preferences'])
            : UserPreferences.fromJson(json),
        countryCode = json['countryCode'];

  Map<String, dynamic> toJson() => {
    'user' : user.toJson(),
    'description' : description,
    'email' : email,
    'permissions' : permissions.toJson(),
    'preferences' : preferences.toJson(),
  };
}