import 'package:tutor/focused.dart';

class UserPermissions {
  // TODO license => permissions
  bool hasSchoolAdminLicense = false;
  bool hasTeacherLicense = false;
  bool hasStudentLicense = false;
  bool hasPersonalLicense = false;

  String get currPermissions => 'permissions[ admin: $hasSchoolAdminLicense |'
      ' teacher: $hasTeacherLicense | student: $hasStudentLicense | personal: $hasPersonalLicense ]';

  UserPermissions() : hasSchoolAdminLicense = false, hasTeacherLicense = false,
      hasStudentLicense = false, hasPersonalLicense = false;

  UserPermissions.fromLicenses(Map<String, dynamic> json) {
    final schoolAdminLicenses = json['schoolAdminLicenses'] as List;
    hasSchoolAdminLicense = schoolAdminLicenses != null && schoolAdminLicenses.isNotEmpty;

    final teacherLicenses = json['teacherLicenses'] as List;
    hasTeacherLicense = teacherLicenses != null && teacherLicenses.isNotEmpty;

    final studentLicenses = json['studentLicenses'] as List;
    hasStudentLicense = studentLicenses != null && studentLicenses.isNotEmpty;

    final personalLicense = PersonalLicense.fromJson(json['personalLicense']);
    hasPersonalLicense = personalLicense != null && personalLicense.isEnabled;
  }

  UserPermissions.fromJson(Map<String, dynamic> json)
      : hasSchoolAdminLicense = json['hasSchoolAdminLicense'],
        hasTeacherLicense = json['hasTeacherLicense'],
        hasStudentLicense = json['hasStudentLicense'],
        hasPersonalLicense = json['hasPersonalLicense'];

  Map<String, dynamic> toJson() => {
    'hasSchoolAdminLicense' : hasSchoolAdminLicense,
    'hasTeacherLicense' : hasTeacherLicense,
    'hasStudentLicense' : hasStudentLicense,
    'hasPersonalLicense' : hasPersonalLicense,
  };
}