import 'package:bson/bson.dart';

class Discipline {
  final ObjectId id;
  final String name;

  Discipline(this.id, this.name);

  Discipline.fromJson(Map<String, dynamic> json)
      : id = ObjectId.fromHexString(json['id']),
        name = json['name'];

  Map<String, dynamic> toJson() => {
    'id' : id.toHexString(),
    'name' : name,
  };
}