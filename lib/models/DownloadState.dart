import 'package:flutter/foundation.dart';
import 'package:get/get_rx/get_rx.dart';

enum DownloadState {
  notDownloaded,
  initializing,
  downloading,
  downloadingPaused,
  unpacking,
  downloaded,
  downloadFailed,
  downloadCancelled,
  unknown
}

extension DownloadStateExtension on DownloadState {
  String get asString => describeEnum(this);

  bool get isDownloaded => this == DownloadState.downloaded;

  bool get isFinished =>
      this == DownloadState.downloaded ||
      this == DownloadState.downloadFailed ||
      this == DownloadState.downloadCancelled;

  bool get isInProgress =>
      this == DownloadState.initializing ||
      this == DownloadState.downloading ||
      this == DownloadState.downloadingPaused ||
      this == DownloadState.unpacking;

  bool get canBeStarted =>
      this == DownloadState.notDownloaded ||
      this == DownloadState.downloadFailed ||
      this == DownloadState.downloadCancelled ||
      this == DownloadState.unknown;
}

class DownloadStateHolder {
  final downloadState = DownloadState.unknown.obs;
  final downloadError = Rx<Exception>(null);
  final downloadProgress = 0.0.obs;

  void resetDownloadState() {
    downloadState.value = DownloadState.unknown;
    downloadProgress.value = 0;
    downloadError.value = null;
  }
}
