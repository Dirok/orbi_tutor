import 'dart:math';

import 'package:bson/bson.dart';
import 'package:path/path.dart' show join;
import 'package:tutor/focused.dart';

class ImageInfo {
  final ObjectId id;
  final ObjectId ownerId;

  final String name;
  final ImageType type;

  String folderPath;
  String url;

  String get path => join(folderPath, name);

  Rectangle<double> rectangle;

  ImageInfo(this.name, this.type, this.ownerId, {this.folderPath, this.url}) : id = ObjectId();

  ImageInfo.fromRealmJson(Map<String, dynamic> json)
      : id = ObjectId.fromHexString(json['_id']),
        ownerId = ObjectId.fromHexString(json['ownerId']),
        name = json['name'],
        type = ImageTypeHelper.fromString(json['type']),
        url = json['url'];

  Map<String, dynamic> toRealmJson() => {
        '_id': id.toHexString(),
        'ownerId': id.toHexString(),
        'name': name,
        'type': type.asString,
        'url': url,
      };
}

// TODO for multiple images on whiteboard
class ImageSet {
  final images = <ImageInfo>[];
}
