import 'package:bson/bson.dart';

class Class {
  final ObjectId id;
  final String name;
  final ObjectId schoolId;

  Class(this.id, this.name, this.schoolId);

  Class.fromJson(Map<String, dynamic> json)
      : id = ObjectId.fromHexString(json['id']),
        name = json['name'],
        schoolId = ObjectId.fromHexString(json['school_id']);

  Map<String, dynamic> toJson() => {
    'id' : id.toHexString(),
    'name' : name,
    'school_id': schoolId.toHexString(),
  };

  @override
  bool operator ==(Object other) => identical(this, other)
    || (other is Class && id == other.id && schoolId == other.schoolId && name == other.name);

  @override
  int get hashCode => id.hashCode;
}