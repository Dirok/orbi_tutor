class UserVolume {
  UserVolume(this.userId, this.volumeLevel);

  final String userId;
  final double volumeLevel;
}
