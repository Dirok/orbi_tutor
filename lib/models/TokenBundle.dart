class TokenBundle {
  final String httpsToken;
  final String refreshToken;

  TokenBundle.fromJson(Map<String, dynamic> json)
      : httpsToken = json['token'],
        refreshToken = json['refreshToken'];
}
