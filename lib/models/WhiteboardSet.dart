
import 'package:bson/bson.dart';
import 'package:tutor/focused.dart';

class WhiteboardSet {
  final ObjectId id;
  final ObjectId ownerId;

  Map<ObjectId, Whiteboard> _whiteboardMap;

  final List<ObjectId> _sequence;

  Iterable<ObjectId> get whiteboardIds => _sequence;

  set whiteboardMap (Map<ObjectId, Whiteboard> value) => _whiteboardMap = value;

  ObjectId _currentWhiteboardId;

  int get length => _whiteboardMap.length;

  Iterable<Whiteboard> get whiteboards =>
      _sequence.map((id) => _whiteboardMap[id]);

  Whiteboard get current => _whiteboardMap[_currentWhiteboardId];
  set current (Whiteboard whiteboard) => _currentWhiteboardId = whiteboard.id;

  int get currentIndex => _sequence.indexOf(_currentWhiteboardId);

  WhiteboardSet(this.ownerId) : id = ObjectId(), _whiteboardMap = {}, _sequence = [];

  insert(int index, Whiteboard whiteboard) {
    var id = whiteboard.id;
    _whiteboardMap[id] = whiteboard;
    _sequence.insert(index, id);
  }

  add(Whiteboard whiteboard) {
    var id = whiteboard.id;
    _whiteboardMap[id] = whiteboard;
    _sequence.add(id);
  }

  bool contains(Whiteboard whiteboard) => _whiteboardMap.containsKey(whiteboard.id);

  Whiteboard getWhiteboardById(ObjectId whiteboardId) =>
      _whiteboardMap.containsKey(whiteboardId) ? _whiteboardMap[whiteboardId] : null;

  bool setCurrentById(ObjectId whiteboardId) {
    if (!_whiteboardMap.containsKey(whiteboardId)) return false;
    _currentWhiteboardId = whiteboardId;
    return true;
  }

  clear() {
    _whiteboardMap.clear();
    _sequence.clear();
    _currentWhiteboardId = null;
  }

  Whiteboard selectItemByIndex(int index) {
    _currentWhiteboardId = _sequence[index];
    return _whiteboardMap[_currentWhiteboardId];
  }

  WhiteboardSet.fromJson(Map<String, dynamic> json) :  // TODO optimize
        _whiteboardMap = (json['Boards'] as Map).map((id, wb) {
          wb['OwnerID'] = json['OwnerID'];
          return MapEntry(ObjectId.fromHexString(id), Whiteboard.fromJson(wb));
        }),
        _sequence = ((json['BoardsSequence']) as List).map((id) => ObjectId.fromHexString(id)).toList(),
        _currentWhiteboardId = ObjectId.fromHexString(json['CurrentBoard']),
        ownerId = ObjectId.fromHexString(json['OwnerID']),
        id = ObjectId(); //STUB

  Map<String, dynamic> toJson() => {
    'OwnerID' : ownerId.toHexString(),
    'CurrentBoard' : _currentWhiteboardId.toHexString(),
    'Boards' : _whiteboardMap.map((id, wb) => MapEntry(id.toHexString(), wb)),
    'BoardsSequence' : _sequence.map((id) => id.toHexString()).toList()
  };

  WhiteboardSet.fromRealmJson(Map<String, dynamic> json) :
        id = ObjectId.fromHexString(json['_id']),
        ownerId = ObjectId.fromHexString(json['ownerId']),
        _currentWhiteboardId = ObjectId.fromHexString(json['currentId']),
        _sequence = (json['whiteboards'] as List).map((id) => ObjectId.fromHexString(id)).toList();

  Map<String, dynamic> toRealmJson() => {
    '_id' : id.toHexString(),
    'ownerId' : ownerId.toHexString(),
    'currentId' : _currentWhiteboardId?.toHexString(),
    'whiteboards' : _sequence.map((id) => id.toHexString()).toList()
  };
}

class WhiteboardSetStub implements WhiteboardSet {
  WhiteboardSetStub(
      this._whiteboard, this._index, this._containingSequenceLength)
      : id = ObjectId(), _currentWhiteboardId = _whiteboard.id, _sequence = [];

  final ObjectId id;

  ObjectId _currentWhiteboardId;

  Map<ObjectId, Whiteboard> _whiteboardMap;

  final Whiteboard _whiteboard;
  final List<ObjectId> _sequence;

  final int _index;
  int _containingSequenceLength;

  ObjectId get ownerId => _whiteboard.ownerId;

  @override
  int get length => _containingSequenceLength;

  @override
  Iterable<Whiteboard> get whiteboards => throw UnimplementedError();

  @override
  Whiteboard get current => _whiteboard;

  @override
  set current(Whiteboard whiteboard) => throw UnimplementedError();

  @override
  int get currentIndex => _index;

  @override
  insert(int index, Whiteboard whiteboard) => _containingSequenceLength++;

  @override
  add(Whiteboard whiteboard) => throw UnimplementedError();

  @override
  bool contains(Whiteboard whiteboard) => throw UnimplementedError();

  @override
  Whiteboard getWhiteboardById(ObjectId whiteboardId) =>
      current.id == whiteboardId ? current : null;

  @override
  clear() => throw UnimplementedError();

  @override
  Whiteboard selectItemByIndex(int index) => throw UnimplementedError();

  @override
  Map<String, dynamic> toJson() => throw UnimplementedError();

  @override
  Iterable<ObjectId> whiteboardIds;

  @override
  bool setCurrentById(ObjectId whiteboardId) => throw UnimplementedError();

  @override
  set whiteboardMap (Map<ObjectId, Whiteboard> value) => throw UnimplementedError();

  @override
  Map<String, dynamic> toRealmJson() => throw UnimplementedError();
}