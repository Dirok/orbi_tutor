import 'package:bson/bson.dart';

class MainWhiteboardInfo {
  final ObjectId id;
  final ObjectId ownerId;
  final int containingSequenceLength;
  final int index;

  MainWhiteboardInfo({
    this.id,
    this.ownerId,
    this.containingSequenceLength,
    this.index,
  });
}
