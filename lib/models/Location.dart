enum Location {
  nowhere,
  server,
  local,
  everywhere,
}

extension LocationEx on Location {
  removeLocationFlag(Location locationToRemove) {
    removeFromEverywhere(Location toRemove) {
      switch(toRemove) {
        case Location.nowhere: return Location.everywhere;
        case Location.everywhere: return Location.nowhere;
        case Location.server: return Location.local;
        case Location.local: return Location.server;
      }
    }

    removeFromServer(Location toRemove) {
      switch(toRemove) {
        case Location.nowhere: return Location.server;
        case Location.everywhere: return Location.nowhere;
        case Location.server: return Location.nowhere;
        case Location.local: return Location.server;
      }
    }

    removeFromLocal(Location toRemove) {
      switch(toRemove) {
        case Location.nowhere: return Location.local;
        case Location.everywhere: return Location.nowhere;
        case Location.server: return Location.local;
        case Location.local: return Location.nowhere;
      }
    }

    final current = this;
    switch (current) {
      case Location.nowhere: return this;
      case Location.everywhere: return removeFromEverywhere(locationToRemove);
      case Location.server: return removeFromServer(locationToRemove);
      case Location.local: return removeFromLocal(locationToRemove);
    }
  }


  addLocationFlag(Location locationToAdd) {
    addToServer(Location toAdd) {
      switch(toAdd) {
        case Location.nowhere: return Location.server;
        case Location.everywhere: return Location.everywhere;
        case Location.server: return Location.server;
        case Location.local: return Location.everywhere;
      }
    }

    addToLocal(Location toAdd) {
      switch(toAdd) {
        case Location.nowhere: return Location.local;
        case Location.everywhere: return Location.everywhere;
        case Location.server: return Location.everywhere;
        case Location.local: return Location.local;
      }
    }

    final current = this;
    switch (current) {
      case Location.nowhere: return locationToAdd;
      case Location.everywhere: return this;
      case Location.server: return addToServer(locationToAdd);
      case Location.local: return addToLocal(locationToAdd);
    }
  }
}
