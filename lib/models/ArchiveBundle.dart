class ArchiveBundle {
  final int size;
  final Uri url;

  ArchiveBundle(this.size, this.url);

  ArchiveBundle.fromJson(Map<String, dynamic> json)
      : size = json['size'],
        url = Uri.parse(json['link']);
}