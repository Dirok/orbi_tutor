import 'package:tutor/focused.dart';

class AuthorizationBundle {
  final TokenBundle tokenBundle;
  final Account account;
  String partner;

  AuthorizationBundle.fromJson(Map<String, dynamic> json)
      : tokenBundle = TokenBundle.fromJson(json),
        account = Account.fromJson(json['account']);
}
