class MultimediaServerParams {
  final String address;
  final int id;
  final String token;

  MultimediaServerParams.fromJson(Map<String, dynamic> json)
      : address = json['address'],
        id = json['roomID'],
        token = json['token'];
}