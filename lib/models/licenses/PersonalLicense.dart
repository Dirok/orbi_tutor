class PersonalLicense {
  final bool isEnabled;
  final DateTime endDateTime;

  PersonalLicense(this.isEnabled, this.endDateTime);

  PersonalLicense.fromJson(Map<String, dynamic> json)
      : isEnabled = json['enabled'],
        endDateTime = DateTime.parse(json['endTime']).toLocal();

  Map<String, dynamic> toJson() => {
    'enabled' : isEnabled,
    'endTime' : endDateTime.toIso8601String(),
  };
}