import 'dart:math';

import 'package:flutter/material.dart';

MaterialColor alphaConvert(Color color) {
  return MaterialColor(color.value,
  {
    50:  _alphaColor(color, 0.1),
    100: _alphaColor(color, 0.2),
    200: _alphaColor(color, 0.3),
    300: _alphaColor(color, 0.4),
    400: _alphaColor(color, 0.5),
    500: _alphaColor(color, 0.6),
    600: _alphaColor(color, 0.7),
    700: _alphaColor(color, 0.8),
    800: _alphaColor(color, 0.9),
    900: _alphaColor(color, 1.0),
  });
}

Color _alphaColor(Color color, double factor) => Color.fromRGBO(
    color.red, color.green, color.blue, factor);


MaterialColor tintConvert(Color color) {
  return MaterialColor(color.value,
  {
    50:  _tintColor(color, 0.9),
    100: _tintColor(color, 0.8),
    200: _tintColor(color, 0.6),
    300: _tintColor(color, 0.4),
    400: _tintColor(color, 0.2),
    500: color,
    600: _shadeColor(color, 0.1),
    700: _shadeColor(color, 0.2),
    800: _shadeColor(color, 0.3),
    900: _shadeColor(color, 0.4),
  });
}

int _tintValue(int value, double factor) =>
    max(0, min((value + ((255 - value) * factor)).round(), 255));

Color _tintColor(Color color, double factor) => Color.fromRGBO(
    _tintValue(color.red, factor),
    _tintValue(color.green, factor),
    _tintValue(color.blue, factor),
    1);

int _shadeValue(int value, double factor) =>
    max(0, min(value - (value * factor).round(), 255));

Color _shadeColor(Color color, double factor) => Color.fromRGBO(
    _shadeValue(color.red, factor),
    _shadeValue(color.green, factor),
    _shadeValue(color.blue, factor),
    1);