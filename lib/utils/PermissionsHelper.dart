import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:tutor/focused.dart';

class PermissionsHelper with TranslatableMixin {
  static PermissionsHelper _instance;
  PermissionsHelper._();
  factory PermissionsHelper() => _instance ?? PermissionsHelper._();

  Future<bool> checkCameraPermission(BuildContext context) =>
      checkPermission(context, Permission.camera,
          translate(Translations.need_camera_permission));

  Future<bool> checkMicPermission(BuildContext context) =>
      checkPermission(context, Permission.microphone,
          translate(Translations.need_mic_permission));

  Future<bool> checkStoragePermission(BuildContext context) =>
      checkPermission(context, Permission.storage,
          translate(Translations.need_storage_permission));

  Future<bool> checkPhotosPermission(BuildContext context) =>
      checkPermission(context, Permission.photos,
          translate(Translations.media_access_permission));

  Future<bool> checkReadPhotosPermission(BuildContext context) =>
      checkPermission(context, Permission.photosAddOnly,
          translate(Translations.media_access_permission));

  Future<bool> checkPermission(BuildContext context, Permission permission, String text) async {
    var status = await permission.status;
    switch (status) {
      case PermissionStatus.limited:
      case PermissionStatus.granted:
        return true;
      case PermissionStatus.denied:
        status = await permission.request();
        Log.error('Request ${permission.toString()}, result: $status');
        if (!status.isGranted) {
          showUserSettings(context, text);
        }
        return status.isGranted;
      case PermissionStatus.permanentlyDenied:
        showUserSettings(context, text);
        return false;
      case PermissionStatus.restricted:
        Log.error('${permission.toString()} is restricted');
        return false;
      default:
        return false;
    }
  }

  Future<bool> showUserSettings(BuildContext context, String text) async {
    await showAlertDialog(translate(Translations.warning), text);
    await openAppSettings();
    return false;
  }
}