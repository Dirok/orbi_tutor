import 'package:device_info_plus/device_info_plus.dart';
import 'package:get/get_utils/src/platform/platform.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:tutor/focused.dart';
import 'package:version/version.dart';

class PlatformUtils {
  static SoftwareEnvironment _environment;
  static PackageInfo _packageInfo;

  static SoftwareEnvironment get environment => _environment;

  static Future init() async {
    _packageInfo = await PackageInfo.fromPlatform();
    _environment ??= await _getEnvironment();
  }

  static Future<SoftwareEnvironment> _getEnvironment() async {
    final deviceInfo = await _getDeviceInfo();

    return SoftwareEnvironment(
        getFullVersion(),
        deviceInfo[0],
        deviceInfo[1]);
  }

  static Future<List<String>> _getDeviceInfo() async => GetPlatform.isAndroid
      ? _getAndroidDeviceInfo()
      : GetPlatform.isIOS
          ? _getIosDeviceInfo()
          : _getUnknownInfo();

  static Future<List<String>> _getAndroidDeviceInfo() async {
    final deviceInfo = DeviceInfoPlugin();
    final androidInfo = await deviceInfo.androidInfo;
    return [
      "${androidInfo.model}",
      "v ${androidInfo.version.release} sdk: ${androidInfo.version.sdkInt}"
    ];
  }

  static Future<List<String>> _getIosDeviceInfo() async {
    final deviceInfo = DeviceInfoPlugin();
    final iosInfo = await deviceInfo.iosInfo;
    return [
      "${iosInfo.utsname.machine}",
      "${iosInfo.systemName} ${iosInfo.systemVersion}"
    ];
  }

  static Future<List<String>> _getUnknownInfo() async =>
      ["Unknown machine", "Unknown OS"];

  static String getFullVersion() =>
      '${_packageInfo.version}.${_packageInfo.buildNumber}';

  static String getVersion() => _packageInfo.version;

  static bool isAppVersionLatest(String input) {
    final appVersion = Version.parse(getVersion());
    final configVersion = Version.parse(input);

    return appVersion >= configVersion;
  }

  static int getBuildNumber() => int.tryParse(_packageInfo.buildNumber);
}
