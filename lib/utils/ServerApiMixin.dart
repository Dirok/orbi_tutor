import 'dart:async';

import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

mixin ServerApiMixin on EventMixin {
  Future<bool> Function() refreshTokenFunc = () => Future.value(false);

  Future<ServerResult<TParseResult>> checkRequest<TRequestResult, TParseResult>(
      Future<TRequestResult> Function() createRequestFunc,
      TParseResult Function(TRequestResult) parseFunc,
      {Duration timeout = Settings.httpsRequestTimeout,
      bool shouldSendEvents = true}) async {
    ServerResult<TParseResult> requestResult;

    requestResult =
        await _internalCheckRequest(createRequestFunc, parseFunc, timeout, shouldSendEvents);

    if (requestResult.isTokenExpired) {
      Log.info('making second request after token request');
      final isTokenRefreshed = await refreshTokenFunc();

      if (isTokenRefreshed)
        requestResult =
            await _internalCheckRequest(createRequestFunc, parseFunc, timeout, shouldSendEvents);
      else
        sendEvent(UnauthorizedError());
    }

    return requestResult;
  }

  Future<ServerResult<TParseResult>> _internalCheckRequest<TRequestResult, TParseResult>(
      Future<TRequestResult> Function() createRequestFunc,
      TParseResult Function(TRequestResult) parseFunc,
      Duration timeout,
      bool shouldSendEvents) async {
    TRequestResult requestResult;

    try {
      requestResult = await createRequestFunc().timeout(timeout);
    } on TimeoutException catch (e) {
      Log.error('Request timeout expired', exception: e);
      if (shouldSendEvents)
        sendEvent(TimeoutError());
      return ServerResult.timeout();
    } on ServerCodeException catch (e) {
      Log.error(e.toString());
      switch (e.errorCode) {
        case 401:
          sendEvent(UnauthorizedError());
          break;
        case 403:
          return ServerResult.tokenExpired();
          break;
        case 400:
        case 404:
        case 500:
        case 502:
          if (shouldSendEvents)
            sendEvent(ServerError());
          break;
      }
      return ServerResult.requestError();
    } catch (e) {
      if (!(e is SocketException)) {
        if (shouldSendEvents)
          sendEvent(RequestError());
      }
      Log.error('Request exception', exception: e);
      return ServerResult.requestError();
    }

    if (requestResult == null) {
      Log.warning('Request failed');
      return ServerResult.fail();
    }

    try {
      final parseResult = parseFunc?.call(requestResult);
      return ServerResult.success(parseResult);
    } catch (e) {
      Log.error('Parse result exception', exception: e);
      if (shouldSendEvents)
        sendEvent(ParseError());
      return ServerResult.parseError();
    }
  }
}
