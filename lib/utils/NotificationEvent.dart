import 'package:flutter/foundation.dart';

enum NotificationEvent { mute, unmute, none }

class NotificationEventHelper {
  static final Map<String, NotificationEvent> _strMap = Map<String, NotificationEvent>.fromIterable(
      NotificationEvent.values,
      key: (v) => describeEnum(v),
      value: (v) => v);

  static NotificationEvent fromString(String value) =>
      _strMap.containsKey(value) ? _strMap[value] : NotificationEvent.none;
}

extension NotificationEventExtension on NotificationEvent {
  String get asString => describeEnum(this);
}
