import 'package:flutter/foundation.dart';

enum NotificationType { none, openLecture }

class NotificationTypeHelper {
  static final Map<String, NotificationType> _strMap = Map<String, NotificationType>.fromIterable(
      NotificationType.values,
      key: (v) => describeEnum(v),
      value: (v) => v);

  static NotificationType fromString(String value) =>
      _strMap.containsKey(value) ? _strMap[value] : NotificationType.none;
}

extension NotificationTypeExtension on NotificationType {
  String get asString => describeEnum(this);
}
