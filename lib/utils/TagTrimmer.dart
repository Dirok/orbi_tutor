import 'package:tutor/focused.dart';

class TagTrimmer {
  static String trimTagBySpace(String fullString) {
    final spacePattern = ' ';
    return fullString.contains(spacePattern)
        ? fullString.substring(0, fullString.indexOf(spacePattern))
        : fullString;
  }

  static String trimTagByLength(String fullString) {
    if (fullString.length <= UIConstants.maxTagTextSize)
      return fullString;

    final trimmed = fullString.substring(0, UIConstants.maxTagTextSize);
    return '$trimmed...';
  }
}