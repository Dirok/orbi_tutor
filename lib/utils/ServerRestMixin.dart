import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tutor/focused.dart';

mixin ServerRestMixin {
  dynamic parseResponse(http.Response response, String methodName,
      {parseJson = true, showLogs = true}) {
    var responseBody;
    if (parseJson)
      responseBody = json.decode(utf8.decode(response.bodyBytes));
    else
      responseBody = response.bodyBytes;

    if (response.statusCode == 200) {
      if (showLogs) {
        Log.info('$methodName success: ${parseJson ? responseBody['message'] : 'ok'}');
      }

      return responseBody;
    } else {
      throw ServerCodeException(methodName, response.statusCode,
          parseJson ? responseBody['message'] : 'failed to download');
    }
  }

  dynamic parseStreamedResponse(http.StreamedResponse response, String methodName) {
    final responseBody = response.stream;

    if (response.statusCode == 200) {
      Log.info('$methodName success: ok');
      return responseBody;
    } else {
      throw ServerCodeException(methodName, response.statusCode, 'failed to download');
    }
  }
}
