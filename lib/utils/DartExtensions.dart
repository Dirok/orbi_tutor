import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

extension ListOfSubscriptionsEx on List<StreamSubscription> {
  void disposeSubscriptions() => this.forEach((s) => s?.cancel());
}

extension ListOfWorkersEx on List<Worker> {
  void disposeWorkers() => this.forEach((w) => w.dispose());
}

extension StreamSubscriptionEx on StreamSubscription {
  void addTo(List<StreamSubscription> list) => list.add(this);
}

extension IterableEx<T> on Iterable<T> {
  void addTo(List<T> list) => list.addAll(this);

  Map<K,V> toMap<K, V>(K Function(T) keyFunc, V Function(T) valueFunc) {
    final map = <K, V>{};
    this.forEach((e) => map[keyFunc(e)] = valueFunc(e));
    return map;
  }
}

extension MapEx<K, V> on Map<K, V> {
  List<T> toList<T>(T Function(K, V) func) {
    final list = <T>[];
    this.forEach((key, value) => list.add(func(key, value)));
    return list;
  }
}

extension StringEx on String {
  /// Remove transparent space character so text will overflow normally
  String get overflow => Characters(this)
      .replaceAll(Characters(''), Characters('\u{200B}'))
      .toString();
}