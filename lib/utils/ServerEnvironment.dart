import 'dart:convert';

import 'package:tutor/focused.dart';

class ServerEnvironment {
  static const String _releaseName = 'release';
  static const String _customName = 'custom';

  final String name;
  final String url;

  bool get isRelease => name.contains(_releaseName);
  bool get isDebug => !isRelease;
  bool get isCustom => name == _customName;

  ServerEnvironment(this.name, [this.url]);

  factory ServerEnvironment.release() {
    return ServerEnvironment(_releaseName, Servers.release);
  }

  factory ServerEnvironment.custom([String url]) {
    return ServerEnvironment(_customName, url);
  }

  factory ServerEnvironment.fromString(String source) {
    final serverMap = Map.from(json.decode(source));
    final name = serverMap['name'] as String;
    final url = serverMap['url'] as String;
    if (name == null || url == null || name.isEmpty || url.isEmpty)
      return ServerEnvironment.release();
    return ServerEnvironment(name, url);
  }

  @override
  String toString() {
    Map<String, String> that = {'name': name, 'url': url};
    return json.encode(that);
  }
}
