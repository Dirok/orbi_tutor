class MathUtils {
  static double remapValues(
      double value,
      double originalMinValue,
      double originalMaxValue,
      double translatedMinValue,
      double translatedMaxValue) {
    if (originalMaxValue - originalMinValue == 0) return 0;

    return (value - originalMinValue) /
           (originalMaxValue - originalMinValue) *
           (translatedMaxValue - translatedMinValue) + translatedMinValue;
  }

  /// Returns value from 0 to 100
  static int interpretPercentage(int currentValue, int maxValue) {
    return (currentValue / maxValue * 100).round();
  }

  /// Convert range 0..127 to 0..1 but inverted
  static double convertVolumeRange(double value) {
    value = 1 - value / 127.0;
    if (value < 0.2) value = 0.2;
    return value;
  }
}