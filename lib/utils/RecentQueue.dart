import 'dart:collection';

import 'package:tutor/focused.dart';

class RecentQueue<T> extends DoubleLinkedQueue<T> {

  final int maxSize;

  RecentQueue(this.maxSize);

  @override
  void add(T value) {
    _controlDuplicates(value);
    super.addFirst(value);
    _controlFixSize();
  }

  @override
  void addAll(Iterable<T> values) {
    values.forEach((v) {
      _controlDuplicates(v);
      super.addFirst(v);
    });

    _controlFixSize();
  }

  @override
  void addFirst(T value) {
    _controlDuplicates(value);
    super.addFirst(value);
    _controlFixSize();
  }

  @override
  void addLast(T value) {
    _controlDuplicates(value);

    Log.warning('Be careful, adding element could be deleted after size controlling, use addFirst instead');
    super.addLast(value);
    _controlFixSize();
  }

  void _controlDuplicates(T value) {
    super.removeWhere((element) => element == value);
  }

  void _controlFixSize() {
    while (super.length > maxSize) {
      super.removeLast();
    }
  }
}