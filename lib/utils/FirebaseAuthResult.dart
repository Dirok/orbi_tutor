import 'package:tutor/focused.dart';

class FirebaseAuthResult<T> {
  final FirebaseAuthResultStatus status;
  final T payload;

  bool get isSuccess => status == FirebaseAuthResultStatus.success;
  bool get isFail => status == FirebaseAuthResultStatus.fail;
  bool get isAccountAlreadyExist => status == FirebaseAuthResultStatus.accountAlreadyExist;
  bool get isOperationNotAllowed => status == FirebaseAuthResultStatus.operationNotAllowed;
  bool get isWeakPassword => status == FirebaseAuthResultStatus.weakPassword;

  FirebaseAuthResult.success(this.payload)
      : this.status = FirebaseAuthResultStatus.success;

  FirebaseAuthResult.fail()
      : this.status = FirebaseAuthResultStatus.fail,
        this.payload = null;

  FirebaseAuthResult.accountAlreadyExist()
      : this.status = FirebaseAuthResultStatus.accountAlreadyExist,
        this.payload = null;

  FirebaseAuthResult.operationNotAllowed()
      : this.status = FirebaseAuthResultStatus.operationNotAllowed,
        this.payload = null;

  FirebaseAuthResult.weakPassword()
      : this.status = FirebaseAuthResultStatus.weakPassword,
        this.payload = null;
}

enum FirebaseAuthResultStatus {
  success,
  fail,
  accountAlreadyExist,
  operationNotAllowed,
  weakPassword,
}

class FailError extends Event {}
class AccountAlreadyExist extends Event {}
class OperationNotAllowed extends Event {}
class WeakPassword extends Event {}
