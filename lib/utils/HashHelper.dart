import 'package:md5_plugin/md5_plugin.dart';
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

class HashHelper {
  static Future<String> calculateMd5(String filePath) async {
    var hash = '';
    final file = File(filePath);
    if (await file.exists()) {
      try {
        hash = await Md5Plugin.getMD5WithPath(filePath);
      } catch (exception) {
        Log.error('Unable to evaluate the MD5 sum :$exception');
      }
    } else {
      Log.error('$filePath does not exits so unable to evaluate its MD5 sum.');
    }
    return hash;
  }
}
