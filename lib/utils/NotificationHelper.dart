import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class NotificationHelper {
  static void handleNotificationAction(NotificationModel notification, OpenDestination type) {
    final _localization = Settings.localization;
    if (notification.formattedTitle(_localization) == null ||
        notification.formattedBody(_localization) == null) return;

    final _push = Get.find<PushService>();
    switch (notification.type) {
      case NotificationType.none:
        break;
      case NotificationType.openLecture:
        Log.info('Opened app from $type state and going to the lecture ${notification.lectureId}');
        _push.sendEvent(NavigateToLectureEvent(notification.lectureId));
        break;
    }
  }
}
