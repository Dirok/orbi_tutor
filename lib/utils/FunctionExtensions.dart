import 'package:get/get.dart';

extension AsyncFunctionExt<R> on Future<R> Function() {
  Future<R> callWithLock(RxBool lock, {bool unlockedOnly = false, R defaultValue}) async {
    if (unlockedOnly && lock.value) return defaultValue;

    lock.value = true;
    final result = await this();
    lock.value = false;
    return result;
  }
}

extension FunctionExt<R> on R Function() {
  R callWithLock(RxBool lock, {bool unlockedOnly = false, R defaultValue}) {
    if (unlockedOnly && lock.value) return defaultValue;

    lock.value = true;
    final result = this();
    lock.value = false;
    return result;
  }
}
