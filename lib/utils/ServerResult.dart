import 'package:tutor/focused.dart';

class ServerResult<T> {
  final ServerResultStatus status;
  final T payload;

  bool get isSuccess => status == ServerResultStatus.success;
  bool get isFail => status == ServerResultStatus.fail;
  bool get isTimeout => status == ServerResultStatus.timeout;
  bool get isRequestError => status == ServerResultStatus.requestError;
  bool get isParseError => status == ServerResultStatus.parseError;
  bool get isTokenExpired => status == ServerResultStatus.tokenExpired;

  ServerResult.success(this.payload) : this.status = ServerResultStatus.success;

  ServerResult.fail()
      : this.status = ServerResultStatus.fail,
        this.payload = null;

  ServerResult.timeout()
      : this.status = ServerResultStatus.timeout,
        this.payload = null;

  ServerResult.requestError()
      : this.status = ServerResultStatus.requestError,
        this.payload = null;

  ServerResult.parseError()
      : this.status = ServerResultStatus.parseError,
        this.payload = null;

  ServerResult.tokenExpired()
      : this.status = ServerResultStatus.tokenExpired,
        this.payload = null;
}

enum ServerResultStatus {
  success,
  fail,
  timeout,
  requestError,
  parseError,
  tokenExpired,
}

class TimeoutError extends Event {}
class RequestError extends Event {}
class ParseError extends Event {}
class UnauthorizedError extends Event {}
class ServerError extends Event {}

class TokenRefreshEvent extends Event {
  final TokenBundle bundle;

  TokenRefreshEvent(this.bundle);
}