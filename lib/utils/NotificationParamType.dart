import 'package:flutter/foundation.dart';

enum NotificationParam { Integer, String, DateTime }

class NotificationParamHelper {
  static final Map<String, NotificationParam> _strMap = Map<String, NotificationParam>.fromIterable(
      NotificationParam.values,
      key: (v) => describeEnum(v),
      value: (v) => v);

  static NotificationParam fromString(String value) =>
      _strMap.containsKey(value) ? _strMap[value] : NotificationParam.String;
}

extension NotificationParamExtension on NotificationParam {
  String get asString => describeEnum(this);
}
