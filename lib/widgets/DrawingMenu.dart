import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class DrawingMenu {
  static const List<Color> _colors = [
    Colors.red,
    Colors.green,
    Colors.blue,
    Colors.amber,
    Colors.black
  ];

  static const double _minStrokeWidthToScreenSizeFactor = 400.0;

  static const List<int> _strokeWidthsScaleFactors = [1, 2, 4, 8, 16, 24];
  static List<double> _strokeWidths;

  Color _pickerColor = _colors[0];

  DrawingViewModel _drawingController;

  bool isInited = false;

  init(BuildContext context, DrawingViewModel drawingController) {
    _drawingController = drawingController;
    _strokeWidths = _calcStrokeWidths(context, _strokeWidthsScaleFactors);
    _drawingController.color.value ??= _pickerColor;
    _drawingController.strokeWidth.value ??= _strokeWidths[0];
    isInited = true;
  }

  static List<double> _calcStrokeWidths(BuildContext context, List<int> strokeWidthScaleFactors) {
    var minStrokeWidth = context.height / _minStrokeWidthToScreenSizeFactor;
    return strokeWidthScaleFactors.map((f) => f * minStrokeWidth).toList();
  }

  Future<void> show(context, DrawingMenuType mode) async {
    Get.bottomSheet(Padding(
        padding: EdgeInsets.all(8.0),
        child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: mode == DrawingMenuType.Color
            ? _getColorList()
            : _getStrokeWidthList())),
    backgroundColor: Colors.black45);
  }

  _getColorList() {
    var colorList = _colors.map((c) => _colorCircle(c)).toList();
    colorList.add(_getColorPicker());
    return colorList;
  }

  Widget _colorCircle(Color color) {
    return GestureDetector(
      onTap: () {
        _drawingController.setColor(color);
        _pickerColor = color;
        Get.back();
      },
      child: ClipOval(
        child: Container(
          height: 60,
          width: 60,
          color: _drawingController.color.value != color ? Colors.transparent : Colors.black12,
          child: Center(
            child: ClipOval(
              child: Container(
                height: 36,
                width: 36,
                color: color,
              ),
            ),
          ),
        ),
      ),
    );
  }

  _getColorPicker() {
    return GestureDetector(
      onTap: () {
        Get.defaultDialog(content: SingleChildScrollView(
              child: ColorPicker(
                pickerColor: _drawingController.color.value,
                onColorChanged: (color) => _pickerColor = color,
                showLabel: true,
                pickerAreaHeightPercent: 0.8,
              ),
            ),
            actions: [
              TextButton(
                child: const Text('Save'),
                onPressed: () {
                  _drawingController.setColor(_pickerColor);
                  Get.back();
                  Get.back();
                },
              ),
            ],
        );
      },
      child: ClipOval(
        child: Container(
          height: 60,
          width: 60,
          color: _colors.contains(_drawingController.color.value) ? Colors.transparent : Colors.white10,
          child: Center(
            child: ClipOval(
              child: Container(
                padding: const EdgeInsets.only(bottom: 16.0),
                height: 36,
                width: 36,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [Colors.red, Colors.green, Colors.blue],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    )),
              ),
            ),
          ),
        ),
      ),
    );
  }

  _getStrokeWidthList() =>
    _strokeWidths.map((sw) => _strokeWidthCircle(sw)).toList();

  Widget _strokeWidthCircle(double strokeWidth) =>
    GestureDetector(
      onTap: () {
        _drawingController.setStrokeWidth(strokeWidth);
        Get.back();
      },
      child: ClipOval(
        child: Container(
          height: 60,
          width: 60,
          color:
          _drawingController.strokeWidth.value != strokeWidth ? Colors.transparent : Colors.black12,
          child: Center(
            child: ClipOval(
              child: Container(
                height: strokeWidth,
                width: strokeWidth,
                color: _drawingController.color.value,
              ),
            ),
          ),
        ),
      ),
    );
}

enum DrawingMenuType { StrokeWidth, Color }