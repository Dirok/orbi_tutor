import 'dart:collection';
import 'dart:math';
import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class DrawingWidget extends StatefulWidget {
  final Rx<Whiteboard> whiteboardObs;
  final DrawingViewModel controller;
  final ILectureServerApi lectureServer;
  final Size surfaceSize;

  const DrawingWidget(this.whiteboardObs, this.controller, this.lectureServer, this.surfaceSize);

  @override
  _DrawingWidgetState createState() => _DrawingWidgetState();
}

class _DrawingWidgetState extends State<DrawingWidget> {
  static const double _minPointsDistance = 10.0;

  final GlobalKey _painterKey = GlobalKey();

  final _userId = appSession.account.id;

  Rx<Whiteboard> _whiteboardObs;

  ILectureServerApi _lectureServer;

  DrawingViewModel _drawingController;

  Size _surfaceSize;

  Whiteboard get _whiteboard => _whiteboardObs.value;

  var _lastPosition = Offset.zero;

  HashSet<int> _pointers = HashSet();
  int _lastDrawingPointer = -1;

  DrawingData _lastDrawingData;

  @override
  void initState() {
    super.initState();

    _lectureServer = widget.lectureServer;
    _drawingController = widget.controller;
    _whiteboardObs = widget.whiteboardObs;
    _surfaceSize = widget.surfaceSize;

    selfSurfaceSize = ScreenSize.fromSize(_surfaceSize);
  }

  @override
  Widget build(BuildContext context) {
    return Listener(
        onPointerDown: onPointerDown,
        onPointerMove: onPointerMove,
        onPointerUp: onPointerEnd,
        onPointerCancel: onPointerEnd,
        child: Obx(() => IgnorePointer(ignoring: !_drawingController.drawingEnabled.value,
            child: Container( // for fullscreen GestureDetector
                width: double.infinity,
                height: double.infinity,
                color: Colors.transparent,
                child: Center(
                  child: ClipRect(
                    child: Obx(() => CustomPaint(
                        key: _painterKey,
                        size: _surfaceSize,
                        painter: DrawingPainter(_whiteboard?.drawingSet),
                    ))
                  )
                )
            )
        ))
    );
  }

  void onPointerDown(PointerEvent details) {
    _pointers.add(details.pointer);
    onPointerMove(details);
  }

  void onPointerMove(PointerEvent details) {
    if (Settings.isStylusModeEnabled
        && details.kind != PointerDeviceKind.stylus) return;

    if (_pointers.length != 1) return;

    if (_lastDrawingPointer != details.pointer) {
      _lastDrawingPointer = details.pointer;
      _createPath();
    }

    if (details.delta == Offset.zero) return;

    final dx = details.position.dx - _lastPosition.dx;
    final dy = details.position.dy - _lastPosition.dy;
    final distance = sqrt(dx * dx + dy * dy);
    if (distance < _minPointsDistance) return;
    _lastPosition = details.position;

    _addPoint(details.position);
  }

  void onPointerEnd(PointerEvent details) {
    _pointers.remove(details.pointer);
    _lastDrawingPointer = -1;

    if(_lectureServer.isConnected)
      _lectureServer.addDrawingPath(_whiteboard.id, _lastDrawingData);
  }

  _createPath() {
    var drawingData = DrawingData(
        _drawingController.color.value, _drawingController.strokeWidth.value, selfSurfaceSize);

    _lastPosition = Offset.zero;

    _whiteboard.addDrawingPath(drawingData, _userId);

    _lastDrawingData = drawingData;

    if (_lectureServer.isConnected)
      _lectureServer.createDrawingPath(_whiteboard.id, drawingData);
  }

  _addPoint(Offset globalPosition) {
    RenderBox renderBox = _painterKey.currentContext.findRenderObject();
    var localPosition = renderBox.globalToLocal(globalPosition);

    var point = DrawingPoint.fromOffset(localPosition);

    var drawingId = _whiteboard.addDrawingPoint(point, _userId);
    _whiteboardObs.refresh();

    if(_lectureServer.isConnected)
      _lectureServer.addDrawingPoint(_whiteboard.id, point, drawingId);
  }
}

class DrawingPainter extends CustomPainter {
  final DrawingSet _drawingSet;

  DrawingPainter(this._drawingSet);

  @override
  void paint(Canvas canvas, Size size) {
    if (_drawingSet == null) return;
    for (var drawingData in _drawingSet.serialDrawingData) {
      if (drawingData.path != null)
        canvas.drawPath(drawingData.path, drawingData.paint);
    }
  }

  @override
  bool shouldRepaint(DrawingPainter oldDelegate) => true;
}