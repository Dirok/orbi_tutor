import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class DoubleBackExitMixin with TranslatableMixin {
  DateTime _currentBackPressTime;

  Future<bool> onBackPressed() {
    var now = DateTime.now();
    if (_currentBackPressTime == null ||
        now.difference(_currentBackPressTime) > Duration(seconds: 2)) {
      _currentBackPressTime = now;
      _showExitSnackBar();
      return Future.value(false);
    }
    return Future.value(true);
  }

  _showExitSnackBar() {
    ScaffoldMessenger.of(Get.context).showSnackBar(
      SnackBar(
          content: Text(translate(Translations.double_back_exit)),
          duration: Duration(seconds: 2)
      ),
    );
  }
}