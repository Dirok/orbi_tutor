import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';
import 'package:url_launcher/url_launcher.dart';

class FocusedApp extends GetMaterialApp with TranslatableMixin, WidgetsBindingObserver {
  static TransitionBuilder _scaledBuilder = (context, child) => MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: child);

  final _vm = FocusedAppViewModel();

  final _eventHandlers = <Type, Function(Event)>{};

  FocusedApp() : super(
      builder: _scaledBuilder,
      debugShowCheckedModeBanner: false,
      onGenerateTitle: (BuildContext context) {
        return 'focusED now';
      },
      theme: _themeData,
      initialRoute: Routes.login,
      getPages: Pages.list,
      supportedLocales: _supportedLocales,
      localizationsDelegates: _localizationDelegates,
      localeResolutionCallback: _localeResolutionCallback) {
    _eventHandlers.addAll({
      InvalidLinkEvent: (e) => _showInvalidLinkDialog(),
      OpenLinkErrorEvent: (e) => _showOpenLinkFailedDialog(),
      LinkAuthorizationSuccessEvent: (e) => _openStudentAuthPage(),
      LinkAuthorizationFailedEvent: (e) => _openStudentAuthInfo(),
      NoSessionEvent: (e) => _showSignInOrAnonymouslyDialog(),
      JoinLectureFailedEvent: (e) => _showJoinLectureFailedDialog(),
      SignInWithKundelikEvent: (e) => _onSignInWithKundelikEvent(),
      ChangeLectureRequestEvent: (e) => _onChangeLectureEvent(e),
      LectureNotExistsEvent: (_) => _showLectureNotExistsDialog(),
      LectureNotAvailableEvent: (_) => _showLectureNotAvailableDialog(),
      LectureInactiveEvent: (_) => _onLectureInactiveEvent(),
      ShowSnackEvent: (e) => _onShowSnackEvent(e),
      OpenScheduledLecturesEvent: (e) => _openScheduledLecturePage(),
      OpenPassedLecturesEvent: (e) => _openPassedLecturePage(),
      OpenGroupLectureEvent: (args) => _onOpenGroupLectureEvent(args),
      OpenPersonalLectureEvent: (args) => _onOpenPersonalLectureEvent(args),
      AutoLogInEvent: (e) => _openMainMenuPage(),
      ServerTimeoutError: (e) => _showServerTimeoutDialog(),
      ServerRequestError: (e) => _showServerRequestErrorDialog(),
      ServerInvalidResultError: (e) => _showServerInvalidResultDialog(),
      ServerUnauthorizedError: (e) => _unauthorizedErrorHandler(),
      ServerCommonError: (e) => _showServerCommonErrorDialog(),
      ShowChangelogEvent: (e) => _openChangelogPage(),
      UpdateNeededEvent: (e) => _openUpdateNeededPage(),
      DeAuthorizeUserEvent: (e) => _deAuthorizeUserHandler(),
      ShowFetchingSubjectsErrorEvent: (e) => _showFetchingSubjectErrorDialog(),
      ShowFetchingUserContactsErrorEvent: (e) => _showFetchingContactsErrorDialog(),
    });
    _vm.listenEvents((e) => _eventHandlers[e.runtimeType]?.call(e));
  }

  @override
  get onReady => () async {
    // HACK wait for creating context
    while (Get.context == null)
      await Future.delayed(Duration(milliseconds: 100));

    return _vm.init();
  };

  Future<void> _showInvalidLinkDialog() async {
    await showAlertDialog(translate(Translations.error),
          translate(Translations.invalid_link));

    return _vm.callPostAuthAction(isSuccess: true);
  }

  Future<void> _showOpenLinkFailedDialog() => showAlertDialog(
      translate(Translations.error),
      translate(Translations.cant_open_link) +
          ' ' +
          translate(Translations.no_internet_connection));

  Future<void> _openStudentAuthPage() => Get.offAllNamed(Routes.studentAuth);
  Future<void> _openStudentAuthInfo() => Get.offAllNamed(Routes.studentAuthInfo);
  Future<void> _openMainMenuPage() => Get.offAllNamed(Routes.mainMenu);

  Future<void> _onShowSnackEvent(ShowSnackEvent event) async {
    final _localization = Settings.localization;
    showSnackbar(event.
        notification.formattedTitle(_localization), event.notification.formattedBody(_localization), (_) {
      Get.back();
      NotificationHelper.handleNotificationAction(event.notification, OpenDestination.Foreground);
    });
  }

  Future<void> _onLectureInactiveEvent() => showAlertDialog(
      translate(Translations.error), translate(Translations.lecture_is_over));

  Future<void> _onChangeLectureEvent(ChangeLectureRequestEvent event) =>
      showAlertDialog(
          translate(Translations.warning), translate(Translations.change_lecture),
          variants: {Translations.yes: () => _vm.openLecture(event.id), Translations.no: null});

  Future<void> _openChangelogPage() async {
    String bodyText;
    try {
      bodyText = await rootBundle.loadString('assets/translations/notes.txt');
    } catch (e) {
      Log.error('Error while opening release notes', exception: e);
      Settings.lastShownChangelog = PlatformUtils.getBuildNumber();
      return;
    }
    await Get.toNamed(Routes.notification, arguments: {
      'title': translate(Translations.whats_new),
      'body': bodyText,
      'button': translate(Translations.get_started),
      'action': () {
        Settings.lastShownChangelog = PlatformUtils.getBuildNumber();
        Get.back();
      },
    });
  }

  Future<void> _openUpdateNeededPage() async {
    _vm.clearSubscriptions();
    await Get.offAllNamed(Routes.notification, arguments: {
      'title': translate(Translations.we_got_better),
      'body': translate(Translations.new_version_available),
      'button': translate(Translations.update),
      'action': () async {
        try {
          final url = GetPlatform.isAndroid ? Constants.androidStoreUrl : Constants.iosStoreUrl;
          launch(url);
        } catch (e) {
          Log.error('Cannot launch update url to market', exception: e);
        }
      },
    });
  }

  Future<void> _showAutoLogInFailDialog() =>
      showAlertDialog(translate(Translations.error),
          translate(Translations.auto_login_fail));

  Future<void> _showSignInOrAnonymouslyDialog() async {
    final isSuccess = await Get.toNamed(Routes.wantToSignIn);
    if (isSuccess == null || !isSuccess)
      return _showSignInFailDialog();

    _openMainMenuPage();

    return _vm.callPostAuthAction(isSuccess: isSuccess);
  }

  Future<void> _showJoinLectureFailedDialog() =>
      showAlertDialog(translate(Translations.error),
          translate(Translations.cant_join_lecture));

  Future<void> _showSignInFailDialog() async {
    await _vm.callPostAuthAction(isSuccess: false);
    return showAlertDialog(translate(Translations.error),
        translate(Translations.not_signed_in));
  }

  Future<void> _onSignInWithKundelikEvent() async {
    final isSuccess = await Get.toNamed(Routes.kundelikAuth);

    if (isSuccess)
      _openMainMenuPage();

    return _vm.callPostAuthAction(isSuccess: isSuccess);
  }

  Future<void> _showLectureNotExistsDialog() =>
      showAlertDialog(translate(Translations.error),
          translate(Translations.lecture_not_exist));

  Future<void> _showLectureNotAvailableDialog() =>
      showAlertDialog(translate(Translations.error),
          translate(Translations.lecture_not_available));

  void _openScheduledLecturePage() {
    _openMainMenuPage();
    Get.toNamed(Routes.scheduledLectures);
    showAlertDialog(translate(Translations.warning),
        translate(Translations.lecture_not_started));
  }

  void _openPassedLecturePage() {
    _openMainMenuPage();
    Get.toNamed(Routes.passedLectures);
    showAlertDialog(translate(Translations.warning),
        translate(Translations.lecture_is_over));
  }

  void _onOpenGroupLectureEvent(OpenGroupLectureEvent e) => _openLecturePage(
      e.isOwner ? Routes.teacherActiveGroupLecture : Routes.studentActiveGroupLecture);

  void _onOpenPersonalLectureEvent(OpenPersonalLectureEvent e) =>
      _openLecturePage(Routes.personalLecture);

  void _openLecturePage(String route) async {
    _openMainMenuPage();
    Get.toNamed(Routes.scheduledLectures);
    Get.toNamed(route);
  }

  Future<void> _showServerTimeoutDialog() => showAlertDialog(
      translate(Translations.failure), translate(Translations.no_main_server_connection));

  Future<void> _showServerRequestErrorDialog() => showAlertDialog(
      translate(Translations.failure),
      translate(Translations.failed_to_load_data) + '.\n' +
          translate(Translations.please_try_later));

  Future<void> _showServerInvalidResultDialog() => showAlertDialog(
      translate(Translations.failure), translate(Translations.server_invalid_result));

  Future<void> _showDeauthorizeUserDialog(String text) => showAlertDialog(
      translate(Translations.error), translate(text));

  void _deAuthorizeUserHandler() =>
      _deauthorizeUser(deauthorizeText: Translations.have_logged_out);

  void _unauthorizedErrorHandler() =>
      _deauthorizeUser(deauthorizeText: Translations.have_not_used_for_while);

  Future<void> _deauthorizeUser({String deauthorizeText}) async {
    if (Get.currentRoute == Routes.login) return;
    await _showDeauthorizeUserDialog(deauthorizeText);
    await _vm.deAuthorizeUser();
    await Get.offAllNamed(Routes.login);
  }

  Future<void> _showServerCommonErrorDialog() => showAlertDialog(
      translate(Translations.failure), translate(Translations.server_error));

  Future<void> _showFetchingSubjectErrorDialog() =>
      showAlertDialog(
          translate(Translations.failure),
          '${translate(Translations.failed_to_update_subjects_list)}. '
              '${translate(Translations.please_try_later)}');

  Future<void> _showFetchingContactsErrorDialog() =>
      showAlertDialog(
        translate(Translations.failure),
        '${translate(Translations.failed_to_update_contacts_list)}. '
            '${translate(Translations.please_try_later)}');

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.detached) {
      _vm.close();
    }
  }

  @override
  Future<bool> didPopRoute() {
    // TODO DoubleBackExit here
    return Future<bool>.value(false);
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addObserver(this);

    return Stack(textDirection: TextDirection.ltr, children: [
      super.build(context),
      Obx(() => _vm.isAuthProcessed.value
          ? getNewProgressIndicator()
          : const SizedBox.shrink()),
      Obx(() => _vm.isConnectedToInternet.value || !_vm.isInited.value
          ? const SizedBox.shrink()
          : _getNoConnectionWidget())
    ]);
  }

  Widget _getNoConnectionWidget() =>
      Align(child: NoConnectionWidget(), alignment: Alignment.bottomCenter);
}

final _supportedLocales = [
  Locale('en', 'US'),
  Locale('ru', 'RU'),
  Locale('kk', 'KZ'),
];

final _themeData = ThemeData(
    brightness: Brightness.light,
    primaryColor: UIConstants.primaryLightColor,
    primarySwatch: tintConvert(UIConstants.primaryDarkColor),
    fontFamily: UIConstants.fontFamily);

final _localizationDelegates = [
  AppLocalizations.delegate,
  GlobalMaterialLocalizations.delegate,
  GlobalWidgetsLocalizations.delegate,
  GlobalCupertinoLocalizations.delegate,
];

Locale _localeResolutionCallback(Locale locale, Iterable<Locale> supportedLocales) {
  for (var supportedLocale in supportedLocales) {
    Log.info('locales: ' + supportedLocale.languageCode.toString());
    if (supportedLocale.languageCode == locale.languageCode) {
      // on ios country code can differ from language code
      //&& supportedLocale.countryCode == locale.countryCode) {
      Log.info('supported');
      return supportedLocale;
    }
    Log.info('not supported');
  }
  return supportedLocales.first;
}