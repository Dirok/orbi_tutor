import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class RegistrationUserNameWidget extends BaseAuthWidget {
  final _vm = Get.find<RegistrationUserNameViewModel>();

  @override
  BaseViewModel get vm => _vm;

  final _nameController = CustomTextEditController();
  final _surnameController = CustomTextEditController();

  final Map<Type, Function(Event)> _eventHandlers = Map();

  RegistrationUserNameWidget() {
    _eventHandlers.addAll({
      UpdateAccountSuccessEvent: (e) => Get.toNamed(Routes.registrationWelcome),
      UpdateAccountFailedEvent: (e) => _showUpdateUserFailedDialog(),
      EmptyFieldsErrorEvent: (e) => _showEmptyFieldsFailedDialog(),
    });
    vm.listenEvents((e) => _eventHandlers[e.runtimeType]?.call(e));
  }

  @override
  List<Widget> getBodyList() => [
    _getHeaderText(),
    const SizedBox(height: 32.0),
    Obx(() => _vm.isSigningIn.value
        ? getNewProgressIndicator()
        : _getUserNameBody()
    )];

  Widget _getHeaderText() => Center(
    child: Text(
      translate(Translations.fill_to_complete_registration),
      style: TextStyle(
        color: UIConstants.defaultTextColor,
        fontSize: UIConstants.appBarFontSize,
        fontFamily: UIConstants.fontFamily,
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.none
      )));

  Widget _getUserNameBody() => Column(
    mainAxisSize: MainAxisSize.min,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      _getNameForm(),
      const SizedBox(height: 4.0),
      _getSurnameForm(),
      const SizedBox(height: 4.0),
      _getButtons(),
    ]);

  Widget _getNameForm() => CustomTextEdit(
      text: _vm.name.value,
      hint: Translations.name,
      onTextChanged: (v) => _vm.name.value = v,
      onFieldSubmitted: (_) => _surnameController.requestFocus(),
      textInputAction: TextInputAction.next,
      controller: _nameController);

  Widget _getSurnameForm() => CustomTextEdit(
      text: _vm.surname.value,
      hint: Translations.surname,
      onTextChanged: (v) => _vm.surname.value = v,
      controller: _surnameController);

  Widget _getButtons() => Center(
    child: Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const SizedBox(width: 160.0),
        _getContinueButton(),
        _getSkipButton(),
      ]),
  );

  Widget _getContinueButton() => Center(
    child: Container(
      constraints: BoxConstraints.tightFor(width: 260.0, height: 48.0),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: UIConstants.primaryDarkColor,
          onPrimary: UIConstants.buttonTextColor,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(50.0))
          )),
        child: Text(translate(Translations.continue_).toUpperCase()),
        onPressed: _vm.updateUserAccount,
      )));

  Widget _getSkipButton() => Container(
    constraints: BoxConstraints.tightFor(width: 160.0, height: 48.0),
    child: TextButton(
      style: TextButton.styleFrom(visualDensity: VisualDensity.compact),
      child: Text(translate(Translations.skip),
          style: TextStyle(
              fontSize: UIConstants.buttonFontSize,
              color: UIConstants.primaryDarkColor)),
      onPressed: _vm.skipAccountUpdating,
    ),
  );

  Future<void> _showUpdateUserFailedDialog() =>
      showAlertDialog(translate(Translations.error),
          translate(Translations.failed_to_update_user_account));

  Future<void> _showEmptyFieldsFailedDialog() =>
      showAlertDialog(translate(Translations.error),
          translate(Translations.cant_update_account));
}