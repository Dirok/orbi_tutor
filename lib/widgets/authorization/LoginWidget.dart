import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class LoginWidget extends TranslatedStatelessWidget {
  final _vm = Get.find<LoginViewModel>();

  final _doubleBackExit = DoubleBackExitMixin();

  final _eventHandlers = <Type, Function(Event)>{};

  final _emailController = CustomTextEditController();
  final _passwordController = CustomTextEditController();

  // !!! Widget is created twice if app was opened by link
  LoginWidget() {
    _vm.clearSubscriptions();

    _eventHandlers.assignAll({
      AuthorizationSuccessEvent: (_) => _openLectureMenu(),
      AuthorizationSuccessWithoutCountryEvent: (e) => _openCountrySelection(),
      AuthorizationFailedEvent: (e) => _showAuthorizationFailedDialog(e),
      OutsideServiceAuthorizationFailedEvent: (_) => _showOutsideServiceAuthorizationFailedDialog(),
      CredentialsChanged: (_) => _onCredentialsChanged()
    });

    _vm.listenEvents((e) => _eventHandlers[e.runtimeType]?.call(e));
  }

  Future<void> _openLectureMenu() => Get.offAllNamed(Routes.mainMenu);

  Future<void> _openCountrySelection() => Get.offAllNamed(Routes.countrySelection);

  Future<void> _showAuthorizationFailedDialog(AuthorizationFailedEvent e) {
    final message = e.isWithCredentials ? Translations.bad_credentials : Translations.failed_to_authorization;
    return showAlertDialog(translate(Translations.error), translate(message));
  }

  Future<void> _showOutsideServiceAuthorizationFailedDialog() =>
      showAlertDialog(translate(Translations.error),
          translate(Translations.failed_authorization_in_outside_service));


  Future<void> _showFieldsValidationFailedDialog() =>
      showAlertDialog(translate(Translations.error),
          translate(Translations.enter_valid_credentials));

  void _onCredentialsChanged() {
    _emailController.setText(_vm.email.value);
    _passwordController.setText(_vm.password.value);
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
    onWillPop: _doubleBackExit.onBackPressed,
    child: Stack(
      alignment: AlignmentDirectional.bottomCenter,
      children: [
        Scaffold(
          extendBodyBehindAppBar: true,
          backgroundColor: UIConstants.primaryLightColor,
          body: SafeArea(
            child: Stack(
              alignment: AlignmentDirectional.bottomCenter,
              children: [
                Center(
                  child: ListView(
                    shrinkWrap: true,
                    padding: EdgeInsets.all(5.0),
                    children: [
                      _getHeaderText(),
                      const SizedBox(height: 8.0),
                      Obx(() => _vm.isSigningIn()
                        ? getNewProgressIndicator()
                        : _getAuthBody())
                    ]),
                  ),
                _getSendRequestButton(),
                _getVersionLabel(),
                Obx(() => _vm.currServerType.value.isRelease
                  ? const SizedBox.shrink()
                  : _getNotReleaseServerLabel()),
            ]),
          ),
        )
      ]));

  Widget _getAuthBody() => Get.context.isLandscape
      ? Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: _authWidgets,
        )
      : Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: _authWidgets,
        );

  List<Widget> get _authWidgets => [
        _getEmailEnterColumn(),
        const SizedBox(width: 32.0),
        Get.context.isLandscape ? _getColumnSeparator() : _getRowSeparator(),
        const SizedBox(width: 32.0),
        _getOtherEnterVariantsColumn(),
      ];

  _getHeaderText() => Center(
    child: Text(
      translate(Translations.welcome_to),
      style: TextStyle(
          color: UIConstants.defaultTextColor,
          fontSize: UIConstants.appBarFontSize,
          fontFamily: UIConstants.fontFamily,
          fontWeight: FontWeight.w400,
          decoration: TextDecoration.none
      )));

  Widget _getEmailEnterColumn() => Column(
    mainAxisSize: MainAxisSize.min,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      _getEmailForm(),
      const SizedBox(height: 4.0),
      _getPasswordForm(),
      if (GetPlatform.isWeb)
        _getObscurePasswordSwitcher(),
      const SizedBox(height: 4.0),
      _getLoginButton(),
      const SizedBox(height: 8.0),
      _getForgetPasswordButton(),
      // _getRegistrationButton(),
    ],
  );

  Widget _getColumnSeparator() => Column(
    mainAxisSize: MainAxisSize.min,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Container(height: 150, child: VerticalDivider(indent: 20, endIndent: 20, color: UIConstants.primaryDarkColor)),
      Text(
          translate(Translations.or).toLowerCase(),
          style: TextStyle(color: UIConstants.primaryDarkColor)),
      Container(height: 150, child: VerticalDivider(indent: 20, endIndent: 20, color: UIConstants.primaryDarkColor)),
    ]);

  Widget _getRowSeparator() =>
      Row(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.center, children: [
        Container(
            width: 150.0,
            height: 50.0,
            child: Divider(indent: 20.0, endIndent: 20.0, color: UIConstants.primaryDarkColor)),
        Text(translate(Translations.or).toLowerCase(),
            style: TextStyle(color: UIConstants.primaryDarkColor)),
        Container(
            width: 150.0,
            height: 50.0,
            child: Divider(indent: 20.0, endIndent: 20.0, color: UIConstants.primaryDarkColor)),
      ]);

  Widget _getOtherEnterVariantsColumn() => Column(
    mainAxisSize: MainAxisSize.min,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      if (GetPlatform.isIOS)
        Column(children: [
            AuthButton(
              textKey: Translations.continue_with_apple,
              icon: Icon(FontAwesomeIcons.apple, size: UIConstants.iconSize),
              onPressed: _vm.signInWithApple,
            ),
            const SizedBox(height: 24.0),
          ]),
      AuthButton(
        textKey: Translations.continue_with_google,
        icon: Icon(FontAwesomeIcons.google, size: UIConstants.iconSize),
        onPressed: _vm.signInWithGoogle,
      ),
      const SizedBox(height: 24.0),
      AuthButton(
        textKey: Translations.continue_with_facebook,
        icon: Icon(FontAwesomeIcons.facebook, size: UIConstants.iconSize),
        onPressed: _vm.signInWithFacebook,
      ),
      const SizedBox(height: 24.0),
      AuthButton(
        textKey: Translations.continue_with_kundelik,
        icon: SvgWidget.asset(Assets.kundelik_logo_icon, width: UIConstants.iconSize, color: UIConstants.primaryDarkColor),
        onPressed: _trySignInWithKundelik,
      ),
    ]);

  Future<void> _trySignInWithKundelik() async {
    final isSuccess = await Get.toNamed(Routes.kundelikAuth);
    if (isSuccess)
      await _openLectureMenu();
  }

  Widget _getVersionLabel() => Align(
      alignment: Get.context.isLandscape ? Alignment.topRight : Alignment.bottomRight,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: Get.context.isLandscape ? 24 : 16, horizontal: 24),
        child: Text('v.${PlatformUtils.getFullVersion()}',
            style: TextStyle(color: UIConstants.primaryDarkColor)),
      ));

  Widget _getSendRequestButton() => Container(
      alignment: Get.context.isLandscape
          ? Alignment.bottomRight
          : Alignment.topRight,
      margin: EdgeInsets.only(bottom: 10.0, right: 10.0),
      child: IconButton(
          icon:  SvgWidget.asset(Assets.bug_icon, width: UIConstants.iconSize),
          color: UIConstants.primaryDarkColor,
          onPressed: () => Get.toNamed(Routes.sendRequest)));

  Widget _getNotReleaseServerLabel() => Container(
    alignment: Alignment.topLeft,
    margin: EdgeInsets.only(top: 20.0, left: 20.0),
    child: Text('${Settings.server.name} server',
        style: TextStyle(color: Colors.redAccent, fontSize: 24))
  );

  Widget _getEmailForm() => EmailTextEdit(
      text: _vm.email.value,
      hint: Translations.login,
      onTextChanged: (v) => _vm.email.value = v,
      onFieldSubmitted: (_) => _passwordController.requestFocus(),
      textInputAction: TextInputAction.next,
      controller: _emailController);

  Widget _getPasswordForm() => Obx(() => PasswordTextEdit(
      text: _vm.password.value,
      hint: Translations.password,
      onTextChanged: (v) => _vm.password.value = v,
      needObscureText: _vm.isObscurePwd.value,
      onNeedObscureChanged: (v) => _vm.isObscurePwd.value = v,
      controller: _passwordController));

  Widget _getObscurePasswordSwitcher() => Center(
    child: Container(
      constraints: BoxConstraints.tightFor(width: 260, height: 48),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Obx(() => Checkbox(
              value: !_vm.isObscurePwd.value,
              onChanged: (bool val) => _vm.isObscurePwd.value = !val)),
          Text(translate(Translations.show_password))
          ])));

  Widget _getLoginButton() => Center(
    child: Container(
      constraints: BoxConstraints.tightFor(width: 260, height: 48),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            primary: UIConstants.primaryDarkColor,
            onPrimary: UIConstants.buttonTextColor,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(50))
            )),
        child: Text(translate(Translations.log_in).toUpperCase()),
        onPressed: _trySignInWithEmail,
      )));

  Widget _getForgetPasswordButton() => TextButton(
    style: TextButton.styleFrom(
      visualDensity: VisualDensity.compact,
    ),
    child: Text(translate(Translations.password_forgot),
      style: TextStyle(
          fontSize: UIConstants.buttonFontSize,
          color: UIConstants.primaryDarkColor)),
    onPressed: () => Get.offNamed(Routes.studentAuthInfo),
  );

  /// TODO: Use after adding registration
  Widget _getRegistrationButton() => TextButton(
    style: TextButton.styleFrom(
      visualDensity: VisualDensity.compact,
    ),
    child: Text(translate(Translations.register),
      style: TextStyle(
          fontSize: UIConstants.buttonFontSize,
          color: UIConstants.primaryDarkColor)),
    onPressed: () => Get.snackbar(translate(Translations.error), translate(Translations.in_developing)),
  );

  Future<void> _trySignInWithEmail() async {
    _emailController.unfocus();
    _passwordController.unfocus();

    if (_vm.isSigningIn.value) return;

    if (_vm.checkServerSwitching())
      return _showServerSelector();

    if (!_validateForms())
      return _showFieldsValidationFailedDialog();

    _vm.signInWithEmail();
  }

  Future<void> _showServerSelector() async {
    final variants = Settings.serverTypeAddress.toMap((s) => s.name,
            (s) => s.isCustom ? _openCustomServer : () => _vm?.switchServer(s));
    return showAlertSelector('Select server', variants);
  }

  Future<void> _openCustomServer() {
    var text = '';
    final controller = CustomTextEditController();
    final serverRegex = RegExp(r'(http|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))?');
    return showCustomAlertDialog('Write server address',
        CustomTextEdit(
            text: text,
            onTextChanged: (v) => text = v,
            controller: controller,
            validator: (v) => serverRegex.hasMatch(v)
                ? null : 'Url should be valid. Ex: https://lol.kek.io'),
        [TintedButton(
            icon: Assets.trash_icon,
            onTap: () {
              if (!controller.isValid) {
                showAlertDialog('Error', 'Invalid url!');
                return;
              }
              Get.back();
              _vm?.switchServer(ServerEnvironment.custom(text));
            })
        ]);
  }

  bool _validateForms() => _emailController.validate() && _passwordController.validate();
}