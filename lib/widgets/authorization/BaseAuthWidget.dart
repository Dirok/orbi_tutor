import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

abstract class BaseAuthWidget extends BaseStatelessWidget {
  final Map<Type, Function(Event)> _eventHandlers = Map();

  BaseAuthWidget() {
    _eventHandlers.addAll({
      AuthorizationFailedEvent: (e) => _showAuthorizationFailedDialog(),
      OutsideServiceAuthorizationFailedEvent: (e) => _showOutsideServiceAuthorizationFailedDialog(),
    });

    vm.listenEvents((e) => _eventHandlers[e.runtimeType]?.call(e));
  }

  @override
  Widget build(BuildContext context) => Scaffold(
    extendBodyBehindAppBar: true,
    backgroundColor: UIConstants.primaryLightColor,
    body: SafeArea(
      child: Stack(
        alignment: AlignmentDirectional.bottomCenter,
        children: [
          Center(
            child: ListView(
                shrinkWrap: true,
                padding: EdgeInsets.all(5.0),
                children: getBodyList(),
            )),
          Get.context.isLandscape
            ? _getLogoIcon()
            : const SizedBox.shrink(),
          _getSendRequestButton(),
          _getVersionLabel(),
          if (Settings.server.isRelease)
            _getNotReleaseServerLabel(),
        ]),
    ));

  List<Widget> getBodyList() => [const SizedBox.shrink()];

  Widget _getLogoIcon() => Container(
    alignment: Alignment.topRight,
    margin: EdgeInsets.only(top: 10.0, right: 10.0),
    child: SvgWidget.asset(Assets.logo_image, width: 150.0),
  );

  Widget _getSendRequestButton() => Container(
      alignment: Alignment.bottomRight,
      margin: EdgeInsets.only(bottom: 10.0, right: 10.0),
      child: IconButton(
          icon:  SvgWidget.asset(Assets.bug_icon, width: UIConstants.iconSize),
          color: UIConstants.primaryDarkColor,
          onPressed: () => Get.toNamed(Routes.sendRequest)));

  Widget _getNotReleaseServerLabel() => Container(
      alignment: Alignment.topLeft,
      margin: EdgeInsets.only(top: 20.0, left: 20.0),
      child: Text('${Settings.server.name}',
          style: TextStyle(color: Colors.redAccent, fontSize: 24.0))
  );

  Widget _getVersionLabel() => Align(
      alignment: Alignment.bottomLeft,
      child: Padding(
        padding: EdgeInsets.symmetric(
            vertical: Get.context.isLandscape ? 24.0 : 16.0, horizontal: 24.0),
        child: Text('v.${PlatformUtils.getFullVersion()}',
            style: TextStyle(color: UIConstants.primaryDarkColor)),
      ));

  Future<void> _showAuthorizationFailedDialog() =>
      showAlertDialog(translate(Translations.error),
          translate(Translations.failed_to_authorization));

  Future<void> _showOutsideServiceAuthorizationFailedDialog() =>
      showAlertDialog(translate(Translations.error),
          translate(Translations.failed_authorization_in_outside_service));
}