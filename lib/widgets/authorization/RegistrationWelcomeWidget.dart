import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class RegistrationWelcomeWidget extends BaseAuthWidget {

  @override
  RegistrationWelcomeViewModel get vm => Get.find<RegistrationWelcomeViewModel>();

  @override
  List<Widget> getBodyList() => [
    _getHeaderText(),
    const SizedBox(height: 16.0),
    _getMainText(),
    const SizedBox(height: 32.0),
    _getContinueButton(),
    ];

  Widget _getHeaderText() => Center(
    child: Text(
      translate(Translations.register_success),
      style: TextStyle(
        color: UIConstants.defaultTextColor,
        fontSize: UIConstants.appBarFontSize,
        fontFamily: UIConstants.fontFamily,
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.none
      )));

  Widget _getMainText() => Center(
      child: Text(
          translate(Translations.welcome_to),
          style: TextStyle(
              color: UIConstants.defaultTextColor,
              fontSize: 28.0,
              fontFamily: UIConstants.fontFamily,
              fontWeight: FontWeight.w400,
              decoration: TextDecoration.none
          )));

  Widget _getContinueButton() => Center(
      child: Container(
          constraints: BoxConstraints.tightFor(width: 130.0, height: 48.0),
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: UIConstants.primaryDarkColor,
                onPrimary: UIConstants.buttonTextColor,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(50.0))
                )),
            child: Text(translate(Translations.ok).toUpperCase()),
            onPressed: () => Get.offAllNamed(Routes.mainMenu),
          )));
}