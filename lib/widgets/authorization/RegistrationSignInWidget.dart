import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class RegistrationSignInWidget extends BaseAuthWidget {
  @override
  RegistrationSignInViewModel get vm => Get.find<RegistrationSignInViewModel>();

  final Map<Type, Function(Event)> _eventHandlers = Map();

  RegistrationSignInWidget() {
    _eventHandlers.addAll({
      AuthorizationSuccessEvent: (e) => _onSuccessSignIn(),
    });
    vm.listenEvents((e) => _eventHandlers[e.runtimeType]?.call(e));
  }

  @override
  List<Widget> getBodyList() => [
    _getHeaderText(),
    const SizedBox(height: 20.0),
    Obx(() => vm.isSigningIn.value
        ? getNewProgressIndicator()
        : _getAuthBody()
    )];

  Widget _getHeaderText() => Center(
    child: Text(
      translate(Translations.register_to_continue),
      textAlign: TextAlign.center,
      style: TextStyle(
        color: UIConstants.defaultTextColor,
        fontSize: UIConstants.appBarFontSize,
        fontFamily: UIConstants.fontFamily,
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.none
      )));

  Widget _getAuthBody() => Column(
    mainAxisSize: MainAxisSize.min,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      if (GetPlatform.isIOS)
        Column(children: [
          AuthButton(
            textKey: Translations.continue_with_apple,
            icon: Icon(FontAwesomeIcons.apple, size: UIConstants.iconSize),
            onPressed: vm.signInWithApple,
          ),
          const SizedBox(height: 16.0),
       ]),
      AuthButton(
        textKey: Translations.continue_with_google,
        icon: Icon(FontAwesomeIcons.google, size: UIConstants.iconSize),
        onPressed: vm.signInWithGoogle,
      ),
      const SizedBox(height: 16.0),
      AuthButton(
        textKey: Translations.continue_with_facebook,
        icon: Icon(FontAwesomeIcons.facebook, size: UIConstants.iconSize),
        onPressed: vm.signInWithFacebook,
      ),
      const SizedBox(height: 16.0),
      AuthButton(
        textKey: Translations.continue_with_kundelik,
        icon: SvgWidget.asset(Assets.kundelik_logo_icon, width: UIConstants.iconSize,
            color: UIConstants.primaryDarkColor),
        onPressed: _trySignInWithKundelik,
      ),
      const SizedBox(height: 4.0),
      _getCancelButton(),
    ]);

  Widget _getCancelButton() => TextButton(
    style: TextButton.styleFrom(
      visualDensity: VisualDensity.compact,
    ),
    child: Text(translate(Translations.cancel),
        style: TextStyle(
            fontSize: UIConstants.buttonFontSize,
            color: UIConstants.primaryDarkColor)),
    onPressed: _onCancelSignIn,
  );

  Future<void> _showCancelRegisterDialog() => showAlertDialog(
    translate(Translations.warning), translate(Translations.cancel_register),
      variants: {
          translate(Translations.continue_): _fullExit,
          translate(Translations.cancel): null
      });

  Future<void> _trySignInWithKundelik() async {
    final isSuccess = await Get.toNamed(Routes.kundelikAuth);
    vm.handleAuthResult(isSuccess ? AuthorizationResult.success : AuthorizationResult.fail);
  }

  Future<void> _onCancelSignIn() async {
    if (vm.skipFullRegister)
      return Get.back(result: false);

    return _showCancelRegisterDialog;
  }

  Future<void> _onSuccessSignIn() async {
    if (vm.skipFullRegister)
      return Get.back(result: true);

    Get.toNamed(Routes.registrationUserName);
  }

  Future<void> _fullExit() async {
    // todo send command to server [clear temp anonymous user] from vm
    return Get.offAllNamed(Routes.login);
  }
}