import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';
import 'package:tutor/view_models/authorization/RegistrationWithEmailViewModel.dart';

class RegistrationWithEmailWidget extends TranslatedStatelessWidget {
  final _vm = Get.find<RegistrationWithEmailViewModel>();

  final _emailController = CustomTextEditController();
  final _passwordController = CustomTextEditController();
  final _passwordRepeatController = CustomTextEditController();

  final Map<Type, Function(Event)> _eventHandlers = Map();

  RegistrationWithEmailWidget() {
    _vm.clearSubscriptions();

    _eventHandlers.assignAll({
      RegistrationSuccessEvent: (e) => _openNextScreen(e),
      RegistrationFailedEvent: (e) => _showRegistrationFailedDialog(e),
      RegistrationAccountAlreadyExistEvent: (e) => _showRegistrationAccountAlreadyExistDialog(e),
      RegistrationOperationNotAllowedEvent: (e) => _showRegistrationOperationNotAllowedDialog(e),
      RegistrationWeakPasswordEvent: (e) => _showRegistrationWeakPasswordDialog(e),
    });

    _vm.listenEvents((e) => _eventHandlers[e.runtimeType]?.call(e));
  }

  Future<void> _openNextScreen(RegistrationSuccessEvent e) => Get.offAllNamed(Routes.login);

  Future<void> _showRegistrationFailedDialog(RegistrationFailedEvent e) =>
      showAlertDialog(
          translate(Translations.error),
          translate(Translations.failed_to_registration));

  Future<void> _showRegistrationAccountAlreadyExistDialog(RegistrationAccountAlreadyExistEvent e) =>
      showAlertDialog(
          translate(Translations.error),
          translate(Translations.failed_to_registration_account_exists));

  Future<void> _showRegistrationOperationNotAllowedDialog(RegistrationOperationNotAllowedEvent e) =>
      showAlertDialog(
          translate(Translations.error),
          translate(Translations.failed_to_registration_operation_not_allowed));

  Future<void> _showRegistrationWeakPasswordDialog(RegistrationWeakPasswordEvent e) =>
      showAlertDialog(
          translate(Translations.error),
          translate(Translations.failed_to_registration_weak_password));

  Future<void> _showFieldsValidationFailedDialog() => showAlertDialog(
    translate(Translations.error), translate(Translations.enter_valid_credentials));

  @override
  Widget build(BuildContext context) => Stack(
    alignment: AlignmentDirectional.bottomCenter, children: [
      Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: UIConstants.primaryLightColor,
        body: SafeArea(
          child: Stack(alignment: AlignmentDirectional.bottomCenter, children: [
            Center(
              child: ListView(
                shrinkWrap: true,
                padding: EdgeInsets.all(5.0),
                children: [
                  // _getHeaderText(),
                  const SizedBox(height: 8.0),
                  _getRegisterBody()
                ]),
            ),
          ]),
        ),
      )
  ]);

  Widget _getRegisterBody() => Column(
    mainAxisSize: MainAxisSize.min,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      _getEmailForm(),
      const SizedBox(height: 4.0),
      _getPasswordForm(),
      if (GetPlatform.isWeb)
        _getObscurePasswordSwitcher(),
      const SizedBox(height: 4.0),
      _getRegisterButton(),
      const SizedBox(height: 8.0),
      _getCancelButton(),
      // _getRegistrationButton(),
    ],
  );

  Widget _getEmailForm() => EmailTextEdit(
      text: _vm.email.value,
      hint: Translations.login,
      onTextChanged: (v) => _vm.email.value = v,
      onFieldSubmitted: (_) => _passwordController.requestFocus(),
      textInputAction: TextInputAction.next,
      controller: _emailController);

  Widget _getPasswordForm() => Column(
      children: [
        Obx(() => PasswordTextEdit(
            text: _vm.password.value,
            hint: Translations.password,
            onTextChanged: (v) => _vm.password.value = v,
            onFieldSubmitted: (_) => _passwordRepeatController.requestFocus(),
            needObscureText: _vm.isObscurePwd.value,
            onNeedObscureChanged: (v) => _vm.isObscurePwd.value = v,
            controller: _passwordController)),
        Obx(() => PasswordTextEdit(
            text: _vm.passwordRepeat.value,
            hint: Translations.password_repeat,
            onTextChanged: (v) => _vm.passwordRepeat.value = v,
            needObscureText: _vm.isObscurePwd.value,
            onNeedObscureChanged: (v) => _vm.isObscurePwd.value = v,
            validator: _validatePasswordRepeat,
            controller: _passwordRepeatController))
      ]);

  String _validatePasswordRepeat(String value) =>
      value == _vm.password.value ? null : translate(Translations.password_mismatch);

  Widget _getObscurePasswordSwitcher() => Center(
    child: Container(
      constraints: BoxConstraints.tightFor(width: 260, height: 48),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Obx(() => Checkbox(
            value: !_vm.isObscurePwd.value,
            onChanged: (bool val) => _vm.isObscurePwd.value = !val)),
          Text(translate(Translations.show_password))
        ])));

  Widget _getRegisterButton() => Center(
    child: Container(
      constraints: BoxConstraints.tightFor(width: 260, height: 48),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: UIConstants.primaryDarkColor,
          onPrimary: UIConstants.buttonTextColor,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(50)))),
        child: Text(translate(Translations.register).toUpperCase()),
        onPressed: _tryRegisterWithEmail,
      )));

  Widget _getCancelButton() => TextButton(
    style: TextButton.styleFrom(
      visualDensity: VisualDensity.compact,
    ),
    child: Text(translate(Translations.cancel),
      style: TextStyle(
        fontSize: UIConstants.buttonFontSize,
        color: UIConstants.primaryDarkColor)),
    onPressed: () => Get.back(),
  );

  Future<void> _tryRegisterWithEmail() =>
      _validateForms() ? _vm.registerWithEmail() : _showFieldsValidationFailedDialog();

  bool _validateForms() => _emailController.validate()
      && _passwordController.validate()
      && _passwordRepeatController.validate();
}
