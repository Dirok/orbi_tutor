import 'dart:core';

import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class StudentAuthWidget extends TranslatedStatelessWidget {
  final _vm = Get.find<StudentAuthViewModel>();

  @override
  Widget build(BuildContext context) {
    return Stack(
        alignment: AlignmentDirectional.bottomCenter,
        children: [
          Scaffold(
              extendBodyBehindAppBar: true,
              backgroundColor: UIConstants.primaryLightColor,
              body: Stack(
                  alignment: AlignmentDirectional.bottomCenter,
                  children: [
                    Center(
                      child: Image.asset(Assets.student_auth_background),
                    ),
                    _getAppBar(context),
                    _getMainListView(context)
                  ])
          )
    ]);
  }

  _getAppBar(BuildContext context) => AppBar(
    backgroundColor: Colors.transparent,
    elevation: 0.0,
    leading: new Container(),
    actions: [
      Padding(
          padding: EdgeInsets.only(right: 16.0, top: 8),
          child: SvgWidget.asset(Assets.logo_image, width: 150))
    ],
  );

  _getMainListView(BuildContext context) => Center(
    child: ListView(shrinkWrap: true, children: [
      const SizedBox(height: 16.0),
      _getHeaderText(context),
      const SizedBox(height: 16.0),
      _getUserImage(context),
      const SizedBox(height: 8.0),
      _getUserText(context,
          appSession.account.user.surname + " " + appSession.account.user.name),
      const SizedBox(height: 48.0),
      _getMainButton(context, Translations.confirm),
      _getSecondaryButton(context, Translations.not_me),
    ])
  );

  _getHeaderText(BuildContext context) => Center(
      child: Container(
          constraints: BoxConstraints.tightFor(width: 300),
          margin: EdgeInsets.all(4),
          child: Text(
            translate(Translations.enter_as_student),
            textAlign: TextAlign.center,
            style: TextStyle(
                color: UIConstants.defaultTextColor,
                fontSize: 20,
                fontFamily: UIConstants.fontFamily,
                fontWeight: FontWeight.w700,
                decoration: TextDecoration.none
            )
        )
      )
  );

  _getUserImage(BuildContext context) => Center(
        child: Container(
            decoration: new BoxDecoration(
                color: UIConstants.primaryDarkColor,
                shape: BoxShape.circle,
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [UIConstants.backgroundColor, UIConstants.primaryDarkColor],
                  stops: [0.0, 0.7],
                )
            ),
            child: SvgWidget.asset(Assets.user_image, width: 64.0, height: 64.0)
        ),
  );

  _getUserText(BuildContext context, String textKey) => Center(
      child: Text(
          textKey,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: UIConstants.primaryDarkColor,
              fontSize: 18,
              fontFamily: UIConstants.fontFamily,
              fontWeight: FontWeight.w700,
              decoration: TextDecoration.none),
      ),
  );

  _getMainButton(BuildContext context, String textKey) => Center(
      child: Container(
          constraints: BoxConstraints.tightFor(width: 200, height: 40),
          margin: EdgeInsets.all(4),
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                padding: EdgeInsets.zero,
                primary: UIConstants.primaryDarkColor,
                onPrimary: UIConstants.buttonTextColor,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10))
                )),
            child: Text(translate(textKey).toUpperCase()),
            onPressed: () async {
              await _vm.saveAuthInfo();
              Get.offNamed(Routes.mainMenu);
            },
          ))
  );

  _getSecondaryButton(BuildContext context, String textKey) => Center(
      child: Container(
          constraints: BoxConstraints.tightFor(width: 200, height: 40),
          margin: EdgeInsets.all(4),
          child: TextButton(
            child: Text(translate(textKey), style: TextStyle(color: UIConstants.purpleIconColor)),
            onPressed: () async {
              await _vm.resetAuthInfo();
              Get.offNamed(Routes.studentAuthInfo);
            },
          ))
  );
}