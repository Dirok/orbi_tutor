import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class WantToSignInWidget extends BaseAuthWidget {
  @override
  WantToSignInViewModel get vm => Get.find<WantToSignInViewModel>();

  final Map<Type, Function(Event)> _eventHandlers = Map();

  WantToSignInWidget() {
    _eventHandlers.addAll({
      AuthorizationSuccessEvent: (e) => Get.back(result: true),
    });
    vm.listenEvents((e) => _eventHandlers[e.runtimeType]?.call(e));
  }

  @override
  List<Widget> getBodyList() => [
    _getHeaderText(),
    const SizedBox(height: 16.0),
    _getMainText(),
    const SizedBox(height: 32.0),
    Obx(() => vm.isSigningIn.value ? getNewProgressIndicator() : _getButtons()),
  ];

  Widget _getHeaderText() => Center(
    child: Text(
      translate(Translations.you_invited_to_lecture),
      style: TextStyle(
        color: UIConstants.defaultTextColor,
        fontSize: UIConstants.appBarFontSize,
        fontFamily: UIConstants.fontFamily,
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.none
      )));

  Widget _getMainText() => Center(
    child: Container(
      margin: EdgeInsets.all(16.0),
      child: Text(
        translate(Translations.want_to_sign_in),
        textAlign: TextAlign.center,
        style: TextStyle(
          color: UIConstants.defaultTextColor,
          fontSize: 18.0,
          fontFamily: UIConstants.fontFamily,
          fontWeight: FontWeight.w400,
          decoration: TextDecoration.none
        )),
    ));

  Widget _getButtons() => Center(
    child: Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const SizedBox(width: 260.0),
        _getSignInButton(),
        _getSignInAnonymouslyButton(),
      ]),
  );

  Widget _getSignInButton() => Center(
    child: Container(
      constraints: BoxConstraints.tightFor(width: 260.0, height: 48.0),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: UIConstants.primaryDarkColor,
          onPrimary: UIConstants.buttonTextColor,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(50.0))
          )),
        child: Text(translate(Translations.log_in).toUpperCase()),
        onPressed: _showSignInDialog,
      )));

  Widget _getSignInAnonymouslyButton() => Container(
    constraints: BoxConstraints.tightFor(width: 260.0, height: 48.0),
    child: TextButton(
      style: TextButton.styleFrom(visualDensity: VisualDensity.compact),
      child: Text(translate(Translations.continue_as_anonymous),
          style: TextStyle(
              fontSize: UIConstants.buttonFontSize,
              color: UIConstants.primaryDarkColor)),
      onPressed: () => vm.signInAnonymously(),
    ),
  );

  Future<void> _showSignInDialog() async {
    final isSignIn = await Get.toNamed(Routes.registrationSignIn,
        arguments: {'skipFullRegister': true});
    if (isSignIn)
      Get.back(result: true);
  }
}