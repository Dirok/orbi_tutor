import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class KundelikAuthWidget extends TranslatedStatelessWidget {
  final _vm = Get.find<KundelikAuthViewModel>();

  final InAppWebViewGroupOptions _options = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        useShouldOverrideUrlLoading: true,
        mediaPlaybackRequiresUserGesture: false,
        clearCache: true,
      ),
      android: AndroidInAppWebViewOptions(
        useHybridComposition: true,
      ),
      ios: IOSInAppWebViewOptions(
        allowsInlineMediaPlayback: true,
      ));

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () => Future.value(false),
        child: Scaffold(
            extendBodyBehindAppBar: true,
            body: SafeArea(
                child: Stack(
                  children: [
                    Container(
                  margin: Get.context.isLandscape
                      ? const EdgeInsets.symmetric(horizontal: 60)
                      : const EdgeInsets.only(top: 60),
                  child: Obx(() =>
                      _vm.isSigningIn()
                          ? getNewProgressIndicator()
                          : _getWebViewBody())),
                    Align(
                      alignment: Alignment.topLeft,
                      child: TintedButton(
                        onTap: () => Get.back(result: false),
                        icon: Assets.back_arrow_icon,
                        size: UIConstants.iconSize,
                      ),
                    ),
                  ]))));
  }

  Widget _getWebViewBody() => Column(
    children: [
      Obx(() => Container(
        padding: const EdgeInsets.only(left: 10.0, right: 10.0),
        child: _vm.progress.value < 1.0
            ? LinearProgressIndicator(value: _vm.progress.value)
            : const SizedBox.shrink())),
      Expanded(
        child: InAppWebView(
          initialUrlRequest: URLRequest(url: Uri.parse(_vm.initLink)),
          initialOptions: _options,
          onLoadStart: (controller, url) {
            _vm.url = url.toString(); // ???
          },
          onLoadStop: (controller, url) async {
            final isSuccess = await _vm.tryAuthorize(url);
            if (isSuccess)
              return Get.back(result: true);
          },
          onLoadError: (controller, url, code, message) {
            //_showKundelikAuthorizationFailedDialog();
          },
          onProgressChanged: (controller, progress) {
            _vm.progress.value = progress / 100.0;
          })),
    ]);
}