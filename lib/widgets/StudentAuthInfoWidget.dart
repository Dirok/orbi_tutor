import 'dart:core';

import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class StudentAuthInfoWidget extends TranslatedStatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: UIConstants.primaryLightColor,
        appBar: _getAppBar(context),
        body: Stack(children: [
          Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Flexible(
                    child: Container(
                        alignment: Alignment.centerLeft,
                        width: 210,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(translate(Translations.contact_admin),
                                style: TextStyle(
                                    color: UIConstants.defaultTextColor,
                                    fontSize: UIConstants.appBarFontSize,
                                    fontFamily: UIConstants.fontFamily,
                                    fontWeight: FontWeight.w700,
                                    decoration: TextDecoration.none)),
                            const SizedBox(height: 16.0),
                            Text(translate(Translations.get_auth_link),
                                style: TextStyle(
                                    color: UIConstants.primaryDarkColor,
                                    fontSize: UIConstants.defaultFontSize,
                                    fontFamily: UIConstants.fontFamily,
                                    fontWeight: FontWeight.w600,
                                    decoration: TextDecoration.none)),
                          ]))),
                Container(
                    child: SvgWidget.asset(Assets.student_auth_info_image)),
              ]),
          _getSendRequestButton(),
        ]));
  }

  Widget _getAppBar(BuildContext context) => AppBar(
    backgroundColor: Colors.transparent,
    elevation: 0.0,
    leading: Builder(
      builder: (BuildContext context) {
        return IconButton(
          icon: SvgWidget.asset(Assets.back_arrow_icon,
              color: UIConstants.primaryDarkColor,
              width: UIConstants.iconSize,
              height: UIConstants.iconSize),
              onPressed: () => Get.offAllNamed(Routes.login));
      }),
    actions: [
      Padding(
          padding: EdgeInsets.only(right: 16.0, top: 8),
          child: SvgWidget.asset(Assets.logo_image, width: 150))
    ]);

  Widget _getSendRequestButton() => Container(
      alignment: Alignment.bottomRight,
      margin: EdgeInsets.only(bottom: 24.0, right: 24.0),
      child: IconButton(
          icon:  SvgWidget.asset(Assets.bug_icon, width: UIConstants.iconSize),
          color: UIConstants.primaryDarkColor,
          onPressed: () => Get.toNamed(Routes.sendRequest)
      ));
}

