import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:tutor/focused.dart';

class CallWidget extends StatefulWidget {

  const CallWidget({Key key}) : super(key: key);

  @override
  _CallWidgetState createState() => new _CallWidgetState();
}

class _CallWidgetState extends State<CallWidget> {

  VideoRoomSignaling _signaling = VideoRoomSignaling();

  RTCVideoRenderer _localRenderer = new RTCVideoRenderer();
  RTCVideoRenderer _remoteRenderer = new RTCVideoRenderer();

  @override
  initState() {
    super.initState();

    initRenderers();
  }

  initRenderers() async {
    await _localRenderer.initialize();
    await _remoteRenderer.initialize();

    _signaling.onAddRemoteStream = ((stream) {
      this.setState(() {
        _remoteRenderer.srcObject = stream;
      });
    });

    _signaling.onRemoveRemoteStream = ((stream) {
      this.setState(() {
        _remoteRenderer.srcObject = null;
      });
    });

    if(_signaling.callState == CallState.Connected) {
      await _signaling.changeMode(true, true); // TODO from license

      setState(() {
        _localRenderer.srcObject = _signaling.localStream;

        if (_signaling.currentRemoteStream != null) {
          _remoteRenderer.srcObject = _signaling.currentRemoteStream;
        }
      });
    }
  }

  @override
  deactivate() async {

    if(_remoteRenderer != null) {
      _remoteRenderer.srcObject = null;
      _remoteRenderer.dispose();
      _remoteRenderer = null;
    }

    if(_localRenderer != null) {
      _localRenderer.srcObject = null;
      _localRenderer.dispose();
      _localRenderer = null;
    }

    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      extendBodyBehindAppBar: true,
      body: OrientationBuilder(builder: (context, orientation) {
        return new Container(
          child: new Stack(children: <Widget>[
            new Positioned(
                left: 0.0, right: 0.0, top: 0.0, bottom: 0.0,
                child: new Container(
                  margin: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: new RTCVideoView(_remoteRenderer),
                  decoration: new BoxDecoration(color: Colors.black54),
                )),
            new Positioned(
              left: 20.0, bottom: 20.0,
              child: new Container(
                width: orientation == Orientation.portrait ? 90.0 : 120.0,
                height:
                orientation == Orientation.portrait ? 120.0 : 90.0,
                child: new RTCVideoView(_localRenderer),
                decoration: new BoxDecoration(color: Colors.black54),
              ),
            ),
          ]),
        );
      }),
    );
  }
}
