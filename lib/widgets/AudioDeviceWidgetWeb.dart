import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart' as rtc;
import 'package:tutor/focused.dart';

class AudioDeviceWidgetWeb extends StatefulWidget {
  final VideoRoomSignaling signaling;

  AudioDeviceWidgetWeb(this.signaling);

  @override
  _AudioDeviceWidgetWebState createState() => _AudioDeviceWidgetWebState();
}

class _AudioDeviceWidgetWebState extends TranslatedState<AudioDeviceWidgetWeb> {
  rtc.RTCVideoRenderer renderer = rtc.RTCVideoRenderer();

  @override
  initState() {
    super.initState();
    initRenderer();

    widget.signaling.onAddRemoteStream = ((stream) {
      this.setState(() {
        renderer.srcObject = stream;
      });
    });

    widget.signaling.onRemoveRemoteStream = ((stream) {
      this.setState(() {
        renderer.srcObject = null;
      });
    });
  }

  initRenderer() async {
    await renderer.initialize();
    final devices = await rtc.navigator.mediaDevices.enumerateDevices();
    renderer.delegate.audioOutput(devices.first.deviceId);
  }



  Future<void> _showAudioDevices() async {
    var devices = await rtc.navigator.mediaDevices.enumerateDevices();
    Map<String, Function> variants = Map();
    for (var device in devices) {
      if (device.kind == 'audiooutput') {
        var deviceName = device.label;
        variants[deviceName] = () => tryChangeAudioDevice(device.deviceId);
      }
    }
    return showAlertSelector(translate(Translations.select_audio_device), variants);
  }

  Future<void> tryChangeAudioDevice(String id) async {
    renderer.delegate.audioOutput(id);
  }

  @override
  Widget build(BuildContext context) => Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          IconButton(
              icon: Icon(
                Icons.speaker,
                color: UIConstants.primaryDarkColor,
              ),
              onPressed: _showAudioDevices),
          SizedBox.shrink(
            child: Visibility(
              child: rtc.RTCVideoView(renderer),
              visible: false,
              maintainState: true,
            ),
          ),
        ],
      );
}