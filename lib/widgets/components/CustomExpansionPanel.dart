import 'package:flutter/material.dart';
import 'package:tutor/focused.dart';

class CustomExpansionPanelList extends StatefulWidget {
  CustomExpansionPanelList({
    @required this.panelList,
  });

  final List<ExpansionPanelData> panelList;

  @override
  _CustomExpansionPanelListState createState() => _CustomExpansionPanelListState();
}

class _CustomExpansionPanelListState extends TranslatedState<CustomExpansionPanelList> {
  @override
  Widget build(BuildContext context) {
    return ExpansionPanelList(
      elevation: 0,
      children: [
        for (var item in widget.panelList) _getPanel(item),
      ],
      expansionCallback: (int index, bool status) {
        setState(() {
          widget.panelList[index].isExpanded = !status;
        });
        widget.panelList[index].performExpand(!status);
      },
    );
  }

  _getPanel(ExpansionPanelData item) {
    return ExpansionPanel(
      isExpanded: item.isExpanded ?? false,
      backgroundColor: Colors.transparent,
      canTapOnHeader: true,
      headerBuilder: (BuildContext context, bool isExpanded) => Container(
          margin: const EdgeInsets.only(left: 20.0, right: 150.0, top: 7.0, bottom: 7.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50.0),
            color: UIConstants.purpleIconColor,
          ),
          child: Center(
            child: Text(
              item.title,
              style: TextStyle(
                color: UIConstants.buttonTextColor,
              ),
            ),
          )),
      body: item.body,
    );
  }
}

class ExpansionPanelData {
  ExpansionPanelData({
    this.body,
    this.isExpanded = false,
    this.title,
    this.performExpand,
  });

  final Widget body;
  final String title;
  bool isExpanded;
  Function(bool) performExpand;
}
