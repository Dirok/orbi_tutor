import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class CustomAppBar extends TranslatedStatelessWidget with PreferredSizeWidget {
  @override
  final Size preferredSize;

  final String titleKey;
  final IconData actionIconData;
  final Function onLeadingPressed;
  final Function onActionPressed;
  final Widget bottomAppBarWidget;

  CustomAppBar(
      {@required this.titleKey,
      this.actionIconData,
      this.onActionPressed,
      this.onLeadingPressed,
      this.bottomAppBarWidget = const SizedBox.shrink()})
      : preferredSize = Size.fromHeight(UIConstants.appBarPreferredHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: UIConstants.primaryLightColor,
      leading: IconButton(
        icon: SvgWidget.asset(Assets.back_arrow_icon, color: UIConstants.primaryDarkColor,
            width: UIConstants.iconSize, height: UIConstants.iconSize),
        onPressed: onLeadingPressed == null ? Get.back : onLeadingPressed,
      ),
      title: Text(translate(titleKey),
          style: TextStyle(
              color: UIConstants.defaultTextColor,
              fontSize: UIConstants.appBarFontSize,
              fontFamily: UIConstants.fontFamily,
              fontWeight: FontWeight.w700,
              decoration: TextDecoration.none)),
      actions: [
        actionIconData == null || onActionPressed == null
          ? const SizedBox.shrink()
          : IconButton(
              icon: Icon(actionIconData,
                size: UIConstants.iconSize, color: UIConstants.purpleIconColor,
              ),
              onPressed: onActionPressed),
      ],
      bottom: PreferredSize(
        preferredSize: preferredSize,
        child: bottomAppBarWidget,
      ),
    );
  }
}