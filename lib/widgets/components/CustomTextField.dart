import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class CustomTextEditController {
  bool Function() _isValidFunc;
  bool Function() _validateFunc;
  void Function() _requestFocusFunc;
  void Function() _unfocusFunc;
  void Function(String) _setTextFunc;

  bool get isValid => _isValidFunc?.call();
  bool validate() => _validateFunc?.call();
  void requestFocus() => _requestFocusFunc?.call();
  void unfocus() => _unfocusFunc?.call();
  void setText(String value) => _setTextFunc?.call(value);
}

class CustomTextEdit extends StatefulWidget {
  final String text;
  final void Function(String) onTextChanged;
  final void Function(String) onFieldSubmitted;
  final void Function(bool) onValidate;
  final String hint;
  final int maxLines;
  final int maxLength;
  final bool isSpaceAvailable;
  final TextInputAction textInputAction;
  final FormFieldValidator<String> validator;
  final CustomTextEditController controller;

  const CustomTextEdit({
    Key key,
    @required this.text,
    this.onTextChanged,
    this.onFieldSubmitted,
    this.onValidate,
    this.hint,
    this.maxLines = 1,
    this.maxLength,
    this.isSpaceAvailable = false,
    this.textInputAction = TextInputAction.done,
    this.validator,
    this.controller}) : super(key: key);

  @override
  CustomTextEditState createState() => CustomTextEditState();
}

class CustomTextEditState<T extends CustomTextEdit> extends TranslatedState<T> {
  final _formKey = GlobalKey<FormState>();
  final _textEditingController = TextEditingController();
  final _focusNode = FocusNode();

  TextInputType get keyboardType => TextInputType.text;
  Widget get suffixIcon => const SizedBox.shrink();
  bool get obscureText => false;

  bool isValid = true;

  @override
  void initState() {
    super.initState();

    _textEditingController.text = widget.text;

    _focusNode.addListener(() {
      SystemChrome.setEnabledSystemUIOverlays([]);

      if (!_focusNode.hasFocus)
        _validateForm();
    });
  }

  void _updateCustomTextEditController() {
    if (widget.controller == null) return;

    widget.controller._isValidFunc = () => isValid;
    widget.controller._validateFunc = _validateForm;
    widget.controller._requestFocusFunc = _focusNode.requestFocus;
    widget.controller._unfocusFunc = _focusNode.unfocus;
    widget.controller._setTextFunc = (v) => _textEditingController.text = v;
  }

  @override
  void dispose() {
    _focusNode.dispose();
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _updateCustomTextEditController();

    return Form(
        key: _formKey,
        child: Container(
            constraints: BoxConstraints.tightFor(width: 260),
            child: Center(child: TextFormField(
              decoration: InputDecoration(
                helperText: '',
                hintText: translate(widget.hint),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: UIConstants.primaryDarkColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: UIConstants.purpleIconColor),
                ),
                suffixIcon: suffixIcon,
              ),
              validator: widget.validator ?? validate,
              keyboardType: keyboardType,
              inputFormatters: widget.isSpaceAvailable
                  ? null : [FilteringTextInputFormatter.deny(' ')],
              obscureText: obscureText,
              controller: _textEditingController,
              focusNode: _focusNode,
              textInputAction: widget.textInputAction,
              maxLines: widget.maxLines,
              maxLength: widget.maxLength,
              onFieldSubmitted: _onFieldSubmitted,
              onChanged: _onChanged,
            ))));
  }

  bool _validateForm() {
    isValid = _formKey.currentState.validate();
    widget.onValidate?.call(isValid);
    return isValid;
  }

  String validate(String value) =>
      value.isNotEmpty ? null : translate(Translations.auth_field_empty);

  void _onChanged(String value) {
    if (!isValid)
      _validateForm();

    widget.onTextChanged?.call(value);
  }

  void _onFieldSubmitted(String value) {
    _focusNode.unfocus();
    widget.onFieldSubmitted?.call(value);
  }
}

class AuthTextEdit extends CustomTextEdit {
  const AuthTextEdit({
    Key key,
    @required String text,
    void Function(String) onTextChanged,
    void Function(String) onFieldSubmitted,
    void Function(bool) onValidate,
    String hint,
    TextInputAction textInputAction,
    CustomTextEditController controller,
    FormFieldValidator<String> validator})
      : super(key: key,
      text: text,
      onTextChanged: onTextChanged,
      onFieldSubmitted: onFieldSubmitted,
      onValidate: onValidate,
      hint: hint,
      textInputAction: textInputAction,
      controller: controller,
      validator: validator);

  @override
  AuthTextEditState createState() => AuthTextEditState();
}

class AuthTextEditState<T extends AuthTextEdit> extends CustomTextEditState<T> {
  @override
  String validate(String value) => super.validate(value)
      ?? (!value.contains(' ') ? null : translate(Translations.auth_field_with_space));
}

class EmailTextEdit extends AuthTextEdit {
  const EmailTextEdit({
    Key key,
    @required String text,
    void Function(String) onTextChanged,
    void Function(String) onFieldSubmitted,
    void Function(bool) onValidate,
    String hint,
    TextInputAction textInputAction,
    CustomTextEditController controller,
    FormFieldValidator<String> validator})
      : super(key: key,
      text: text,
      onTextChanged: onTextChanged,
      onFieldSubmitted: onFieldSubmitted,
      onValidate: onValidate,
      hint: hint,
      textInputAction: textInputAction,
      controller: controller,
      validator: validator);

  @override
  _EmailTextEditState createState() => _EmailTextEditState();
}

class _EmailTextEditState extends AuthTextEditState<EmailTextEdit> {
  @override
  TextInputType get keyboardType => TextInputType.emailAddress;

  @override
  String validate(String value) => super.validate(value)
      ?? (GetUtils.isEmail(value) ? null : translate(Translations.email_invalid));
}

class PasswordTextEdit extends AuthTextEdit {
  final bool needObscureText;
  final void Function(bool) onNeedObscureChanged;

  const PasswordTextEdit({
    Key key,
    @required String text,
    @required this.needObscureText,
    @required this.onNeedObscureChanged,
    void Function(String) onTextChanged,
    void Function(String) onFieldSubmitted,
    void Function(bool) onValidate,
    String hint,
    TextInputAction textInputAction,
    CustomTextEditController controller,
    FormFieldValidator<String> validator})
      : super(key: key,
      text: text,
      onTextChanged: onTextChanged,
      onFieldSubmitted: onFieldSubmitted,
      onValidate: onValidate,
      hint: hint,
      textInputAction: textInputAction,
      controller: controller,
      validator: validator);

  @override
  _PasswordTextEditState createState() => _PasswordTextEditState();
}

class _PasswordTextEditState extends AuthTextEditState<PasswordTextEdit> {
  @override
  Widget get suffixIcon => GetPlatform.isWeb ? super.suffixIcon : IconButton(
      icon: Icon(widget.needObscureText ? Icons.visibility : Icons.visibility_off),
      color: UIConstants.primaryDarkColor,
      onPressed: () {
        widget.onNeedObscureChanged?.call(!widget.needObscureText);
      });

  @override
  bool get obscureText => widget.needObscureText;

  @override
  TextInputType get keyboardType => TextInputType.visiblePassword;

  @override
  String validate(String value) => super.validate(value)
      ?? (value.length >= 6 ? null : translate(Translations.password_unsafe));
}