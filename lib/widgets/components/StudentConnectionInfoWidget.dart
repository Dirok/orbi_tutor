import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class StudentConnectionInfoWidget extends StatefulWidget {
  StudentConnectionInfoWidget(this.connectionInfo);

  final StudentConnectionInfoViewModel connectionInfo;

  @override
  _StudentConnectionInfoWidgetState createState() =>
      _StudentConnectionInfoWidgetState();
}

class _StudentConnectionInfoWidgetState
    extends TranslatedState<StudentConnectionInfoWidget>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  bool get isConnected => widget.connectionInfo.isConnected;

  String get message => '${widget.connectionInfo.studentName} '
      '${translate(isConnected ? Translations.connected : Translations.disconnected)}';

  @override
  void initState() {
    _controller = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this);

    super.initState();
    _animate();
  }

  @override
  void deactivate() {
    _controller.dispose();
    super.deactivate();
  }

  Future<void> _animate() async {
    await _controller.forward();
    await Future.delayed(Duration(milliseconds: 2000));
    await _controller.reverse();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: FadeTransition(
        opacity: _controller,
        child: Container(
          color: isConnected ? Colors.green.withAlpha(196) : Colors.red.withAlpha(196),
          width: Get.size.width,
          height: 40,
          alignment: Alignment.centerLeft,
          padding: const EdgeInsets.only(left: 16),
          child: Text(message,
            style: TextStyle(color: Colors.white, fontSize: 14),
          ),
        ),
      ),
    );
  }
}
