import 'package:flutter/material.dart';
import 'package:tutor/focused.dart';

class TintedButton extends StatelessWidget {
  const TintedButton({
    Key key,
    @required this.onTap,
    @required this.icon,
    this.size = 84.0,
    this.color = UIConstants.backgroundColor,
    this.iconAngle = 0.0,
  }) : super(key: key);

  final Function onTap;
  final String icon;
  final Color color;
  final double size;
  final double iconAngle;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      decoration: ShapeDecoration(
        color: UIConstants.defaultTextColor.withOpacity(0.5),
        shape: CircleBorder(),
      ),
      child: IconButton(
        icon: Transform.rotate(
            angle: iconAngle,
            child: SvgWidget.asset(icon, height: size, color: color,)
        ),
        onPressed: onTap,
      ),
    );
  }
}
