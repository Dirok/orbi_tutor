// import 'dart:html' if (dart.library.io) 'package:universal_io/io.dart';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get_utils/src/platform/platform.dart';

typedef WidgetBuilder = Widget Function(BuildContext context);

class SvgWidget {
  static Widget asset(
    String assetName, {
    Key key,
    bool matchTextDirection = false,
    AssetBundle bundle,
    String package,
    double width,
    double height,
    BoxFit fit = BoxFit.contain,
    Alignment alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder placeholderBuilder,
    Color color,
    BlendMode colorBlendMode = BlendMode.srcIn,
    String semanticsLabel,
    bool excludeFromSemantics = false,
    Clip clipBehavior = Clip.hardEdge,
  }) {

    return GetPlatform.isMobile
        ? SvgPicture.asset(assetName,
            key: key,
            matchTextDirection: matchTextDirection,
            bundle: bundle,
            package: package,
            width: width,
            height: height,
            fit: fit,
            alignment: alignment,
            allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
            placeholderBuilder: placeholderBuilder,
            color: color,
            colorBlendMode: colorBlendMode,
            semanticsLabel: semanticsLabel,
            excludeFromSemantics: excludeFromSemantics,
            clipBehavior: clipBehavior)
        : SvgPicture.network("/assets/$assetName",
            width: width,
            height: height,
            fit: fit,
            color: color,
            alignment: alignment);
  }
}
