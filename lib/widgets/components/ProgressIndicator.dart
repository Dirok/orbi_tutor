import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

Widget getProgressIndicator() => _getProgressIndicator();

// TODO what bottom 100 ???
Widget getNewProgressIndicator() => _getProgressIndicator(padding: EdgeInsets.only(bottom: 100));

Widget _getProgressIndicator({EdgeInsetsGeometry padding}) => Container(
    padding: padding,
    width: Get.size.width,
    height: Get.size.height,
    color: Colors.transparent,
    child: Center(
        child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(UIConstants.primaryDarkColor))
    ));

class UploadCircleProgressIndicator extends TranslatedStatelessWidget {
  final int progress;

  UploadCircleProgressIndicator({@required this.progress});

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Stack(
              alignment: AlignmentDirectional.center,
              children: [
                _getBoldText('$progress %'),
                _getCircleProgressIndicator(context, progress),
              ]
            ),
            const SizedBox(height: 8.0),
            _getHeaderText(context, progress),
          ]
        ),
      ),
    );
  }

  _getCircleProgressIndicator(BuildContext context, int progress) => SizedBox(
    height: 50.0,
    width: 50.0,
    child: CircularProgressIndicator(
      value: progress == 100 ? null : progress / 100,
      valueColor: progress == 100
          ? AlwaysStoppedAnimation<Color>(Colors.green)
          : AlwaysStoppedAnimation<Color>(UIConstants.primaryDarkColor),
      strokeWidth: 8.0,
    ),
  );

  _getHeaderText(BuildContext context, int progress) {
    if (progress == 0)
      return _getBoldText(translate(Translations.preparing));
    else if (progress == 100)
      return _getBoldText(translate(Translations.finishing));
    else
      return _getBoldText(translate(Translations.sending));
  }

  _getBoldText(String textKey) => Text(
    textKey,
    style: TextStyle(
        color: UIConstants.defaultTextColor,
        fontSize: 12,
        fontFamily: UIConstants.fontFamily,
        fontWeight: FontWeight.w600,
        decoration: TextDecoration.none),
  );
}

class DownloadLineProgressIndicator extends TranslatedStatelessWidget {
  final int progress;

  DownloadLineProgressIndicator({@required this.progress});

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: [
          LinearProgressIndicator(
            value: progress.toDouble(),
            valueColor: progress == 100
                ? AlwaysStoppedAnimation<Color>(Colors.green)
                : AlwaysStoppedAnimation<Color>(UIConstants.primaryDarkColor),
          ),
          Center(
            child: Text('$progress %')
          ),
        ],
      ),
    );
  }
}