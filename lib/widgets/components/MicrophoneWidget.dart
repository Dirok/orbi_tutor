import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:rive/rive.dart';
import 'package:tutor/focused.dart';

class MicrophoneWidget extends StatefulWidget {
  final RxDouble volume;

  const MicrophoneWidget(this.volume);

  @override
  _MicrophoneWidgetState createState() => _MicrophoneWidgetState();
}

class _MicrophoneWidgetState extends State<MicrophoneWidget> {
  Artboard _riveArtboard;
  SMIInput<double> _progress;

  @override
  void initState() {
    super.initState();
    rootBundle.load('assets/animations/microphone.riv').then(
      (data) async {
        final file = RiveFile.import(data);
        final artboard = file.mainArtboard;
        final controller =
            StateMachineController.fromArtboard(artboard, 'StateM');
        if (controller == null) {
          Log.error('Animation controller is null');
          return;
        }

        artboard.addController(controller);
        _progress = controller.findInput('Progress');
        setState(() {
          _riveArtboard = artboard;
        });
        widget.volume.listen((event) {
          setValue(20, (event.clamp(0.1, 1) * 100).roundToDouble());
        });
      },
    );
  }

  void setValue(double from, double to) {
    _progress?.value = from;
    Timer.periodic(Duration(milliseconds: 100), (timer) {
      if (mounted) {
        from += 5;
        _progress.change(from);
        if (_progress.value >= to) timer?.cancel();
      } else
        timer?.cancel();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,
      width: 48,
      padding: const EdgeInsets.all(9),
      child: _riveArtboard == null
          ? const SizedBox.shrink()
          : Rive(artboard: _riveArtboard),
    );
  }
}
