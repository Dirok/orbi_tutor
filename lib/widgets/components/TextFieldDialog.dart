import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

Future<String> showTextFieldDialog({
  String title,
  String message = '',
  String text = '',
  String hint,
  TextInputAction textInputAction = TextInputAction.done,
  Map<String, void Function(String)> variants = const {'OK': null},
  barrierDismissible = true,
  int maxLines = 1,
  int maxLength
}) async {

  String result = text;

  final buttons = variants.toList((text, action) => TextButton(
    child: Text(text),
    onPressed: () {
      Get.back();
      action?.call(result);
    },
  ));

  final dialog = SimpleDialog(
    insetPadding: MediaQuery.of(Get.context).viewInsets,
    title: Text(title, style: TextStyle(fontFamily: UIConstants.fontFamily)),
      children: <Widget>[
        if (message.isNotEmpty)
          Container(
            margin: EdgeInsets.only(left: 20.0),
            child: Text(
              message,
              style: TextStyle(
                fontSize: UIConstants.defaultFontSize,
                fontFamily: UIConstants.fontFamily,
              ))),
        Container(
          width: Get.width / 2,
          margin: EdgeInsets.only(left: 20.0, right: 20.0),
          child: CustomTextEdit(
            text: text,
            hint: hint,
            onTextChanged: (v) => result = v,
            maxLines: maxLines,
            maxLength: maxLength,
            isSpaceAvailable: true,
            textInputAction: TextInputAction.done),
        ),
        Container(
          margin: EdgeInsets.only(right: 20.0),
          child: Wrap(
            alignment: WrapAlignment.end,
            children: buttons,
          ),
        ),
      ]);

  return Get.dialog(dialog, barrierDismissible: barrierDismissible);
}
