import 'package:flutter/material.dart';
import 'package:tutor/focused.dart';

class AuthButton extends TranslatedStatelessWidget {
  final String textKey;
  final Widget icon;
  final Function onPressed;

  AuthButton({@required this.textKey, @required this.icon, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Center(
        child: OutlinedButton.icon(
          style: OutlinedButton.styleFrom(
            minimumSize: Size(260.0, 48.0),
            alignment: Alignment.centerLeft,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0),
            ),
            side: BorderSide(width: 1, color: UIConstants.primaryDarkColor),
            primary: UIConstants.primaryDarkColor,
          ),
          icon: icon,
          label: Text(translate(textKey),
            style: TextStyle(
              fontSize: UIConstants.buttonFontSize,
              color: UIConstants.primaryDarkColor)),
          onPressed: onPressed,
        )));
  }
}