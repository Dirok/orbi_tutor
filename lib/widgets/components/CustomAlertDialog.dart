import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

Future<void> showAlertDialog(String title, String message,
    {Map<String, Function> variants = const {'OK': null}, barrierDismissible = true}) {

  final buttons = variants.toList((text, action) =>
      TextButton(
          child: Text(text),
          onPressed: () {
            Get.back();
            action?.call();
          }));

  final dialog = AlertDialog(
    title: Text(title),
    content: Text(message),
    actions: buttons,
  );

  return Get.dialog(dialog, barrierDismissible: barrierDismissible);
}

Future<void> showAlertSelector(
    String title, Map<String, Function> variants, {barrierDismissible = true,}) {

  final options = variants.toList((text, action) =>
      SimpleDialogOption(
          child: Padding(
              padding: EdgeInsets.only(top: 8.0),
              child: Text(text, style: TextStyle(fontSize: 18.0))),
          onPressed: () {
            Get.back();
            action?.call();
          }));

  final dialog = SimpleDialog(
    title: Text(title, style: TextStyle(fontWeight: FontWeight.w600)),
    children: options,
  );

  return Get.dialog(dialog, barrierDismissible: barrierDismissible);
}

Future<void> showCustomAlertDialog(String title, Widget content, [List<Widget> actions]) {
  final dialog = AlertDialog(
    title: Text(title),
    content: content,
    actions: actions,
  );

  return Get.dialog(dialog);
}

Future<void> showSnackbar(String title, String body, Function(GetBar) callback) async =>
    Get.showSnackbar(
      GetBar(
        snackPosition: SnackPosition.TOP,
        title: title,
        duration: Duration(seconds: Constants.snackbarDurationSeconds),
        message: body,
        onTap: callback,
      ),
    );
