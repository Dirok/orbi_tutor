import 'package:flutter/material.dart';
import 'package:tutor/focused.dart';

class NoConnectionWidget extends TranslatedStatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.red.withAlpha(196),
      margin: EdgeInsets.only(bottom: 32.0),
      padding: EdgeInsets.all(8.0),
      height: 40,
      child: Center(
        child: Text(
            translate(Translations.no_internet_connection),
            textDirection: TextDirection.ltr,
            style: TextStyle(color: Colors.white, fontSize: 14)),
      ),
    );
  }
}