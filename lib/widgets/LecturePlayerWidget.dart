import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class LecturePlayerWidget extends StatelessWidget with TranslatableMixin {
  final DrawingViewModel drawingController = DrawingViewModel()..drawingEnabled.value = false;

  final LecturePlayerViewModel _vm = Get.put(LecturePlayerViewModel());

  @override
  Widget build(BuildContext context) {
    // var args = ModalRoute.of(context).settings.arguments;
    selfScreenSize ??= ScreenSize.fromSize(MediaQuery.of(context).size);
    return WillPopScope(
        onWillPop: () async => true,
        child: Stack(
            children: [
              _getBody(),
              _getProgressFutureBuilder()
            ])
    );
  }

  Widget _getBody() => Scaffold(
      extendBodyBehindAppBar: true,
      body: Obx(() => Stack(children: [
        _getWhiteboardWidget(),
        _getNameWidget()
      ])),
    floatingActionButton: _getFloatingActionButton(),
  );

  Widget _getWhiteboardWidget() => Center(
      child: WhiteboardWidget(_vm.currentWhiteboard, null, null));

  Widget _getNameWidget() => Align(
        alignment: Alignment.topLeft,
        child: Padding(
            padding: EdgeInsets.zero,
            child: SafeArea(
                child: Text(_vm.ownerName.value,
                    style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.w700,
                        color: UIConstants.defaultTextColor)
                )
            )
        )
    );

  Widget _getProgressFutureBuilder() =>
      Obx(() => FutureBuilder<void>(
        future: _vm.initializeFuture.value,
        builder: (context, snapshot) =>
        snapshot.connectionState == ConnectionState.done
            || snapshot.connectionState == ConnectionState.none
            ? SizedBox()
            : getProgressIndicator()
      ));

  Widget _getFloatingActionButton() =>
      Obx(() => _vm.isPlayed.value ? _getStopButton() : _getPlayButton());

  FloatingActionButton _getPlayButton() => FloatingActionButton(
      child: SvgWidget.asset(Assets.play_icon, color: Colors.white),
      backgroundColor: UIConstants.purpleIconColor,
      onPressed: () => _vm.play()
  );

  FloatingActionButton _getStopButton() => FloatingActionButton(
      child: SvgWidget.asset(Assets.stop_icon, color: Colors.white),
      backgroundColor: UIConstants.purpleIconColor,
      onPressed: () => _vm.stop()
  );
}