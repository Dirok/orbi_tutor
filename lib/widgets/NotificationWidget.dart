import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

class NotificationWidget extends TranslatedStatelessWidget {
  @override
  Widget build(BuildContext context) {
    final args = Get.arguments as Map;

    return Material(
      child: Container(
        color: UIConstants.primaryLightColor,
        padding: const EdgeInsets.all(24),
        child: Stack(
          children: [
            Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    args?.containsKey('title') ?? false && args['title'] != null
                        ? args['title']
                        : 'Error, please restart app',
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: UIConstants.defaultTextColor,
                    ),
                  ),
                  const SizedBox(height: 16),
                  Text(
                    args?.containsKey('body') ?? false && args['body'] != null ? args['body'] : '',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: UIConstants.primaryDarkColor,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 16),
                  Container(
                    constraints: BoxConstraints.tightFor(width: 240, height: 48),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: UIConstants.primaryDarkColor,
                          onPrimary: UIConstants.buttonTextColor,
                          shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(Radius.circular(50)))),
                      child: Text(args?.containsKey('button') ?? false && args['button'] != null
                          ? args['button']
                          : 'Ok'),
                      onPressed: args?.containsKey('action') ?? false && args['action'] != null
                          ? args['action']
                          : exit(0),
                    ),
                  ),
                ],
              ),
            ),
            if (Settings.server.isDebug)
              Align(
                alignment: Alignment.topLeft,
                child: Text('${Settings.server.name} server',
                    style: TextStyle(color: Colors.redAccent, fontSize: 24)),
              ),
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                  icon: SvgWidget.asset(Assets.bug_icon, width: UIConstants.iconSize),
                  color: UIConstants.primaryDarkColor,
                  onPressed: () => Get.toNamed(Routes.sendRequest)),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Text('v.${PlatformUtils.getFullVersion()}',
                  style: TextStyle(color: UIConstants.primaryDarkColor)),
            ),
          ],
        ),
      ),
    );
  }
}
