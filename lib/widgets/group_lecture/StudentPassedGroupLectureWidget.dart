import 'dart:core';

import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class StudentPassedGroupLectureWidget extends PassedGroupLectureWidget {
  final _vm = Get.find<StudentPassedGroupLectureViewModel>();

  @override
  StudentPassedGroupLectureViewModel get vm => _vm;

  @override
  getFloatingActionButton() => null;
}