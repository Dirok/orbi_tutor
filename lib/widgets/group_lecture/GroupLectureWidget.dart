import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

abstract class GroupLectureWidget extends LectureWidget {
  GroupLectureViewModel get vm;

  Widget getAdditionalWidget() => SizedBox.shrink();

  @override
  Widget getNameWidget() {
    return Align(
        alignment: Alignment.topLeft,
        child: Padding(
            padding: EdgeInsets.zero,
            child: SafeArea(
                child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Obx(() => IconButton(
                          icon: SvgWidget.asset(
                              vm.isSelfWhiteboardSetActive.value ? Assets.user_whiteboard_icon : Assets.home_whiteboard_icon,
                              color: vm.allowNavigateWorkspaces.value ? UIConstants.primaryDarkColor : UIConstants.primaryLightColor),
                          onPressed: vm.allowNavigateWorkspaces.value ? vm.changeWhiteboardsSource : null
                      )),
                      Obx(() => SizedBox(
                           width: Get.size.width * UIConstants.speakingUserWidthFactor,
                           child: Text(
                             vm.ownerName.value.overflow,
                             maxLines: 1,
                             overflow: TextOverflow.ellipsis,
                             style: TextStyle(
                                 fontSize: 18.0,
                                 fontWeight: FontWeight.w700,
                                 color: UIConstants.defaultTextColor),
                             ),
                      )),
                ]
                )
            )
        )
    );
  }
}