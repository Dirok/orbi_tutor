import 'dart:async';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class TeacherActiveGroupLectureWidget extends ActiveGroupLectureWidget with SnapshotableMixin {
  final _vm = Get.find<TeacherActiveGroupLectureViewModel>();

  @override
  ActiveGroupLectureViewModel get vm => _vm;

  TeacherActiveGroupLectureWidget() : super() {
    eventHandlers.addAll({
      ServerOperationErrorEvent: (e) => _onServerOperationError(e)
    });
  }

  @override
  List<Widget> getButtons() => [
    getButton(Assets.plus_icon, _vm.addMainWhiteboard),
    Obx(() => getButton(_vm.hasAudio.value ? Assets.mic_on_icon : Assets.mic_off_icon, _vm.changeMicState)),
    getButton(Assets.image_icon, _vm.addImageWithProgress),
    if (!GetPlatform.isWeb)
      getButton(Assets.photo_icon, takePhoto),
    Obx(() => getButton(Assets.pen_icon, showColorMenu, color: _vm.drawingController.color.value)),
    Obx(() => getButton(Assets.thickness_icon, showStrokeWidthMenu, color: _vm.drawingController.color.value)),
    getButton(Assets.revert_icon, _vm.undoDrawing),
    getButton(Assets.options_icon, _showOptions),
  ];

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Center(
          child: super.build(context),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: IgnorePointer(
            child: Container(
              color: Colors.transparent,
              child: Obx(
                () => _vm.studentConnectionInfoViewModel.value == null
                    ? const SizedBox.shrink()
                    : StudentConnectionInfoWidget(
                        _vm.studentConnectionInfoViewModel.value,
                      ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  _showOptions() {
    return showModalBottomSheet(
        context: Get.context,
        builder: (BuildContext bc) {
          return ListView(
              padding: EdgeInsets.all(8.0),
              shrinkWrap: true,
              children: [
                _getSnapshotOption(),
                // _getInstallOnHolderOption(),
                _getSettingsOption(),
                if (_vm.isActiveLecture) _getCheckingModeOption(),
                _getExitOption()
              ]
          );
        }
    );
  }

  ListTile _getSnapshotOption() => ListTile(
      leading: SvgWidget.asset(Assets.share_icon, width: buttonSize, color: UIConstants.primaryDarkColor),
      title: Text(translate(Translations.share_snapshot)),
      onTap: () async {
        Get.back();
        final result = await generateWhiteboardSnapshot(snapshotGlobalKey.currentContext);
        if (!result) {
          await showAlertDialog(
              translate(Translations.failure), translate(Translations.cant_share_snapshot));
        }
      }
  );

  // ListTile _getInstallOnHolderOption() => ListTile(
  //     leading: Padding(
  //         padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
  //         child: SvgWidget.asset(
  //             Assets.install_on_holder_icon,
  //             width: buttonSize,
  //             color: UIConstants.primaryDarkColor
  //         )
  //     ),
  //     title: Text(translate(Translations.install_on_holder)),
  //     onTap: () => Get.offNamed(Routes.installOnHolder)
  // );

  ListTile _getSettingsOption() => ListTile(
      leading: Padding(
          padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
          child: SvgWidget.asset(
              Assets.settings_icon,
              width: buttonSize,
              color: UIConstants.primaryDarkColor
          )
      ),
      title: Text(translate(Translations.settings)),
      onTap: () => Get.offNamed(Routes.settings)
  );

  ListTile _getCheckingModeOption() => ListTile(
      leading: SvgWidget.asset(
          Assets.stop_icon,
          width: buttonSize,
          color: UIConstants.primaryDarkColor
      ),
      title: Text(translate(Translations.finish_lecture_for_students)),
      onTap: () async {
        await showAlertDialog(
            translate(Translations.finish_lecture_for_students),
            translate(Translations.want_to_finish),
            variants: {
              translate(Translations.yes): _vm.setLectureCheckingMode,
              translate(Translations.no): null
            });
        Get.back();
      }
  );

  ListTile _getExitOption() => ListTile(
      leading: SvgWidget.asset(
          Assets.close_icon,
          width: buttonSize,
          color: UIConstants.primaryDarkColor
      ),
      title: Text(translate(Translations.exit)),
      onTap: () async => _vm.isOnline.value ? await _exitLectureVariants() : await exitLecture()
  );

  Future<void> _exitLectureVariants() {
    return showAlertSelector(translate(Translations.exit), {
      translate(Translations.finish_lecture_exit): () async {
        await _vm.exitLectureWithFinish();
        await close();
      },
      translate(Translations.exit_lecture_without_finish): () async {
        await _vm.exitLectureWithoutFinish();
        await close();
      }
    });
  }

  @override
  getFloatingActionButton() =>
      Obx(() => _vm.isOnline.value ? _getStudentsListButton() : _getStartLectureButton());

  Widget _getStudentsListButton() => Obx(() => FloatingActionButton(
      child: Stack(children:[
        SvgWidget.asset(Assets.students_list_icon),
        if (_vm.raisedHandsCount.value > 0)
          _getHandsCountWidget()
      ]),
      backgroundColor: UIConstants.purpleIconColor,
      onPressed: showStudentList
  ));

  Widget _getHandsCountWidget() => Align(
      alignment: Alignment.topRight,
      child: Container(
          width: 24.0,
          height: 24.0,
          alignment: Alignment.center,
          padding: EdgeInsets.zero,
          decoration: ShapeDecoration(
              color: UIConstants.redLogoColor,
              shape: CircleBorder()),
          child: Text(_vm.raisedHandsCount.value.toString(),
              textAlign: TextAlign.center,
              style: TextStyle(color: UIConstants.buttonTextColor, fontSize: 10.0))
      )
  );

  Widget _getStartLectureButton() => FloatingActionButton(
      child: SvgWidget.asset(Assets.play_icon, color: Colors.white),
      backgroundColor: UIConstants.purpleIconColor,
      onPressed: () async {
        if (_vm.canStartLecture) {
          await showAlertDialog(_vm.subjectName,
              translate(Translations.want_start_lecture),
              variants: {
                translate(Translations.yes): _tryStartLecture,
                translate(Translations.no): null});
        } else {
          await showAlertDialog(_vm.subjectName,
              translate(Translations.you_cannot_teach_more_than_two_lecture));
        }
      });

  Future<void> _tryStartLecture() async {
    if (_vm.checkConnectivityAndNotify()) {
      await _vm.tryStartLecture();
    } else {
      await showAlertDialog(translate(Translations.error),
          translate(Translations.no_internet_connection));
    }
  }

  _onServerOperationError(ServerOperationErrorEvent e) =>
      showAlertDialog(translate(Translations.error), translate(e.errorMessage));
}