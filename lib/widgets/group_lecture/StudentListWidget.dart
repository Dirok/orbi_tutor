import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class StudentListWidget extends StatelessWidget with TranslatableMixin {
  final IStudentListViewModel _vm;

  StudentListWidget(IStudentListViewModel vm) : _vm = vm;

  @override
  Widget build(BuildContext context) {
    if (_vm.studentViewModelList.length == 0)
      return _getNoStudentsMessage(context);

    return Stack(
      children: [
        Padding(
            padding: EdgeInsets.only(top: 24.0),
            child: context.isTablet
                ? _getDoubleColumnStudentList(context)
                : _getSingleColumnStudentList(context)),
        _getCloseBottomSheetButton(context),
      ],
    );
  }

  ListTile _getNoStudentsMessage(BuildContext context) {
    return ListTile(
      leading: SvgWidget.asset(
          Assets.user_whiteboard_icon,
          color: UIConstants.primaryDarkColor,
          width: 36.0),
      title: Text(
          '${translate(Translations.no_students)} ${_vm.studentViewModelList}'),
    );
  }

  Widget _getCloseBottomSheetButton(BuildContext context) {
    return Align(
        alignment: Alignment.topCenter,
        child: IconButton(
            alignment: Alignment.topCenter,
            padding: EdgeInsets.only(
              left: 100,
              right: 100,
              bottom: 32,
            ),
            color: Colors.transparent,
            icon: Transform.rotate(
              angle: -pi / 2,
              child: SvgWidget.asset(Assets.back_arrow_icon, height: 24.0),
            ),
            onPressed: () => Get.back()));
  }

  Widget _getDoubleColumnStudentList(BuildContext context) {
    return Obx(() => GridView.count(
          crossAxisCount: 2,
          shrinkWrap: true,
          childAspectRatio: (context.width / 2.0) / 48.0,
          children: List.generate(_vm.studentViewModelList.length,
              (i) => _createStudentListElement(i)),
        ));
  }

  Widget _getSingleColumnStudentList(BuildContext context) {
    return Obx(() => ListView.builder(
        padding: const EdgeInsets.all(8),
        shrinkWrap: true,
        itemCount: _vm.studentViewModelList.length,
        itemBuilder: (_, int i) => _createStudentListElement(i)));
  }

  _createStudentListElement(int index) {
    final userModel = _vm.studentViewModelList[index];
    return _vm.isActiveLecture
        ? StudentListElementActiveGroupLectureWidget(userModel)
        : StudentListElementPassedGroupLectureWidget(userModel);
  }
}
