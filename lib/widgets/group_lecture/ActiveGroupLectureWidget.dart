import 'dart:async';
import 'dart:core';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

abstract class ActiveGroupLectureWidget<T extends ActiveGroupLectureViewModel> extends GroupLectureWidget
    with WidgetsBindingObserver, MultimediaLectureWidgetMixin<T> {
  T get vm;

  ActiveGroupLectureWidget() : super() {
    eventHandlers.addAll({
      ImageTooLargeErrorEvent: (_) => _onImageTooLargeError(),
      SendImageFailedEvent: (_) => _onSendImageFailedEvent(),
      LectureDestroyedEvent: (_) => _onLectureDestroyedEvent(),
    });
    WidgetsBinding.instance.addObserver(this);
  }

  Future<void> _onImageTooLargeError() => showAlertDialog(
      translate(Translations.failure), translate(Translations.image_is_too_large));

  Future<void> _onSendImageFailedEvent() => showAlertDialog(
      translate(Translations.failure), translate(Translations.send_image_failed));

  Future<void> _onLectureDestroyedEvent() => showAlertDialog(
      translate(Translations.lecture_finished),
      translate(Translations.lecture_auto_finished),
      variants: {translate(Translations.ok): close},
      barrierDismissible: false);

  @override
  Future<void> close() {
    WidgetsBinding.instance.removeObserver(this);
    return Get.offNamedUntil(
        Routes.scheduledLectures, ModalRoute.withName(Routes.mainMenu));
  }

  @override
  Widget build(BuildContext context) => super.build(context);

  @override
  Widget getAdditionalWidget() => Obx(() => Stack(
      children:[
        if (vm.speakingStudent.value.isNotEmpty)
          getSpeakingPerson(),
        if (vm.isOnline.value)
          _getPinWidget(),
        if (vm.isAudioInited.value || kIsWeb)
          getAudioDeviceWidget(),
        if (vm.isOnline.value && !vm.isConnects)
          _getNoConnectionMessage(),
      ]
  ));

  Widget _getNoConnectionMessage() => Obx(() {
      if (!vm.isConnectedToInternet.value) return SizedBox.shrink();

      if (!vm.isLectureServerConnected.value)
        return _getNoServerConnectionMessage(Translations.no_multimedia_server_connection);
      else if (!vm.isMultimediaServerConnected.value)
        return _getNoServerConnectionMessage(Translations.no_lecture_server_connection);
      else
        return SizedBox.shrink();
  });

  Widget _getNoServerConnectionMessage(String translationKey) => Align(
        alignment: Alignment.topCenter,
        child: Container(
            child: Text(
                translate(translationKey),
                style: TextStyle(color: Colors.white, fontSize: 14)),
            margin: EdgeInsets.all(16.0),
            padding: EdgeInsets.all(8.0),
            decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8.0))),
                color: Colors.red.withAlpha(196))
        )
    );

  Widget _getPinWidget() => Obx(() => Align(
      alignment: Alignment.topCenter,
      child: Padding(
          padding: EdgeInsets.zero,
          child: IconButton(
              icon: SvgWidget.asset(vm.isPinEnabled.value ? Assets.pin_on_icon : Assets.pin_off_icon),
              onPressed: () => vm.setPinState(!vm.isPinEnabled.value)
          )
      )
  ));

  Future<void> showStudentList() => showModalBottomSheet(
      context: Get.context,
      isScrollControlled: !Get.context.isTablet,
      builder: (_) => StudentListWidget(vm));
}