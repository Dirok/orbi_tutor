import 'dart:async';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class TeacherPassedGroupLectureWidget extends PassedGroupLectureWidget {
  final _vm = Get.find<TeacherPassedGroupLectureViewModel>();

  @override
  TeacherPassedGroupLectureViewModel get vm => _vm;

  @override
  getFloatingActionButton() => _getStudentsListButton();

  _getStudentsListButton() => FloatingActionButton(
      child: SvgWidget.asset(Assets.students_list_icon),
      backgroundColor: UIConstants.purpleIconColor,
      onPressed: _showStudentList
  );

  Future<void> _showStudentList() => showModalBottomSheet(
      context: Get.context,
      isScrollControlled: !Get.context.isTablet,
      builder: (_) => StudentListWidget(vm));
}