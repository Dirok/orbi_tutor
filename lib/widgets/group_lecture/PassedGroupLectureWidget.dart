import 'dart:async';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

abstract class PassedGroupLectureWidget extends GroupLectureWidget with SnapshotableMixin {

  @override
  List<Widget> getButtons() => [
    getButton(Assets.share_icon, _shareSnapshot, padding: EdgeInsets.only(top: 6.0, bottom: 6.0)),
    getButton(Assets.settings_icon, () => Get.toNamed(Routes.settings), padding: EdgeInsets.only(top: 6.0, bottom: 6.0)),
    getButton(Assets.close_icon, exitLecture)
  ];

  Future<void> _shareSnapshot() async {
    final result = await generateWhiteboardSnapshot(snapshotGlobalKey.currentContext);
    if (!result) {
      showAlertDialog(
          translate(Translations.failure), translate(Translations.cant_share_snapshot));
    }
  }

  @override
  Future<void> close() =>
      Get.offNamedUntil(Routes.passedLectures, ModalRoute.withName(Routes.mainMenu));
}