import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class StudentListElementPassedGroupLectureWidget extends StatelessWidget with TranslatableMixin {
  final StudentListElementViewModel _vm;

  StudentListElementPassedGroupLectureWidget(StudentListElementViewModel vm) : _vm = vm;

  @override
  Widget build(BuildContext context) => _getStudentListTile();

  ListTile _getStudentListTile() => ListTile(
    leading: SvgWidget.asset(Assets.eye_in_board_icon,
        color: _vm.hasWhiteboards.value ? UIConstants.primaryDarkColor : UIConstants.primaryLightColor),
    title: Text(_vm.name),
    onTap: _vm.hasWhiteboards.value ? _showWhiteboards : null,
  );

  void _showWhiteboards() {
    Get.back();
    _vm.openWhiteboards();
  }
}