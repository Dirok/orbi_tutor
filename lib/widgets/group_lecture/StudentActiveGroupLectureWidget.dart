import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class StudentActiveGroupLectureWidget extends ActiveGroupLectureWidget {
  final _vm = Get.find<StudentActiveGroupLectureViewModel>();

  @override
  StudentActiveGroupLectureViewModel get vm => _vm;

  StudentActiveGroupLectureWidget() : super() {
    eventHandlers[LectureFinishedEvent] = _onLectureFinished;
  }

  @override
  List<Widget> getButtons() => [
    Obx(() => getButton(Assets.plus_icon, _vm.isSelfWhiteboardSetActive.value && !_vm.isPinEnabled.value ? _vm.insertSelfWhiteboard : null)),
        Obx(
          () => getButton(
            _vm.hasAudio.value ? Assets.mic_on_icon : Assets.mic_off_icon,
            _vm.allowSpeak.value && !_vm.micStateChanging.value
                ? _vm.changeMicState
                : null,
            shimmer: _vm.micStateChanging.value,
          ),
        ),
    getButton(Assets.eye_in_board_icon, showStudentList),
    getButton(Assets.settings_icon, () => Get.toNamed(Routes.settings),
            padding: EdgeInsets.only(top: 6.0, bottom: 6.0)),
    Obx(() => getButton(Assets.image_icon, _vm.isSelfWhiteboardSetActive.value ? _vm.addImageWithProgress : null)),
    if (!GetPlatform.isWeb)
      Obx(() =>
          getButton(Assets.photo_icon, _vm.isSelfWhiteboardSetActive.value ? takePhoto : null)),
    Obx(() => getButton(Assets.pen_icon,
        _vm.isSelfWhiteboardSetActive.value ? showColorMenu : null,
        color: _vm.isSelfWhiteboardSetActive.value ? _vm.drawingController.color.value : null)),
    Obx(() => getButton(Assets.thickness_icon,
        _vm.isSelfWhiteboardSetActive.value ? showStrokeWidthMenu : null,
        color: _vm.isSelfWhiteboardSetActive.value ? _vm.drawingController.color.value : null)),
    Obx(() => getButton(Assets.revert_icon, _vm.isSelfWhiteboardSetActive.value ? _vm.undoDrawing : null)),
    getButton(Assets.close_icon, exitLecture),
  ];

  @override
  Future<void> close() async {
    await vm.closeMultimedia();
    return super.close();
  }

  @override
  getFloatingActionButton() => Obx(() => FloatingActionButton(
      child: SvgWidget.asset(Assets.raise_hand_icon, color: Colors.white, width: buttonSize),
      backgroundColor: _vm.isHandRaised.value ? UIConstants.redLogoColor : UIConstants.purpleIconColor,
      onPressed: _vm.raiseHand
  ));

  Future<void> _onLectureFinished(_) async {
    await _vm.disposeConnection();

    if (Get.isDialogOpen)
      Get.back();

    return showAlertDialog(
        translate(Translations.lecture_finished), translate(Translations.teacher_finished_lesson),
        variants: {translate(Translations.ok): close}, barrierDismissible: false);
  }
}