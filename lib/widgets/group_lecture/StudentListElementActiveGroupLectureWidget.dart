import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class StudentListElementActiveGroupLectureWidget extends StatelessWidget with TranslatableMixin {
  final StudentListElementActiveGroupLectureViewModel _vm;

  StudentListElementActiveGroupLectureViewModel get vm => _vm;

  StudentListElementActiveGroupLectureWidget(
      StudentListElementActiveGroupLectureViewModel vm) : _vm = vm;

  @override
  Widget build(BuildContext context) => _getStudentListTile();

  ListTile _getStudentListTile() => ListTile(
    contentPadding: EdgeInsets.only(left: 8.0, right: 8.0),
    leading: getLeadingWidget(),
    title: getTitleWidget());

  Widget getLeadingWidget() =>
      Obx(() => getCircleIcon(vm.isOnline.value ? Colors.green : Colors.red));

  Widget getTitleWidget() => getNameWidget();

  Widget getNameWidget() =>
      Text(vm.name.overflow, overflow: TextOverflow.ellipsis, maxLines: 1);

  Widget getCircleIcon(Color color) => Container(
    width: 16.0,
    height: 16.0,
    margin: EdgeInsets.all(16.0),
    decoration: ShapeDecoration(
        color: color,
        shape: CircleBorder()),
  );
}

class ControllableStudentListElementActiveGroupLectureWidget
    extends StudentListElementActiveGroupLectureWidget with TranslatableMixin {

  final ControllableStudentListElementActiveGroupLectureViewModel _vm;

  @override
  ControllableStudentListElementActiveGroupLectureViewModel get vm => _vm;

  ControllableStudentListElementActiveGroupLectureWidget(
      ControllableStudentListElementActiveGroupLectureViewModel vm): _vm = vm, super(vm);

  @override
  Widget getLeadingWidget() => Obx(() => vm.isOnline.value
      ? (vm.isHandRaised.value ? _getLowerHandButton() : getCircleIcon(Colors.green))
      : getCircleIcon(Colors.red));

  @override
  Widget getTitleWidget() => Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(child: getNameWidget()),
        _getButtons()
      ]);

  Widget _getButtons() =>
      Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
        Obx(_getWhiteboardButton),
        Obx(_getMicButton),
      ]);

  Widget _getLowerHandButton() => IconButton(
      padding: EdgeInsets.zero,
      icon: SvgWidget.asset(Assets.raise_hand_icon, width: 32.0),
      onPressed: vm.lowerHand);

  Widget _getWhiteboardButton() => !vm.hasWhiteboards.value ? SizedBox.shrink() : IconButton(
      padding: EdgeInsets.zero,
      icon: SvgWidget.asset(
        vm.isPresentedNow.value
            ? Assets.eye_in_board_icon
            : Assets.empty_board_icon,
        color: UIConstants.primaryDarkColor,
      ),
      onPressed: () {
        Get.back();
        vm.openWhiteboards();
      }
  );

  Widget _getMicButton() => !vm.isOnline.value
      ? const SizedBox.shrink()
      : vm.allowSpeak.value && vm.isMicEnabled.value
      ? GestureDetector(
    onTap: vm.setAllowSpeak,
    child: MicrophoneWidget(vm.volumeLevel),
  )
      : IconButton(
    onPressed: vm.setAllowSpeak,
    padding: EdgeInsets.zero,
    icon: SvgWidget.asset(
      vm.allowSpeak.value
          ? vm.isMicEnabled.value
          ? Assets.mic_on_icon
          : Assets.mic_off_icon
          : Assets.mic_off_student_icon,
    ),
  );
}