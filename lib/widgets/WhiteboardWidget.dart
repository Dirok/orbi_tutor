import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class WhiteboardWidget extends StatelessWidget {
  final _imageService = Get.find<ImageService>();

  final Size _surfaceSize = selfScreenSize.width / selfScreenSize.height <= Settings.whiteboardAspectRatio
      ? Size(selfScreenSize.width, selfScreenSize.width / Settings.whiteboardAspectRatio)
      : Size(selfScreenSize.height * Settings.whiteboardAspectRatio, selfScreenSize.height);

  final Rx<Whiteboard> _whiteboardObs;
  final DrawingViewModel _drawingController;
  final ILectureServerApi _lectureServer;

  WhiteboardWidget(Rx<Whiteboard> whiteboardObs, DrawingViewModel drawingController, ILectureServerApi lectureServer)
      : _whiteboardObs = whiteboardObs, _drawingController = drawingController, _lectureServer = lectureServer;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        // onScaleStart: (details) {
        //   print('scale start');
        // },
        // onScaleUpdate: (details) {
        //   print('scale update');
        // },
        // onScaleEnd: (details) {
        //   print('scale end');
        // },
        child: Stack(children: [_getSurfaceWidget(), _getDrawingWidget()])
    );
  }

  Widget _getSurfaceWidget() {
    return Center(
        child: Container(
            width: _surfaceSize.width,
            height: _surfaceSize.height,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: UIConstants.backgroundColor,
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3)),
              ],
            ),
            child: Obx(() => _getWhiteboardImage(_whiteboardObs.value))));
  }

  Widget _getDrawingWidget() =>
      DrawingWidget(_whiteboardObs, _drawingController, _lectureServer, _surfaceSize);

  Widget _getWhiteboardImage(Whiteboard wb) {
    if (wb?.image == null) return const SizedBox.shrink();

    final file = _imageService.getImageBytesFromLocalStorage(wb.image);
    if (file == null) return const SizedBox.shrink();

    return Image.memory(file, fit: BoxFit.contain, height: double.infinity);
  }
}
