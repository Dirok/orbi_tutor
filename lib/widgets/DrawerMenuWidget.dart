import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class DrawerMenu with TranslatableMixin {
  static DrawerMenu _instance;
  DrawerMenu._();
  factory DrawerMenu() => _instance ??= DrawerMenu._();
  final  _vm = DrawerMenuViewModel();

  Drawer getDrawer(BuildContext context) => Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          _getDrawerHeader(),
          if (!GetPlatform.isWeb)
            _getDrawerElement(context, Assets.install_on_holder_icon,
                Translations.install_on_holder, Routes.camera),
          _getDrawerElement(context, Assets.settings_icon, Translations.settings, Routes.settings),
          ListTile(
            leading: Icon(Icons.info_outline, color: UIConstants.purpleIconColor),
            title: Text(translate(Translations.about)),
            onTap: () async {
              showAboutDialog(context: context,
                  applicationName: 'I\'m focusED',
                  applicationVersion: 'v. ' + PlatformUtils.getFullVersion(),
                  applicationIcon: SvgWidget.asset(Assets.full_logo_image, height: 70.0, width: 70.0)
              );
            },
          ),
          const Divider(),
          ListTile(
              leading: Icon(Icons.exit_to_app, color: UIConstants.purpleIconColor),
              title: Text(translate(Translations.log_out)),
              onTap: () => showAlertDialog(appSession.account.user.shortName, translate(Translations.want_to_exit),
                  variants: {translate(Translations.yes): () async {
                    await _vm.logOut();
                    Get.offAllNamed(Routes.login);
                  }, translate(Translations.no): null})
          ),
          // AppSession.debugMode
          //   ? ListTile(
          //       leading: Icon(Icons.smoking_rooms),
          //       title: Text('Some debug function'),
          //       // onTap: () => _someDebugFunction(),
          //   )
          //   : SizedBox.shrink(),
        ],
      )
  );

  _getDrawerHeader() => Container(
        height: 100.0,
        child: DrawerHeader(
          decoration: BoxDecoration(color: UIConstants.primaryLightColor),
          padding: EdgeInsets.all(0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                  decoration: BoxDecoration(
                      color: UIConstants.primaryDarkColor,
                      shape: BoxShape.circle,
                      gradient: LinearGradient(
                        begin: Alignment.bottomCenter,
                        end: Alignment.topCenter,
                        colors: [
                          UIConstants.backgroundColor,
                          UIConstants.primaryDarkColor
                        ],
                        stops: [0.0, 0.7],
                      )),
                  margin: const EdgeInsets.only(left: 24, right: 16),
                  child: SvgWidget.asset(Assets.user_image,
                      width: 64.0, height: 64.0)),
              Expanded(
                child: Tooltip(
                  message: appSession.account.user.fullName,
                  child: Text(
                    appSession.account.user.surnameName.overflow,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(
                      color: UIConstants.defaultTextColor,
                      fontSize: 16.0,
                      fontFamily: UIConstants.fontFamily,
                      fontWeight: FontWeight.w700,
                      decoration: TextDecoration.none,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );

  _getDrawerElement(BuildContext context, String iconName, String textKey,
      String routeKey) =>
      ListTile(
          leading: SvgWidget.asset(iconName,
              color: UIConstants.purpleIconColor, width: 24.0, height: 24.0),
          title: Text(translate(textKey)),
          onTap: () => Navigator.popAndPushNamed(context, routeKey));

//   _testGetStudentLinks() async {
//     final List currSchools = await httpRest.getSchools(token: appSession.httpToken);
//     currSchools.forEach((element) {
//       Log.info(element);
//     });
//     final currSchool = currSchools[0];
//
//     final List classes = await httpRest.getClasses(token: appSession.httpToken, schoolID: currSchool["id"]);
//     classes.forEach((element) {
//       Log.info(element);
//     });
//     final currClass = classes[0];
//
//     final List students    = await httpRest.getStudents(token: appSession.httpToken, classID: currClass["id"]);
//     students.forEach((element) {
//       Log.info(element);
//     });
//
//     await httpRest.updateClassLinks(token: appSession.httpToken, classID: currClass["id"]);
//
//     final Map links = await httpRest.getClassLinks(token: appSession.httpToken, classID: currClass["id"]);
//     Log.info('links:');
//     for (int i = 0; i < students.length; i++) {
//       String name = students[i]['name'];
//       String surname = students[i]['surname'];
//       String current = name + ' ' + surname;
//       Log.info(current + ':  ' + 'https://iamfocused.io' + links[current].toString());
//     }
//   }
//
//   _testUpdateDebugLinks() async {
//     final List currSchools = await httpRest.getSchools(
//         token: appSession.httpToken);
//     final List classes = await httpRest.getClasses(
//         token: appSession.httpToken, schoolID: currSchools[0]["id"]);
//     final currClass = classes[1];
//
//     final List students = await httpRest.getStudents(
//         token: appSession.httpToken, classID: currClass["id"]);
//     students.forEach((element) {
//       Log.info(element);
//     });
//
//     final bool updated = await httpRest.updateClassLinks(
//         token: appSession.httpToken, classID: currClass["id"]);
//     if (updated) {
//       final Map links = await httpRest.getClassLinks(
//           token: appSession.httpToken, classID: currClass["id"]);
//       Log.info('links:');
//       for (int i = 0; i < students.length; i++) {
//         String name = students[i]['name'];
//         String surname = students[i]['surname'];
//         String current = name + ' ' + surname;
//         Log.info(current + ':  ' + 'https://imfocused.io' +
//             links[current].toString()); // TODO only debug links
//       }
//     }
//   }
//
//   _testUserCreation() async {
//     final List currSchools = await httpRest.getSchools(token: appSession.httpToken);
//     final currSchool = currSchools[0];
//
//     final List classes = await httpRest.getClasses(
//         token: appSession.httpToken, schoolID: currSchool["id"]);
//     final currClass = classes[0];
//
//     final List currDiscipline = await httpRest.getSchoolDisciplines(token: appSession.httpToken, schoolID: currSchool["id"]);
//
//     final Map response = await httpRest.createUser(
//         token: appSession.httpToken,
//         userName: 'Дарт',
//         userPatronymic: 'Викторович',
//         userSurname: 'Вейдер',
//         email: 'deadline@zadolbal.ru',
//         password: '1234567',
//         role: 'teacher');
//     final bool result = await httpRest.addUserToClass(token: appSession.httpToken, userID: response["account"]["id"], classID: currClass["id"]);
//
// //    await httpRest.addTeacherToDiscipline(token: appSession.httpToken, userID: "5f6f1eadcf938cb78d647256", disciplineID: currDiscipline.first["id"]);
// //     await httpRest.deleteUser(token: appSession.httpToken, userID: "5f6f1eadcf938cb78d647256");
//   }
//
//   _someDebugFunction() async {
//     // test_testUserCreation();
//     //_testGetStudentLinks();
//     _testUpdateDebugLinks();
//   }

}