import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

mixin SnapshotableMixin {
  Future<bool> generateWhiteboardSnapshot(BuildContext context) async {
    RenderRepaintBoundary boundary = context?.findRenderObject();
    if (boundary == null) {
      // probably there is no widgets for current whiteboard
      return false;
    }

    final image = await boundary.toImage(pixelRatio: 2.0);
    final byteData = await image.toByteData(format: ImageByteFormat.png);
    final pngBytes = byteData.buffer.asUint8List();

    File file;
    try {
      final tempDir = await getTemporaryDirectory();
      file = await new File('${tempDir.path}/snapshot.png').create();
      await file.writeAsBytes(pngBytes);
      await Share.shareFiles([file.path]);
    }
    catch (e) {
      Log.info("failed to write and share png : $e");
      return false;
    }
    finally {
      if (file != null) file.deleteSync();
    }

    return true;
  }
}