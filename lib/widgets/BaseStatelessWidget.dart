import 'package:tutor/focused.dart';

abstract class BaseStatelessWidget extends TranslatedStatelessWidget {
  BaseViewModel get vm;

  final Map<Type, Function(Event)> eventHandlers = Map();

  BaseStatelessWidget() {
    eventHandlers.addAll({
      NoNetworkEvent: (_) => _onNoNetworkEvent(),
    });
    vm.clearSubscriptions();
    vm.listenEvents((e) => eventHandlers[e.runtimeType]?.call(e));
  }

  Future<void> _onNoNetworkEvent() => showAlertDialog(
      translate(Translations.error), translate(Translations.no_internet_connection));
}
