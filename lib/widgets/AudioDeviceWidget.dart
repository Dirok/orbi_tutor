import 'package:audio_manager_plugin/AudioOutputDevice.dart';
import 'package:flutter/material.dart';
import 'package:tutor/focused.dart';

class AudioDeviceWidget extends TranslatedStatelessWidget {
  static const Map<AudioOutputDevice, String> _audioDeviceTranslationKeyMap = {
    AudioOutputDevice.receiver: Translations.receiver,
    AudioOutputDevice.speaker: Translations.speaker,
    AudioOutputDevice.headphones: Translations.headphones,
    AudioOutputDevice.bluetooth: Translations.bluetooth,
    AudioOutputDevice.unknown: Translations.unknown,
  };

  static const Map<AudioOutputDevice, IconData> _audioDeviceIconDataMap = {
    AudioOutputDevice.receiver: Icons.phone_in_talk,
    AudioOutputDevice.speaker: Icons.speaker_phone,
    AudioOutputDevice.headphones: Icons.headset,
    AudioOutputDevice.bluetooth: Icons.bluetooth_audio,
    AudioOutputDevice.unknown: Icons.device_unknown
  };

  final AudioDeviceViewModel _vm;

  AudioDeviceWidget(AudioDeviceViewModel vm) : _vm = vm;

  Future<void> _showAudioDevices() async {
    var devices = await _vm.getAudioDeviceList();
    Map<String, Function> variants = Map();
    for (var device in devices) {
      var deviceName = translate(_audioDeviceTranslationKeyMap[device]);
      variants[deviceName] = () => _vm.tryChangeAudioDevice(device);
    }
    return showAlertSelector(translate(Translations.select_audio_device), variants);
  }

  @override
  Widget build(BuildContext context) => IconButton(
      icon: Icon(
          _audioDeviceIconDataMap[_vm.selectedAudioDevice.value],
          color: UIConstants.primaryDarkColor),
      onPressed: _showAudioDevices
  );
}