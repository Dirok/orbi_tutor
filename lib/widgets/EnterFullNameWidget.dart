import 'dart:core';

import 'package:flutter/material.dart';
import 'package:tutor/focused.dart';

class EnterFullNameWidget extends TranslatedStatelessWidget {
  final TextEditingController _surnameController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _patronymicController = TextEditingController();

  _onConfirm() {
  }

  _onCancel() {
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: UIConstants.backgroundColor,
        body: Stack(children: [
          SizedBox.expand(
            child: Image.asset(Assets.enter_your_name_background,
            fit: BoxFit.fill),
          ),
          _getAppBar(context),
          Center(
            child: ListView(
                shrinkWrap: true,
                children: [
                  _getHeaderText(context),
                  const SizedBox(height: 32.0),
                  _getTextEdit(context, _surnameController, Translations.surname),
                  _getTextEdit(context, _nameController, Translations.name),
                  _getTextEdit(context, _patronymicController, Translations.patronymic),
                  const SizedBox(height: 16.0),
                  _getButton(context, UIConstants.primaryDarkColor, Translations.confirm, _onConfirm),
                  _getButton(context, UIConstants.redLogoColor, Translations.cancel, _onCancel),
                ]),
          )
        ])
    );
  }

  _getAppBar(BuildContext context) => AppBar(
    backgroundColor: Colors.transparent,
    elevation: 0.0,
//    leading: Builder(
//      builder: (BuildContext context) {
//        return IconButton(
//          icon: SvgWidget.asset(Assets.back_arrow_icon,
//              color: UIConstants.primaryDarkColor,
//              width: 24.0,
//              height: 24.0),
//          onPressed: () => Navigator.of(context).pop(),
//        );
//      },
//    ),
    actions: [
      Padding(
          padding: EdgeInsets.only(right: 16.0, top: 8),
          child: SvgWidget.asset(Assets.logo_image, width: 150))
    ],
  );

  _getHeaderText(BuildContext context) => Center(
    child: Text(
      translate(Translations.enter_your_name),
      style: TextStyle(
          color: UIConstants.defaultTextColor,
          fontSize: 20,
          fontFamily: UIConstants.fontFamily,
          fontWeight: FontWeight.w700,
          decoration: TextDecoration.none
      )
    )
  );

  _getTextEdit(BuildContext context, TextEditingController controller, String textKey) => Center(
      child: Container(
        constraints: BoxConstraints.tightFor(width: 300, height: 40),
        margin: EdgeInsets.all(4),
        decoration: ShapeDecoration(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          color:  UIConstants.backgroundColor,
          shadows: [new BoxShadow(
            offset: Offset(3.0, 3.0),
            color: Colors.grey.withOpacity(0.5),
            blurRadius: 5.0,
          )],
        ),
        child: TextField(
          controller: controller,
          textAlign: TextAlign.center,
          textAlignVertical: TextAlignVertical.top,
          decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(width: 0.0, color: Colors.transparent),
              ),
              labelText: translate(textKey),
              labelStyle: TextStyle(color: UIConstants.primaryDarkColor)
          ),
        )
    )
  );

  _getButton(BuildContext context, Color color, String textKey, Function onPressedAction) => Center(
    child: Container(
        constraints: BoxConstraints.tightFor(width: 250),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            padding: EdgeInsets.zero,
            primary: color,
            onPrimary: UIConstants.buttonTextColor,
            shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10)))
          ),
          child: Text(translate(textKey).toUpperCase()),
          onPressed: onPressedAction,
        )
    )
  );
}