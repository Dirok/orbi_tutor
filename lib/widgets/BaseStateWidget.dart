import 'package:flutter/widgets.dart';
import 'package:tutor/focused.dart';

abstract class BaseStateWidget<T extends StatefulWidget> extends TranslatedState<T> {
  BaseViewModel get vm;

  final Map<Type, Function(Event)> eventHandlers = Map();

  String translate(String key) => AppLocalizations.of(context).translate(key);

  BaseStateWidget() {
    eventHandlers.addAll({
      NoNetworkEvent: (_) => _onNoNetworkEvent(),
    });
    vm.listenEvents((e) => eventHandlers[e.runtimeType]?.call(e));
  }

  Future<void> _onNoNetworkEvent() => showAlertDialog(
      translate(Translations.error), translate(Translations.no_internet_connection));
}
