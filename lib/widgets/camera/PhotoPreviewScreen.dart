import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class PhotoPreviewScreen extends StatefulWidget {
  PhotoPreviewScreen() {
    if(Get.arguments != null) {
      imagePath = Get.arguments['path'];
      needSave = Get.arguments['needSave'];
    }
  }

  String imagePath;
  bool needSave;

  @override
  _PhotoPreviewScreenState createState() => _PhotoPreviewScreenState();
}

class _PhotoPreviewScreenState extends TranslatedState<PhotoPreviewScreen> {
  final _storageService = Get.find<StorageService>();

  String _imagePath;
  bool _needSave;

  @override
  initState() {
    _imagePath = widget.imagePath;
    _needSave = widget.needSave;
    super.initState();
  }

  Future<void> _saveImage(BuildContext context) async {
    final isGranted = GetPlatform.isIOS
        ? await PermissionsHelper().checkReadPhotosPermission(context)
        : await PermissionsHelper().checkStoragePermission(context);
    if (isGranted) {
      final result = await _storageService.saveImageToGallery(_imagePath);
      showAlertDialog(
          translate(result ? Translations.success : Translations.error),
          translate(result ? Translations.image_saved : Translations.image_not_saved));
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          Get.back(result: CameraPreviewResult.Cancel);
          return Future.value(false);
        },
        child: Material(
      child: Stack(
        children: [
          Center(
            child: Image.file(
              File(_imagePath),
              fit: BoxFit.fitHeight,
              height: double.infinity,
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: TintedButton(
              onTap: () => Get.back(result: CameraPreviewResult.Cancel),
              icon: Assets.close_white_icon,
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Container(
              margin: EdgeInsets.only(right: 8.0, top: 8.0),
              child: TintedButton(
                onTap: () => _saveImage(context),
                icon: Assets.download_icon,
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TintedButton(
                    onTap: () => Get.back(result: CameraPreviewResult.Retake),
                    icon: Assets.retake_icon,
                  ),
                  if (_needSave)
                    TintedButton(
                      onTap: () => Get.back(result: CameraPreviewResult.Accept),
                      icon: Assets.send_icon,
                    ),
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
