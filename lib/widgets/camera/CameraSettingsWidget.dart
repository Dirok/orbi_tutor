import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class CameraSettingsWidget extends TranslatedStatelessWidget {
  static const Map<CameraResolution, String> _cameraResolutionLabels = {
    CameraResolution.high: '720p (1280x720)',
    CameraResolution.veryHigh: '1080p (1920x1080)',
    CameraResolution.ultraHigh: '2160p (3840x2160)',
  };

  final _cameraResolution = Settings.cameraResolution.obs;

  final isChanged = false.obs;

  Future<void> displayCameraResolutionDialog() async {
    return showCustomAlertDialog(
        translate(Translations.camera_resolution),
        getCameraResolutionRadioList());
  }

  getCameraResolutionRadioList() {
    final listWidget = CameraResolution.values
        .map((cameraResolution) =>
            getCameraResolutionRadioListTitle(cameraResolution))
        .toList();
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: listWidget,
    );
  }

  Widget getCameraResolutionRadioListTitle(CameraResolution cameraResolution) {
    return Obx(
      () => RadioListTile<CameraResolution>(
        title: Text(_cameraResolutionLabels[cameraResolution]),
        value: cameraResolution,
        groupValue: _cameraResolution.value,
        onChanged: (CameraResolution value) async {
          Get.back();
          _cameraResolution.value = value;
          Settings.cameraResolution = value;
          isChanged.value = true;
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Get.back(result: isChanged.value);
        return false;
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: const Radius.circular(20),
            topRight: const Radius.circular(20),
          ),
          color: UIConstants.backgroundColor,
        ),
        height: 200,
        child: Column(
          children: [
            const SizedBox(height: 16),
            Obx(
              () => ListTile(
                  title: Text(translate(Translations.camera_resolution)),
                  subtitle: _cameraResolution?.value == null
                      ? null
                      : Text(_cameraResolutionLabels[_cameraResolution.value]),
                  onTap: () async => await displayCameraResolutionDialog()),
            ),
          ],
        ),
      ),
    );
  }
}
