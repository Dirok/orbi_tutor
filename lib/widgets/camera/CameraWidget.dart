import 'dart:async';
import 'dart:io';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:path/path.dart' show join;
import 'package:tutor/focused.dart';

class CameraWidget extends StatefulWidget {
  const CameraWidget({Key key}) : super(key: key);

  @override
  CameraWidgetState createState() => CameraWidgetState();
}

class CameraWidgetState extends TranslatedState<CameraWidget> {
  CameraWidgetState() {
    customImagePath = Get.arguments;
  }

  Camera _camera;

  bool _isRecognition = false;

  // Need to cache arguments as popping bottom sheet will overwrite them
  String customImagePath;

  CameraUIProvider get _cameraUIProvider => _camera as CameraUIProvider;

  Future<void> _initCamera() async {
    final isGranted = await PermissionsHelper().checkCameraPermission(context);
    if (!isGranted) {
      await showAlertDialog(
        translate(Translations.error),
        translate(Translations.need_camera_permission),
      );
      Get.back(result: CameraResult.Cancelled);
      return;
    }
    _camera ??= Camera();
    await _camera.init();
    //await _camera.startHandDetection();
    //wait _camera.startPaperDetection();
  }

  Future<void> _reInitCamera() async {
    await _camera?.dispose();
    _camera = null;
    await _initCamera();
    setState(() {});
  }

  @override
  void dispose() {
    _camera?.dispose();

    super.dispose();
  }

  _addPhoto(bool resetCache) async {
    if (_isRecognition) return;
    if (Settings.isNeedToSaveImages) {
      var isGranted = await PermissionsHelper().checkStoragePermission(context);
      if (!isGranted) return;
    }

    setState(() {
      _isRecognition = true;
    });

    final imagePath = customImagePath == null
        ? join(Settings.tmpFolderPath, '${DateTime.now()}.png')
        : customImagePath;

    // Need to reset the cache as Image.file is caching images
    final image = File(imagePath);
    if (await image.exists()) await image.delete();
    imageCache.evict(FileImage(image));

    final result = await _camera.takePicture(imagePath, resetCache);

    setState(() {
      _isRecognition = false;
    });

    switch (result) {
      case CameraResult.Success:
      case CameraResult.RecognitionError:
      case CameraResult.RecognitionCrash:
        final result = await Get.toNamed(Routes.photoPreview, arguments: {
          'path': imagePath,
          'needSave': customImagePath != null
        });
        if (result == CameraPreviewResult.Retake)
          return;
        else if (result == CameraPreviewResult.Cancel) {
          Get.back(result: CameraResult.Cancelled);
          return;
        }
        break;
      default:
        await showAlertDialog(
            translate(Translations.failure),
            translate(Translations.photo_capturing_error));
        break;
    }
    Get.back(result: result);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          Get.back(result: CameraResult.Cancelled);
          return Future.value(false);
        },
        child: Scaffold(
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.black,
          body: FutureBuilder<void>(
              future: _initCamera(),
              builder: (context, snapshot) {
                if (snapshot.connectionState != ConnectionState.done &&
                    snapshot.connectionState != ConnectionState.none)
                  return getProgressIndicator();

                return Stack(
                    children: [
                      if(_camera != null && _camera.isInitialized)
                        _getCameraPreviewWidget(),
                      _getCloseButton(),
                      _getCentralIndicator(),
                      _getRightButtonsGroup(),
                    ]);
              }),
          floatingActionButton: _getShootButton(),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        ));
  }

  Widget _getCameraPreviewWidget() => Center(
      child: AspectRatio(
          aspectRatio: _camera.aspectRatio,
          child: Transform(
              alignment: Alignment.center,
              child: Stack(
                  alignment: FractionalOffset.center,
                  children:[
                    _cameraUIProvider?.createPreviewWidget(),
                    VisualizerWidget(_cameraUIProvider?.visualizerController)
                  ]),
              transform: Matrix4.rotationX(
                  Settings.isCameraVerticalMirroringEnabled ? math.pi : 0)
          )
      )
  );

  Widget _getCloseButton() => Align(
    alignment: Alignment.topLeft,
    child: TintedButton(
      onTap: () => Get.back(result: CameraResult.Cancelled),
      icon: Assets.close_white_icon,
    ));

  Widget _getCentralIndicator() => Center(
      child: !_isRecognition
          ? SvgWidget.asset(Assets.camera_center_icon, width: 84.0)
          : getProgressIndicator());
  
  Widget _getRightButtonsGroup() => Align(
    alignment: Alignment(1, -1),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        _getSettingsButton(),
        _getMirroringButton(),
        _getRecognitionButton(),
      ],
    ));

  Widget _getSettingsButton() => TintedButton(
    onTap: _showBottomSettings,
    icon: Assets.settings_white_icon);

  Widget _getMirroringButton() => TintedButton(
    onTap: _changeVerticalMirroring,
    icon: Assets.mirror_vertical_icon,
    color: Settings.isCameraVerticalMirroringEnabled
        ? UIConstants.primaryDarkColor
        : UIConstants.backgroundColor,
    iconAngle: -math.pi / 2.0);

  Widget _getRecognitionButton() => TintedButton(
    onTap: changePaperRecognitionState,
    icon: Assets.paper_recognition_icon,
    color: Settings.isPaperRecognitionEnabled
        ? UIConstants.primaryDarkColor
        : UIConstants.backgroundColor);

  FloatingActionButton _getShootButton() => FloatingActionButton(
      child: SvgWidget.asset(
        Assets.photo_icon,
        color: Colors.white,
        width: 40.0,
      ),
      backgroundColor: UIConstants.purpleIconColor,
      onPressed: () async => await _addPhoto(true));

  void _showBottomSettings() async {
    final isSettingsChanged = await Get.bottomSheet(
      CameraSettingsWidget(),
      backgroundColor: Colors.transparent,
    );

    if (isSettingsChanged)
      _reInitCamera();
  }

  void _changeVerticalMirroring() => setState(() =>
      Settings.isCameraVerticalMirroringEnabled = !Settings.isCameraVerticalMirroringEnabled);

  void changePaperRecognitionState() => setState(() =>
      Settings.isPaperRecognitionEnabled = !Settings.isPaperRecognitionEnabled);
}
