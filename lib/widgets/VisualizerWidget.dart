import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vector_math/vector_math.dart' as vector_math;

class VisualizerController
{
  AnimationController _animationController;
  Animation<double> _animation;

  List<vector_math.Vector2> _targetPoints;
  List<vector_math.Vector2> _basePoints;
  int _imageWidth = -1, _imageHeight = -1;

  void setAnimation(AnimationController controller, Animation<double> animation) {
    _animationController = controller;
    _animation = animation;
    _animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        if (_targetPoints != null) {
          _basePoints = []..addAll(_targetPoints);
          _targetPoints = null;
          _animationController.forward(from: 0.0);
        } else {
          _basePoints = null;
          _targetPoints = null;
        }
      }
    });
    if (_targetPoints != null)
      _animationController.forward(from: 0.0);
  }

  void setTargetPoints(List<vector_math.Vector2> points, int imageWidth, int imageHeight) {
    _basePoints = getCurrentPoints();
    _targetPoints = points;
    _imageWidth = imageWidth;
    _imageHeight = imageHeight;
    if (_targetPoints != null)
      _animationController.forward(from: 0.0);
  }

  List<vector_math.Vector2> getTargetPoints() {
    return _targetPoints;
  }

  List<vector_math.Vector2> getCurrentPoints() {
    if (_basePoints == null)
      return (_targetPoints == null) ? null : ([]..addAll(_targetPoints));
    if (_targetPoints == null)
      return []..addAll(_basePoints);
    int size = math.min((_basePoints.length), _targetPoints.length);
    double t = _animation.value;
    double invT = 1.0 - t;
    return List.generate(size, (i) => _basePoints[i] * t + _targetPoints[i] * invT);
  }

  bool get basePointsAreExist => (_basePoints != null);
  bool get targetPointsAreExist => (_targetPoints != null);

  int get imageWidth => _imageWidth;
  int get imageHeight => _imageHeight;

  double get restAnimationTime => _animation.value;
}


class VisualizerWidget extends StatefulWidget {
  final VisualizerController _controller;

  VisualizerWidget(VisualizerController controller): _controller = controller;

  @override
  _VisualizerWidgetState createState() => _VisualizerWidgetState(this._controller);
}

class _VisualizerWidgetState extends State<VisualizerWidget> with TickerProviderStateMixin
{
  VisualizerController _controller;

  _VisualizerWidgetState(VisualizerController controller) {
    _controller = controller;
    AnimationController animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 200),
    );
    var tween = Tween(begin: 1.0, end: 0.0);
    Animation<double> animation = tween.animate(animationController)
      ..addListener(() {
        setState(() {});
      });
    _controller.setAnimation(animationController, animation);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned.fill(
        child: CustomPaint(
            painter: VisualizerPainter(this._controller)
        ));
  }
}

class VisualizerPainter extends CustomPainter {
  VisualizerController _controller;

  vector_math.Matrix4 _imageTransform;
  Color lineColor = Color.fromRGBO(255, 200, 150, 1);
  double drawCornerSize = 0.3;
  double drawSideSize = 0.3;

  VisualizerPainter(VisualizerController controller,
                    { double angle = 90.0,
                      bool horizontalFlip = false,
                      bool verticalFlip = false } ) {
    const double RAD2EULER = math.pi / 180.0;

    _controller = controller;
    _imageTransform = vector_math.Matrix4.rotationZ(RAD2EULER * angle);
    if (horizontalFlip || verticalFlip) {
       var T = vector_math.Matrix4.identity();
       if (horizontalFlip)
        T.setEntry(0, 0, -1.0);
       if (verticalFlip)
         T.setEntry(1, 1, -1.0);
       _imageTransform = T * _imageTransform;
    }
  }

  @override
  void paint(Canvas canvas, Size size) {
    List<vector_math.Vector2> points = _controller.getCurrentPoints();
    if (points == null)
      return;

    var centerT = _offsetToCenter(_controller.imageWidth * 0.5,
                                  _controller.imageHeight * 0.5);
    var T = vector_math.Matrix4.inverted(centerT) * _imageTransform * centerT;
    // do base transformation
    points = _transformPoint(points, T);

    // fit to screen
    var borderPoints = _getRectPoints(_controller.imageWidth,
                                      _controller.imageHeight);
    borderPoints = _transformPoint(borderPoints, T);
    var fitMatrix = _getAspectMatrix(_getBB(borderPoints), size);
    points = _transformPoint(points, fitMatrix);

    _drawEdges(canvas, points);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }

  vector_math.Matrix4 _offsetToCenter(double centerX, double centerY)
  {
    var T = vector_math.Matrix4.identity();
    T.setEntry(0, 3, - centerX);
    T.setEntry(1, 3, - centerY);
    return T;
  }

  List<vector_math.Vector2> _transformPoint(List<vector_math.Vector2> inPoints,
                                            vector_math.Matrix4 T) {
    const double eps = 1e-3;

    return List.generate(inPoints.length, (i) {
      final v2 = inPoints[i];
      final vt = T * vector_math.Vector4(v2.x, v2.y, 1.0, 1.0);

      if (vt.w.abs() < eps || vt.z.abs() < eps)
        return vector_math.Vector2(0.0, 0.0);

      double div = vt.z * vt.w;
      return vector_math.Vector2(vt.x / div, vt.y / div);
    });
  }

  List<vector_math.Vector2> _getRectPoints(int width, int height) {
    return [vector_math.Vector2(0.0, 0.0),
            vector_math.Vector2(width.toDouble(), 0.0),
            vector_math.Vector2(width.toDouble(), height.toDouble()),
            vector_math.Vector2(0.0, height.toDouble())];
  }

  vector_math.Aabb2 _getBB(List<vector_math.Vector2> inPoints) {
    double maxValue = 1e+4;
    var bbMin = vector_math.Vector2(maxValue, maxValue);
    var bbMax = vector_math.Vector2(-maxValue, -maxValue);
    for (int i = 0; i < inPoints.length; ++i) {
      var v = inPoints[i];
      if (v.x < bbMin.x)
        bbMin.x = v.x;
      if (v.y < bbMin.y)
        bbMin.y = v.y;
      if (v.x > bbMax.x)
        bbMax.x = v.x;
      if (v.y > bbMax.y)
        bbMax.y = v.y;
    }

    return vector_math.Aabb2.minMax(bbMin, bbMax);
  }

  vector_math.Matrix4 _getAspectMatrix(vector_math.Aabb2 bb,
      Size viewportSize) {
    var bbSize = bb.max - bb.min;
    double kX = viewportSize.width.toDouble() / bbSize.x;
    double kY = viewportSize.height.toDouble() / bbSize.y;
    double scale = math.min(kX, kY);
    var offset = vector_math.Vector2(viewportSize.width,
                                         viewportSize.height) * 0.5 -
                 bb.center * scale;
    vector_math.Matrix4 T = vector_math.Matrix4.identity();
    T.setEntry(0, 3, offset.x);
    T.setEntry(1, 3, offset.y);
    T.setEntry(0, 0, scale);
    T.setEntry(1, 1, scale);
    return T;
  }

  void _drawEdges(Canvas canvas, List<vector_math.Vector2> points) {
    Paint paint = Paint();
    paint.color = this.lineColor;
    if (_controller.basePointsAreExist && !_controller.targetPointsAreExist) {
      paint.strokeWidth = (3.0 * _controller.restAnimationTime);
    } else {
      paint.strokeWidth = 3;
    }
    double halfDrawCornerSize = this.drawCornerSize * 0.5;
    double halfDrawSideSize = this.drawSideSize * 0.5;
    for (int i = 0; i < 4; ++i) {
      vector_math.Vector2 p0 = points[(i + 3) % 4];
      vector_math.Vector2 p1 = points[i];
      vector_math.Vector2 p2 = points[(i + 1) % 4];

      var c1 = p1 + (p2 - p1) * halfDrawCornerSize;
      var c2 = p1 + (p0 - p1) * halfDrawCornerSize;

      canvas.drawLine(Offset(p1.x, p1.y), Offset(c1.x, c1.y), paint);
      canvas.drawLine(Offset(p1.x, p1.y), Offset(c2.x, c2.y), paint);

      var eA1 = p1 + (p2 - p1) * (0.5 - halfDrawSideSize);
      var eA2 = p1 + (p2 - p1) * (0.5 + halfDrawSideSize);

      var eB1 = p1 + (p0 - p1) * (0.5 - halfDrawSideSize);
      var eB2 = p1 + (p0 - p1) * (0.5 + halfDrawSideSize);

      canvas.drawLine(Offset(eA1.x, eA1.y), Offset(eA2.x, eA2.y), paint);
      canvas.drawLine(Offset(eB1.x, eB1.y), Offset(eB2.x, eB2.y), paint);
    }
  }
}