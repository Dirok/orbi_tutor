import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class SettingsWidget extends BaseStatelessWidget {
  final _vm = Get.find<SettingsViewModel>();

  @override
  BaseViewModel get vm => _vm;

  static const List<double> _cameraDelayValues = [0, 0.25, 0.5, 0.75, 1, 2, 3, 4];

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  SnackBar get _saveImagesSnackBar => SnackBar(
    content: Text(
      translate(Translations.image_will_saved_to_gallery),
      style: TextStyle(fontSize: 16)),
    backgroundColor: Colors.grey,
    duration: Duration(seconds: 2),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          translate(
            Translations.settings,
          ),
        ),
        actions: [
          Center(
            child: Padding(
              padding: const EdgeInsets.only(right: 16),
              child: Text(
                PlatformUtils.getFullVersion(),
              ),
            ),
          ),
        ],
      ),
      body: ListView(
        padding: EdgeInsets.zero,
        children: [
          if (!GetPlatform.isWeb)
            ListTile(
              title: Text(translate(Translations.install_on_holder)),
              onTap: () => Get.toNamed(Routes.camera),
            ),
          Obx(() =>
            SwitchListTile(
              value: _vm.isStylusModeEnabled.value,
              title: Text(translate(Translations.stylus_mode)),
              onChanged: (value) => _vm.setIsStylusModeEnabled(value),
            ),
          ),
          Obx(() =>
            SwitchListTile(
              value: _vm.isNeedToSaveImages.value,
              title: Text(translate(Translations.save_images)),
              onChanged: (value) async {
                _vm.setIsNeedToSaveImages(value);
                if(value) ScaffoldMessenger.of(context).showSnackBar(_saveImagesSnackBar);
              },
            ),
          ),
          ListTile(
            title: Text(translate(Translations.share_logs)),
            onTap: () async => await _showSendRequestWidget(context),
          ),
        ]
      )
    );
  }

  _showSendRequestWidget(BuildContext context) async {
    await Get.toNamed(Routes.sendRequest);
  }

  displayCameraDelayDialog(BuildContext context) async =>
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(translate(Translations.camera_delay)),
          content: Obx(() =>
            Container(
              width: 100.0,
              height: _cameraDelayValues.length / 2 * 56.0,
              child: GridView.count(
                crossAxisCount: 2,
                shrinkWrap: true,
                childAspectRatio: 4,
                children: List.generate(
                  _cameraDelayValues.length, (index) =>
                  getCameraDelayRadioListTitle(_cameraDelayValues[index])),
              )
            ),
          )
        );
      }
    );

  Widget getCameraDelayRadioListTitle(double cameraDelay) =>
    Obx(() => RadioListTile<double>(
      title: Text('${_getCameraDelayStr(cameraDelay)} ${translate(Translations.sec)}'),
      value: cameraDelay,
      groupValue: _vm.cameraDelaySeconds.value,
      onChanged: (double value) {
        _vm.setCameraDelaySeconds(value);
        Get.back();
      },
    ),
  );

  String _getCameraDelayStr(double cameraDelay) =>
      cameraDelay > 0 && cameraDelay < 1 ? cameraDelay.toString() : cameraDelay.toStringAsFixed(0);
}
