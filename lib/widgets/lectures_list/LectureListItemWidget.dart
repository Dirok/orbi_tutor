import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:intl/intl.dart';
import 'package:tutor/focused.dart';

abstract class LectureListItemWidget extends TranslatedStatelessWidget {
  LectureListItemViewModel get vm;

  bool get needUseTwoRowLabel;

  Widget _getDateLabel() {
    var startDateTime = vm.startDateTime;
    var timeStr = DateFormat('HH:mm').format(startDateTime);
    if (needUseTwoRowLabel) {
      String dateStr = startDateTime.year != DateTime.now().year
          ? DateFormat('dd MMMM yyyy').format(startDateTime)
          : DateFormat('dd MMMM').format(startDateTime);
      return _getTwoRowLabel(timeStr, dateStr);
    }
    else
      return _getBoldText(DateFormat('HH:mm').format(startDateTime));
  }

  Widget _getTwoRowLabel(String topText, String bottomText) => Column(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      _getBoldText(topText),
      Text(
          bottomText,
          style: TextStyle(
            fontSize: 12,
            fontFamily: UIConstants.fontFamily,
            fontWeight: FontWeight.w300,
          ),
      ),
    ],
  );

  Widget _getBoldText(String text) => Text(
    text,
    style: TextStyle(
        color: UIConstants.defaultTextColor,
        fontSize: UIConstants.defaultFontSize,
        fontFamily: UIConstants.fontFamily,
        fontWeight: FontWeight.w700,
        decoration: TextDecoration.none
    ),
  );
}

class ScheduledLectureListItemWidget extends LectureListItemWidget {

  final ScheduledLectureListItemViewModel _vm;

  @override
  LectureListItemViewModel get vm => _vm;

  ScheduledLectureListItemWidget(this._vm);

  @override
  bool get needUseTwoRowLabel => _vm.isActiveOrChecking;

  @override
  Widget build(BuildContext context) => Container(
      margin: EdgeInsets.only(left: 40),
      child: Obx(() => ListTile(
        dense: true,
        leading: _getDateLabel(),
        title: Text(_vm.subjectName),
        trailing: _vm.currentUserIsLectureOwner
            ? Text(_vm.ownerName) // todo add groups & participants
            : Text(_vm.ownerName),
        selectedTileColor: UIConstants.primaryLightColor,
        selected: _vm.isSelected.value,
        onTap: _vm.isSelectionMode ? _vm.performSelectAction : _vm.performDefaultAction,
        onLongPress: _vm.performSelectAction,
      ),
    ),
  );
}

class PassedLectureListItemWidget extends LectureListItemWidget  {

  final PassedLectureListItemViewModel _vm;
  final progressBarHeight = 50.0;

  @override
  LectureListItemViewModel get vm => _vm;

  @override
  bool get needUseTwoRowLabel => _vm.isDownloaded;

  PassedLectureListItemWidget(this._vm);

  @override
  Widget build(BuildContext context) => Container(
    margin: EdgeInsets.only(left: 40),
    child: Column(
      children: [
        ListTile(
          dense: true,
          leading: _getDateLabel(),
          title: Text(_vm.subjectName),
          subtitle: _getWarningLabel(_vm.warningType),
          trailing: _vm.currentUserIsLectureOwner
              ? Text(_vm.ownerName) // todo add groups & participants
              : Text(_vm.ownerName),
          enabled:
              (_vm.warningType != PassedLectureWarningType.fileProcessed &&
                  _vm.warningType != PassedLectureWarningType.onChecking),
          onTap: _vm.performDefaultAction,
          onLongPress: _vm.showLectureOptions,
        ),
        Obx(() => AnimatedContainer(
              duration: Duration(milliseconds: 500),
              curve: Curves.fastOutSlowIn,
              height: _vm.showProgress ? progressBarHeight : 0.0,
              child: ClipRect(
                child: Stack(
                  alignment: Alignment.center,
                  fit: StackFit.loose,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: LinearProgressIndicator(
                        value: _vm.progress,
                      ),
                    ),
                    // Text(
                    //   "$_progressPercentValue %",
                    //   overflow: TextOverflow.ellipsis,
                    //   style: TextStyle(color: Colors.black),
                    // ),
                  ],
                ),
              ),
            ),
        ),
      ],
    ),
  );

  Widget _getWarningLabel(PassedLectureWarningType warningType) {
    if (warningType == PassedLectureWarningType.none)
      return null;

    Icon warningIcon;
    Text warningText;
    if (warningType == PassedLectureWarningType.localFileOnly) {
      warningIcon = Icon(
          Icons.sync_problem_rounded,
          size: 20.0,
          color: UIConstants.redLogoColor);
      warningText = Text(
          translate(Translations.local_copy_only),
          style: TextStyle(color: UIConstants.redLogoColor));
    }
    else {
      warningIcon = Icon(
          Icons.update_rounded,
          size: 20.0,
          color: Colors.grey);
      warningText = Text(warningType == PassedLectureWarningType.onChecking
          ? translate(Translations.on_checking)
          : translate(Translations.processing),
          style: TextStyle(color: Colors.grey));
    }
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        warningIcon,
        SizedBox(width: 4.0),
        warningText,
      ],
    );
  }
}