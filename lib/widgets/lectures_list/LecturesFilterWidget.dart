import 'dart:core';

import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:intl/intl.dart';
import 'package:tutor/focused.dart';

class LecturesFilterWidget extends TranslatedStatelessWidget {
  final _shortDateFormat = DateFormat('d.M.y');

  final LecturesFilterViewModel _vm = Get.find<LecturesFilterViewModel>();

  final hasSchoolLicense = appSession.account.hasSchoolLicense;

  String get ownersFilterKey => hasSchoolLicense ? Translations.teachers : Translations.owners;
  String get groupsFilterKey => hasSchoolLicense ? Translations.classes : Translations.groups;

  @override
  Widget build(BuildContext context) => Container(
      width: 400,
      child: Drawer(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _getFilters(),
            _getButtons(),
          ],
        ),
      ),
    );

  Widget _getFilters() => Expanded(
        child: ListView(
          children: [
            _getLectureStartTimeFilter(),
            CustomExpansionPanelList(
              panelList: [
                if (_vm.ownersViewModelList.isNotEmpty)
                  ExpansionPanelData(
                    isExpanded: _vm.isOwnersExpanded,
                    body: _getOwnersFilter(),
                    title: translate(ownersFilterKey),
                    performExpand: (isExpanded) => _vm.isOwnersExpanded = isExpanded,
                  ),
                if (_vm.groupsViewModelList.isNotEmpty)
                  ExpansionPanelData(
                    isExpanded: _vm.isGroupsExpanded,
                    body: _getGroupsFilter(),
                    title: translate(groupsFilterKey),
                    performExpand: (isExpanded) => _vm.isGroupsExpanded = isExpanded,
                  ),
                if (_vm.subjectsViewModelList.isNotEmpty)
                  ExpansionPanelData(
                    isExpanded: _vm.isSubjectsExpanded,
                    body: _getSubjectsFilter(),
                    title: translate(Translations.disciplines),
                    performExpand: (isExpanded) => _vm.isSubjectsExpanded = isExpanded,
                  ),
              ],
            ),
          ],
        ),
      );

  Widget _getOwnersFilter() => _getFilter(_vm.ownersViewModelList);
  Widget _getGroupsFilter() => _getFilter(_vm.groupsViewModelList);
  Widget _getSubjectsFilter() => _getFilter(_vm.subjectsViewModelList);

  Widget _getFilter(List<CheckboxFilterListElementViewModel> items) => Column(
        children: items
            .map(
              (item) => Padding(
                padding: EdgeInsets.all(4.0),
                child: Container(
                  child: _getListViewCard(item),
                ),
              ),
            ).toList(),
      );

  Widget _getListViewCard(CheckboxFilterListElementViewModel vm) => Obx(() => CheckboxListTile(
      title: Text(vm.name),
      value: vm.filterValue.value,
      onChanged: vm.setValue));

  Widget _getButtons() => Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        _getResetButton(),
        _getApplyButton()
      ]);

  Widget _getResetButton() => Expanded(
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
          primary: UIConstants.primaryLightColor,
          onPrimary: UIConstants.primaryDarkColor,
          shape: const BeveledRectangleBorder(
            borderRadius: BorderRadius.zero,
          )),
      child: Text(translate(Translations.reset).toUpperCase()),
      onPressed: _vm.resetFilter,
    )
  );

  Widget _getApplyButton() => Expanded(
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
          primary: UIConstants.primaryDarkColor,
          onPrimary: UIConstants.buttonTextColor,
          shape: const BeveledRectangleBorder(
            borderRadius: BorderRadius.zero,
          )),
      child: Text(translate(Translations.ready).toUpperCase()),
      onPressed: Get.back,
    )
  );

  Widget _getLectureStartTimeFilter() => Container(
    margin: EdgeInsets.only(top: 10.0),
    child: Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        _getDateTimeWidget(translate(Translations.from), _vm.fromDateTimeViewModel),
        _getDateTimeWidget(translate(Translations.to), _vm.toDateTimeViewModel)
      ],
    ),
  );

  Widget _getDateTimeWidget(String title, DateTimeFilterViewModel vm) => Container(
    constraints: BoxConstraints.tightFor(width: 185.0, height: 40.0),
    margin: EdgeInsets.all(4.0),
    decoration: ShapeDecoration(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      color:  UIConstants.backgroundColor,
      shadows: [BoxShadow(
        offset: Offset(3.0, 3.0),
        color: Colors.grey.withOpacity(0.5),
        blurRadius: 5.0,
      )],
    ),
    child: ListTile(
      dense: true,
      visualDensity: VisualDensity.compact,
      horizontalTitleGap: VisualDensity.minimumDensity,
      leading: Text('$title:'),
      title: Obx(() => Align(
        alignment: Alignment.center,
        child: Text(
            vm.isLimit ? '' : _shortDateFormat.format(vm.filterValue.value),
            style: TextStyle(
                color: UIConstants.defaultTextColor,
                fontSize: UIConstants.defaultFontSize,
                fontFamily: UIConstants.fontFamily,
                fontWeight: FontWeight.w600,
                decoration: TextDecoration.none)
        ),
      )),
      trailing: Icon(
          Icons.calendar_today_rounded,
          color: UIConstants.primaryDarkColor
      ),
      onTap: () => _pickDateTime(vm),
    ),
  );

  Future<void> _pickDateTime(DateTimeFilterViewModel vm) async {
    var pickedDate = await showDatePicker(
        context: Get.context,
        initialDate: _getInitialDate(vm),
        firstDate: vm == _vm.fromDateTimeViewModel
            ? _vm.fromDateTimeViewModel.defaultValue
            : _vm.fromDateTimeViewModel.filterValue.value,
        lastDate: vm == _vm.toDateTimeViewModel
            ? _vm.toDateTimeViewModel.defaultValue
            : _vm.toDateTimeViewModel.filterValue.value);
        
    if (pickedDate != null)
      vm.setValue(pickedDate);
  }

  DateTime _getInitialDate(DateTimeFilterViewModel vm) {
    var now = DateTime.now();
    var canUseNow = _vm.fromDateTimeViewModel.filterValue.value.isBefore(now)
        && _vm.toDateTimeViewModel.filterValue.value.isAfter(now);
    return !vm.isLimit
      ? vm.filterValue.value
      : canUseNow
        ? now
        : vm == _vm.fromDateTimeViewModel
          ? _vm.toDateTimeViewModel.filterValue.value
          : _vm.fromDateTimeViewModel.filterValue.value;
  }
}