import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:intl/intl.dart';
import 'package:tutor/focused.dart';

abstract class LecturesListWidget extends BaseStatelessWidget {
  final longDateFormat = DateFormat('d MMMM yyyy');
  final shortDateFormat = DateFormat('d MMMM');

  final Map<Type, Function(Event)> eventHandlers = Map();

  final scaffoldKey = GlobalKey<ScaffoldState>();

  LecturesListViewModel get vm;

  String get appBarTitleTextKey;
  String get noLecturesLabelTextKey;
  bool get isNoLectures;

  LecturesListWidget() {
    vm.listenEvents((e) => eventHandlers[e.runtimeType]?.call(e));
    eventHandlers.addAll({
      ListUpdateErrorEvent: (_) => _onListUpdateErrorEvent(),
    });
  }

  _onListUpdateErrorEvent() => showAlertDialog(
      translate(Translations.error),
      translate(Translations.failed_to_update_lecture_list));

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: _getAppBar(),
        key: scaffoldKey,
        endDrawer: LecturesFilterWidget(),
        endDrawerEnableOpenDragGesture: false,
        extendBodyBehindAppBar: true,
        backgroundColor: UIConstants.backgroundColor,
        body: Obx(() => vm.isLectureUpdating.value
            ? getProgressIndicator()
            : Stack(
                children: [
                  if (isNoLectures) _getNoLecturesText(),
                  RefreshIndicator(
                    edgeOffset: 60.0,
                    displacement: 60.0,
                    onRefresh: () async {
                      vm.updateList();
                    },
                    child: _getListView(),
                  ),
                ],
              )),
      );

  Widget _getAppBar() => AppBar(
    backgroundColor: UIConstants.primaryLightColor,
    leading: Obx(() => !vm.isSelectionMode()
        ? IconButton(
            icon: SvgWidget.asset(Assets.back_arrow_icon, color: UIConstants.primaryDarkColor,
              width: UIConstants.iconSize, height: UIConstants.iconSize),
            onPressed: () => Get.back(),
        )
        : IconButton(
            icon: SvgWidget.asset(Assets.revert_icon,
                color: UIConstants.primaryDarkColor,
                width: UIConstants.iconSize,
                height: UIConstants.iconSize),
            onPressed: vm.clearSelectedLectures
          )),
    title: Obx(() => Text(!vm.isSelectionMode()
        ? translate(appBarTitleTextKey)
        : vm.selectedLectures.length.toString(),
        style: TextStyle(
            color: UIConstants.defaultTextColor,
            fontSize: UIConstants.appBarFontSize,
            fontFamily: UIConstants.fontFamily,
            fontWeight: FontWeight.w700,
            decoration: TextDecoration.none))),
    actions: [
      Obx(() => !vm.isSelectionMode()
          ? IconButton(
              icon: Icon(Icons.filter_list_alt,
                size: UIConstants.iconSize, color: UIConstants.purpleIconColor,
              ),
              onPressed: _openEndDrawer)
          : const SizedBox.shrink()),
      Obx(() => vm.selectedLectures.length == 1
          ? IconButton(
              icon: Icon(
                Icons.open_in_new,
                size: UIConstants.iconSize,
                color: UIConstants.purpleIconColor,
              ),
              onPressed: () {
                _tryOpenLecture(vm.selectedLectures.first);
              },
          )
          : const SizedBox.shrink()),
      Obx(() => vm.isSelectionMode()
          ? IconButton(
              icon: Icon(
                Icons.checklist,
                size: UIConstants.iconSize,
                color: UIConstants.purpleIconColor,
              ),
              onPressed: vm.selectAllLectures
            )
          : const SizedBox.shrink()),
      Obx(() => vm.isSelectionMode()
          ? IconButton(
              icon: Icon(
                Icons.clear_all,
                size: UIConstants.iconSize,
                color: UIConstants.purpleIconColor,
              ),
              onPressed: vm.clearSelectedLectures
            )
          : const SizedBox.shrink()),
      Obx(() => vm.isSelectionMode()
          ? IconButton(
              icon: Icon(
                Icons.delete,
                size: UIConstants.iconSize,
                color: UIConstants.purpleIconColor,
              ),
              onPressed: _showMultiDeletionDialog
            )
          : const SizedBox.shrink()),
    ]

  );

  Widget _getNoLecturesText() => IgnorePointer(
        child: Center(
          child: Text(translate(noLecturesLabelTextKey),
              style: TextStyle(
                  color: UIConstants.defaultTextColor,
                  fontSize: 18.0,
                  fontFamily: UIConstants.fontFamily,
                  fontWeight: FontWeight.w400,
                  decoration: TextDecoration.none)),
        ));

  Widget _getListView();

  Widget _getTextTitleLabel(String titleKey) => Align(
    alignment: Alignment.centerLeft,
    child: Container(
      constraints: BoxConstraints.tightFor(width: 150.0, height: 30.0),
      margin: EdgeInsets.only(top: 8.0, left: 40.0),
      decoration: ShapeDecoration(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0)),
          color: UIConstants.primaryDarkColor,
          shadows: [
            new BoxShadow(
              offset: Offset(2.0, 2.0),
              color: Colors.grey.withOpacity(0.5),
              blurRadius: 3.0,
            )
          ]),
      child: Align(
        alignment: Alignment.center,
        child: Text(
          translate(titleKey),
          style: TextStyle(
              color: UIConstants.buttonTextColor,
              fontSize: 14,
              fontFamily: UIConstants.fontFamily,
              fontWeight: FontWeight.w700,
              decoration: TextDecoration.none),
        ),
      ),
    ),
  );

  Widget _getDateLabel(DateTime date) => Align(
    alignment: Alignment.centerLeft,
    child: Container(
      constraints: BoxConstraints.tightFor(width: 150.0, height: 30.0),
      margin: EdgeInsets.only(top: 8.0, left: 40.0),
      decoration: ShapeDecoration(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0)),
          color: UIConstants.purpleIconColor,
          shadows: [
            new BoxShadow(
              offset: Offset(2.0, 2.0),
              color: Colors.grey.withOpacity(0.5),
              blurRadius: 3.0,
            )
          ]),
      child: Align(
        alignment: Alignment.center,
        child: Text(
          date.year == DateTime.now().year
            ? shortDateFormat.format(date)
            : longDateFormat.format(date),
          style: TextStyle(
              color: UIConstants.buttonTextColor,
              fontSize: UIConstants.defaultFontSize,
              fontFamily: UIConstants.fontFamily,
              fontWeight: FontWeight.w700,
              decoration: TextDecoration.none),
        ),
      ),
    ),
  );

  Widget _getDateSeparator() => Align(
      alignment: Alignment.bottomCenter,
      child: Divider(indent: 20, endIndent: 20, color: Colors.grey),
    );

  bool _checkDateIsNew(DateTime lectureDateTime) {
    var lectureDate = DateTime(lectureDateTime.year, lectureDateTime.month, lectureDateTime.day);
    var isNewDate = lectureDate.compareTo(vm.lastDate) != 0;
    vm.lastDate = lectureDate;
    return isNewDate;
  }

  bool _checkDateSeparatorIsNeed(RxList lecturesListAtTop) {
    var needSeparator = vm.needAddDateSeparator || lecturesListAtTop.isNotEmpty;
    vm.needAddDateSeparator = true;
    return needSeparator;
  }

  Widget _getTopicGroupedLecturesList(String header, RxList topicLectures) => Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisSize: MainAxisSize.min,
      children: <Widget>[]
        ..add(_getTextTitleLabel(header))
        ..addAll(_getTopLecturesList(topicLectures))
  );

  Widget _getDateGroupedLecturesList(RxList lecturesList) => Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[]
        ..addAll(_getBottomLecturesList(lecturesList))
  );

  Iterable<Widget> _getTopLecturesList(RxList items);
  Iterable<Widget> _getBottomLecturesList(RxList items);

  Future<bool> deleteLecture(LectureListItemViewModel lecture, {bool removeFromServer = false}) async {
    final isDeleted = await vm.deleteLecture(lecture, removeFromServer: removeFromServer);
    if (isDeleted) return true;
    await showLectureDeletionErrorDialog();
    return false;
  }

  Future<void> deleteLectures(Iterable<LectureListItemViewModel> lectures,
      {bool removeFromServer = false}) async {
    // todo add the ability to delete a list within one request
    for (final lecture in lectures) {
      final isDeleted = await deleteLecture(lecture, removeFromServer: removeFromServer);
      if (!isDeleted) break;
    }

    vm.clearSelectedLectures();
  }

  Future<void> showLectureDeletionErrorDialog() => showAlertDialog(
      translate(Translations.failure), translate(Translations.cant_delete_lecture));

  void _openEndDrawer() => scaffoldKey.currentState.openEndDrawer();

  void _tryOpenLecture(LectureListItemViewModel lecture);

  Future<void> _showMultiDeletionDialog();
}

class ScheduledLecturesListWidget extends LecturesListWidget {
  final ScheduledLecturesListViewModel _vm = Get.find<ScheduledLecturesListViewModel>();

  @override
  LecturesListViewModel get vm => _vm;

  @override
  String get noLecturesLabelTextKey => Translations.no_scheduled_lectures;

  @override
  String get appBarTitleTextKey => Translations.scheduled_lectures;

  @override
  bool get isNoLectures => _vm.scheduledActiveLectures.isEmpty
      && _vm.scheduledPlannedLectures.isEmpty;

  ScheduledLecturesListWidget() : super() {
    eventHandlers.addAll({
      ShowLectureOptionsEvent: (args) => _onShowLectureOptionsEvent(args),
      OpenGroupLectureEvent: (args) => _onOpenGroupLectureEvent(args),
      OpenPersonalLectureEvent: (args) => _onOpenPersonalLectureEvent(args),
    });
  }
  
  @override
  Widget build(BuildContext context) => super.build(context);

  @override
  Widget _getListView() => ListView(
    children: [
      if (_vm.scheduledActiveLectures.isNotEmpty)
        _getTopicGroupedLecturesList(Translations.active, _vm.scheduledActiveLectures),
      if (_vm.scheduledPlannedLectures.isNotEmpty)
        _getDateGroupedLecturesList(_vm.scheduledPlannedLectures),
    ],
  );

  @override
  Iterable<Widget> _getTopLecturesList(RxList items) =>
      items.map((item) => _getListViewCard(item));

  @override
  Iterable<Widget> _getBottomLecturesList(RxList items) {
    vm.updateGroupedLecturesList();
    return items.map((item) {
      final lecture = item as ScheduledLectureListItemViewModel;
      if (lecture == null) return const SizedBox.shrink();

      final isNewDate = _checkDateIsNew(lecture.startDateTime);

      final isNeedSeparator = _checkDateSeparatorIsNeed(_vm.scheduledActiveLectures);

      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          if (isNewDate && isNeedSeparator)
            _getDateSeparator(),
          if (isNewDate)
            _getDateLabel(lecture.startDateTime),
          _getListViewCard(lecture)
        ]);
    });
  }

  @override
  void _tryOpenLecture(LectureListItemViewModel lecture) => _vm.tryOpenLecture(lecture);

  Widget _getListViewCard(ScheduledLectureListItemViewModel lecture) => Padding(
      padding: EdgeInsets.all(4.0),
      child: ScheduledLectureListItemWidget(lecture)
  );

  Future<void> _onShowLectureOptionsEvent(ShowLectureOptionsEvent args) {
    final itemViewModel = args.itemViewModel as ScheduledLectureListItemViewModel;
    return showModalBottomSheet(
      context: Get.context,
      builder: (BuildContext buildContext) {
        return ListView(
          padding: EdgeInsets.all(8.0),
          shrinkWrap: true,
          children: [
            if (itemViewModel.isActiveOrChecking || args.isOwner)
              _getOpenLectureOption(itemViewModel),
            if (args.isOwner && itemViewModel.isPlanned)
              _getDeletionOption(itemViewModel)
          ],
        );
      },
    );
  }

  void _onOpenGroupLectureEvent(OpenGroupLectureEvent e) => Get.offNamed(
      e.isOwner ? Routes.teacherActiveGroupLecture : Routes.studentActiveGroupLecture);

  void _onOpenPersonalLectureEvent(OpenPersonalLectureEvent e) => Get.offNamed(Routes.personalLecture);

  Widget _getOpenLectureOption(ScheduledLectureListItemViewModel lecture) => ListTile(
        leading: SvgWidget.asset(Assets.eye_in_board_icon,
            width: UIConstants.buttonSize,
            color: UIConstants.primaryDarkColor),
        title: Text(translate(Translations.open_lecture)),
        onTap: () {
          Get.back();
          _vm.tryOpenLecture(lecture);
        });

  Widget _getDeletionOption(ScheduledLectureListItemViewModel lecture) => ListTile(
      leading: SvgWidget.asset(Assets.trash_icon,
          width: UIConstants.buttonSize,
          color: UIConstants.primaryDarkColor),
      title: Text(translate(Translations.delete_lecture)),
      onTap: () async {
        Get.back();
        await _showDeletionDialog(lecture);
      });

  Future<void> _showDeletionDialog(ScheduledLectureListItemViewModel lecture) {
    final currLecture = '${lecture.subjectName} - ${lecture.ownerName}'
        ' - ${DateFormat('HH:mm').format(lecture.startDateTime)}';

    final buttons = {
      translate(Translations.yes): () => deleteLecture(lecture, removeFromServer: true),
      translate(Translations.no): null };

    return showAlertDialog(
        currLecture, translate(Translations.want_to_delete_lecture), variants: buttons);
  }

  @override
  Future<void> _showMultiDeletionDialog() {
    final items = vm.selectedLectures;

    final currLecture = items.length > 1
        ? '${translate(Translations.delete_lecture)} (${items.length})'
        : '${items.first.subjectName} - ${items.first.ownerName}'
        ' - ${DateFormat('HH:mm').format(items.first.startDateTime)}';

    return showAlertDialog(currLecture, translate(Translations.want_to_delete_lecture),
        variants: {
          translate(Translations.yes): () => deleteLectures(items, removeFromServer: true),
          translate(Translations.no): null});
  }
}

class PassedLecturesListWidget extends LecturesListWidget {
  final PassedLecturesListViewModel _vm = Get.find<PassedLecturesListViewModel>();

  @override
  LecturesListViewModel get vm => _vm;

  @override
  String get noLecturesLabelTextKey => Translations.no_finished_lectures;

  @override
  String get appBarTitleTextKey => Translations.finished_lectures;

  @override
  bool get isNoLectures => _vm.passedNotDownloadedLectures.isEmpty
      && _vm.passedDownloadedLectures.isEmpty;

  PassedLecturesListWidget() : super() {
    eventHandlers.addAll({
      ShowLectureOptionsEvent: (args) => _onShowLectureOptionsEvent(args),
      OpenGroupLectureEvent: (args) => _onOpenGroupLectureEvent(args),
      OpenPersonalLectureEvent: (args) => _onOpenPersonalLectureEvent(args),
      PlayerNotImplementedEvent: onPlayerNotImplemented,
      LectureAlreadyDownloadingEvent: onLectureAlreadyDownloading,
      NotEnoughSpaceEvent: onNotEnoughSpace,
      HashNoMatchEvent: onHashNoMatch,
      UnknownErrorEvent: onUnknownError,
    });
  }

  Future<void> onPlayerNotImplemented(_) =>
      showAlertDialog(translate(Translations.error),
          translate(Translations.in_developing));

  Future<void> onLectureAlreadyDownloading(_) =>
      showAlertDialog(translate(Translations.error),
          translate(Translations.please_wait) + '. ' + translate(Translations.busy_downloading));

  Future<void> onNotEnoughSpace(_) =>
      showAlertDialog(translate(Translations.failure),
          translate(Translations.not_enough_space));

  Future<void> onHashNoMatch(_) =>
      showAlertDialog(translate(Translations.failure),
          translate(Translations.failed_to_download_lecture));

  Future<void> onUnknownError(_) =>
      showAlertDialog(translate(Translations.error),
          translate(Translations.unknown_error));

  @override
  Widget build(BuildContext context) => super.build(context);

  @override
  Widget _getListView() => ListView(
    children: [
      if (_vm.passedDownloadedLectures.isNotEmpty)
        _getTopicGroupedLecturesList(Translations.downloaded, _vm.passedDownloadedLectures),
      if (_vm.passedNotDownloadedLectures.isNotEmpty)
        _getDateGroupedLecturesList(_vm.passedNotDownloadedLectures),
    ],
  );

  @override
  Iterable<Widget> _getTopLecturesList(RxList items) =>
      items.map((item) => _getListViewCard(item));

  @override
  Iterable<Widget> _getBottomLecturesList(RxList items) {
    vm.updateGroupedLecturesList();
    return items.map((item) {
      final lecture = item as PassedLectureListItemViewModel;
      if (lecture == null) return SizedBox.shrink();

      final isNewDate = _checkDateIsNew(lecture.startDateTime);

      final isNeedSeparator = _checkDateSeparatorIsNeed(_vm.passedDownloadedLectures);

      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          if (isNewDate && isNeedSeparator)
            _getDateSeparator(),
          if (isNewDate)
            _getDateLabel(lecture.startDateTime),
          _getListViewCard(lecture)
        ]);
    });
  }

  @override
  void _tryOpenLecture(LectureListItemViewModel lecture) {
    // todo choose how to open
  }

  Widget _getListViewCard(PassedLectureListItemViewModel lecture) => Padding(
      padding: EdgeInsets.all(4.0),
      child: PassedLectureListItemWidget(lecture)
  );

  Future<void> _onShowLectureOptionsEvent(ShowLectureOptionsEvent args) {
    var itemViewModel = args.itemViewModel as PassedLectureListItemViewModel;
    return showModalBottomSheet(
      context: Get.context,
      builder: (BuildContext buildContext) {
        return ListView(
          padding: EdgeInsets.all(8.0),
          shrinkWrap: true,
          children: [
            if (itemViewModel.isDownloaded)
              _getOpenAsBoardOption(itemViewModel),
            if (itemViewModel.isDownloaded)
              _getOpenAsPlayerOption(itemViewModel),
            _getDeletionOption(itemViewModel),
          ],
        );
      },
    );
  }

  void _onOpenGroupLectureEvent(OpenGroupLectureEvent e) => Get.offNamed(
      e.isOwner ? Routes.teacherPassedGroupLecture : Routes.studentPassedGroupLecture);

  void _onOpenPersonalLectureEvent(OpenPersonalLectureEvent e) => Get.offNamed(Routes.personalLecture);

  Widget _getOpenAsBoardOption(PassedLectureListItemViewModel lecture) => ListTile(
        leading: SvgWidget.asset(Assets.eye_in_board_icon,
            width: UIConstants.buttonSize,
            color: UIConstants.primaryDarkColor),
        title: Text(translate(Translations.view_boards)),
        onTap: () {
          Get.back();
          _vm.tryOpenLectureAsBoards(lecture);
        });

  Widget _getOpenAsPlayerOption(PassedLectureListItemViewModel lecture) => ListTile(
        leading: SvgWidget.asset(Assets.play_icon,
            width: UIConstants.buttonSize,
            color: UIConstants.primaryDarkColor),
        title: Text(translate(Translations.play_the_lecture)), // todo translate
        onTap: () {
          Get.back();
          _vm.tryOpenLectureAsPlayer(lecture);
        });

  Widget _getDeletionOption(PassedLectureListItemViewModel lecture) => ListTile(
          leading: SvgWidget.asset(Assets.trash_icon,
              width: UIConstants.buttonSize,
              color: UIConstants.primaryDarkColor),
          title: Text(translate(Translations.delete_lecture)),
          onTap: () {
            Get.back();
            _showDeletionDialog(lecture);
          });

  FutureOr<void> _showDeletionDialog(PassedLectureListItemViewModel lecture) {
    final currLecture = '${lecture.subjectName} - ${lecture.ownerName}'
        ' - ${DateFormat('HH:mm').format(lecture.startDateTime)}';

    final buttons = {
      translate(Translations.cancel): null,
      if (lecture.isDownloaded)
        translate(Translations.remove_from_device):
          () => deleteLecture(lecture, removeFromServer: false),
      if (lecture.currentUserIsLectureOwner)
        translate(Translations.remove_completely):
          () => deleteLecture(lecture, removeFromServer: true)
    };

    return showAlertDialog(currLecture,
        translate(Translations.want_to_delete_lecture), variants: buttons);
  }

  @override
  Future<void> _showMultiDeletionDialog() {
    final items = vm.selectedLectures;

    final currLecture = items.length > 1
        ? translate(Translations.want_to_delete_subject)
        :'${items.first.subjectName} - ${items.first.ownerName}'
        ' - ${DateFormat('HH:mm').format(items.first.startDateTime)}';

    return showAlertDialog(currLecture, translate(Translations.want_to_delete_lecture),
        variants: {
          translate(Translations.yes): () => deleteLectures(items, removeFromServer: true),
          translate(Translations.no): null,
          translate(Translations.remove_from_device): () => deleteLectures(items, removeFromServer: false)});
  }
}