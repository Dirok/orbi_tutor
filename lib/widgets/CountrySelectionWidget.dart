import 'package:country_provider/country_provider.dart' as cp;
import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class CountrySelectionWidget extends BaseStatelessWidget {
  final _vm = Get.find<CountrySelectionViewModel>();

  @override
  BaseViewModel get vm => _vm;

  CountrySelectionWidget() {
    eventHandlers.addAll({
      AddCountrySuccessEvent: (_) => Get.toNamed(Routes.mainMenu),
      AddCountryFailedEvent: (_) => _showUpdateUserFailedDialog(),
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: UIConstants.primaryLightColor,
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 24.0,
            vertical: 24.0,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                children: [
                  _getNotReleaseServerLabel(),
                  _getTitle(),
                  _getSendRequestButton(),
                ],
              ),
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Expanded(child: const SizedBox.shrink()),
                      SizedBox(
                        width: 260.0,
                        child: _getDropdownButton(),
                      ),
                      _getTooltip()
                    ],
                  ),
                  const SizedBox(height: 10.0),
                  _getConfirmButton(),
                ],
              ),
              _getVersionLabel(),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _showUpdateUserFailedDialog() => showAlertDialog(
      translate(Translations.error),
      translate(Translations.country_selection_error));

  Widget _getTitle() => Expanded(
        child: Center(
          child: Text(
            translate(Translations.choose_country),
            style: TextStyle(
              color: UIConstants.defaultTextColor,
              fontSize: UIConstants.appBarFontSize,
              fontFamily: UIConstants.fontFamily,
              fontWeight: FontWeight.w400,
              decoration: TextDecoration.none,
            ),
          ),
        ),
      );

  Widget _getConfirmButton() {
    return Container(
      constraints: BoxConstraints.tightFor(width: 260, height: 48),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: UIConstants.primaryDarkColor,
          onPrimary: UIConstants.buttonTextColor,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(50)),
          ),
        ),
        child: Text(translate(Translations.confirm)),
        onPressed: _vm.confirmCountryRequest,
      ),
    );
  }

  Widget _getDropdownButton() {
    return Obx(
      () => _vm.selectedCountry != null
          ? DropdownButton<cp.Country>(
              value: _vm.selectedCountry?.value ?? _vm.countries[0],
              icon: const Icon(
                Icons.keyboard_arrow_down_rounded,
                color: UIConstants.primaryDarkColor,
              ),
              iconSize: 24.0,
              style: const TextStyle(color: UIConstants.lightGrayColor),
              underline: Container(
                height: 1.0,
                color: UIConstants.primaryDarkColor,
              ),
              isExpanded: true,
              onChanged: (cp.Country newValue) {
                _vm.selectedCountry.value = newValue;
              },
              items: _vm.countries
                  .map<DropdownMenuItem<cp.Country>>((cp.Country value) {
                return DropdownMenuItem<cp.Country>(
                  value: value,
                  child: Text(
                    value.name,
                    style: TextStyle(
                      color: UIConstants.darkGrayColor,
                      fontSize: 14.0,
                      fontFamily: UIConstants.fontFamily,
                    ),
                  ),
                );
              }).toList(),
            )
          : const SizedBox.shrink(),
    );
  }

  Widget _getTooltip() {
    return Expanded(
        child: Align(
            alignment: Alignment.centerLeft,
            child: SizedBox(
                width: 40.0,
                child: Tooltip(
                    message: translate(Translations.country_selection_tooltip),
                    child: SvgWidget.asset(Assets.question_icon)))));
  }

  Widget _getNotReleaseServerLabel() => Expanded(
      child: Text('${Settings.server.name} server',
          style: TextStyle(color: Colors.redAccent, fontSize: 24.0)));

  Widget _getVersionLabel() => Text('v.${PlatformUtils.getFullVersion()}',
      style: TextStyle(color: UIConstants.primaryDarkColor));

  Widget _getSendRequestButton() => Expanded(
      child: Align(
          alignment: Alignment.topRight,
          child: IconButton(
              icon:
                  SvgWidget.asset(Assets.bug_icon, width: UIConstants.iconSize),
              color: UIConstants.primaryDarkColor,
              onPressed: () => Get.toNamed(Routes.sendRequest))));
}
