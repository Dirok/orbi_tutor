import 'dart:async';
import 'dart:core';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart' hide Translations;
import 'package:shimmer/shimmer.dart';
import 'package:tutor/focused.dart';

abstract class LectureWidget extends BaseStatelessWidget {
  final GlobalKey snapshotGlobalKey = GlobalKey();

  final double buttonSize = 36.0;
  final double _buttonSpacingHorizontal = 8.0;
  final double _buttonSpacingVertical = 4.0;

  final DrawingMenu _drawingMenu = DrawingMenu();

  LectureViewModel get vm;

  LectureWidget() {
  // FIXME widget is created again after ImagePicker call (???)
    vm.clearSubscriptions();

    eventHandlers.addAll({
      CameraCapturingErrorEvent: (e) => _onCameraCapturingError(),
      ImageExportFailedEvent: (e) => _onImageExportFailed(),
    });
    vm.listenEvents((e) => eventHandlers[e.runtimeType]?.call(e));
  }

  Future<void> takePhoto() async {
    if (!await PermissionsHelper().checkCameraPermission(Get.context)) {
      return;
    }
    vm.rememberCurrentWhiteboard();
    final imagePath = vm.generateImagePath();
    final result = await Get.toNamed(Routes.camera, arguments: imagePath);
    await vm.addPhotoWithProgress(imagePath, result);
    vm.forgetMemorizedWhiteboard();
  }

  Future<void> _onCameraCapturingError() => showAlertDialog(
      translate(Translations.failure), translate(Translations.photo_capturing_error));

  Future<void> _onImageExportFailed() => showAlertDialog(
      translate(Translations.failure), translate(Translations.image_export_failed));

  Future<void> showColorMenu() => _drawingMenu.show(Get.context, DrawingMenuType.Color);
  Future<void> showStrokeWidthMenu() => _drawingMenu.show(Get.context, DrawingMenuType.StrokeWidth);

  Future<void> exitLecture() async {
    return showAlertDialog(
        translate(Translations.exit), translate(Translations.want_to_exit),
        variants: {translate(Translations.yes): close, translate(Translations.no): null});
  }

  Future<void> close();

  @override
  Widget build(BuildContext context) {
    if (!_drawingMenu.isInited)
      _drawingMenu.init(context, vm.drawingController);

    selfScreenSize ??= ScreenSize.fromSize(MediaQuery.of(context).size);

    return WillPopScope(
      onWillPop: () async => false,
      child: Stack(
        children: [
          _getBody(),
          _getProgressFutureBuilder(),
        ],
      ),
    );
  }

  _getBody() => Scaffold(
    extendBodyBehindAppBar: true,
    body: _getWorkspaceWidgets(),
    floatingActionButton: getFloatingActionButton(),
  );

  Widget _getWorkspaceWidgets() => Stack(
      children: [
        RepaintBoundary(
            key: snapshotGlobalKey,
            child: Stack(
              children: [
                _getWhiteboardWidget(),
                getNameWidget()
              ],
            )
        ),
        _getButtonsPanelWidget(),
        getAdditionalWidget()
      ]
  );

  Widget getAdditionalWidget() => SizedBox.shrink();

  Widget _getWhiteboardWidget() => Center(
      child: WhiteboardWidget(vm.currentWhiteboard, vm.drawingController, vm.lectureServer));

  Widget _getNavigationWidget() => Obx(
        () => vm.allowNavigateWhiteboards.value && vm.whiteboardsCount.value > 1
            ? getNavigationWidget()
            : getWhiteboardNumberWidget());

  Widget _getButtonsPanelWidget() => SafeArea(
      child: Column(children: [
        _getNavigationWidget(),
        SizedBox(height: 16.0),
        _getButtonsPanel()
      ])
  );

  Widget getNameWidget() => SizedBox.shrink();

  Widget getNavigationWidget() {
    return Align(
        alignment: Alignment.topRight,
        child: Padding(
            padding: EdgeInsets.only(top: 6.0),
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ConstrainedBox(
                      constraints: BoxConstraints.tightFor(width: buttonSize, height: buttonSize),
                      child: Obx(() => TextButton(
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Transform.rotate(
                            angle: pi,
                            child: SvgWidget.asset(
                                Assets.arrow_right_icon,
                                width: 20,
                                height: 20,
                                color: vm.isFirstWhiteboard.value ? UIConstants.primaryLightColor : UIConstants.primaryDarkColor
                            ),
                          ),
                          onPressed: vm.isFirstWhiteboard.value ? null : vm.goToLeftWhiteboard
                      ))
                  ),
                  Obx(() => Container(
                    alignment: Alignment.center,
                    constraints: BoxConstraints.tightFor(
                        width: buttonSize * vm.indexTitleWidthFactor.value,
                        height: buttonSize),
                    decoration: ShapeDecoration(
                        color: UIConstants.primaryLightColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(buttonSize)
                        )
                    ),
                    child: Text('${vm.currentWhiteboardIndex.value + 1}/${vm.whiteboardsCount.value}',
                        style: TextStyle(
                            color: UIConstants.primaryDarkColor,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w700
                        )
                    ),
                  )),
                  ConstrainedBox(
                      constraints: BoxConstraints.tightFor(width: buttonSize, height: buttonSize),
                      child: Obx(() => TextButton(
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: SvgWidget.asset(
                              Assets.arrow_right_icon,
                              width: 20,
                              height: 20,
                              color: vm.isLastWhiteboard.value ? UIConstants.primaryLightColor : UIConstants.primaryDarkColor
                          ),
                          onPressed: vm.isLastWhiteboard.value ? null : vm.goToRightWhiteboard
                      ))
                  )
                ]
            )
        )
    );
  }

  Widget getWhiteboardNumberWidget() {
    return Align(
      alignment: Alignment.topRight,
      child: Padding(
        padding: EdgeInsets.only(
          top: 6,
          right: 16,
        ),
        child: Obx(
          () => Container(
            alignment: Alignment.center,
            constraints: BoxConstraints.tightFor(
                width: buttonSize * vm.indexTitleWidthFactor.value,
                height: buttonSize),
            decoration: ShapeDecoration(
                color: UIConstants.primaryLightColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(buttonSize))),
            child: Text(
                '${vm.currentWhiteboardIndex.value + 1}/${vm.whiteboardsCount.value}',
                style: TextStyle(
                    color: UIConstants.primaryDarkColor,
                    fontSize: 16.0,
                    fontWeight: FontWeight.w700)),
          ),
        ),
      ),
    );
  }

  Widget _getButtonsPanel() {
    var buttons = getButtons();
    if (buttons == null || buttons.length == 0) return SizedBox.shrink();

    var buttonsInRow = buttons.length > 3 ? 2 : 1;

    return Align(
      alignment: Alignment.topRight,
      child: Container(
        constraints: BoxConstraints.tightFor(
            width: buttonSize * buttonsInRow + _buttonSpacingHorizontal * (buttonsInRow + 1)
        ),
        padding: EdgeInsets.all(_buttonSpacingHorizontal),
        decoration: ShapeDecoration(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10.0),
                  bottomLeft: Radius.circular(10.0)
              )
          ),
          color: UIConstants.backgroundColor,
          shadows: [BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            blurRadius: 5.0,
          )],
        ),
        child: Wrap(
          alignment: WrapAlignment.center,
          runAlignment: WrapAlignment.center,
          spacing: _buttonSpacingHorizontal,
          runSpacing: _buttonSpacingVertical,
          children: buttons,
        ),
      ),
    );
  }

  List<Widget> getButtons();

  getButton(String iconAssetName, Function onPressedAction,
          {Color color, EdgeInsetsGeometry padding, bool shimmer = false}) =>
      Container(
          constraints:
              BoxConstraints.tightFor(width: buttonSize, height: buttonSize),
          child: TextButton(
            style: TextButton.styleFrom(
              padding: padding ?? EdgeInsets.zero,
            ),
            child: Shimmer.fromColors(
              baseColor: color ??
                  (shimmer || onPressedAction != null
                      ? UIConstants.primaryDarkColor
                      : UIConstants.primaryLightColor),
              highlightColor: color ??
                  (shimmer || onPressedAction == null
                      ? UIConstants.primaryLightColor
                      : UIConstants.primaryDarkColor),
              enabled: shimmer,
              child: SvgWidget.asset(iconAssetName,
                  color: color ??
                      (onPressedAction == null
                          ? UIConstants.primaryLightColor
                          : UIConstants.primaryDarkColor)),
            ),
            onPressed: onPressedAction,
          ));

  // _clearDrawing() async {
  //   await showAlertDialog(context,
  //       'Clearing', 'Want to delete everything you draw?',
  //       {'Yes': drawingController.clear, 'No': null});
  // }

  Widget getFloatingActionButton();

  _getProgressFutureBuilder() {
    return Obx(() => FutureBuilder<void>(
        future: vm.initializeFuture.value,
        builder: (context, snapshot) =>
        snapshot.connectionState == ConnectionState.done
            || snapshot.connectionState == ConnectionState.none
            ? SizedBox()
            : getProgressIndicator()
    ));
  }
}