import 'package:flutter/foundation.dart';

import 'package:get/get.dart' hide Translations;
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tutor/focused.dart';

mixin MultimediaLectureWidgetMixin<T extends MultimediaLectureViewModelMixin> on WidgetsBindingObserver {
  final _vm = Get.find<T>();

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      Log.info('Show active lecture push');
      _vm.showPush();
    } else if (state == AppLifecycleState.resumed) {
      Log.info('Hide active lecture push and change output to priority');
      _vm.rollbackAudioDeviceToPriority();
      _vm.hidePush();
    }
  }

  Widget getAudioDeviceWidget() => Align(
      alignment: Alignment.bottomLeft,
      child: Padding(
          padding: EdgeInsets.zero,
          child: SafeArea(
              child: kIsWeb
                  ? AudioDeviceWidgetWeb(_vm.multimediaServer)
                  : AudioDeviceWidget(_vm.audioDeviceViewModel)
          )
      )
  );

  Widget getSpeakingPerson() {
    return Align(
      alignment: Alignment(1, -1),
      child: IgnorePointer(
        child: Material(
          color: Colors.transparent,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white54,
              borderRadius: BorderRadius.circular(15),
            ),
            height: 30,
            width: Get.size.width * UIConstants.speakingUserWidthFactor +
                UIConstants.microIconWidth,
            padding: EdgeInsets.only(
              right: 16,
              top: 2,
              bottom: 2,
            ),
            margin: EdgeInsets.only(
              left: 6,
              right: Get.size.width * UIConstants.speakingUserMarginWidthFactor,
              top: 6,
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  Assets.mic_on_icon,
                  color: UIConstants.purpleIconColor,
                ),
                SizedBox(
                  width: Get.size.width * UIConstants.speakingUserWidthFactor,
                  child: Obx(
                        () => Text(
                      _vm.speakingStudent.value.overflow,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: UIConstants.purpleIconColor,
                        fontSize: 14,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}