import 'dart:async';
import 'dart:core';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class PersonalLectureWidget<T extends PersonalLectureViewModel> extends LectureWidget
    with SnapshotableMixin, WidgetsBindingObserver, MultimediaLectureWidgetMixin<T> {
  final _vm = Get.find<T>();

  @override
  LectureViewModel get vm => _vm;

  PersonalLectureWidget() : super() {
    eventHandlers.addAll({
      // StudentConnectedEvent: (args) => _onStudentConnected(args),
      // StudentDisconnectedEvent: (args) => _onStudentDisconnected(args),
      ServerOperationErrorEvent: (args) => _onServerOperationError(args)
    });
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  Future<void> close() async {
    WidgetsBinding.instance.removeObserver(this);
    Get.offNamedUntil(
        Routes.scheduledLectures, ModalRoute.withName(Routes.mainMenu));
  }

  @override
  Widget getAdditionalWidget() => Obx(() => Stack(
      children:[
        if (_vm.speakingStudent.value.isNotEmpty)
          getSpeakingPerson(),
        if (_vm.isAudioInited.value || kIsWeb)
          getAudioDeviceWidget(),
        if (_vm.isOnline.value && !_vm.isConnects)
          _getNoConnectionMessage(),
      ]
  ));

  Widget _getNoConnectionMessage() => Obx(() {
    if (!vm.isConnectedToInternet.value) return SizedBox.shrink();

    if (!_vm.isLectureServerConnected.value)
      return _getNoServerConnectionMessage(Translations.no_lecture_server_connection);
    else if (!_vm.isMultimediaServerConnected.value)
      return _getNoServerConnectionMessage(Translations.no_multimedia_server_connection);
    else
      return SizedBox.shrink();
  });

  Widget _getNoServerConnectionMessage(String translationKey) => Align(
      alignment: Alignment.topCenter,
      child: Container(
          child: Text(
              translate(translationKey),
              style: TextStyle(color: Colors.white, fontSize: 14)),
          margin: EdgeInsets.all(16.0),
          padding: EdgeInsets.all(8.0),
          decoration: ShapeDecoration(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0))),
              color: Colors.red.withAlpha(196))
      )
  );

  @override
  List<Widget> getButtons() => [
    getButton(Assets.plus_icon, _vm.addWhiteboard),
    Obx(() => getButton(_vm.hasAudio.value ? Assets.mic_on_icon : Assets.mic_off_icon, _vm.changeMicState)),
    getButton(Assets.image_icon, _vm.addImageWithProgress),
    getButton(Assets.photo_icon, takePhoto),
    Obx(() => getButton(Assets.pen_icon, showColorMenu, color: _vm.drawingController.color.value)),
    Obx(() => getButton(Assets.thickness_icon, showStrokeWidthMenu, color: _vm.drawingController.color.value)),
    getButton(Assets.revert_icon, _vm.undoDrawing),
    getButton(Assets.options_icon, _showOptions),
  ];

  _showOptions() {
    return showModalBottomSheet(
        context: Get.context,
        builder: (BuildContext bc) {
          return ListView(
              padding: EdgeInsets.all(8.0),
              shrinkWrap: true,
              children: [
                _getSnapshotOption(),
                // _getInstallOnHolderOption(),
                _getSettingsOption(),
                if (_vm.isActiveLecture)
                  _getCheckingModeOption(),
                _getExitOption()
              ]
          );
        }
    );
  }

  ListTile _getSnapshotOption() => ListTile(
      leading: SvgWidget.asset(Assets.share_icon, width: buttonSize, color: UIConstants.primaryDarkColor),
      title: Text(translate(Translations.share_snapshot)),
      onTap: () async {
        Get.back();
        final result = await generateWhiteboardSnapshot(snapshotGlobalKey.currentContext);
        if (!result) {
          await showAlertDialog(
              translate(Translations.failure), translate(Translations.cant_share_snapshot));
        }
      }
  );

  // ListTile _getInstallOnHolderOption() => ListTile(
  //     leading: Padding(
  //         padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
  //         child: SvgWidget.asset(
  //             Assets.install_on_holder_icon,
  //             width: buttonSize,
  //             color: UIConstants.primaryDarkColor
  //         )
  //     ),
  //     title: Text(translate(Translations.install_on_holder)),
  //     onTap: () => Get.offNamed(Routes.installOnHolder)
  // );

  ListTile _getSettingsOption() => ListTile(
      leading: Padding(
          padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
          child: SvgWidget.asset(
              Assets.settings_icon,
              width: buttonSize,
              color: UIConstants.primaryDarkColor
          )
      ),
      title: Text(translate(Translations.settings)),
      onTap: () => Get.offNamed(Routes.settings)
  );

  ListTile _getCheckingModeOption() => ListTile(
      leading: SvgWidget.asset(
          Assets.stop_icon,
          width: buttonSize,
          color: UIConstants.primaryDarkColor
      ),
      title: Text(translate(Translations.finish_lecture_for_students)),
      onTap: () async {
        await showAlertDialog(
            translate(Translations.finish_lecture_for_students),
            translate(Translations.want_to_finish),
            variants: {
              translate(Translations.yes): _vm.setLectureCheckingMode,
              translate(Translations.no): null});
        Get.back();
      }
  );

  ListTile _getExitOption() => ListTile(
      leading: SvgWidget.asset(
          Assets.close_icon,
          width: buttonSize,
          color: UIConstants.primaryDarkColor
      ),
      title: Text(translate(Translations.exit)),
      onTap: () async => _vm.isOnline.value ? await _exitLectureVariants() : await exitLecture()
  );

  Future<void> _exitLectureVariants() {
    return showAlertSelector(translate(Translations.exit), {
      translate(Translations.finish_lecture_exit): () async {
        await _vm.exitLectureWithFinish();
        await close();
      },
      translate(Translations.exit_lecture_without_finish): () async {
        await _vm.exitLectureWithoutFinish();
        await close();
      }
    });
  }

  @override
  getFloatingActionButton() =>
      Obx(() => _vm.isOnline.value ? SizedBox.shrink() : _getStartLectureButton());

  _getStartLectureButton() => FloatingActionButton(
      child: SvgWidget.asset(Assets.play_icon, color: Colors.white),
      backgroundColor: UIConstants.purpleIconColor,
      onPressed: () async {
        if (!_vm.canStartLecture()) {
          await showAlertDialog(_vm.subjectName,
              translate(Translations.you_cannot_teach_more_than_two_lecture));
          return;
        }
        await showAlertDialog(
            _vm.subjectName, translate(Translations.want_start_lecture),
            variants: {
              translate(Translations.yes): _tryStartLecture,
              translate(Translations.no): null});
      });

  Future<void> _tryStartLecture() {
    if (_vm.checkConnectivityAndNotify()) {
      return _vm.tryStartLecture();
    } else {
      return showAlertDialog(
          translate(Translations.error), translate(Translations.no_internet_connection));
    }
  }

  // _onStudentConnected(StudentConnectionEvent args) async {
  //   var text = args.studentName + ' ' + translate(Translations.connected);
  //   var backgroundColor = Colors.green.withAlpha(196);
  //   await _showStudentSnackBar(text, backgroundColor);
  // }
  //
  // _onStudentDisconnected(StudentConnectionEvent args) async {
  //   var text = args.studentName + ' ' + translate(Translations.disconnected);
  //   var backgroundColor = Colors.red.withAlpha(196);
  //   await _showStudentSnackBar(text, backgroundColor);
  // }

  _onServerOperationError(ServerOperationErrorEvent args) =>
      showAlertDialog(translate(Translations.error), translate(args.errorMessage));

  _showStudentSnackBar(String text, Color backgroundColor) => Get.showSnackbar(GetBar(
      messageText: Text(text, style: TextStyle(color: Colors.white, fontSize: 14)),
      backgroundColor: backgroundColor,
      duration: Duration(seconds: 3)));
}