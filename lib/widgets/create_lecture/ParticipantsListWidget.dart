import 'package:flutter/material.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class ParticipantsListWidget extends BaseStatelessWidget {
  final GlobalKey<TagsState> _tagStateKey = GlobalKey<TagsState>();

  @override
  ParticipantsListViewModel get vm => Get.find<ParticipantsListViewModel>();

  ParticipantsListWidget() {
    eventHandlers.addAll({
      ShowParticipantInfoEvent: (args) => _showParticipantInfoDialog(args),
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: CustomAppBar(
          titleKey: Translations.participants_list,
          actionIconData: Icons.search, onActionPressed: _showSearchParticipantWidget
      ),
      backgroundColor: UIConstants.backgroundColor,
      floatingActionButton: _getConfirmButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: _getMainBody(),
  );

  Widget _getMainBody() => SafeArea(
    child: Obx(() => vm.isGroupsUpdating.value || vm.isContactsUpdating.value
        ? getNewProgressIndicator()
        : vm.isNoContacts
            ? _getNoContactsText()
            : RefreshIndicator(
                edgeOffset: 60.0,
                onRefresh: () async {
                  vm.fetchUserContacts();
                },
                child:Column(
                  children: [
                    _getSelectedBubbles(),
                    _getAllContactList(),
                ])),
    ));

  Widget _getAllContactList() => Expanded(
    child: ListView(
      children: [
          CustomExpansionTile(
              headerBackgroundColor: UIConstants.backgroundColor,
              textColor: UIConstants.defaultTextColor,
              iconColor: UIConstants.defaultTextColor,
              title: _getBlockHeaderText(Translations.groups),
              initiallyExpanded: true,
              children: [
                _getGroups()
              ]),
          CustomExpansionTile(
              headerBackgroundColor: UIConstants.backgroundColor,
              textColor: UIConstants.defaultTextColor,
              iconColor: UIConstants.defaultTextColor,
              title: _getBlockHeaderText(Translations.contacts),
              initiallyExpanded: true,
              children: [
                _getContacts()
              ]),
     ]));

  Widget _getBlockHeaderText(String textKey) => Text(
      translate(textKey),
      style: TextStyle(
          color: UIConstants.defaultTextColor,
          fontSize: UIConstants.tabBarFontSize,
          fontFamily: UIConstants.fontFamily,
          fontWeight: FontWeight.w500,
          decoration: TextDecoration.none
      ));

  Widget _getGroups() => _getParticipantList(vm.userGroupsItemList);
  Widget _getContacts() => _getParticipantList(vm.userContactsItemList);

  Widget _getParticipantList(RxList participantList) => Column(
      mainAxisSize: MainAxisSize.min,
      children: participantList.map((item) => _getParticipantListViewCard(item)).toList(),
    );
  
  Widget _getParticipantListViewCard(ParticipantListItemViewModel item) =>
        Padding(
            padding: EdgeInsets.all(4.0),
            child: item.contact is Group
                ? UserGroupListItemWidget(item)
                : UserContactListItemWidget(item)
        );

  Widget _getNoContactsText() => IgnorePointer(
      child: Container(
          margin: EdgeInsets.all(20.0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _getNoContactHeaderText(),
                _getInfoTextStr(translate(Translations.easy_to_fix_this)),
                _getInfoTextStr(
                    translate(Translations.create_lecture_without_participants), needMarker: true),
                _getInfoTextStr(
                    translate(Translations.send_invite_link), needMarker: true),
                _getInfoTextStr(
                    translate(Translations.can_see_contacts_after_lecture), needMarker: true),
              ])));

  Widget _getNoContactHeaderText() => Padding(
      padding: EdgeInsets.only(bottom: 10.0),
      child:  Text(translate(Translations.no_contacts),
          style: TextStyle(
              color: UIConstants.defaultTextColor,
              fontSize: 18.0,
              fontFamily: UIConstants.fontFamily,
              fontWeight: FontWeight.w500,
              decoration: TextDecoration.none
          )));

  Widget _getInfoTextStr(String text, {bool needMarker = false}) => Padding(
      padding: EdgeInsets.all(4.0),
      child: Text(needMarker ? '• $text' : text,
          style: TextStyle(
              color: UIConstants.defaultTextColor,
              fontSize: UIConstants.defaultFontSize,
              fontFamily: UIConstants.fontFamily,
              fontWeight: FontWeight.w500,
              decoration: TextDecoration.none
          )));

  Widget _getSelectedBubbles() => Container(
      constraints: BoxConstraints.tightFor(width: double.infinity, height: 48.0),
      padding: EdgeInsets.symmetric(horizontal: 8.0),
      alignment: Alignment.centerLeft,
      child: vm.selectedParticipants.isNotEmpty
          ? _getTagsList()
          : Text(translate(Translations.select_participants),
          style: TextStyle(
            color: UIConstants.lightGrayColor,
            fontSize: UIConstants.defaultFontSize,
            fontFamily: UIConstants.fontFamily,
          )));

  Widget _getTagsList() => Obx(() => Tags(
      key: _tagStateKey,
      horizontalScroll: true,
      itemCount: vm.selectedParticipants.length,
      itemBuilder: (int index) {
        final item = vm.selectedParticipants[index];

        return ItemTags(
            key: Key(index.toString()),
            index: index,
            title: item.tagName,
            textStyle: TextStyle(
              fontSize: UIConstants.defaultFontSize,
              fontFamily: UIConstants.fontFamily,
            ),
            pressEnabled: true,
            activeColor: UIConstants.primaryDarkColor,
            splashColor: UIConstants.primaryDarkColor,
            combine: ItemTagsCombine.withTextBefore,
            onPressed: (element) => vm.removeParticipant(item),
            removeButton: ItemTagsRemoveButton(
              size: 18.0,
              backgroundColor: UIConstants.primaryDarkColor,
              color: UIConstants.buttonTextColor,
              onRemoved: () => vm.removeParticipant(item),
            ), // OR null,
        );
      },
    ));

  Widget _getConfirmButton() => FloatingActionButton(
      child: Icon(Icons.check_rounded, color: UIConstants.buttonTextColor),
      backgroundColor: UIConstants.purpleIconColor,
      onPressed: () => Get.back()
  );

  Future<void> _showSearchParticipantWidget() async {
    final searchResult = await showSearch<ContactViewModel>(
        context: Get.context, delegate: SearchParticipant(vm.searchParticipantList));
    if (searchResult == null)
      return;

    vm.addParticipant(searchResult);
  }

  Future<void> _showParticipantInfoDialog(ShowParticipantInfoEvent args) {
    if (args.participant is! Group) return null;

    // region todo del this temp dialog after second group task stage will be finished
    final participant = args.participant as Group;
    return showAlertDialog(participant.fullName, participant.membersNames.toString());
    // end region

    // todo add showGroupMembers widget from second group task stage
  }
}