import 'package:flutter/material.dart';

import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

abstract class ParticipantListItemWidget extends TranslatedStatelessWidget {
  ParticipantListItemViewModel get vm;

  @override
  Widget build(BuildContext context) => Container(
    margin: EdgeInsets.only(left: 40),
    child: ListTile(
      dense: true,
      leading: _getAvatar(),
      title: Text(vm.name),
      trailing: Obx(() => vm.isSelected.value
           ? SvgWidget.asset(Assets.checkbox_icon, color: UIConstants.primaryDarkColor)
           : SvgWidget.asset(Assets.checkbox_blank_icon, color: UIConstants.primaryDarkColor)),
      onTap: vm.selectParticipantAction,
      onLongPress: vm.participantInfoAction,
    ),
  );

  Widget _getAvatar();
}

class UserGroupListItemWidget extends ParticipantListItemWidget {
  final UserGroupListItemViewModel _vm;

  @override
  UserGroupListItemViewModel get vm => _vm;

  UserGroupListItemWidget(this._vm);

  @override
  Widget _getAvatar() => Container(
    child: SvgWidget.asset(Assets.groups_icon),
  );
}

class UserContactListItemWidget extends ParticipantListItemWidget {
  final UserContactListItemViewModel _vm;

  @override
  UserContactListItemViewModel get vm => _vm;

  UserContactListItemWidget(this._vm);

  @override
  Widget _getAvatar() => Container(
    child: SvgWidget.asset(Assets.contacts_icon),
  );
}