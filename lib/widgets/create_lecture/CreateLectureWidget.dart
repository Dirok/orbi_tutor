import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:intl/intl.dart';
import 'package:tutor/focused.dart';

class CreateLectureWidget extends BaseStatelessWidget {
  final oneYearDuration = Duration(days: 365);
  final dateFormat = DateFormat('dd.MM.yyyy');  // todo [OT-664]

  final _vm = Get.find<CreateLectureViewModel>();

  @override
  BaseViewModel get vm => _vm;

  String get defGroupName => '${_vm.subject.value.name} ${dateFormat.format(_vm.date.value)}';

  CreateLectureWidget() {
    eventHandlers.addAll({
      CantOpenLectureEvent: (_) => _onCantOpenLectureEvent(),
      OpenGroupLectureEvent: (args) => _onOpenGroupLectureEvent(args),
      OpenPersonalLectureEvent: (args) => _onOpenPersonalLectureEvent(args),
    });
  }

  void _onCantOpenLectureEvent() => showAlertDialog(
      translate(Translations.failure),
      translate(Translations.cant_start_lecture));

  void _onOpenGroupLectureEvent(OpenGroupLectureEvent e) => Get.offNamed(
      e.isOwner ? Routes.teacherActiveGroupLecture : Routes.studentActiveGroupLecture);

  void _onOpenPersonalLectureEvent(OpenPersonalLectureEvent e) =>
      Get.offNamed(Routes.personalLecture);

  @override
  Widget build(BuildContext context) => Scaffold(
      extendBodyBehindAppBar: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: UIConstants.backgroundColor,
      appBar: CustomAppBar(titleKey: Translations.create_new_lecture),
      body: _getMainBody(),
  );

  Widget _getMainBody() => SafeArea(
    child: Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        _getLectureSettingsTable(),
        _getStartLectureButton(),
      ],
    ),
  );

  Widget _getLectureSettingsTable() => Expanded(
    child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(top: 8.0),
      children: [
        Obx(() => ListTile(
            title: _getBoldText(translate(Translations.lecture_date)),
            trailing: _getListTileTrailing(DateFormat('dd.MM.yyyy').format(_vm.date.value)),
            onTap: () => _vm.isCreatingLecture.value ? null : _selectDateAsync())),
        Obx(() => ListTile(
            title: _getBoldText(translate(Translations.lecture_time)),
            trailing: _getListTileTrailing(
                _vm.isDefaultTimeSet.value
                  ? translate(Translations.current_time)
                  : _vm.time.value.format(Get.context)),
            onTap: () => _vm.isCreatingLecture.value ? null : _selectTimeAsync())),
        Divider(height: 5.0, indent: 20.0, endIndent: 20.0),
        Obx(() => ListTile(
            title: _getBoldText(translate(Translations.chosen_discipline)),
            trailing:  _getListTileTrailing(
                _vm.subject.value.isUnknown
                    ? translate(Translations.not_chosen)
                    : _vm.subject.value.name),
            onTap: () => _vm.isCreatingLecture.value ? null : _selectSubjectAsync())),
        Obx(() => ListTile(
            title: _getBoldText(translate(Translations.participants)),
            trailing:  _getListTileTrailing(
                _vm.participantsList.isEmpty
                    ? translate(Translations.no_participants)
                    : _vm.participantsList.map((item) => item.tagName).join(', '),
                textColor: _vm.participantsList.isEmpty
                    ? UIConstants.lightGrayColor
                    : UIConstants.primaryDarkColor),
            onTap: () => _vm.isCreatingLecture.value ? null : _selectParticipantsAsync())),
        Divider(height: 5.0, indent: 20.0, endIndent: 20.0),
        Obx(() => ListTile(
            title: _getBoldText(translate(Translations.lecture_description)),
            trailing: _getListTileTrailing(
                _vm.description.value.isEmpty
                    ? translate(Translations.optional)
                    : _vm.description.value,
                textColor: _vm.description.value.isEmpty
                    ? UIConstants.lightGrayColor
                    : UIConstants.primaryDarkColor),
            onTap: _openDescriptionModal,
          ),
        ),
        // todo add policy
      ]),
  );

  void _openDescriptionModal() => showTextFieldDialog(
      title: translate(Translations.add_description),
      text: _vm.description.value,
      hint: Translations.lecture_description,
      textInputAction: TextInputAction.done,
      maxLines: 3,
      maxLength: 120,
      variants: {translate(Translations.ok): (v) => _vm.description.value = v});

  Widget _getStartLectureButton() => Center(
    child: Container(
      constraints: BoxConstraints.tightFor(width: 200.0, height: 50.0),
      margin: EdgeInsets.all(16.0),
      child: Obx(() => ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: UIConstants.primaryDarkColor,
              onPrimary: UIConstants.buttonTextColor,
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)))),
          child: Text(translate(Translations.create).toUpperCase()),
          onPressed: (_vm.isCreatingLecture.value || _vm.subject.value.isUnknown)
              ? null
              : () {
                FocusScope.of(Get.context).unfocus();
                _showCreationOptions();
          }))));

  Widget _getBoldText(String textKey) => Text(
    textKey,
    style: TextStyle(
        color: UIConstants.defaultTextColor,
        fontSize: 16,
        fontFamily: UIConstants.fontFamily,
        fontWeight: FontWeight.w700,
        decoration: TextDecoration.none
    ));

  Widget _getListTileTrailing(String textKey, {Color textColor = UIConstants.primaryDarkColor}) =>
      Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              width: Get.width / 2 - 10,
              child: Text(
                textKey,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.end,
                style: TextStyle(fontWeight: FontWeight.bold).copyWith(
                  color: textColor,
                ),
              ),
            ),
            Icon(Icons.arrow_right, color: UIConstants.primaryDarkColor)
          ]);

  void _selectDateAsync() async {
    final now = DateTime.now();
    var pickedDate = await showDatePicker(
        context: Get.context,
        initialDate: _vm.date.value,
        firstDate: now,
        lastDate: now.add(oneYearDuration));
    if (pickedDate == null) return;

    _vm.date.value = pickedDate;
    Log.info(_vm.date.value);
  }

  void _selectTimeAsync() async {
    var displayedTime = _vm.time.value ?? TimeOfDay.now();
    var pickedTime = await showTimePicker(context: Get.context, initialTime: displayedTime);
    if (pickedTime == null) return;

    _vm.time.value = pickedTime;
    Log.info(_vm.time.value);
    _vm.isDefaultTimeSet.value = false;
  }

  void _selectSubjectAsync() async {
    final selectResult = await Get.toNamed(Routes.selectSubject);
    if (selectResult != null)
      _vm.subject.value = selectResult;
  }

  void _selectParticipantsAsync() => Get.toNamed(Routes.participantsList);

  void _showCreationOptions() async {
    bool checkingResult = _vm.checkParticipantsInGroup() || _vm.participantsList.isEmpty;
    if (!checkingResult)
      await _showParticipantsNotInGroupDialog();

    checkingResult = await _vm.checkLectureSettings();
    if (!checkingResult)
      checkingResult = await _showSubjectInPastDialog();
    if (!checkingResult)
      return null;

    bool creationResult = await _vm.tryCreateLecture();
    if (!creationResult) {
      _showCreateLectureErrorDialog();
      return null;
    }

    return showAlertSelector(translate(Translations.lecture_was_created), {
      translate(Translations.open_lecture): _vm.openLecture,
      translate(Translations.schedule_new_lecture): null,
      translate(Translations.return_to_scheduled): () => Get.offNamedUntil(
          Routes.scheduledLectures, ModalRoute.withName(Routes.mainMenu))
    });
  }

  Future<void> _showParticipantsNotInGroupDialog() => showTextFieldDialog(
      title: translate(Translations.group_name),
      text: _vm.groupName.isNotEmpty ? _vm.groupName.value : defGroupName,
      hint: defGroupName,
      textInputAction: TextInputAction.done,
      message: '${translate(Translations.participants_not_group_members)}.\n'
                  '${translate(Translations.create_group_for_fast_lesson_creation)}',
      variants: {
        translate(Translations.ok): (v) => _vm.groupName.value = v,  // todo make group (at second group task stage)
        translate(Translations.skip): null
      },
    );

  Future<bool> _showSubjectInPastDialog() async {
    bool isNeedAbort = true;

    await showAlertDialog(
        '${translate(Translations.current_time)} - ${DateFormat('HH:mm').format(DateTime.now()).toString()}',
        '${translate(Translations.schedule_lecture_in_past)}.\n'
            '${translate(Translations.want_to_schedule_lecture)}',
        variants: {
          translate(Translations.yes): () => {isNeedAbort = false},
          translate(Translations.no): null});

    return isNeedAbort;
  }

  Future<void> _showCreateLectureErrorDialog() => showAlertDialog(
      translate(Translations.failure), translate(Translations.cant_create_lecture));
}