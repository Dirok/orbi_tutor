import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class SearchSubject extends SearchDelegate<Subject> {
  final SelectSubjectViewModel vm;

  SearchSubject(this.vm);

  @override
  List<Widget> buildActions(BuildContext context) => [
    IconButton(
      icon: Icon(Icons.clear, color: UIConstants.primaryDarkColor),
      onPressed: () => query = '',
    )];

  @override
  Widget buildLeading(BuildContext context) =>
      IconButton(
          icon: SvgWidget.asset(Assets.back_arrow_icon, color: UIConstants.primaryDarkColor,
              width: UIConstants.iconSize, height: UIConstants.iconSize),
          onPressed: () => Get.back()
      );

  @override
  Widget buildResults(BuildContext context) => const SizedBox.shrink();

  @override
  Widget buildSuggestions(BuildContext context) {
    List<Subject> suggestionList = vm.getSuggestionList(query);

    return ListView.builder(
      itemCount: suggestionList.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(
            suggestionList[index].name,
          ),
          leading: query.isEmpty ? Icon(Icons.access_time) : const SizedBox(),
          onTap: () {
            vm.setRecentSearchedSubject(suggestionList[index]);
            Get.back(result: suggestionList[index]);
          });
      });
  }
}