import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class SubjectItemWidget extends BaseStatelessWidget {
  final SubjectItemViewModel _vm;
  final isRecent;

  SubjectItemViewModel get vm => _vm;

  SubjectItemWidget(this._vm, this.isRecent);

  @override
  Widget build(BuildContext context) => Obx(() =>
    ListTile(
      leading: isRecent
        ? Icon(Icons.access_time)
        : const SizedBox.shrink(),
      title: Text(vm.subject.name),
      selectedTileColor: UIConstants.primaryLightColor,
      selected: _vm.isCustom && _vm.isSelected.value,
      onTap: !_vm.isSelectionMode
        ? _vm.performDefaultAction
        : _vm.performSelectAction,
      onLongPress: _vm.performSelectAction,
      trailing: _vm.isCustom
        ? _getTrailingIcon()
        : const SizedBox.shrink(),
    ),
  );

  Widget _getTrailingIcon() {
    if (_vm.isSelectionMode) {
      return IconButton(
        icon: SvgWidget.asset(
          _vm.isSelected.value
            ? Assets.checkbox_icon
            : Assets.checkbox_blank_icon,
          width: UIConstants.iconSize,
          color: UIConstants.primaryDarkColor),
        onPressed: _vm.isSelectionMode
          ? _vm.performSelectAction
          : () async => await _showSubjectDeletionDialog(_vm)
      );
    } else {
      return _getSubjectDeletionButton();
    }
  }

  Widget _getSubjectDeletionButton() => IconButton(
    icon: SvgWidget.asset(Assets.trash_icon,
      width: UIConstants.buttonSize,
      color: UIConstants.primaryDarkColor),
    onPressed: () async => await _showSubjectDeletionDialog(_vm)
  );

  Future<void> _showSubjectDeletionDialog(SubjectItemViewModel item) async =>
    await showAlertDialog(
      '${item.subject.name}', translate(Translations.want_to_delete_subject),
      variants: {
        translate(Translations.yes): _vm.performDeleteSubject,
        translate(Translations.no): null
      });
}
