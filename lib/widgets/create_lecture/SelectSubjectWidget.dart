import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class SelectSubjectWidget extends BaseStatelessWidget {
  final _vm = Get.find<SelectSubjectViewModel>();
  BaseViewModel get vm => _vm;

  SelectSubjectWidget() {
    eventHandlers.addAll({
      SubjectDeleteErrorEvent: (_) => _onSubjectDeleteErrorEvent(),
      SubjectGetResultEvent: (e) => _onSubjectGetResultEvent(e),
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
      backgroundColor: UIConstants.backgroundColor,
      appBar: _getAppBar(),
      body: _getListViewBody(),
      floatingActionButton: _getSearchSubjectButton(),
  );

  Widget _getAppBar() => AppBar(
    backgroundColor: UIConstants.primaryLightColor,
    leading: Obx(() => !_vm.isSelectionMode()
      ? IconButton(
        icon: SvgWidget.asset(Assets.back_arrow_icon, color: UIConstants.primaryDarkColor,
            width: UIConstants.iconSize, height: UIConstants.iconSize),
        onPressed: () => Get.back()
      )
      : IconButton(
        icon: SvgWidget.asset(Assets.revert_icon,
          color: UIConstants.primaryDarkColor,
          width: UIConstants.iconSize,
          height: UIConstants.iconSize),
        onPressed: _vm.clearSelectedSubjects
    )),
    title: Obx(() => Text(!_vm.isSelectionMode()
      ? translate(Translations.select_subject)
      : _vm.selectedSubjects.length.toString(),
      style: TextStyle(
        color: UIConstants.defaultTextColor,
        fontSize: UIConstants.appBarFontSize,
        fontFamily: UIConstants.fontFamily,
        fontWeight: FontWeight.w700,
        decoration: TextDecoration.none)),
    ),
    actions: [
      Obx(() => !_vm.isSelectionMode()
        ? Container(
            margin: const EdgeInsets.only(right: 16.0),
            child: IconButton(
              icon: SvgWidget.asset(Assets.plus_icon,
                color: UIConstants.primaryDarkColor,
                width: UIConstants.iconSize * 2),
              onPressed: _showSubjectCreationDialog,
            ))
        : const SizedBox.shrink()),
      Obx(() => _vm.isSelectionMode()
        ? IconButton(
            icon: Icon(
              Icons.checklist,
              size: UIConstants.iconSize,
              color: UIConstants.primaryDarkColor,
            ),
            onPressed: _vm.selectAllSubjects
          )
        : const SizedBox.shrink()),
      Obx(() => _vm.isSelectionMode()
        ? IconButton(
            icon: Icon(
              Icons.clear_all,
              size: UIConstants.iconSize,
              color: UIConstants.primaryDarkColor,
            ),
            onPressed: _vm.clearSelectedSubjects
          )
        : const SizedBox.shrink()),
      Obx(() => _vm.isSelectionMode()
        ? IconButton(
            icon: SvgWidget.asset(Assets.trash_icon,
              width: UIConstants.buttonSize,
              color: UIConstants.primaryDarkColor),
            onPressed: () {
              _showSubjectMultiDeletionDialog(_vm.selectedSubjects);
            }
          )
        : const SizedBox.shrink()),
    ]);

  Widget _getListViewBody() => SafeArea(
    child: ListView(
        children: [
          Obx(() => _vm.userSubjects.isNotEmpty
            ? _getUserSubjectsList(_vm.userSubjects)
            : const SizedBox.shrink()),

          Obx(() => _vm.isPredefSubjectListVisible.value || _vm.userSubjects.isEmpty
            ? _getPredefinedSubjectList(_vm.thinoutedPredefSubject)
            : _getShowAllButton()),
        ]),
  );

  Widget _getSearchSubjectButton() => FloatingActionButton(
      child: Icon(Icons.search, size: UIConstants.iconSize, color: UIConstants.buttonTextColor),
      backgroundColor: UIConstants.purpleIconColor,
      onPressed: _showSearchSubjectWidget,
  );

  Widget _getUserSubjectsList(RxList<SubjectItemViewModel> subjects) => Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: []
        ..addAll(_getSubjectList(subjects, isRecent: true))
        ..add(_getListSeparator())
  );

  Widget _getPredefinedSubjectList(RxList<SubjectItemViewModel> subjects) => Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: []
        ..addAll(_getSubjectList(subjects))
  );

  Iterable<Widget> _getSubjectList(RxList<SubjectItemViewModel> items, {bool isRecent = false}) =>
      items.map((item) => SubjectItemWidget(item, isRecent));

  Widget _getShowAllButton() => TextButton(
    style: TextButton.styleFrom(
      visualDensity: VisualDensity.compact,
    ),
    child: Text(translate(Translations.show_all),
        style: TextStyle(
            fontSize: UIConstants.buttonFontSize,
            color: UIConstants.primaryDarkColor)),
    onPressed: () => _vm.isPredefSubjectListVisible.value = !_vm.isPredefSubjectListVisible.value,
  );

  Widget _getListSeparator() => Align(
    alignment: Alignment.bottomCenter,
    child: Divider(indent: 20.0, endIndent: 20.0, color: Colors.grey),
  );

  Future<void> _showSearchSubjectWidget() async {
    final searchResult = await showSearch<Subject>(
      context: Get.context,
      delegate: SearchSubject(_vm));
    if (searchResult == null)
      return;

    _vm.selectedSubject.value = searchResult;
    Get.back(result: searchResult);
  }

  Future<void> _showSubjectCreationDialog() => showTextFieldDialog(
      title: translate(Translations.enter_new_subject_name),
      hint: Translations.name,
      textInputAction: TextInputAction.done,
      variants: { translate(Translations.create): _checkNewSubjectName},
      maxLength: 50);

  Future<void> _showSubjectAlreadyExistDialog(Subject existingSubject) async => await showAlertDialog(
      translate(Translations.warning), translate(Translations.subject_already_exist),
      variants: {
        translate(Translations.yes): () => Get.back(result: existingSubject),
        translate(Translations.no): null
      });

  Future<void> _showSubjectCreationErrorDialog() => showAlertDialog(
      translate(Translations.failure), translate(Translations.cant_create_subject));

  Future<void> _showSubjectMultiDeletionDialog(RxSet<SubjectItemViewModel> selectedSubjects) {
    final currSubject = '${translate(Translations.chosen_discipline)} ' +
      (selectedSubjects.length > 1
      ? '(${selectedSubjects.length}) - ' +
        selectedSubjects.fold('', (prev, element) =>
          prev + (prev.isEmpty ? '' : ', ') + element.subject.name)
      : '- ${selectedSubjects.first.subject.name}');

    return showAlertDialog(translate(Translations.want_to_delete_subject), currSubject,
      variants: {
        translate(Translations.yes): () async => await _deleteSubjects(selectedSubjects),
        translate(Translations.no): null
      });
  }

  Future<void> _createNewSubject(String subjectName) async {
    final createdSubject = await _vm.tryCreateSubject(subjectName);
    if (createdSubject == null)
      _showSubjectCreationErrorDialog();
  }

  Future<void> _deleteSubjects(Iterable<SubjectItemViewModel> subjects) async {
    // todo add the ability to delete a list within one request
    subjects.forEach((item) async {
      item.performDeleteSubject();
    });
    _vm.clearSelectedSubjects();
  }

  Future<void> _checkNewSubjectName(String subjectName) async {
    final name = subjectName.trim();
    if (name.length > 0) {
      final existingSubject = _vm.searchForDuplicates(name);
      existingSubject != null
          ? _showSubjectAlreadyExistDialog(existingSubject)
          : _createNewSubject(name);
    } else {
      _showSubjectCreationErrorDialog();
    }
  }

  Future<void> _showSubjectDeletionErrorDialog() => showAlertDialog(
      translate(Translations.failure), translate(Translations.cant_delete_subject));

  void _onSubjectDeleteErrorEvent() => _showSubjectDeletionErrorDialog();

  void _onSubjectGetResultEvent(SubjectGetResultEvent e) => Get.back(result: e.subject);
}
