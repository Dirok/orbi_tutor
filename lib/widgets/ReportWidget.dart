import 'dart:core';

import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class ReportWidget extends BaseStatelessWidget {
  final _vm = Get.find<ReportViewModel>();

  final _emailController = CustomTextEditController();
  final _issueController = CustomTextEditController();

  @override
  BaseViewModel get vm => _vm;

  ReportWidget() {
    eventHandlers.addAll({
      CollectionErrorEvent: (_) => _showLogsCollectionErrorDialog(),
      UploadErrorEvent: (_) => _showLogsUploadingErrorDialog(),
      TimeoutErrorEvent: (_) => _showTimeoutErrorDialog(),
      PlatformWebEvent: (_) => _showPlatformWebErrorDialog(),
    });
  }

  void _showLogsCollectionErrorDialog() {
    Log.warning('logs collection error');
    showAlertDialog(
        translate(Translations.failure), translate(Translations.failed_to_create_report));
  }

  void _showLogsUploadingErrorDialog() {
    Log.warning('logs uploading error');
    showAlertDialog(translate(Translations.failure), translate(Translations.failed_to_send_report));
  }

  void _showTimeoutErrorDialog() {
    Log.warning('logs uploading error');
    showAlertDialog(
        translate(Translations.failure), translate(Translations.no_report_server_connection));
  }

  void _updateSendLock() =>
      _vm.isSendLocked.value = !_emailController.isValid || !_issueController.isValid;

  void _showPlatformWebErrorDialog() {
    Log.warning('platform web error');
    showAlertDialog(translate(Translations.failure), translate(Translations.platform_web_error));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () => _onBackPressed(context),
        child: GestureDetector(
          onTap: FocusScope.of(context).unfocus,
          child: Stack(
            alignment: AlignmentDirectional.bottomCenter,
            children: [
              SafeArea(
                child: Scaffold(
                  extendBodyBehindAppBar: false,
                  appBar: _getAppBar(context),
                  body: ListView(
                    shrinkWrap: true,
                    padding: EdgeInsets.all(16.0),
                    children: [
                      ListTile(title: _getBoldText(translate(Translations.report_description))),
                      const SizedBox(height: 8.0),
                      _getEmailForm(),
                      _getIssueForm(),
                      const SizedBox(height: 8.0),
                      _getMainButton(),
                    ],
                  ),
                ),
              ),
              Obx(
                () => _vm.isSendingRequest.value
                    ? UploadCircleProgressIndicator(progress: _vm.progressPercentValue.value)
                    : const SizedBox.shrink(),
              )
            ],
          ),
        ));
  }

  Widget _getAppBar(BuildContext context) => AppBar(
        backgroundColor: UIConstants.primaryLightColor,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: SvgWidget.asset(Assets.back_arrow_icon,
                  color: UIConstants.primaryDarkColor, width: 24.0, height: 24.0),
              onPressed: () => _onBackPressed(context),
            );
          },
        ),
        title: Text(translate(Translations.report_an_issue),
            style: TextStyle(
                color: UIConstants.defaultTextColor,
                fontSize: 20,
                fontFamily: UIConstants.fontFamily,
                fontWeight: FontWeight.w700,
                decoration: TextDecoration.none)),
      );

  Widget _getEmailForm() => EmailTextEdit(
      text: _vm.email.value,
      hint: Translations.login,
      onTextChanged: (v) {
        _vm.email.value = v;
        _updateSendLock();
      },
      onFieldSubmitted: (_) => _issueController.requestFocus(),
      textInputAction: TextInputAction.next,
      controller: _emailController);

  Widget _getIssueForm() => CustomTextEdit(
      text: _vm.issue.value,
      hint: Translations.describe_issue,
      onTextChanged: (v) {
        _vm.issue.value = v;
        _updateSendLock();
      },
      textInputAction: TextInputAction.next,
      controller: _issueController);

  Widget _getMainButton() => Center(
        child: Container(
          constraints: BoxConstraints.tightFor(width: 200, height: 50),
          margin: EdgeInsets.all(16),
          child: Obx(
            () => ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: UIConstants.primaryDarkColor,
                  onPrimary: UIConstants.buttonTextColor,
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)))),
              child: Text(translate(Translations.send).toUpperCase()),
              onPressed: (_vm.isSendLocked.value || _vm.isSendingRequest.value)
                  ? null
                  : () {
                      FocusScope.of(Get.context).unfocus();
                      sendRequest();
                    },
            ),
          ),
        ),
      );

  Future<void> sendRequest() async {
    if (!_vm.checkConnectivityAndNotify()) return;

    if (await _vm.trySendRequest()) {
      showAlertDialog(
          translate(Translations.success), translate(Translations.report_sent_successfully));
    }
  }

  Widget _getBoldText(String textKey) => Text(
        textKey,
        style: TextStyle(
            color: UIConstants.defaultTextColor,
            fontSize: 16,
            fontFamily: UIConstants.fontFamily,
            fontWeight: FontWeight.w700,
            decoration: TextDecoration.none),
      );

  Future<bool> _onBackPressed(BuildContext context) async {
    /// this temporary: for pre cache some variants
    await precacheImage(AssetImage(Assets.student_auth_info_background), context);

    Get.back();
    return false;
  }
}
