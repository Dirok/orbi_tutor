import 'dart:async';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class MainMenuWidget extends BaseStatelessWidget {
  @override
  BaseViewModel get vm => Get.find<MainMenuViewModel>();

  final DoubleBackExitMixin _doubleBackExit = DoubleBackExitMixin();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _doubleBackExit.onBackPressed,
        child: Stack(children: [
          Scaffold(
            backgroundColor: UIConstants.primaryLightColor,
            appBar: _getAppBar(context),
            drawer: DrawerMenu().getDrawer(context),
            body: Container(
              alignment: Alignment.center,
              child: Container(
                height: Get.context.isLandscape
                    ? Get.size.height * UIConstants.mainMenuButtonHeightFactor
                    : Get.size.height,
                width: Get.context.isLandscape
                    ? null
                    : Get.size.width * UIConstants.mainMenuButtonHeightFactor,
                child: Get.context.isLandscape
                    ? Row(
                        mainAxisAlignment:
                            appSession.account.hasCreationPermissions
                                ? MainAxisAlignment.spaceEvenly
                                : MainAxisAlignment.center,
                        children: _getMainButtons(),
                      )
                    : SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Container(
                          height: Get.size.height,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: _getMainButtons(),
                          ),
                        ),
                      ),
              ),
            ),
          ),
          if (Settings.server.isDebug)
            _getNotReleaseServerLabel()
        ]));
  }

  List<Widget> _getMainButtons() => [
        const Spacer(),
        _getImageButtonWithText(
            Translations.scheduled,
            Assets.scheduled_lessons_image,
            () => Get.toNamed(Routes.scheduledLectures)),
        appSession.account.hasCreationPermissions
            ? const Spacer()
            : const SizedBox.shrink(),
        appSession.account.hasCreationPermissions
            ? _getImageButtonWithText(Translations.create_new,
                Assets.new_lesson_image, _onCreateLecturePressed)
            : const SizedBox.shrink(),
        const Spacer(),
        _getImageButtonWithText(
            Translations.finished,
            Assets.finished_lessons_image,
            () => Get.toNamed(Routes.passedLectures)),
        const Spacer(),
        Get.context.isLandscape
            ? SizedBox(height: 32)
            : const SizedBox.shrink(),
      ];

  Widget _getImageButtonWithText(
          String textKey, String imageAssetName, Function action) =>
      Column(
        children: [
          SizedBox(
            height: (Get.context.isLandscape ? Get.size.height : Get.size.width) *
                    UIConstants.mainMenuButtonWidthFactor,
            child: AspectRatio(
              aspectRatio: 1,
              child: _getImageButton(imageAssetName, action),
            ),
          ),
          const SizedBox(height: 16.0),
          _getText(translate(textKey)),
        ],
      );

  Future<void> _onCreateLecturePressed() async {
    if (!vm.checkConnectivityAndNotify()) return;

    if (appSession.partner == Partners.kundelik) {
      return showAlertDialog(translate(Translations.warning),
        translate(Translations.create_lecture_kundelik), variants: {
        translate(Translations.continue_): () => Get.toNamed(Routes.createLecture),
        translate(Translations.cancel): null });
    }

    return Get.toNamed(Routes.createLecture);
  }

  _getAppBar(BuildContext context) => AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: SvgWidget.asset(Assets.main_menu_icon,
                  color: UIConstants.primaryDarkColor,
                  width: UIConstants.iconSize,
                  height: UIConstants.iconSize),
              onPressed: () => Scaffold.of(context).openDrawer(),
            );
          },
        ),
        centerTitle: false,
        title: Text(translate(Translations.my_lectures),
            style: TextStyle(
                color: UIConstants.defaultTextColor,
                fontSize: UIConstants.appBarFontSize,
                fontFamily: UIConstants.fontFamily,
                fontWeight: FontWeight.w700,
                decoration: TextDecoration.none)),
        actions: [
          if (Get.context.isLandscape)
            Padding(
                padding: EdgeInsets.only(right: 16.0, top: 8),
                child: SvgWidget.asset(Assets.logo_image, width: 150))
        ],
      );

  _getImageButton(String imageAssetName, Function action) =>
      ElevatedButton(
        style: ElevatedButton.styleFrom(
            padding: EdgeInsets.zero,
            primary: UIConstants.backgroundColor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10)
            )),
        child: SvgWidget.asset(imageAssetName, width: double.infinity, height: double.infinity),
        onPressed: action,
      );

  _getText(String data) => Text(data,
        style: TextStyle(
            color: UIConstants.primaryDarkColor,
            fontSize: 20,
            fontFamily: UIConstants.fontFamily,
            fontWeight: FontWeight.w700,
            decoration: TextDecoration.none)
    );

  _getNotReleaseServerLabel() => Container(
      alignment: Get.context.isLandscape
          ? Alignment.topCenter
          : Alignment.bottomCenter,
      margin: EdgeInsets.only(top: 24.0),
      child: Text('${Settings.server.name} server',
          style: TextStyle(color: Colors.redAccent, fontSize: 24)));
}