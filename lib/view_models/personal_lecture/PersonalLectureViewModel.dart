import 'dart:async';

import 'package:bson/bson.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

class PersonalLectureViewModel extends LectureViewModel
    with MultimediaLectureViewModelMixin
    implements IActiveLectureViewModel {
  static const _lectureServerReconnectInterval = Duration(seconds: 3);

  final List<StreamSubscription> subscribes = [];

  final _lectureServer = LectureServerRealmApi();

  final studentViewModelById = <ObjectId, StudentListElementActiveGroupLectureViewModel>{};
  StudentListElementActiveGroupLectureViewModel teacherViewModel;

  final isLectureServerConnects = false.obs;
  final isLectureServerConnected = false.obs;

  bool get isConnects => isLectureServerConnects.value && isMultimediaServerConnects.value;

  final isOnline = false.obs;

  WhiteboardSet _whiteboardSet;

  Timer _lectureServerReconnectionTimer;

  @override
  ILectureServerApi get lectureServer => _lectureServer;

  @override
  String get imagesFolderPath => lecture.imagesFolderPath;

  bool get isActiveLecture => lecture.state == LectureState.active;

  User get teacher => lecture.owner;

  @override
  WhiteboardSet get currentWhiteboardSet => _whiteboardSet;

  @override
  void onInit() {
    super.onInit();

    subscribeLectureServerEvents();

    initMultimediaMixin(this);

    hasAudio.value = true;

    teacherViewModel = StudentListElementActiveGroupLectureViewModel(teacher);

    initializeFuture.value = _init();
  }

  @override
  void onClose() async {
    if (lecture.state == LectureState.active)
      multimediaServer.leaveRoom();

    await audioDeviceViewModel.deinit();

    subscribes.forEach((s) => s.cancel());
    unsubscribeLectureServerEvents();

    await multimediaServer.close();
    await lectureServer.close();

    super.onClose();
  }

  subscribeLectureServerEvents() {
    lectureServer
      ..mainWhiteboardChanged = _onCurrentWhiteboardChanged
      ..imageReceived = _onImageReceived
      ..userWhiteboardInserted = _onUserWhiteboardInserted
      ..whiteboardImageChanged = _onWhiteboardImageChanged
      ..drawingPathAdded = _onDrawingPathAdded
      ..drawingPointAdded = _onDrawingPointAdded
      ..lastDrawingPathRemoved = _onLastDrawingPathRemoved
      ..drawingCleared = _onDrawingCleaned
      ..connectionClosed = _onLectureServerConnectionClosed
      ..userConnected = _onUserConnected
      ..userDisconnected = _onUserDisconnected
      // ..userHandStateChanged = _onUserHandStateChanged
      ..userMicStateChanged = _userMicStateChanged;
  }

  unsubscribeLectureServerEvents() {
    lectureServer
      ..mainWhiteboardChanged = null
      ..imageReceived = null
      ..userWhiteboardInserted = null
      ..whiteboardImageChanged = null
      ..drawingPathAdded = null
      ..drawingPointAdded = null
      ..lastDrawingPathRemoved = null
      ..drawingCleared = null
      ..connectionClosed = null
      ..userConnected = null
      ..userDisconnected = null
      // ..userHandStateChanged = null
      ..userMicStateChanged = null;
  }

  Future<void> initConnection() async {
    subscribes.add(isConnectedToInternet
        .listen((_) async => await _tryConnectToServers()));

    await _tryConnectToServers();
  }

  Future<void> _tryConnectToServers() async {
    if (!isConnectedToInternet.value) return;

    if (!lectureServer.isConnected)
      await _tryConnectToLectureServer();

    if (!multimediaServer.isConnected)
      await tryConnectToMultimediaServer();
  }

  Future<void> _tryConnectToLectureServer() async {
    isLectureServerConnects.value = true;

    isLectureServerConnected.value = await lectureServer.connect(lecture);
    if (!isLectureServerConnected.value) {
      Log.error('Unable to connect to lecture server ${lecture.server}');

      _startLectureServerReconnectionTimer();

      isLectureServerConnects.value = false;
      return;
    }

    // initializeFuture.value = onLectureServerConnected();

    isLectureServerConnects.value = false;
    return;
  }

  @override
  Future<void> insertWhiteboard(WhiteboardSet whiteboardSet) async {
    super.insertWhiteboard(whiteboardSet);
    // if (lecture.state == LectureState.active)
      await insertWhiteboardOnline(whiteboardSet);
  }

  Future<void> loadOrCreateWhiteboardSet() async {
    _whiteboardSet = await lectureServer.getCommonWhiteboardSet();

    if (_whiteboardSet != null) {
      if(_whiteboardSet.length > 0)
        await loadWhiteboardSetData(_whiteboardSet);
      else
        await insertWhiteboard(_whiteboardSet);
    } else {
      _whiteboardSet = WhiteboardSet(userId);
      await lectureServer.sendCommonWhiteboardSet(_whiteboardSet);
      await insertWhiteboard(_whiteboardSet);
    }
  }

  void _onLectureServerConnectionClosed() {
    isLectureServerConnected.value = false;
    _startLectureServerReconnectionTimer();
  }

  void _startLectureServerReconnectionTimer() {
    _lectureServerReconnectionTimer?.cancel();
    if (lecture.state == LectureState.active)
      _lectureServerReconnectionTimer = Timer(_lectureServerReconnectInterval, _tryConnectToLectureServer);
  }

  Future<void> insertWhiteboardOnline(WhiteboardSet whiteboardSet) async {
    final wb = whiteboardSet.current;
    await lectureServer.sendInsertWhiteboard(whiteboardSet.currentIndex, wb, whiteboardSet.id);
    await lectureServer.sendSetCurrentWhiteboard(wb.id, whiteboardSet.id);
    await lectureServer.subscribeWhiteboard(wb.id);
  }

  Future<void> changeWhiteboard(int index) async {
    super.changeWhiteboard(index);
    // if (lecture.state != LectureState.active) return;
    if (!lectureServer.isConnected) return;
    try {
      await lectureServer.sendSetCurrentWhiteboard(currentWhiteboard.value.id, _whiteboardSet.id);
    } on CommandFailedException catch (e) {
      Log.error(e);
    }
  }

  void _onWhiteboardImageChanged(ObjectId whiteboardId, String imageName) {
    Log.warning('Not implemented');

    // TODO change for any whiteboard
    // if (whiteboardId == currentWhiteboard.value.id)
    //   currentWhiteboard.update((val) => val.name = imageName);
  }

  void _onImageReceived(ImageMessage msg) async {
    var wb = _getWhiteboardById(msg.whiteboardId);
    if (wb == null) return;

    if (!wb.imageIds.contains(msg.imageInfo.id))
      wb.images.add(msg.imageInfo);

    if (wb == currentWhiteboard.value)
      currentWhiteboard.refresh();
  }

  Future<void> loadWhiteboardSetData(WhiteboardSet whiteboardSet) async {
    await loadWhiteboardDataAndSubscribe(whiteboardSet.current);
    setCurrentWhiteboard(whiteboardSet);
    preloadDataForNotActiveWhiteboards(whiteboardSet);
  }

  Future<void> loadWhiteboardDataAndSubscribe(Whiteboard wb) async {
    await loadWhiteboardData(wb);
    await lectureServer.subscribeWhiteboard(wb.id);
  }

  Future<void> loadWhiteboardData(Whiteboard wb) async {
    await _loadImage(wb);
    await _loadDrawingData(wb);
  }

  Future<void> preloadDataForNotActiveWhiteboards(WhiteboardSet whiteboardSet) async {
    for (var wb in whiteboardSet.whiteboards) {
      if (wb != whiteboardSet.current)
        await loadWhiteboardDataAndSubscribe(wb);
    }
  }

  Future<void> _loadDrawingData(Whiteboard wb) async =>
      wb.drawingSet = await lectureServer.getAllDrawingData(wb);

  Future<void> _loadImage(Whiteboard wb) async {
    if (wb.image == null) return;
    wb.setImageFolderPath(imagesFolderPath);
    if (!await File(wb.image.path).exists())
      lectureServer.requestWhiteboardImage(wb.id, wb.image);
  }

  @override
  Future<bool> addPhoto(Whiteboard wb, String imagePath, CameraResult result) async {
    final wb = currentWhiteboard.value;
    final isSuccess = await super.addPhoto(wb, imagePath, result);
    return isSuccess ? _trySendImage(wb) : false;
  }

  @override
  Future<bool> addImage(Whiteboard wb) async {
    var wb = currentWhiteboard.value;
    final isSuccess = await super.addImage(wb);
    return isSuccess ? _trySendImage(wb) : false;
  }

  Future<bool> _trySendImage(Whiteboard wb) async {
    // if (lecture.state != LectureState.active) return false;
    if (!lectureServer.isConnected) return false;

    final isExists = await File(wb.image.path).exists();
    if (!isExists) return false;

    final result = await lectureServer.uploadImage(lecture.key, wb.image, lecture.id, wb.id);
    if (result == SendDataResult.success) return true;

    // TODO send fail events

    wb.images.removeLast();

    if (wb == currentWhiteboard.value)
      currentWhiteboard.refresh();

    return false;
  }

  _onDrawingPathAdded(ObjectId whiteboardId, DrawingData drawingData, ObjectId userId) {
    var wb = _getWhiteboardById(whiteboardId);
    if (wb == null) return;

    wb.addDrawingPath(drawingData, userId);

    if (wb == currentWhiteboard.value)
      currentWhiteboard.refresh();
  }

  _onDrawingPointAdded(ObjectId whiteboardId, DrawingPoint point, ObjectId userId) {
    var wb = _getWhiteboardById(whiteboardId);
    if (wb == null) return;

    wb.addDrawingPoint(point, userId);

    if (wb == currentWhiteboard.value)
      currentWhiteboard.refresh();
  }

  _onLastDrawingPathRemoved(ObjectId whiteboardId, ObjectId userId) {
    var wb = _getWhiteboardById(whiteboardId);
    if (wb == null) return;

    var removedId = wb.removeLastDrawingPath(userId);

    if (wb == currentWhiteboard.value && removedId != null)
      currentWhiteboard.refresh();
  }

  _onDrawingCleaned(ObjectId whiteboardId) async {
    var wb = _getWhiteboardById(whiteboardId);
    if (wb == null) return;

    wb.clearDrawing();

    if (wb == currentWhiteboard.value)
      currentWhiteboard.refresh();
  }

  undoDrawing() {
    var removedId = currentWhiteboard.value.removeLastDrawingPath(userId);
    if (removedId == null) return;

    currentWhiteboard.refresh();

    if(lectureServer.isConnected)
      lectureServer.undoDrawing(currentWhiteboard.value.id, removedId);
  }

  clearDrawing() {
    currentWhiteboard.value.clearDrawing();

    currentWhiteboard.refresh();

    if(lectureServer.isConnected)
      lectureServer.clearDrawing(currentWhiteboard.value.id);
  }

  Whiteboard _getWhiteboardById(ObjectId whiteboardId) {
    if (_whiteboardSet != null) {
      var wb = _whiteboardSet.getWhiteboardById(whiteboardId);
      if (wb != null) return wb;
    }

    return null;
  }

  ///////////////////////////

  Future<void> _init() async {
    isLectureServerConnected.value =
      await lectureServer.connect(lecture, jwt: appSession.firebaseJwt, userId: userId);

    await loadOrCreateWhiteboardSet();

    if (lecture.state == LectureState.active) {
      isOnline.value = true;
      await initConnection();
    }
  }

  Future<void> addWhiteboard() => insertWhiteboard(currentWhiteboardSet);

  Future<void> tryStartLecture() async {
    Lecture lectureInfo;
    try {
      lectureInfo = await lectureDataService.tryStartLecture(lecture.id);
    } on ServerOperationException catch (e) {
      Log.error(e);
      final error = e.errorType;
      final errorMessage = error == ServerOperationError.lectureAlreadyActive
          ? Translations.lecture_already_active_error
          : Translations.error;
      sendEvent(ServerOperationErrorEvent(errorMessage));
      return;
    }

    if (lectureInfo == null) {
      Log.error('Failed to start lecture');
      return;
    }

    lecture = lectureInfo;

    isOnline.value = true;

    await startMultimediaServer();

    await initConnection();
  }

  void _onCurrentWhiteboardChanged(MainWhiteboardInfo info) {
    if (_whiteboardSet.setCurrentById(info.id))
      setCurrentWhiteboard(_whiteboardSet);
  }

  void _onUserWhiteboardInserted(Whiteboard wb, int index) async {
    wb.setImageFolderPath(imagesFolderPath);

    _whiteboardSet.insert(index, wb);

    updateWhiteboardIndexTitle(_whiteboardSet);

    await loadWhiteboardData(wb);
    await lectureServer.subscribeWhiteboard(wb.id);
  }

  Future<void> _setStudentAllowSpeak(ObjectId studentId, bool allow) async {
    if (!lectureServer.isConnected) return;

    bool allowSpeak;
    try {
      allowSpeak = await lectureServer.sendStudentAllowSpeak(studentId, allow);
    } on CommandFailedException catch(e) {
      Log.error('sendStudentAllowSpeak() error', exception: e);
      return;
    }

    // if (_studentViewModelById.containsKey(studentId)) {
    //   var studentViewModel = _studentViewModelById[studentId];
    //   studentViewModel.allowSpeak.value = allowSpeak;
    // }
  }

  _onUserConnected(ObjectId userId) async {
    // if (!_studentViewModelById.containsKey(userId)) return;
    if (!lectureServer.isConnected) return;

    UserState state;
    try {
      state = await lectureServer.getUserState(userId);
    } on CommandFailedException catch (e) {
      Log.error('_onUserConnected($userId) error', exception: e);
      return;
    }

    // _studentViewModelById[userId]
    //   ..isOnline.value = true
    //   ..hasWhiteboards.value = true
    //   ..allowSpeak.value = state.allowSpeak
    //   ..isHandRaised.value = state.isHandRaised
    //   ..isMicEnabled.value = state.isMicEnabled;
    //
    // _updateHandsInfo(userId, state.isHandRaised);
    //
    // studentViewModelList.sort();

    var studentName = _getStudentName(userId);
    // sendEvent(StudentConnectedEvent(studentName));
  }

  _onUserDisconnected(ObjectId userId) {
    // if (!_studentViewModelById.containsKey(userId)) return;
    //
    // _studentViewModelById[userId]
    //   ..isOnline.value = false
    //   ..allowSpeak.value = false
    //   ..isHandRaised.value = false
    //   ..isMicEnabled.value = false;
    //
    // _updateHandsInfo(userId, false);
    //
    // studentViewModelList.sort();

    var studentName = _getStudentName(userId);
    // sendEvent(StudentDisconnectedEvent(studentName));
  }

  _userMicStateChanged(ObjectId userId, bool isEnabled) {
    // if (!_studentViewModelById.containsKey(userId)) return;
    //
    // var studentViewModel = _studentViewModelById[userId];
    // studentViewModel.isMicEnabled.value = isEnabled;
  }

  Future<void> exitLectureWithFinish() async {
    isOnline.value = false;

    await multimediaServer.leaveRoom();
    await stopMultimediaServer();

    final response = await lectureDataService.tryStopLecture(lecture);
    if (!response)
      Log.warning('Lecture not finished correctly');

    subscribes.forEach((s) => s.cancel());

    await lectureStorageService.removeLectureData(lecture);
  }

  Future<void> setLectureCheckingMode() async {
    await multimediaServer.leaveRoom();
    await stopMultimediaServer();

    final response = await lectureDataService.trySetCheckingMode(lecture);
    if (!response)
      Log.warning('Lecture not finished correctly');

    lecture.state = LectureState.checking;
  }

  Future<void> exitLectureWithoutFinish() async {
    isOnline.value = false;
    await multimediaServer.leaveRoom();
    subscribes.forEach((s) => s.cancel());
  }

  bool canStartLecture() {
    return lectureDataService.allLecturesMap.values
        .where((element) => element.state == LectureState.active)
        .length < Settings.allowedNumOfActiveLectures;
  }

  String _getStudentName(ObjectId studentId) => 'USER'; //TODO

  Future<void> onMicStateChanged(bool isEnabled) => lectureServer.setMicState(isEnabled);
}