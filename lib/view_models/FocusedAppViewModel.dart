import 'dart:async';
import 'dart:convert';

import 'package:bson/bson.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class FocusedAppViewModel extends BaseViewModel {
  final _linkService = Get.find<UriLinkService>();
  final _authService = Get.find<AuthorizationService>();
  final _serverUpdatesServices = Get.find<ServerUpdatesService>();
  final _lectureDataService = Get.find<LectureDataService>();
  final _userDataService = Get.find<UserDataService>();
  final _pushService = Get.find<PushService>();
  final _configService = Get.find<ConfigService>();
  final _mainServer = Get.find<MainServerApi>();

  final Map<Type, Function(Event)> _linkServiceEventHandlers = Map();
  final Map<Type, Function(Event)> _firebaseSingInServiceEventHandlers = Map();
  final Map<Type, Function(Event)> _mainServerEventHandlers = Map();
  final Map<Type, Function(Event)> _configEventHandlers = Map();
  final Map<Type, Function(Event)> _pushServiceEventHandlers = Map();
  final Map<Type, Function(Event)> _userDataServiceEventHandlers = Map();

  final _subscriptions = <StreamSubscription>[];

  final isAuthProcessed = false.obs;

  final isInited = false.obs;

  Future<void> Function() _postAuthAction;

  bool isNewVersion() => PlatformUtils.getBuildNumber() > Settings.lastShownChangelog;

  FocusedAppViewModel() {
    _mainServerEventHandlers.addAll({
      TimeoutError: (e) => sendEvent(ServerTimeoutError()),
      RequestError: (e) => sendEvent(ServerRequestError()),
      ParseError: (e) => sendEvent(ServerInvalidResultError()),
      UnauthorizedError: (e) => sendEvent(ServerUnauthorizedError()),
      ServerError: (e) => sendEvent(ServerCommonError()),
      TokenRefreshEvent: (e) => _refreshToken(e),
    });

    _linkServiceEventHandlers.addAll({
      ParseFailedLinkEvent: (e) => _onParseFailedLinkEvent(),
      SignInLinkEvent: (e) => _onSignInLinkEvent(e),
      OpenLectureLinkEvent: (e) => _onOpenLectureLinkEvent(e)
    });

    _configEventHandlers.addAll({
      UpdateNeededEvent: (e) => sendEvent(UpdateNeededEvent()),
      ServersFetchedEvent: (e) => _onServersFetchedEvent(),
    });

    _pushServiceEventHandlers.addAll({
      NavigateToLectureEvent: (e) => _onNavigateToLectureEvent(e),
      ShowSnackEvent: (e) => _onShowSnackEvent(e),
    });

    _firebaseSingInServiceEventHandlers.addAll({
      SignedOutEvent: (e) => sendEvent(DeAuthorizeUserEvent()),
      FirebaseJwtUpdatedEvent: (e) => _setFirebaseToken(e),
    });

    _userDataServiceEventHandlers.addAll({
      FetchingSubjectsErrorEvent: (e) => sendEvent(ShowFetchingSubjectsErrorEvent()),
      FetchingUserContactsErrorEvent: (e) => sendEvent(ShowFetchingUserContactsErrorEvent()),
    });

    _pushService.listenEvents((e) => _pushServiceEventHandlers[e.runtimeType]?.call(e));
  }

  Future<void> init() async {
    Settings.localization = AppLocalizations.of(Get.context).locale.languageCode;
    Settings.country = AppLocalizations.of(Get.context).locale.countryCode;
    Log.info('countryCode: ${Settings.country} languageCode: ${Settings.localization}');

    if (isNewVersion()) {
      Log.info('New version of an app available');
      sendEvent(ShowChangelogEvent());
    }

    _configService.listenEvents((e) => _configEventHandlers[e.runtimeType]?.call(e));

    _subscriptions.add(
        _mainServer.listenEvents((e) =>
            _mainServerEventHandlers[e.runtimeType]?.call(e)));

    await _initConnectivity();

    _subscriptions.add(
        _linkService.listenEvents((e) =>
            _linkServiceEventHandlers[e.runtimeType]?.call(e)));

    _subscriptions.add(_authService.listenEvents(
        (e) => _firebaseSingInServiceEventHandlers[e.runtimeType]?.call(e)));

    _subscriptions.add(_userDataService.listenEvents(
            (e) => _userDataServiceEventHandlers[e.runtimeType]?.call(e)));

    isInited.value = true;
  }

  void _setFirebaseToken(FirebaseJwtUpdatedEvent event) {
    if (appSession == null)
      Log.info('appSession is null');
    else {
      Log.info('Firebase token updated');
      appSession.setFirebaseJwt(event.jwt);
    }
  }


  Future<void> _initConnectivity() async {
    final initialLinkEvent = _linkService.lastEvent;
    if (initialLinkEvent != null) {
      if (isConnectedToInternet.value)
        await _linkServiceEventHandlers[initialLinkEvent.runtimeType]
            ?.call(initialLinkEvent);
      else
        sendEvent(OpenLinkErrorEvent()); // Отправить ивент, что переход по ссылке не удался
    }

    await _tryAutoLogIn();
  }

  void close() {
    _subscriptions..forEach((s) => s.cancel())..clear();
    super.onClose();
  }

  Future<void> _refreshToken(TokenRefreshEvent event) async {
    appSession.setHttpToken(event.bundle.httpsToken);
    appSession.setRefreshToken(event.bundle.refreshToken);
  }

  Future<void> deAuthorizeUser() async {
    appSession.fcmToken = await _pushService.refreshToken();
    await appSession.reset();
    _serverUpdatesServices.deInit();
  }

  Future<void> _onServersFetchedEvent() async {
    // TODO move to ConfigService as getServers()
    final Map<String, String> servs =
        Map<String, String>.from(json.decode(_configService.config['servers'].asString()));
    final serversList = <ServerEnvironment>[];
    servs.forEach((key, value) {
      serversList.add(ServerEnvironment(key, value));
    });
    Settings.serverTypeAddress.addAll(serversList);
  }

  Future<bool> _tryAutoLogIn() async {
    if (appSession == null || appSession.isAuthorized || isAuthProcessed.value) return false;
    final isSuccess = await _authService.tryAutoLogin.callWithLock(isAuthProcessed);
    sendEvent(AutoLogInEvent());
    return isSuccess;
  }

  Future<void> callPostAuthAction({@required bool isSuccess}) async {
    if (isSuccess)
      await _postAuthAction?.call();

    _postAuthAction = null;
  }

  void _onParseFailedLinkEvent() {
    _postAuthAction = _tryAutoLogIn;
    sendEvent(InvalidLinkEvent());
  }

  Future<void> _onSignInLinkEvent(SignInLinkEvent args) async {
    final authResult = await (() async {
      await appSession?.reset();
      return _authService.authorizeWithLink(args.link);
    }).callWithLock(isAuthProcessed);

    if (authResult == AuthorizationResult.success)
      sendEvent(LinkAuthorizationSuccessEvent());
    else if (authResult == AuthorizationResult.fail)
      sendEvent(LinkAuthorizationFailedEvent());
  }

  Future<void> _onOpenLectureLinkEvent(OpenLectureLinkEvent args) async {
    final lectureId = args.lectureId;
    final partner = args.partner;

    if (partner != null && partner.isNotEmpty) {
      if (appSession?.partner != partner) {
        Log.info('User not logged in with partner $partner - try with him');
        _trySignInWithPartnerAndOpenLecture(partner, lectureId);
        return;
      }
    }

    if (appSession == null) {
      Log.info('User not logged in, ask about log in or anonymously entering');
      _postAuthAction = () => _tryOpenLecture(lectureId);
      sendEvent(NoSessionEvent());
      return;
    } else if (!appSession.isAuthorized) {
      Log.info('App session exists, but not authorized - wait for authorization');
      final isSuccess = await _tryAutoLogIn();
      if (!isSuccess) {
        if (partner != null && partner.isNotEmpty)
          _trySignInWithPartnerAndOpenLecture(partner, lectureId);
        else // for auth by hands
          _postAuthAction = () => _tryOpenLecture(lectureId);
        return;
      }
    }

    return _tryOpenLecture(lectureId);
  }

  void _trySignInWithPartnerAndOpenLecture(String partner, ObjectId lectureId) {
    _postAuthAction = () => _tryOpenLecture(lectureId);

    switch(partner) {
      case Partners.kundelik:
        sendEvent(SignInWithKundelikEvent());
        break;
      default:
        Log.error('Unknown partner: $partner');
        _postAuthAction = null;
        break;
    }
  }

  Future<void> _tryOpenLecture(ObjectId lectureId) async {
    _lectureDataService.init();

    final currentLectureId = _lectureDataService.currentLecture?.id;

    if (currentLectureId == null) {
      openLecture(lectureId);
    } else if (currentLectureId != lectureId) {
      sendEvent(ChangeLectureRequestEvent(lectureId));
    }
  }

  Future<void> openLecture(ObjectId lectureId) async {
    final lecture = await _lectureDataService.getLecture(lectureId)
        ?? await _lectureDataService.joinLecture(lectureId);

    if (lecture == null) {
      sendEvent(JoinLectureFailedEvent());
      return;
    }

    if (lecture.isActive)
      await _tryOpenActiveLecture(lecture);
    else if (lecture.isPlanned)
      sendEvent(OpenScheduledLecturesEvent());
    else if (lecture.isPassed)
      sendEvent(OpenPassedLecturesEvent());
  }

  Future<void> _tryOpenActiveLecture(Lecture lecture) async {
    await _lectureDataService.fetchParticipantsToLecture(lecture);

    if (!lecture.participants.contains(appSession.account.user)) {
      Log.error('User not found in lecture participants after join!');
      sendEvent(JoinLectureFailedEvent());
      return;
    }

    _lectureDataService.currentLecture = lecture;

    var isGroupLecture = false;

    if (lecture.serverType == LectureServerType.unknown) {
      Log.error('Lecture has unknown server type!');
      return;
    } else if (lecture.serverType == LectureServerType.lectureServer) {
      if (appSession.account.hasSchoolLicense) {
        isGroupLecture = true;
      } else {
        Log.error('User not licensed for group lecture');
        return;
      }
    }

    sendEvent(OpenLectureEventFactory.create(
        isGroupLecture: isGroupLecture,
        isOwner: lecture.currentUserIsLectureOwner));
  }

  Future<void> _onNavigateToLectureEvent(NavigateToLectureEvent event) async {
    if (event.lectureId == null) {
      Log.error('No lecture id in notification');
      return;
    }

    await Future.delayed(Duration(milliseconds: 1000));
    await _tryOpenLecture(event.lectureId);
  }

  void _onShowSnackEvent(ShowSnackEvent event) => sendEvent(ShowSnackEvent(event.notification));
}

class InvalidLinkEvent extends Event {}
class LinkAuthorizationSuccessEvent extends Event {}
class LinkAuthorizationFailedEvent extends Event {}
class NoSessionEvent extends Event {}
class JoinLectureFailedEvent extends Event {}
class SignInEvent extends Event {}
class AutoLogInEvent extends Event {}
class SignInWithKundelikEvent extends Event {}
class LectureNotExistsEvent extends Event {}
class LectureNotAvailableEvent extends Event {}
class OpenScheduledLecturesEvent extends Event {}
class OpenPassedLecturesEvent extends Event {}
class ShowChangelogEvent extends Event {}
class DeAuthorizeUserEvent extends Event {}

class ShowFetchingSubjectsErrorEvent extends Event {}
class ShowFetchingUserContactsErrorEvent extends Event {}

class ServerTimeoutError extends Event {}
class ServerRequestError extends Event {}
class ServerInvalidResultError extends Event {}
class ServerUnauthorizedError extends Event {}
class ServerCommonError extends Event {}
