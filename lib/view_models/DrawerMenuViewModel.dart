import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class DrawerMenuViewModel extends BaseViewModel {
  final _pushService = Get.find<PushService>();

  Future<void> logOut() async {
    appSession.fcmToken = await _pushService.refreshToken();
    await appSession.reset();
    Get.find<ServerUpdatesService>().deInit();
  }
}