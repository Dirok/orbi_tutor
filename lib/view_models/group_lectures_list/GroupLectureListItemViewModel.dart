import 'package:bson/bson.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

typedef void LectureListAction(LectureListItemViewModel lecture);
typedef bool IsSelectionMode();

abstract class LectureListItemViewModel extends GetxController {
  final _userDataService = Get.find<UserDataService>();

  final Lecture lecture;
  final LectureListAction openBoardsAction;
  final LectureListAction showOptionsAction;
  final LectureListAction selectItemAction;
  final isSelected = false.obs;
  final IsSelectionMode _isSelectionMode;

  Subject _subject;

  ObjectId get id => lecture.id;
  LectureState get state => lecture.state;
  DateTime get startDateTime => lecture.startDateTime;
  String get subjectName => _subject?.name;
  // String get groupName => lecture.group.name;  // todo add groups and participants
  String get ownerName => lecture.owner.shortName;

  bool get currentUserIsLectureOwner => lecture.currentUserIsLectureOwner;

  LectureListItemViewModel(this.lecture, this.openBoardsAction, this.showOptionsAction,
      this.selectItemAction, this._isSelectionMode) {
    _subject = _userDataService.getSubjectById(lecture.subjectName);
  }

  void showLectureOptions();
  void performDefaultAction();
  void performSelectAction() {
    if (!currentUserIsLectureOwner) return;
    isSelected.value = !isSelected.value;
    selectItemAction?.call(this);
  }

  bool get isSelectionMode => _isSelectionMode?.call() ?? false;
}

class ScheduledLectureListItemViewModel extends LectureListItemViewModel
    implements Comparable<ScheduledLectureListItemViewModel>{

  bool get isActiveOrChecking => lecture.isActive || lecture.isOnTeacherCheck;
  bool get isPlanned => lecture.isPlanned;

  @override
  ScheduledLectureListItemViewModel(Lecture lecture, LectureListAction openBoardsAction,
      LectureListAction showOptionsAction, LectureListAction selectItemAction, IsSelectionMode isSelectionMode)
      : super(lecture, openBoardsAction, showOptionsAction, selectItemAction, isSelectionMode);

  @override
  int compareTo(ScheduledLectureListItemViewModel other) {
    var adt = lecture.startDateTime;
    var bdt = other.lecture.startDateTime;
    return adt.compareTo(bdt);
  }

  @override
  void showLectureOptions() {
    if (!lecture.isActive && !currentUserIsLectureOwner)
      return null;
    showOptionsAction?.call(this);
  }

  @override
  void performDefaultAction() => openBoardsAction?.call(this);

  @override
  void performSelectAction() {
    if (!isPlanned) return;
    super.performSelectAction();
  }

}

class PassedLectureListItemViewModel extends LectureListItemViewModel
    implements Comparable<PassedLectureListItemViewModel>{

  final LectureListAction _downloadLectureAction;
  final _download = Rx<Download>(null);

  bool get isDownloaded => lecture.dataLocation.value == Location.local || lecture.dataLocation.value == Location.everywhere;
  double get progress => lecture.downloadProgress.value;
  bool get showProgress => lecture.downloadState.value.isInProgress ?? false;
  bool get hasActiveDownload => _download.value != null;

  set download(Download value) => _download.value = value;

  PassedLectureWarningType get warningType {
    if (lecture.dataLocation() == Location.local)
      return PassedLectureWarningType.localFileOnly;
    if (lecture.isProcessingOnServer)
      return PassedLectureWarningType.fileProcessed;
    if (lecture.isOnTeacherCheck)
      return PassedLectureWarningType.onChecking;
    return PassedLectureWarningType.none;
  }

  @override
  PassedLectureListItemViewModel(Lecture lecture, LectureListAction openBoardsAction, LectureListAction showOptionsAction,
      this._downloadLectureAction, Download currentDownload, LectureListAction selectItemAction, IsSelectionMode isSelectionMode)
      : super(lecture, openBoardsAction, showOptionsAction, selectItemAction, isSelectionMode) {
    if (currentDownload != null)
      _download.value = currentDownload;
  }

  @override
  int compareTo(PassedLectureListItemViewModel other) {
    var adt = lecture.startDateTime;
    var bdt = other.lecture.startDateTime;
    return bdt.compareTo(adt);
  }

  @override
  void showLectureOptions() {
    if (!isDownloaded && !currentUserIsLectureOwner)
      return null;
    showOptionsAction?.call(this);
  }

  @override
  void performDefaultAction() {
    isDownloaded ? openBoardsAction?.call(this) : _downloadLectureAction?.call(this);
  }
}

enum PassedLectureWarningType {
  none,
  localFileOnly,
  fileProcessed,
  onChecking,
}