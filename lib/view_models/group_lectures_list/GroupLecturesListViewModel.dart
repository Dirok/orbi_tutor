import 'dart:async';

import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

abstract class LecturesListViewModel extends BaseViewModel {
  final _lectureDataService = Get.find<LectureDataService>();
  final _serverUpdatesService = Get.find<ServerUpdatesService>();
  final _userDataService = Get.find<UserDataService>();
  final _filtersViewModel = Get.find<LecturesFilterViewModel>();

  final List<StreamSubscription> _subscribes = [];

  RxBool get isLectureUpdating => _lectureDataService.isLectureUpdating;

  DateTime lastDate;
  var needAddDateSeparator;
  final selectedLectures = Set<LectureListItemViewModel>().obs;
  Iterable allLecturesList;
  RxBool get _isSelectionMode => RxBool(selectedLectures.length > 0);

  @override
  void onInit() {
    super.onInit();

    _serverUpdatesService.init();

    _lectureDataService
        .allLecturesMap
        .listen((_) => updateGroupedLecturesList())
        .addTo(_subscribes);

    _userDataService
        .userSubjectsMap
        .listen((_) => _filtersViewModel.updateFilters())
        .addTo(_subscribes);

    isConnectedToInternet
        .listen((_) => updateGroupedLecturesList())
        .addTo(_subscribes);

    updateGroupedLecturesList();

    _filtersViewModel.listenEvents((event) {
      if (event is ListUpdateErrorEvent)
        sendEvent(ListUpdateErrorEvent());
    });
  }

  @override
  void onClose() {
    _subscribes.disposeSubscriptions();
    super.onClose();
  }

  void updateGroupedLecturesList() {
    lastDate = DateTime(1970, 1, 1);
    needAddDateSeparator = false;
  }

  Future<void> updateList() async {
    await _userDataService.tryUpdateUserSubjects();
    updateGroupedLecturesList();
    selectedLectures.clear();
  }

  void showLectureOptions(LectureListItemViewModel lectureItem) =>
      sendEvent(ShowLectureOptionsEvent(lectureItem.currentUserIsLectureOwner, lectureItem));

  void selectItemAction(LectureListItemViewModel item) {
    if (item.isSelected.value) {
      selectedLectures.add(item);
    } else {
      selectedLectures.remove(item);
    }
  }

  void selectAllLectures() {
    allLecturesList.forEach((item) {
      if (!item.isSelected.value) {
        item.performSelectAction();
      }
    });
  }

  void clearSelectedLectures() {
    selectedLectures.forEach((item) {
      item.isSelected.value = false;
    });
    selectedLectures.clear();
  }

  bool isSelectionMode() => _isSelectionMode.value;

  Future<bool> deleteLecture(LectureListItemViewModel lecture, {bool removeFromServer}) =>
    _lectureDataService.deleteLecture(lecture.id, removeFromServer: removeFromServer);
}

class ScheduledLecturesListViewModel extends LecturesListViewModel {

  final scheduledActiveLectures = <ScheduledLectureListItemViewModel>[].obs;
  final scheduledPlannedLectures = <ScheduledLectureListItemViewModel>[].obs;
  Iterable get allLecturesList => scheduledPlannedLectures;
  bool _checkLecturesIsScheduled(Lecture lecture) {
    final lectureIsActiveOrPlanned = lecture.isActive || lecture.isPlanned;

    if (lecture.currentUserIsLectureParticipant && lectureIsActiveOrPlanned)
      return true;

    if (lecture.currentUserIsLectureOwner && (lecture.isOnTeacherCheck || lectureIsActiveOrPlanned))
      return true;

    return false;
  }

  @override
  void updateGroupedLecturesList() {
    super.updateGroupedLecturesList();

    var activeLectures = <ScheduledLectureListItemViewModel>[];
    var plannedLectures = <ScheduledLectureListItemViewModel>[];

    // TODO: Avoid adding null to the map
    final allLectures = Map.of(_lectureDataService.allLecturesMap);
    for (final lecture in allLectures.values) {
      if (lecture == null) continue;
      if (lecture.isUnknown) continue;

      if (!_checkLecturesIsScheduled(lecture)) continue;

      final lectureItemVM = ScheduledLectureListItemViewModel(
          lecture, tryOpenLecture, showLectureOptions, selectItemAction, isSelectionMode);

      if (lecture.isOnTeacherCheck || lecture.isActive) {
        activeLectures.add(lectureItemVM);
      } else {
        plannedLectures.add(lectureItemVM);
      }
    }

    activeLectures.sort();
    plannedLectures.sort();

    scheduledActiveLectures.assignAll(activeLectures);
    scheduledPlannedLectures.assignAll(plannedLectures);
  }

  FutureOr<void> tryOpenLecture(LectureListItemViewModel item) async {
    if ((item.lecture.currentUserIsLectureParticipant && item.state != LectureState.active) ||
        !checkConnectivityAndNotify()) return null;

    await _lectureDataService.fetchParticipantsToLecture(item.lecture);
    _lectureDataService.currentLecture = item.lecture;

    sendEvent(OpenLectureEventFactory.create(
        isGroupLecture: appSession.account.hasSchoolLicense,
        isOwner: item.currentUserIsLectureOwner));
  }
}

class PassedLecturesListViewModel extends LecturesListViewModel {

  final passedDownloadedLectures = <PassedLectureListItemViewModel>[].obs;
  final passedNotDownloadedLectures = <PassedLectureListItemViewModel>[].obs;
  Iterable get allLecturesList => passedDownloadedLectures.followedBy(passedNotDownloadedLectures);

  bool _checkLectureIsPassed(Lecture lecture) {
    final lectureIsProcessingOrPassed = lecture.isProcessingOnServer || lecture.isPassed;

    if (lecture.currentUserIsLectureOwner && lectureIsProcessingOrPassed)
      return true;

    if (lecture.currentUserIsLectureParticipant && (lecture.isOnTeacherCheck
        || lectureIsProcessingOrPassed))
      return true;

    return false;
  }

  PassedLectureListItemViewModel _createLectureVM(Lecture lecture) {
    // проверить есть ли загрузка
    final download = _lectureDataService.downloadsMap[lecture.id];

    return PassedLectureListItemViewModel(lecture, tryOpenLectureAsBoards,
        showLectureOptions, tryDownloadLecture, download, selectItemAction, isSelectionMode);
  }

  @override
  void updateGroupedLecturesList() {
    super.updateGroupedLecturesList();

    var downloadedLectures = <PassedLectureListItemViewModel>[];
    var notDownloadedLectures = <PassedLectureListItemViewModel>[];

    final allLectures = Map.of(_lectureDataService.allLecturesMap);
    for (final lecture in allLectures.values) {
      if (lecture.isUnknown) continue;

      if (!_checkLectureIsPassed(lecture)) continue;

      final lectureItemVM = _createLectureVM(lecture);

      if (lecture.hasDataLocation(Location.local)) {
        downloadedLectures.add(lectureItemVM);
      } else {
        notDownloadedLectures.add(lectureItemVM);
      }
    }

    downloadedLectures.sort();
    notDownloadedLectures.sort();

    passedDownloadedLectures.assignAll(downloadedLectures);
    passedNotDownloadedLectures.assignAll(notDownloadedLectures);
  }

  void tryOpenLectureAsPlayer(LectureListItemViewModel item) =>
    sendEvent(PlayerNotImplementedEvent());

  FutureOr<void> tryOpenLectureAsBoards(LectureListItemViewModel item) async {
    await _lectureDataService.fetchParticipantsToLecture(item.lecture);
    if (!checkConnectivityAndNotify()) return;
    _lectureDataService.currentLecture = item.lecture;
    sendEvent(OpenLectureEventFactory.create(
        isGroupLecture: appSession.account.hasSchoolLicense,
        isOwner: item.currentUserIsLectureOwner));
  }

  // todo lecture downloading
  FutureOr<void> tryDownloadLecture(LectureListItemViewModel item) async {
    if (!checkConnectivityAndNotify()) return;
  }
  // FutureOr<void> tryDownloadLesson(GroupLessonListItemViewModel lessonItem) async {
  //   if (!checkConnectivityAndNotify()) return;
  //   final lesson = lessonItem as PassedGroupLessonListItemViewModel;
  //   if (lesson.isDownloaded) {
  //     Log.warning('This lesson is already downloaded');
  //     return;
  //   }
  //
  //   if (lesson.hasActiveDownload) {
  //     Log.warning('This lesson is now downloading');
  //     sendEvent(LessonAlreadyDownloadingEvent());
  //     return;
  //   }
  //
  //   final id = lesson.id;
  //
  //   try {
  //     final download = await lessonsService.getOrCreateLessonDownload(id);
  //
  //     lesson.download = download;
  //
  //     StreamSubscription error;
  //     error = download.currentState.downloadError.listen((exception) {
  //       if (exception.runtimeType != StorageException) return;
  //       lesson.download = null;
  //       Log.error('Hashes didn\'t match: ${exception.toString()}');
  //       sendEvent(HashNoMatchEvent());
  //       error.cancel();
  //     });
  //
  //     download.start();
  //   } on LessonDownloadException catch (lde) {
  //     Log.error(lde);
  //     switch (lde.errorType) {
  //       case LessonDownloadErrorType.storagePermissionDenied:
  //       case LessonDownloadErrorType.noSuchLesson:
  //         break;
  //       case LessonDownloadErrorType.notEnoughSpace:
  //         sendEvent(NotEnoughSpaceEvent());
  //         break;
  //       default:
  //         sendEvent(UnknownErrorEvent());
  //     }
  //   } on UnpackingException catch (ue) {
  //     Log.error("failed to unpack downloaded lesson $id to path: ${ue.path}");
  //     sendEvent(UnknownErrorEvent());
  //   } catch (e) {
  //     Log.error("Unknown exception on lesson $id download", exception: e);
  //     sendEvent(UnknownErrorEvent());
  //   }
  // }
}

class ShowLectureOptionsEvent extends Event {
  final bool isOwner;
  final LectureListItemViewModel itemViewModel;
  ShowLectureOptionsEvent(this.isOwner, this.itemViewModel);
}

class NoNetworkEvent extends Event {}
class PlayerNotImplementedEvent extends Event {}
class LectureAlreadyDownloadingEvent extends Event {}
class NotEnoughSpaceEvent extends Event {}
class HashNoMatchEvent extends Event {}
class ListUpdateErrorEvent extends Event {}
class UnknownErrorEvent extends Event {}