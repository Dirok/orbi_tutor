import 'dart:async';
import 'dart:core';

import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

abstract class LecturesFilterViewModel extends BaseViewModel {
  final _lectureDataService = Get.find<LectureDataService>();

  DateTimeFilterViewModel fromDateTimeViewModel;
  DateTimeFilterViewModel toDateTimeViewModel;

  final ownersViewModelList = <CheckboxFilterListElementViewModel>[];
  final groupsViewModelList = <CheckboxFilterListElementViewModel>[];
  final subjectsViewModelList = <CheckboxFilterListElementViewModel>[];

  bool get isOwnersExpanded => appSession.account.preferences.isOwnersExpanded;
  set isOwnersExpanded(bool value) => appSession.account.preferences.isOwnersExpanded = value;

  bool get isGroupsExpanded => appSession.account.preferences.isGroupsExpanded;
  set isGroupsExpanded(bool value) => appSession.account.preferences.isGroupsExpanded = value;

  bool get isSubjectsExpanded => appSession.account.preferences.isSubjectsExpanded;
  set isSubjectsExpanded(bool value) => appSession.account.preferences.isSubjectsExpanded = value;

  List<StreamSubscription> _subscribes = [];

  LectureListFilter _filters;

  @override
  void onInit() {
    super.onInit();
    updateFilters();
  }

  @override
  void onClose() {
    _subscribes.disposeSubscriptions();
    super.onClose();
  }

  void updateFilters() {
    _filters.updateAllFilters();

    _updateCheckboxFilters();

    _updateSubscribeOnFilterChanging();
  }

  void _updateCheckboxFilters() {
    fromDateTimeViewModel = DateTimeFilterViewModel(_filters.fromDateTimeFilter);
    toDateTimeViewModel = DateTimeFilterViewModel(_filters.toDateTimeFilter);

    fromDateTimeViewModel.setValue(
      appSession.account.preferences.filterFromDateTime
        ?? _filters.fromDateTimeFilter.value
    );
    toDateTimeViewModel.setValue(
      appSession.account.preferences.filterToDateTime
        ?? _filters.toDateTimeFilter.value
    );

    ownersViewModelList.assignAll(_filters.ownersFilterList.map((f) => CheckboxFilterListElementViewModel(f)).toList());
    groupsViewModelList.assignAll(_filters.groupsFilterList.map((f) => CheckboxFilterListElementViewModel(f)).toList());
    subjectsViewModelList.assignAll(_filters.subjectsFilterList.map((f) => CheckboxFilterListElementViewModel(f)).toList());
  }

  void _updateSubscribeOnFilterChanging() {
    _subscribes.clear();

    fromDateTimeViewModel.filterValue.listen((_) {
      applyNewFilter();
      appSession.account.preferences.filterFromDateTime = fromDateTimeViewModel.filterValue.value;
    }).addTo(_subscribes);
    toDateTimeViewModel.filterValue.listen((_) {
      applyNewFilter();
      appSession.account.preferences.filterToDateTime = toDateTimeViewModel.filterValue.value;
    }).addTo(_subscribes);

    ownersViewModelList.forEach((e) => e.filterValue.listen((_) => applyNewFilter()).addTo(_subscribes));
    groupsViewModelList.forEach((e) => e.filterValue.listen((_) => applyNewFilter()).addTo(_subscribes));
    subjectsViewModelList.forEach((e) => e.filterValue.listen((_) => applyNewFilter()).addTo(_subscribes));
  }

  Future<void> applyNewFilter() async {
    Log.info('applyNewFilter');
    final result = await _lectureDataService.updateLectureList();
    if (!result) sendEvent(ListUpdateErrorEvent());
  }

  void resetFilter() {
    fromDateTimeViewModel.reset();
    toDateTimeViewModel.reset();
    ownersViewModelList.forEach((e) => e.reset());
    groupsViewModelList.forEach((e) => e.reset());
    subjectsViewModelList.forEach((e) => e.reset());

    appSession.account.preferences.filterFromDateTime = fromDateTimeViewModel.defaultValue;
    appSession.account.preferences.filterToDateTime = toDateTimeViewModel.defaultValue;
    applyNewFilter();
  }

  void expandedOwners(ExpansionPanelData panel) {
    appSession.account.preferences.isOwnersExpanded = panel.isExpanded;
  }

  void expandedGroups(ExpansionPanelData panel) {
    appSession.account.preferences.isGroupsExpanded = panel.isExpanded;
  }

  void expandedSubjects(ExpansionPanelData panel) {
    appSession.account.preferences.isSubjectsExpanded = panel.isExpanded;
  }
}

class ScheduledLecturesFilterViewModel extends LecturesFilterViewModel {
  LectureListFilter get _filters => _lectureDataService.scheduledLecturesFilter;
}

class PassedLecturesFilterViewModel extends LecturesFilterViewModel {
  LectureListFilter get _filters => _lectureDataService.passedLecturesFilter;
}

class FilterViewModel<T> extends GetxController {
  final Filter<T> _filter;

  final filterValue = Rx<T>(null);

  FilterViewModel(this._filter) {
    filterValue.value = _filter.value;
  }

  void setValue(T value) {
    this.filterValue.value = value;
    _filter.value = value;
  }

  void reset() {
    _filter.reset();
    this.filterValue.value = _filter.value;
  }
}

class DateTimeFilterViewModel extends FilterViewModel<DateTime> {
  bool get isLimit => filterValue.value == _filter.defaultValue;
  DateTime get defaultValue => _filter.defaultValue;
  DateTimeFilterViewModel(Filter<DateTime> filter) : super(filter);
}

class CheckboxFilterListElementViewModel extends FilterViewModel<bool> {
  String get name => (_filter as ToggledFilter).name;
  CheckboxFilterListElementViewModel(ToggledFilter filter) : super(filter);
}