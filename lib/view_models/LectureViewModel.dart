import 'dart:async';
import 'dart:typed_data';

import 'package:bson/bson.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' show join;
import 'package:screen/screen.dart';
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

abstract class LectureViewModel extends BaseViewModel {
  final drawingController = DrawingViewModel();
  final lectureStorageService = Get.find<LectureStorageService>();
  final lectureDataService = Get.find<LectureDataService>();
  final _userDataService = Get.find<UserDataService>();

  final _imageService = Get.find<ImageService>();

  final ImagePicker _picker = ImagePicker();
  final Camera _camera = Camera();

  Subject _subject;

  Lecture lecture;

  WhiteboardSet get currentWhiteboardSet;

  var currentWhiteboard = Rx<Whiteboard>(null);

  var indexTitleWidthFactor = 1.obs;
  var whiteboardsCount = 0.obs;
  var currentWhiteboardIndex = 0.obs;
  var isFirstWhiteboard = false.obs;
  var isLastWhiteboard = false.obs;

  var allowNavigateWhiteboards = true.obs;

  var initializeFuture = Future<void>.value().obs;

  Whiteboard _memorizedWhiteboard;

  ObjectId get userId => appSession.account.id;

  String get subjectName => _subject?.name;

  String get imagesFolderPath => lecture.imagesFolderPath;

  ILectureServerApi get lectureServer => null;

  @override
  void onInit() {
    super.onInit();

    if (!GetPlatform.isWeb)
      Screen.keepOn(true);

    if (lectureDataService.currentLecture == null) return;

    lecture = lectureDataService.currentLecture;
    lectureStorageService.initLectureData(lecture);

    _subject = _userDataService.getSubjectById(lecture.subjectName);
  }

  @override
  void onClose() async {
    await _camera.dispose();
    lectureDataService.currentLecture = null;

    if (!GetPlatform.isWeb) {
      await _camera.dispose();
      Screen.keepOn(false);
    }

    super.onClose();
  }

  setCurrentWhiteboard(WhiteboardSet whiteboardSet) {
    currentWhiteboard.value = whiteboardSet.current;
    updateWhiteboardIndexTitle(whiteboardSet);
  }

  updateWhiteboardIndexTitle(WhiteboardSet whiteboardSet) {
    indexTitleWidthFactor.value = _getIndexTitleWidthFactor(whiteboardSet);
    whiteboardsCount.value = whiteboardSet?.length ?? 0;
    currentWhiteboardIndex.value = whiteboardSet?.currentIndex ?? 0;
    isFirstWhiteboard.value = currentWhiteboardIndex.value == 0;
    isLastWhiteboard.value = currentWhiteboardIndex.value == whiteboardsCount.value - 1;
  }

  int _getIndexTitleWidthFactor(WhiteboardSet whiteboardSet) {
    if (whiteboardSet == null || whiteboardSet.length < 10) return 1;
    if (whiteboardSet.length < 100) return 2;

    // 100+
    var number = whiteboardSet.length;
    var count = 0;
    while (number != 0) {
      count++;
      number = number~/10;
    }
    return count;
  }

  insertWhiteboard(WhiteboardSet whiteboardSet) {
    var newIndex = whiteboardSet.length == 0 ? 0 : whiteboardSet.currentIndex + 1;
    var wb = Whiteboard(whiteboardSet.ownerId);
    whiteboardSet.insert(newIndex, wb);
    whiteboardSet.current = wb;
    setCurrentWhiteboard(whiteboardSet);
  }

  Future<void> goToLeftWhiteboard() async {
    var index = currentWhiteboardSet.currentIndex - 1;
    if (index >= 0)
      await changeWhiteboard(index);
  }

  Future<void> goToRightWhiteboard() async {
    var index = currentWhiteboardSet.currentIndex + 1;
    if (index < currentWhiteboardSet.length)
      await changeWhiteboard(index);
  }

  Future<void> changeWhiteboard(int index) async {
    currentWhiteboardSet.selectItemByIndex(index);
    setCurrentWhiteboard(currentWhiteboardSet);
  }

  String generateImagePath() {
    final imageName = '${ObjectId().toHexString()}.png';
    return join(imagesFolderPath, imageName);
  }

  rememberCurrentWhiteboard() => _memorizedWhiteboard = currentWhiteboard.value;
  forgetMemorizedWhiteboard() => _memorizedWhiteboard = null;

  Future<void> addPhotoWithProgress(String imagePath, CameraResult result) async {
    if (result == CameraResult.Cancelled) {
      Log.info('Camera result is Cancelled');
      return;
    }

    if (result == CameraResult.CapturingError) {
      Log.info('Camera result is CapturingError');
      return;
    }

    if (_memorizedWhiteboard == null) {
      Log.error('Memorized whiteboard not found');
      return;
    }

    return initializeFuture.value = addPhoto(_memorizedWhiteboard, imagePath, result);
  }

  Future<bool> addPhoto(Whiteboard wb, String imagePath, CameraResult result) async {
    if (imagePath == null || imagePath.isEmpty) {
      Log.error('Image path is null or empty');
      sendEvent(ImageExportFailedEvent());
      return false;
    }

    final imageName = imagePath?.split('/')?.last;
    final imageType = result == CameraResult.Success
        ? (Settings.isPaperRecognitionEnabled ? ImageType.Recognized : ImageType.Photo)
        : ImageType.Unrecognized;

    final imageInfo = ImageInfo(imageName, imageType, userId, folderPath: imagesFolderPath);

    wb.images.add(imageInfo);

    if (wb == currentWhiteboard.value)
      currentWhiteboard.refresh();

    return true;
  }

  addImageWithProgress() => initializeFuture.value = addImage(currentWhiteboard.value);

  Future<bool> addImage(Whiteboard wb) async {
    if (!GetPlatform.isWeb && !await PermissionsHelper().checkPhotosPermission(Get.context))
      return false;

    XFile pickedFile;

    try {
      pickedFile = await _picker.pickImage(source: ImageSource.gallery);
    } catch (e) {
      Log.error('Pick file failed', exception: e);
      sendEvent(ImageExportFailedEvent());
      return false;
    }

    if (pickedFile == null || pickedFile.path == null) {
      Log.warning('No picked file');
      return false;
    }

    var imageName;
    // TODO: Make unified view when backend is alive https://orbiglass.atlassian.net/browse/OT-666
    if (GetPlatform.isWeb)
      imageName = '${ObjectId().toHexString()}.png';
    else
      imageName = '${ObjectId().toHexString()}.' + pickedFile.path.split('.').last;
    final imagePath = join(imagesFolderPath, imageName);

    try {
      await _imageService.compressFromFileAndSave(pickedFile.path,
          imageName: imageName, targetPath: imagePath);
    } catch (e) {
      Log.error('Copy picked file failed', exception: e);
      sendEvent(ImageExportFailedEvent());
      return false;
    }

    final imageInfo = ImageInfo(imageName, ImageType.Picture, userId, folderPath: imagesFolderPath);

    wb.images.add(imageInfo);

    if (wb == currentWhiteboard.value)
      currentWhiteboard.refresh();

    return true;
  }

  Future<Uint8List> getFileData(String filePath) async {
    final file = File(filePath);
    if (!await file.exists()) {
      Log.error('$filePath file does not exists');
      return null;
    }
    return file.readAsBytes();
  }
}

class CameraCapturingErrorEvent extends Event {}
class ImageExportFailedEvent extends Event {}