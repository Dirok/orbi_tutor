import 'dart:ui';

import 'package:get/get.dart';
import 'package:tutor/AppSession.dart';

class DrawingViewModel extends GetxController {
  final drawingEnabled = true.obs;
  final color = Rx<Color>(null);
  final strokeWidth = RxDouble(0);

  DrawingViewModel() {
    color.value = appSession.account.preferences.pickerColor;
    strokeWidth.value = appSession.account.preferences.strokeWidth;
  }

  // todo change color field to setter and edit access to it value in other classes
  void setColor(Color value) {
    color.value = value;
    appSession.account.preferences.pickerColor = value;
  }

  // todo change strokeWith field to setter and edit access to it value in other classes
  void setStrokeWidth(double value) {
    strokeWidth.value = value;
    appSession.account.preferences.strokeWidth = value;
  }
}