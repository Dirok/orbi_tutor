import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class BaseViewModel extends GetxController with EventMixin {
  final connectivityService = Get.find<ConnectivityService>();

  RxBool get isConnectedToInternet => connectivityService.isConnectedToInternet;

  bool checkConnectivityAndNotify() {
    final connectivity = connectivityService.isConnectedToInternet.value;
    if (!connectivity)
      sendEvent(NoNetworkEvent());
    return connectivity;
  }

  @override
  void onClose() {
    cancelEvents();
    super.onClose();
  }
}
