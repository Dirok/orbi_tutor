import 'package:audio_manager_plugin/AudioOutputDevice.dart';
import 'package:audio_manager_plugin/audio_manager_plugin.dart';
import 'package:get/get.dart';

class AudioDeviceViewModel extends GetxController {
  var isInited = false.obs;
  var selectedAudioDevice = AudioOutputDevice.headphones.obs;

  Future<void> init() async {
    await setPriorityAudioDevice();
    AudioManagerPlugin.setOutputChangedCallback(_outputDeviceChanged);
    isInited.value = true;
  }

  Future<void> _outputDeviceChanged(AudioOutputDevice device, AudioDeviceEvent event) async {
    if (event == AudioDeviceEvent.added && device.priority < selectedAudioDevice.value.priority) {
      await tryChangeAudioDevice(device);
    }
    else if (event == AudioDeviceEvent.removed && device == selectedAudioDevice.value) {
      await setPriorityAudioDevice();
    }
    else { // for interrupt RTCAudioManager
      await Future.delayed(Duration(seconds: 2));
      var current = await AudioManagerPlugin.getCurrentOutput();
      if (current != selectedAudioDevice.value)
        await tryChangeAudioDevice(selectedAudioDevice.value);
    }
  }

  Future<void> setPriorityAudioDevice() async {
    var available = await AudioManagerPlugin.getAvailableOutputs();
    for (var device in available) {
      var isChanged = await tryChangeAudioDevice(device);
      if (isChanged) break;
    }
  }

  Future<bool> tryChangeAudioDevice(AudioOutputDevice device) async {
    var result = await AudioManagerPlugin.setOutputDevice(device);
    if (result) selectedAudioDevice.value = device;
    return result;
  }

  Future<void> deinit() async {
    await AudioManagerPlugin.resetOutputDevice();
    AudioManagerPlugin.setOutputChangedCallback(null);
    isInited.value = false;
  }

  Future<List<AudioOutputDevice>> getAudioDeviceList() => AudioManagerPlugin.getAvailableOutputs();
}