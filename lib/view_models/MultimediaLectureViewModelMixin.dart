import 'dart:async';

import 'package:audio_manager_plugin/audio_manager_plugin.dart';
import 'package:bson/bson.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

mixin MultimediaLectureViewModelMixin<T extends IActiveLectureViewModel> on EventMixin {
  static const _serverReconnectInterval = Duration(seconds: 3);

  final List<StreamSubscription> _subscribes = [];

  final multimediaServer = VideoRoomSignaling();
  final _mainServer = Get.find<MainServerApi>();

  final _lectureDataService = Get.find<LectureDataService>();
  final _pushService = Get.find<PushService>();

  final audioDeviceViewModel = AudioDeviceViewModel();

  Timer _reconnectionTimer;

  bool _isMicStateChanging = false;

  bool _isDisposed = false;

  T _vm;

  final isMultimediaServerConnects = false.obs;
  final isMultimediaServerConnected = false.obs;

  final hasAudio = false.obs;
  final hasVideo = false.obs;

  final allowSpeak = true.obs;

  final micStateChanging = false.obs;

  final speakingStudent = ''.obs;

  RxBool get isAudioInited => audioDeviceViewModel.isInited;

  void initMultimediaMixin(T vm) {
    _vm = vm;
    _subscribeMultimediaServerEvents();
  }

  Future<void> disposeMultimedia() async {
    if (_isDisposed) return;

    _subscribes.forEach((s) => s.cancel());

    _reconnectionTimer?.cancel();

    await multimediaServer.close();

    await audioDeviceViewModel.deinit();

    await hidePush();

    _isDisposed = true;
  }

  Future<void> startMultimediaServer() async {
    final result = await _mainServer.startMultimediaServer(_vm.lecture.id);
    if (!result.isSuccess)
      Log.error('Failed to start multimedia server');
  }

  Future<void> stopMultimediaServer() async {
    final result = await _mainServer.stopMultimediaServer(_vm.lecture.id);
    if (!result.isSuccess)
      Log.error('Failed to stop multimedia server');
  }

  Future<void> closeMultimedia() async {
    if (_vm.lecture.state != LectureState.active
        && !multimediaServer.isConnected) return;
      await multimediaServer.leaveRoom();
  }

  void _startReconnectionTimer() {
    _reconnectionTimer?.cancel();
    _reconnectionTimer = Timer(_serverReconnectInterval, tryConnectToMultimediaServer);
  }

  void _subscribeMultimediaServerEvents() async {
    _subscribes.add(
        multimediaServer.onConnectionStateChange.listen((state) {
          if (state == ConnectionStatus.Closed || state == ConnectionStatus.Error) {
            isMultimediaServerConnected.value = false;
            _startReconnectionTimer();
          }
        }));

    _subscribes.add(multimediaServer.onVolumeChange.listen((state) {
      if (state.userId == null) return;
      changeUserVolumeLevel(ObjectId.fromHexString(state.userId), state.volumeLevel);
    }));

    _subscribes.add(multimediaServer.onEventReceived.listen((event) {
      Log.info('Media session is ended by $event');
      if (event == MediaEvent.NoSession || event == MediaEvent.Destroyed)
        _checkLectureIsActive(event);
    }));
  }

  Future<void> _checkLectureIsActive(MediaEvent event) async {
    final lect = await _lectureDataService.getLecture(_vm.lecture.id, needRefresh: true);
    Log.info('Lecture state is ${lect.isActive}');
    if (!lect.isActive) {
      Log.info('Lecture destroyed or cancelled with event $event');
      sendEvent(LectureDestroyedEvent());
    }
  }

  void changeUserVolumeLevel(ObjectId userId, double volumeLevel) {
    Log.info('Change person $userId mic volume to $volumeLevel');
    var user = _vm.teacherViewModel;

    if (user.id == userId)
      user.volumeLevel.value = volumeLevel;
    else if (_vm.studentViewModelById.containsKey(userId))
      _vm.studentViewModelById[userId].volumeLevel.value = volumeLevel;

    _vm.studentViewModelById.values.forEach((element) {
      if (element.volumeLevel.value > user.volumeLevel.value)
        user = element;
    });

    speakingStudent.value = user.volumeLevel.value > Constants.minAudioVolume ? user.name : '';
  }

  Future<void> tryConnectToMultimediaServer() async {
    if (_vm.lecture.state != LectureState.active) return;

    isMultimediaServerConnects.value = true;

    final result = await _mainServer.getMultimediaServer(_vm.lecture.id);
    if (!result.isSuccess) {
      Log.error('Failed to get multimedia server params');
      isMultimediaServerConnects.value = false;
      return;
    }

//TODO
//       if (mode == VideoRoomMode.PublisherAudio || mode == VideoRoomMode.PublisherVideo)
//         await PermissionsHelper().checkMicPermission(context);
//
//       if (mode == VideoRoomMode.PublisherVideo)
//         await PermissionsHelper().checkCameraPermission(context);

    final params = result.payload;

    isMultimediaServerConnected.value = await multimediaServer.connect(
        params, appSession.account.user.fullName,
        appSession.account.id.toHexString(), hasAudio.value, hasVideo.value);

    if (!isMultimediaServerConnected.value) {
      Log.error('Unable to connect to multimedia server ${params.address} with id ${params.id}');
      isMultimediaServerConnects.value = false;
      return;
    }

    AudioManagerPlugin.focusAudio();

    await _setMicState(allowSpeak.value && hasAudio.value);

    await Future.delayed(Duration(seconds: 3));

    await _initPush();

    await audioDeviceViewModel.init();

    isMultimediaServerConnects.value = false;

    return;
  }

  Future<void> _initPush() async {
    _pushService.clearSubscriptions();
    _pushService.listenEvents((event) async {
      if (_isMicStateChanging) return;

      if (event is MuteMicEvent) {
        Log.info('notification action: mute');
        await _setMicState(false);
      } else if (event is UnmuteMicEvent) {
        Log.info('notification action: unmute');
        await _setMicState(true);
      } else {
        return;
      }
      await showPush();
    });
  }

  Future<void> showPush() async {
    if (_isDisposed) {
      Log.info('Push show cancelled');
      return;
    }
    _pushService.showActiveLectureNotification(hasAudio: hasAudio.value, allowSpeak: allowSpeak.value);
  }

  Future<void> hidePush() => _pushService.hideActiveLectureNotification();

  Future<void> rollbackAudioDeviceToPriority() =>
      audioDeviceViewModel.setPriorityAudioDevice();

  Future<void> changeMicState() async {
    if (_isMicStateChanging) return;
    await _setMicState(!hasAudio.value);
  }

  void onAllowSpeakChanged(bool allowSpeak) async {
    this.allowSpeak.value = allowSpeak;
    await _setMicState(allowSpeak);
  }

  Future<void> _setMicState(bool state, {revert = true}) async {
    if (!multimediaServer.isConnected || hasAudio.value == state) return;
    _isMicStateChanging = true;

    var isSuccess = true;
    for (var i = 0; i < Constants.retryMicStateCount; i++) {
      try {
        Log.info('changing mic state to $state try: ${i + 1}');
        await (() => _tryChangeMicState(state))
            .callWithLock(micStateChanging)
            .timeout(Duration(seconds: Constants.micStateTimeout));
        isSuccess = true;
        break;
      } catch (e) {
        isSuccess = false;
        Log.error('setMicState error', exception: e);
      } finally {
        Log.info(
            'Mic state ${isSuccess ? '' : 'not '}changed to $state after ${i + 1} tries');
      }
    }

    _isMicStateChanging = false;
    if (isSuccess) {
      hasAudio.value = state;
    } else if (revert) {
      Log.info('Mic state revert state');
      _setMicState(!state, revert: false);
    }
  }

  Future<void> _tryChangeMicState(bool isEnabled) async {
    await multimediaServer.changeMode(isEnabled, hasVideo.value);

    await _vm.onMicStateChanged(isEnabled);

    if (!isEnabled)
      changeUserVolumeLevel(appSession.account.id, Constants.minAudioVolume);
  }
}