import 'dart:async';

import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class RegistrationWithEmailViewModel extends BaseViewModel {
  final _mainServer = Get.find<MainServerApi>();
  final _push = Get.find<PushService>();

  final email = ''.obs;
  final password = ''.obs;
  final passwordRepeat = ''.obs;

  final isObscurePwd = true.obs;
  final isObscurePwdRepeat = true.obs;
  final isRegister = false.obs;

  final _authService = Get.find<AuthorizationService>();
  final _firebaseAuthService = Get.find<FirebaseSignInService>();

  Future<void> registerWithEmail() => _registerWithEmail.callWithLock(isRegister);

  Future<void> _registerWithEmail() async {
    try {
      final result = await _firebaseAuthService.registerWithEmail(email.value, password.value);
      switch (result.status) {
        case FirebaseAuthResultStatus.fail:
          sendEvent(RegistrationFailedEvent());
          return;
        case FirebaseAuthResultStatus.weakPassword:
          sendEvent(RegistrationWeakPasswordEvent());
          return;
        case FirebaseAuthResultStatus.accountAlreadyExist:
          sendEvent(RegistrationAccountAlreadyExistEvent());
          return;
        case FirebaseAuthResultStatus.operationNotAllowed:
          sendEvent(RegistrationOperationNotAllowedEvent());
          return;
        case FirebaseAuthResultStatus.success:
          final authResult = await _authService.authorizeWithJwt(result.payload);
          _handleAuthResult(authResult);
      }
    } catch (e) {
      Log.error('_registerWithEmail exception', exception: e);
      sendEvent(RegistrationFailedEvent());
    }
  }

  void _handleAuthResult(AuthorizationResult authResult) {
    if (authResult == AuthorizationResult.success) {
      _mainServer.updateFcmToken(_push.fcmToken);
      sendEvent(RegistrationSuccessEvent());
    } else {
      sendEvent(RegistrationFailedEvent());
    }
  }
}

class RegistrationSuccessEvent extends Event {}
class RegistrationAccountAlreadyExistEvent extends Event {}
class RegistrationOperationNotAllowedEvent extends Event {}
class RegistrationWeakPasswordEvent extends Event {}
class RegistrationFailedEvent extends Event {}
