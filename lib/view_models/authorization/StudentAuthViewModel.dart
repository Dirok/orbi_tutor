import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class StudentAuthViewModel extends BaseViewModel {
  final _serverUpdatesServices = Get.find<ServerUpdatesService>();
  final _push = Get.find<PushService>();
  final _mainServer = Get.find<MainServerApi>();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }

  saveAuthInfo() async {
    await appSession.save();
    _mainServer.token = appSession.httpToken;
    await _serverUpdatesServices.reInit();
    _mainServer.updateFcmToken(_push.fcmToken);
  }

  resetAuthInfo() async {
    await appSession.reset();
    await _serverUpdatesServices.deInit();
  }
}
