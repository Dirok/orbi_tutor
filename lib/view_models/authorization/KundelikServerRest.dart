import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:tutor/focused.dart';

class KundelikServerRest {
  final String _url;

  KundelikServerRest(this._url);

  /// grantType: AuthorizationCode (code -> token), RefreshToken
  Future<Map<String, dynamic>> authorize({
    @required String code,
    @required String clientID,
    @required String clientSecret,
    @required String grantType,
    String refreshToken = ''}) async {

    /// set up POST request arguments
    final headers = {"Content-type": "application/json"};
    final jsonBody = json.encode({
      "code": code,
      "client_id": clientID,
      "client_secret": clientSecret ,
      "grant_type": grantType,
      //"refreshToken": refreshToken,
    });
    Log.info('jsonBody: $jsonBody');

    try {
      /// make POST request
      Log.info('------ authOnKundelikServer request ------');
      final uri = Uri.parse('$_url/v2.0/authorizations');
      Log.info(uri);
      final response = await http
          .post(uri, headers: headers, body: jsonBody)
          .timeout(Settings.httpsRequestTimeout*2); // default timeout not enough

      final responseBody = json.decode(utf8.decode(response.bodyBytes)) as Map<String, dynamic>;
      if (response.statusCode == 200) {
        Log.info('authOnKundelikServer success');
        return responseBody;
      } else {
        Log.info(
            'authOnKundelikServer error: type=${responseBody["type"]} description=${responseBody["description"]}');
        return null;
      }
    }
    on TimeoutException catch (e) {
      Log.info('TimeoutException: $e');
      throw e;
    }
    catch (e) {
      Log.info('Exception $e');
      throw e;
    }
  }
}