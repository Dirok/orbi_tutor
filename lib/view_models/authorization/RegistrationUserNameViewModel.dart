import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class RegistrationUserNameViewModel extends BaseViewModel {
  final _mainServer = Get.find<MainServerApi>();

  final isSigningIn = false.obs;

  final name = ''.obs;
  final surname = ''.obs;

  bool get isEmptyFields => name.isEmpty && surname.isEmpty;

  Future<void> updateUserAccount() => _updateUserCredentials.callWithLock(isSigningIn);

  Future<void> _updateUserCredentials() async {
    if (isEmptyFields) {
      sendEvent(EmptyFieldsErrorEvent());
      return;
    }

    final account = appSession?.account;
    if (account == null) {
      sendEvent(UpdateAccountFailedEvent());
      return;
    }

    final currUser = account.user;
    final newUser = User(currUser.id, name.value, surname.value, currUser.patronymic);

    try {
      final updateResult = await _mainServer?.updateUserOld(newUser);
      if (!updateResult.isSuccess) {
        sendEvent(UpdateAccountFailedEvent());
        return;
      }

      appSession.account.user = newUser;

      updateResult.payload
        ? sendEvent(UpdateAccountSuccessEvent())
        : sendEvent(UpdateAccountFailedEvent());

    } catch (e) {
      Log.error('_updateUserCredentials exception', exception: e);
      return false;
    }
  }

  void skipAccountUpdating() =>
      sendEvent(isEmptyFields ? EmptyFieldsErrorEvent() : UpdateAccountSuccessEvent());
}

class UpdateAccountSuccessEvent extends Event {}
class UpdateAccountFailedEvent extends Event {}
class EmptyFieldsErrorEvent extends Event {}