import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class WantToSignInViewModel extends BaseViewModel {
  final isSigningIn = false.obs;

  final _authService = Get.find<AuthorizationService>();
  final _firebaseAuthService = Get.find<FirebaseSignInService>();
  final _mainServer = Get.find<MainServerApi>();
  final _pushService = Get.find<PushService>();

  void signInAnonymously() => _signInAnonymously.callWithLock(isSigningIn);

  Future<void> _signInAnonymously() async {
    try {
      final jwt = await _firebaseAuthService.signInAnonymously();
      if (jwt == null) {
        sendEvent(OutsideServiceAuthorizationFailedEvent());
        return;
      }

      final authResult = await _authService.authorizeWithJwt(jwt, isAnonymous: true);

      if (authResult == AuthorizationResult.success) {
        _mainServer.updateFcmToken(_pushService.fcmToken);
        sendEvent(AuthorizationSuccessEvent());
      }
      else
        sendEvent(AuthorizationFailedEvent());

    } catch (e) {
      Log.error('_signInAnonymously exception', exception: e);
    }
  }
}