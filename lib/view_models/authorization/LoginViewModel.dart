import 'dart:async';

import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class LoginViewModel extends BaseViewModel {
  final _mainServer = Get.find<MainServerApi>();
  final _push = Get.find<PushService>();

  static const _serverSwitchingLogin = 'switch@server.com';
  static const _serverSwitchingPassword = 'Switch.Server';

  static const String _debugTeacherLogin = 'teacher0@demo.ru';
  static const String _debugWebTeacherLogin = 'teacher3@demo.ru';
  // static const String _debugTeacherLogin = 'persona1@demo.ru';
  static const String _debugTeacherPassword = '123456';

  final email = ''.obs;
  final password = ''.obs;

  final isObscurePwd = true.obs;
  final isSigningIn = false.obs;

  final currServerType = Settings.server.obs;

  final _authService = Get.find<AuthorizationService>();
  final _firebaseAuthService = Get.find<FirebaseSignInService>();
  final _countryProviderService = Get.find<CountryProviderService>();

  @override
  void onInit() {
    super.onInit();

    _setDefaultUserCredentials();

    //if (GetPlatform.isWeb)
    //  switchServer(ServerEnvironment.custom('https://debug.imfocused.io'));
  }

  bool checkServerSwitching() => email.value == _serverSwitchingLogin
      && password.value == _serverSwitchingPassword;

  Future<void> switchServer(ServerEnvironment server) async {
    Settings.server = server;
    currServerType.value = server;
    _mainServer.uri = Settings.server.url;

    Log.info('Server switched to ${Settings.server.name} (${Settings.server.url})');

    _setDefaultUserCredentials();
  }

  void _setDefaultUserCredentials() {
    final isDebug = Settings.server.isDebug;

    email.value = isDebug ? (GetPlatform.isWeb ? _debugWebTeacherLogin : _debugTeacherLogin) : '';
    password.value = isDebug ? _debugTeacherPassword : '';

    sendEvent(CredentialsChanged());
  }

  void signInWithEmail() => _signInWithEmail.callWithLock(isSigningIn);
  void signInWithGoogle() => _signInWithGoogle.callWithLock(isSigningIn);
  void signInWithFacebook() => _signInWithFacebook.callWithLock(isSigningIn);
  void signInWithApple() => _signInWithApple.callWithLock(isSigningIn);

  Future<void> _signInWithEmail() async {
    final authResult = await _authService.authorizeWithEmail(email.value, password.value);
    _handleAuthResult(authResult, isWithCredentials: true);
  }

  Future<void> _signInWithGoogle() async {
    try {
      final jwt = await _firebaseAuthService.signInWithGoogle();
      if (jwt == null) {
        sendEvent(OutsideServiceAuthorizationFailedEvent());
        return;
      }

      final authResult = await _authService.authorizeWithJwt(jwt);
      _handleAuthResult(authResult);
    } catch (e) {
      Log.error('_signInWithGoogle exception', exception: e);
    }
  }

  Future<void> _signInWithFacebook() async {
    try {
      final jwt = await _firebaseAuthService.signInWithFacebook();
      if (jwt == null) {
        sendEvent(OutsideServiceAuthorizationFailedEvent());
        return;
      }
      if (jwt.isEmpty) return;   // cancel case

      final authResult = await _authService.authorizeWithJwt(jwt);
      _handleAuthResult(authResult);
    } catch (e) {
      Log.error('_signInWithFacebook exception', exception: e);
    }
  }

  Future<void> _signInWithApple() async {
    try {
      final jwt = await _firebaseAuthService.signInWithApple();
      if (jwt == null) {
        sendEvent(OutsideServiceAuthorizationFailedEvent());
        return;
      }

      final authResult = await _authService.authorizeWithJwt(jwt);
      _handleAuthResult(authResult);
    } catch (e) {
      Log.error('_signInWithApple exception', exception: e);
    }
  }

  void _handleAuthResult(AuthorizationResult authResult,
      {bool isWithCredentials}) {
    if (authResult == AuthorizationResult.success) {
      _mainServer.updateFcmToken(_push.fcmToken);
      if (appSession.account.countryCode == 0 &&
          _countryProviderService.countries != null)
        sendEvent(AuthorizationSuccessWithoutCountryEvent());
      else
        sendEvent(AuthorizationSuccessEvent());
    } else {
      sendEvent(AuthorizationFailedEvent(isWithCredentials: isWithCredentials));
    }
  }
}

class AuthorizationSuccessEvent extends Event {}
class AuthorizationSuccessWithoutCountryEvent extends Event {}
class AuthorizationFailedEvent extends Event {
  final bool isWithCredentials;
  AuthorizationFailedEvent({this.isWithCredentials = false});
}
class OutsideServiceAuthorizationFailedEvent extends Event {}
class CredentialsChanged extends Event {}