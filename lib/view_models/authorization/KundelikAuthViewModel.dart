import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class KundelikAuthorizationFailedEvent extends Event {}

class KundelikAuthViewModel extends BaseViewModel {
  static const String _responseType = 'code';   // can be 'code' / 'token'

  static String get _redirectUrl => Settings.server.url;

  final isSigningIn = false.obs;
  final progress = 0.0.obs;
  final _authService = Get.find<AuthorizationService>();

  KundelikAuthCredentials _authCredentials;
  String initLink;

  String url;

  @override
  void onInit() {
    super.onInit();

    _authCredentials = _getKundelikCredentials();
    initLink = _createInitLink();
  }

  KundelikAuthCredentials _getKundelikCredentials() {
    if (Settings.server.isRelease)
      return KundelikAuthCredentials();
    return KundelikAuthStagingCredentials();
  }

  String _createInitLink() {
    final scope =
        '${KundelikAuthorizationScopes.CommonInfo.asString},'
        '${KundelikAuthorizationScopes.EducationalInfo.asString},'
        '${KundelikAuthorizationScopes.FriendsAndRelatives.asString},';

    return '${_authCredentials.server}/oauth2'
        '?response_type=$_responseType'
        '&client_id=${_authCredentials.clientId}'
        '&client_secret=${_authCredentials.secretKey}'
        '&scope=$scope'
        '&redirect_uri=$_redirectUrl';
  }

  Future<bool> tryAuthorize(Uri url) async {
    if (url.origin != _redirectUrl) return false;
    return (() =>_tryAuthorize(url))
        .callWithLock(isSigningIn, unlockedOnly: true, defaultValue: false);
  }

  Future<bool> _tryAuthorize(Uri url) async {
    final kundelikAuthBundle = await _signInKundelikServer(url.toString());
    if (kundelikAuthBundle?.accessToken == null) return false;
    final authResult = await _authService.authorizeWithKundelik(kundelikAuthBundle.accessToken);
    return authResult == AuthorizationResult.success;
  }

  Future<KundelikAuthorizationBundle> _signInKundelikServer(String url) async {
    Log.info('Try sign in by kundelik server rest');

    var regExp = RegExp("code=(.*)&");
    final code = regExp.firstMatch(url.toString())?.group(1);

    if (code == null) {
      regExp = RegExp("error=(.*)&");
      final error = regExp.firstMatch(url.toString())?.group(1);
      Log.error('Failed kundelik auth, error: $error');
      return null;
    }

    try {
      final kundelikServer = KundelikServerRest(_authCredentials.api);
      final response = await kundelikServer.authorize(
          code: code, clientID: _authCredentials.clientId, clientSecret: _authCredentials.secretKey,
          grantType: KundelikAuthorizationGrantType.AuthorizationCode.asString);

      Log.info('kundelikServer.authorization response: $response');
      return response != null ? KundelikAuthorizationBundle.fromJson(response) : null;
    } catch (e) {
      Log.error('_signInKundelikServer error', exception: e);
      return null;
    }
  }
}

class KundelikAuthCredentials {
  final String server = 'https://login.kundelik.kz';
  final String api = 'https://api.kundelik.kz';
  final String clientId = 'b360754fdb6c4f39bafef6bd9b668c2f';
  final String secretKey = '976ce94de75341e4bca306545f5f067f';
}

class KundelikAuthStagingCredentials implements KundelikAuthCredentials {
  final String server = 'https://login.staging.kundelik.kz';
  final String api = 'https://api.staging.kundelik.kz';
  final String clientId = 'ed502b4dd44c4ec6b5ad39d0ca6359b0';
  final String secretKey = 'b71f1cd5248f47508e4f9e9ab35afad6';
}