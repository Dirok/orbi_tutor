import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class RegistrationSignInViewModel extends BaseViewModel {
  final isSigningIn = false.obs;

  final _authService = Get.find<AuthorizationService>();
  final _firebaseAuthService = Get.find<FirebaseSignInService>();
  final _mainServer = Get.find<MainServerApi>();
  final _push = Get.find<PushService>();

  bool skipFullRegister = false;

  @override
  void onInit() {
    super.onInit();

    if (Get.arguments != null)
      skipFullRegister = Get.arguments['skipFullRegister'] ?? false;
  }

  @override
  void onClose() {
    super.onClose();
  }

  void signInWithGoogle() => _signInWithGoogle.callWithLock(isSigningIn);
  void signInWithFacebook() => _signInWithFacebook.callWithLock(isSigningIn);
  void signInWithApple() => _signInWithApple.callWithLock(isSigningIn);

  Future<void> _signInWithGoogle() async {
    try {
      final jwt = await _firebaseAuthService.signInWithGoogle();
      if (jwt == null) {
        sendEvent(OutsideServiceAuthorizationFailedEvent());
        return;
      }

      final authResult = await _authService.authorizeWithJwt(jwt);
      handleAuthResult(authResult);
    } catch (e) {
      Log.error('_signInWithGoogle exception', exception: e);
    }
  }

  Future<void> _signInWithFacebook() async {
    try {
      final jwt = await _firebaseAuthService.signInWithFacebook();
      if (jwt == null) {
        sendEvent(OutsideServiceAuthorizationFailedEvent());
        return;
      }
      if (jwt.isEmpty) return;  // cancel case

      final authResult = await _authService.authorizeWithJwt(jwt);
      handleAuthResult(authResult);
    } catch (e) {
      Log.error('_signInWithFacebook exception', exception: e);
    }
  }

  Future<void> _signInWithApple() async {
    try {
      final jwt = await _firebaseAuthService.signInWithApple();
      if (jwt == null) {
        sendEvent(OutsideServiceAuthorizationFailedEvent());
        return;
      }

      final authResult = await _authService.authorizeWithJwt(jwt);
      handleAuthResult(authResult);
    } catch (e) {
      Log.error('_signInWithApple exception', exception: e);
    }
  }

  void handleAuthResult(AuthorizationResult authResult) {
    if (authResult == AuthorizationResult.success) {
      _mainServer.updateFcmToken(_push.fcmToken);
      sendEvent(AuthorizationSuccessEvent());
    } else {
      sendEvent(AuthorizationFailedEvent());
    }
  }
}