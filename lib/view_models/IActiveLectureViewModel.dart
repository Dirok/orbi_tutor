import 'package:bson/bson.dart';
import 'package:tutor/focused.dart';

abstract class IActiveLectureViewModel {
  Lecture get lecture;
  StudentListElementActiveGroupLectureViewModel get teacherViewModel;
  Map<ObjectId, StudentListElementActiveGroupLectureViewModel> get studentViewModelById;
  Future<void> onMicStateChanged(bool isEnabled);
}