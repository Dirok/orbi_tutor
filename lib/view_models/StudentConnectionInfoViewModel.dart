import 'package:get/get.dart';

class StudentConnectionInfoViewModel extends GetxController {
  StudentConnectionInfoViewModel(this.studentName, {this.isConnected});

  final String studentName;
  final bool isConnected;
}
