import 'dart:async';
import 'dart:core';

import 'package:get/get.dart';
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

class ReportViewModel extends BaseViewModel {
  static const Duration sendRequestTimeout = Duration(seconds: 30);

  final progressPercentValue = 0.obs;

  final isSendingRequest = false.obs;
  final isSendLocked = true.obs;
  final isInited = false.obs;

  final email = (appSession?.account?.email ?? Settings.emailForLogs ?? '').obs;
  final issue = ''.obs;

  int _progressValue = 0;

  Future<bool> trySendRequest() => _trySendRequest.callWithLock(isSendingRequest);

  Future<bool> _trySendRequest() async {
    if (GetPlatform.isWeb) {
      sendEvent(PlatformWebEvent());
      return false;
    }

    progressPercentValue.value = 0;
    Settings.emailForLogs = email.value;

    final logFiles = await _collectLogs(email.value);
    if (logFiles.isEmpty) return false;

    Log.info('Collect logs - success');

    final sendResult = await _sendLogs(email.value, issue.value, logFiles);
    if (!sendResult) return false;

    Log.info('Send logs - success');

    for (var file in logFiles)
      Log.info('Logs: ${file.path} was sent to server');

    return true;
  }

  Future<List<File>> _collectLogs(String email) async {
    try {
      return await LogExportService.exportLogs(email);
    } catch (e) {
      Log.error('Collect logs failed', exception: e);
      sendEvent(CollectionErrorEvent());
    }
    return <File>[];
  }

  Future<bool> _sendLogs(
    String email,
    String issue,
    List<File> files,
  ) async {
    try {
      final report = IssueReport(email, issue, files);

      await ReportService()
          .sendReport(report: report, onUploadProgress: _setUploadProgress)
          .timeout(sendRequestTimeout);

      return true;
    } on SocketException catch (_) {
      sendEvent(NoNetworkEvent());
    } on TimeoutException catch (e) {
      Log.error('Timeout on sending logs', exception: e);
      sendEvent(TimeoutErrorEvent());
    } catch (e) {
      Log.error('Sending logs failed', exception: e);
      sendEvent(UploadErrorEvent());
    }
    return false;
  }

  void _setUploadProgress(int sentBytes, int totalBytes) {
    final currentProgress =
        MathUtils.interpretPercentage(sentBytes, totalBytes);

    if (currentProgress != _progressValue) {
      _progressValue = currentProgress;
      progressPercentValue.value = _progressValue;
    }
  }
}

class CollectionErrorEvent extends Event {}

class PlatformWebEvent extends Event {}

class UploadErrorEvent extends Event {}

class TimeoutErrorEvent extends Event {}
