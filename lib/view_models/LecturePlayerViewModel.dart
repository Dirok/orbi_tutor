import 'dart:async';
import 'dart:collection';
import 'dart:convert';

import 'package:bson/bson.dart';
import 'package:get/get.dart';
import 'package:path/path.dart' show join;
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

class LecturePlayerViewModel extends GetxController {
  static const String _archiveExt = '.zip';
  static const String _lectureActionsFileName = 'action';

  Lecture _lecture;

  List<LectureAction> _lectureActionList;

  Map<String, Function(Map<String, dynamic>)> _eventMap;

  Map<int, dynamic> _eventTimeMap = Map();

  HashSet<ObjectId> _usedWhiteboardIds = HashSet();

  WhiteboardSet _selfWhiteboardSet;
  WhiteboardSet _otherWhiteboardSet;

  bool _isSelfWhiteboardSetActive = true;

  WhiteboardSet get _currentWhiteboardSet =>
      _isSelfWhiteboardSetActive ? _selfWhiteboardSet : _otherWhiteboardSet;

  var initializeFuture = Future<void>.value().obs;
  var isPlayed = false.obs;
  var currentWhiteboard = Whiteboard(appSession.account.id).obs;
  var ownerName = ''.obs;

  setCurrentWhiteboard(Whiteboard value) {
    _currentWhiteboardSet?.current = value;
    currentWhiteboard.value = value;
    ownerName.value = _getWhiteboardOwnerName(value);
  }

  Map<String, Account> _userMap = Map();

  LecturePlayerViewModel() {
    _createEventMap();

    initializeFuture.value = _loadData();
  }

  String _getWhiteboardOwnerName(Whiteboard wb) {
    if (wb == null) return '';
    if (wb.ownerId == _lecture.owner.id)
      return _lecture.owner.shortName;
    if (_userMap.containsKey(wb.ownerId))
      return _userMap[wb.ownerId].user.shortName;
    return "Unknown";
  }

  void play() {
    isPlayed.value = true;
    _startPlaying();
  }

  void stop() {
    isPlayed.value = false;
  }

  Future<void> _loadData() async {
    var lectureId = '5fd22273c041b05aa0b75015'; // TODO remove

    var isExtracted = await _extractLecture(lectureId);
    if (!isExtracted) return;

    _lectureActionList = await _loadActionList(lectureId);
    if (_lectureActionList == null || _lectureActionList.length == 0) return;

    for(var action in _lectureActionList) {
      _eventTimeMap[action.elapsedMs] = action;
      if (action.messageType == MainWhiteboardChangedMessage.messageType) {
        var msg = MainWhiteboardChangedMessage.fromJson(action.data);
        _usedWhiteboardIds.add(msg.id);
      }
    }
  }

  Future<bool> _extractLecture(String lectureId) async {
    var fileName = lectureId + _archiveExt;

    var sourcePath = join(Settings.downloadsFolderPath, fileName);
    var targetPath = join(Settings.lecturesFolderPath, lectureId);

    return await FileService().extractZip(sourcePath, targetPath);
  }

  Future<List<LectureAction>> _loadActionList(String lectureId) async {
    var path = join(Settings.lecturesFolderPath, lectureId, _lectureActionsFileName);
    var file = File(path);
    if (!await file.exists()) {
      Log.error('Lecture actions file not found');
      return [];
    }

    var jsonStrList = await file.readAsLines();
    List<LectureAction> actionList = [];
    var decoder = JsonDecoder();
    for (var jsonStr in jsonStrList) {
      var json = decoder.convert(jsonStr);
      var action = LectureAction.fromJson(json);
      actionList.add(action);
    }

    return actionList;
  }

  int _currentActionNumber = 0;
  int _lastActionTime = 0;

  _startPlaying() async {
    if (_lectureActionList == null || _lectureActionList.length == 0) {
      stop();
      return;
    }

    while (isPlayed.value) {
      var action = _lectureActionList[_currentActionNumber];

      var delay = action.elapsedMs - _lastActionTime;
      await Future.delayed(Duration(milliseconds: delay));

      print(action.messageType + ' on ' + action.elapsedMs.toString());

      if (_eventMap.containsKey(action.messageType))
        _eventMap[action.messageType].call(action.data);

      _currentActionNumber++;
      _lastActionTime = action.elapsedMs;
    }
  }

  ////////////

  _createEventMap() {
    _eventMap = {
      UserWhiteboardInsertedMessage.messageType: _onUserWhiteboardInserted,
      WhiteboardImageChangedMessage.messageType: _onWhiteboardImageChanged,
      DrawingCreatePathMessage.messageType: _onDrawingPathAdded,
      DrawingAddPointMessage.messageType: _onDrawingPointAdded,
      DrawingUndoMessage.messageType: _onLastDrawingPathRemoved,
      DrawingEraseMessage.messageType: _onDrawingCleaned,
      // UserConnectedMessage.messageType: _onUserConnected,
      // UserDisconnectedMessage.messageType: _onUserDisconnected,
      // HandStateChangedMessage.messageType: _onUserHandStateChanged,
      MainWhiteboardChangedMessage.messageType: _onMainWhiteboardChanged,
      PinStateChangedMessage.messageType: _onPinStateChanged,
      // AllowSpeakChangedMessage.messageType: _onAllowSpeakChanged,
      LectureFinishedMessage.messageType: _onLectureFinished,
/////
//       VideoRoomParamsMessage.messageType: _onVideoRoomParams,
      PinStateMessage.messageType: _getPinState,
      MainWhiteboardMessage.messageType: _getMainWhiteboardId,
      WhiteboardInsertedMessage.messageType: _sendInsertWhiteboard,
      CurrentWhiteboardChangedMessage.messageType: _sendSetCurrentWhiteboard,
      // UserStateMessage.messageType: _getUserState,
      UserWhiteboardsMessage.messageType: _getUserWhiteboardsInfo,
      // SubscribeWhiteboardMessage.messageType: _subscribeWhiteboard,
      // UnsubscribeWhiteboardMessage.messageType: _unsubscribeWhiteboard,
      WhiteboardInfoMessage.messageType: _loadWhiteboardInfo,
      // OnlineUsersMessage.messageType: _getOnlineUserList,
      // UserStateListMessage.messageType: _getStudentStates,
      GetAllDrawingPathsMessage.messageType: _getAllDrawingData,
    };
  }

  T _try<T>(T Function() func) {
    try {
      return func?.call();
    } catch (e) {
      Log.error(e);
      return null;
    }
  }

  _onUserWhiteboardInserted(Map<String, dynamic> json) {
    var msg = _try(() => UserWhiteboardInsertedMessage.fromJson(json));
    if (msg == null) return;
  }

  _onWhiteboardImageChanged(Map<String, dynamic> json) {
    var msg = _try(() => WhiteboardImageChangedMessage.fromJson(json));
    if (msg == null) return;

  }

  _onDrawingPathAdded(Map<String, dynamic> json) {
    var msg = _try(() => DrawingCreatePathMessage.fromJson(json));
    if (msg == null) return;

  }

  _onDrawingPointAdded(Map<String, dynamic> json) {
    var msg = _try(() => DrawingAddPointMessage.fromJson(json));
    if (msg == null) return;

  }

  _onLastDrawingPathRemoved(Map<String, dynamic> json) {
    var msg = _try(() => DrawingUndoMessage.fromJson(json));
    if (msg == null) return;

  }

  _onDrawingCleaned(Map<String, dynamic> json) {
    var msg = _try(() => DrawingEraseMessage.fromJson(json));
    if (msg == null) return;

  }

  // _onUserConnected(Map<String, dynamic> json) {
  //   var msg = _try(() => UserConnectedMessage.fromJson(json));
  //   if (msg == null) return;
  //
  // }
  //
  // _onUserDisconnected(Map<String, dynamic> json) {
  //   var msg = _try(() => UserDisconnectedMessage.fromJson(json));
  //   if (msg == null) return;
  //
  // }
  //
  // _onUserHandStateChanged(Map<String, dynamic> json) {
  //   var msg = _try(() => HandStateChangedMessage.fromJson(json));
  //   if (msg == null) return;
  //
  // }

  _onMainWhiteboardChanged(Map<String, dynamic> json) {
    var msg = _try(() => MainWhiteboardChangedMessage.fromJson(json));
    if (msg == null) return;

  }

  _onPinStateChanged(Map<String, dynamic> json) {
    var msg = _try(() => PinStateChangedMessage.fromJson(json));
    if (msg == null) return;

  }

  // _onAllowSpeakChanged(Map<String, dynamic> json) {
  //   var msg = _try(() => AllowSpeakChangedMessage.fromJson(json));
  //   if (msg == null) return;
  //
  // }

  _onLectureFinished(Map<String, dynamic> json) {
     // lessonFinished?.call();
  }


  // _onVideoRoomParams(Map<String, dynamic> json) async {
  //   var msg = _try(() => VideoRoomParamsMessage.fromJson(json));
  //   if (msg == null) return;
  //
  // }

  _getPinState(Map<String, dynamic> json) async {
    var msg = _try(() => PinStateMessage.fromJson(json));
    if (msg == null) return;

  }

  _getMainWhiteboardId(Map<String, dynamic> json) async {
    var msg = _try(() => MainWhiteboardMessage.fromJson(json));
    if (msg == null) return;

  }

  _sendInsertWhiteboard(Map<String, dynamic> json) async {
    var msg = WhiteboardInsertedMessage.fromJson(json);
    if (msg == null) return;

  }

  _sendSetCurrentWhiteboard(Map<String, dynamic> json) async {
    var msg = CurrentWhiteboardChangedMessage.fromJson(json);
    if (msg == null) return;

  }

  // _getUserState(Map<String, dynamic> json) async {
  //   var msg = _try(() => UserStateMessage.fromJson(json));
  //   if (msg == null) return;
  //
  // }

  _getUserWhiteboardsInfo(Map<String, dynamic> json) async {
    var msg = _try(() => UserWhiteboardsMessage.fromJson(json));
    if (msg == null) return;

  }

  // _subscribeWhiteboard(Map<String, dynamic> json) async {
  //   var msg = _try(() => SubscribeWhiteboardMessage.fromJson(json));
  //   if (msg == null) return;
  //
  // }

  // _unsubscribeWhiteboard(Map<String, dynamic> json) async {
  //   var msg = _try(() => UnsubscribeWhiteboardMessage.fromJson(json));
  //   if (msg == null) return;
  //
  // }

  _loadWhiteboardInfo(Map<String, dynamic> json) async {
    var msg = _try(() => WhiteboardInfoMessage.fromJson(json));
    if (msg == null) return;

  }

  // _getOnlineUserList(Map<String, dynamic> json) async {
  //   var msg = _try(() => OnlineUsersMessage.fromJson(json));
  //   if (msg == null) return;
  //
  // }

  // _getStudentStates(Map<String, dynamic> json) async {
  //   var msg = _try(() => UserStateListMessage.fromJson(json));
  //   if (msg == null) return;
  //
  // }

  _getAllDrawingData(Map<String, dynamic> json) async {
    var msg = _try(() => GetAllDrawingPathsMessage.fromJson(json));
    if (msg == null) return;

  }
}
