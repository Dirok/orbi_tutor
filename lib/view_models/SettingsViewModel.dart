import 'dart:core';

import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class SettingsViewModel extends BaseViewModel {

  final cameraDelaySeconds = Settings.cameraDelaySeconds.obs;
  final isNeedToSaveImages = (appSession.account.preferences.isNeedToSaveImages ?? Settings.isNeedToSaveImages).obs;
  final isStylusModeEnabled = (appSession.account.preferences.isStylusModeEnabled ?? Settings.isStylusModeEnabled).obs;

  void setCameraDelaySeconds(double value) {
    cameraDelaySeconds.value = value;
    Settings.cameraDelaySeconds = value;
  }

  void setIsNeedToSaveImages(bool value) {
    isNeedToSaveImages.value = value;
    Settings.isNeedToSaveImages = value;
    appSession.account.preferences.isNeedToSaveImages = value;
  }

  void setIsStylusModeEnabled(bool value) {
    isStylusModeEnabled.value = value;
    Settings.isStylusModeEnabled = value;
    appSession.account.preferences.isStylusModeEnabled = value;
  }
}
