import 'dart:async';

import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

typedef void SubjectListAction(SubjectItemViewModel subject);
typedef bool IsSubjectSelectionMode();

class SubjectItemViewModel extends BaseViewModel {
  final Subject subject;
  final SubjectListAction chooseSubject;
  final SubjectListAction deleteSubject;
  final SubjectListAction selectItemAction;
  final IsSubjectSelectionMode _isSelectionMode;
  final isSelected = false.obs;
  bool get isCustom => subject.isCustom;

  SubjectItemViewModel(this.subject, this.chooseSubject, this.deleteSubject, this.selectItemAction, this._isSelectionMode);

  void performDefaultAction() => chooseSubject?.call(this);

  void performDeleteSubject() => deleteSubject?.call(this);

  void performSelectAction() {
    if (!isCustom) return;
    isSelected.value = !isSelected.value;
    selectItemAction?.call(this);
  }

  bool get isSelectionMode => _isSelectionMode?.call() ?? false;
}
