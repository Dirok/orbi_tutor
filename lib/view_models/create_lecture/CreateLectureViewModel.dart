import 'package:bson/bson.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class CreateLectureViewModel extends BaseViewModel {
  final _mainServer = Get.find<MainServerApi>();
  final _subjectSelector = Get.find<SelectSubjectViewModel>();
  final _participantSelector = Get.find<ParticipantsListViewModel>();

  final isCreatingLecture = false.obs;

  final isDefaultTimeSet = true.obs;

  final time = TimeOfDay(hour: 0, minute: 0).obs;
  final date = DateTime.now().obs;

  final description = ''.obs;
  final groupName = ''.obs;

  Rx<Subject> get subject => _subjectSelector.selectedSubject;

  RxList<ContactViewModel> get participantsList => _participantSelector.selectedParticipants;
  
  bool get hasPersonalLicense => appSession.account.permissions.hasPersonalLicense;

  DateTime utcLectureStart;

  Lecture _lecture;

  @override
  void onInit() {
    super.onInit();
    isDefaultTimeSet.value = true;
  }

  bool checkParticipantsInGroup() =>
      participantsList.length == 1 && participantsList[0].contact is Group;

  Future<bool> checkLectureSettings() async {
    var usedTime = isDefaultTimeSet.value ? TimeOfDay.now() : time.value;
    utcLectureStart = DateTime(date.value.year, date.value.month,
        date.value.day, usedTime.hour, usedTime.minute).toUtc();

    var utcNow = DateTime.now().toUtc();
    var diff = utcLectureStart.difference(utcNow);

    if (diff < Duration(minutes: -5)) {
      Log.warning('trying of set wrong time ' + diff.toString());
      return false;
    }
    return true;
  }

  Future<bool> tryCreateLecture()  => _createLecture.callWithLock(isCreatingLecture);

  Future<bool> _createLecture() async {

    // todo add work with policy
    final policy = LecturePolicy.opened;

    final participantsIds = Set<ObjectId>();
    participantsList.forEach((contactVM) => participantsIds.addAll(contactVM.contact.membersIds));

    final serverType = appSession.account.hasSchoolLicense
        ? LectureServerType.lectureServer
        : LectureServerType.realm;

    final createResult = await _mainServer.createLecture(
        subject.value,
        utcLectureStart,
        policy,
        serverType,
        participantsIds: participantsIds.toList(),
        description: description.value);

    if (!createResult.isSuccess) {
      Log.info('createLecture failed');
      return false;
    }

    _lecture = createResult.payload;

    return true;
  }

  Future<void> openLecture() async {
    if (_lecture == null) {
      sendEvent(CantOpenLectureEvent());
      return;
    }

    final _lectureDataService = Get.find<LectureDataService>();
    await _lectureDataService.fetchParticipantsToLecture(_lecture);
    _lectureDataService.currentLecture = _lecture;

    sendEvent(OpenLectureEventFactory.create(
        isGroupLecture: appSession.account.hasSchoolLicense,
        isOwner: _lecture.currentUserIsLectureOwner));
  }
}