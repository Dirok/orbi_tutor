import 'package:get/get.dart';
import 'package:tutor/focused.dart';

typedef void ParticipantListAction(ParticipantListItemViewModel participantItem);

abstract class ParticipantListItemViewModel extends GetxController {
  final ContactViewModel participant;
  final ParticipantListAction selectItemAction;
  final isSelected = false.obs;

  IContact get contact => participant.contact;
  String get name => participant.contact.fullName;

  ParticipantListItemViewModel(this.participant, this.selectItemAction, {bool isSelectedItem}) {
    isSelected.value = isSelectedItem;
  }

  void selectParticipantAction() {
    isSelected.value = !isSelected.value;
    selectItemAction?.call(this);
  }

  void participantInfoAction();
}

class UserGroupListItemViewModel extends ParticipantListItemViewModel
    implements Comparable<UserGroupListItemViewModel> {
  final ParticipantListAction itemInfoAction;

  @override
  UserGroupListItemViewModel(GroupViewModel group, ParticipantListAction selectItemAction,
      this.itemInfoAction, {bool isSelected = false})
      : super(group, selectItemAction, isSelectedItem: isSelected);

  @override
  void participantInfoAction() => itemInfoAction?.call(this);

  @override
  int compareTo(UserGroupListItemViewModel other) {
    var adt = name;
    var bdt = other.name;
    return adt.compareTo(bdt);
  }
}

class UserContactListItemViewModel extends ParticipantListItemViewModel
    implements Comparable<UserContactListItemViewModel> {

  UserContactListItemViewModel(FriendViewModel contact, ParticipantListAction selectItemAction,
      {bool isSelected = false})
      : super(contact, selectItemAction, isSelectedItem: isSelected);

  @override
  void participantInfoAction() => null;

  @override
  int compareTo(UserContactListItemViewModel other) {
    var adt = name;
    var bdt = other.name;
    return adt.compareTo(bdt);
  }
}

