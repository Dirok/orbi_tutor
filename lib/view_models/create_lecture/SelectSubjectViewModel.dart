import 'dart:async';

import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class SelectSubjectViewModel extends BaseViewModel {
  final _serverUpdatesService = Get.find<ServerUpdatesService>();
  final _userDataService = Get.find<UserDataService>();

  final List<StreamSubscription> _subscribes = [];

  final isPredefSubjectListVisible = false.obs;

  final selectedSubject = Subject.unknown().obs;

  final userSubjects = <SubjectItemViewModel>[].obs;
  final thinoutedPredefSubject = <SubjectItemViewModel>[].obs;

  final predefinedSubjects = <SubjectItemViewModel>[];
  final searchSubjectsList = <SubjectItemViewModel>[];

  final selectedSubjects = Set<SubjectItemViewModel>().obs;
  Iterable get allCustomSubjectsList => userSubjects.value;

  RxBool get _isSelectionMode => RxBool(selectedSubjects.length > 0);

  @override
  void onInit() {
    super.onInit();

    _updateSelectedSubject();

    _serverUpdatesService.init();

    _userDataService
        .predefinedSubjectsMap
        .listen((_) => _updatePredefinedSubjectList())
        .addTo(_subscribes);
    _updatePredefinedSubjectList();

    _userDataService
        .userSubjectsMap
        .listen((_) => _updateUserSubjectList())
        .addTo(_subscribes);
    _updateUserSubjectList();
  }

  @override
  void onClose() {
    _subscribes.disposeSubscriptions();
    super.onClose();
  }

  Subject searchForDuplicates(String newSubjectName) {
    final lowerCaseSubjectName = newSubjectName.toLowerCase();
    final contains = searchSubjectsList.where((s) => s.subject.name.toLowerCase() == lowerCaseSubjectName);
    return contains.isNotEmpty ? contains.first.subject : null;
  }

  Future<Subject> tryCreateSubject(String subjectName) async {
    return await _userDataService.tryCreateSubject(subjectName);
  }

  Future<bool> tryDeleteSubject(Subject subject) async {
    final deletionResult = await _userDataService.tryDeleteSubject(subject);

    if (deletionResult && selectedSubject.value == subject)
      selectedSubject.value = Subject.unknown();

    return deletionResult;
  }

  void _updatePredefinedSubjectList() async {
    predefinedSubjects.clear();
    Map.of(_userDataService.predefinedSubjectsMap).forEach((key, value) {
      predefinedSubjects.add(SubjectItemViewModel(
          value, chooseSubject, deleteSubject, selectItemAction, isSelectionMode));
    });
    _updateUserSubjectList();
  }

  void _updateUserSubjectList() {
    final currUserSubjects = <SubjectItemViewModel>[];
    final currThinoutedSubject = <SubjectItemViewModel>[];

    currThinoutedSubject.assignAll(predefinedSubjects);

    final subjects = Map.of(_userDataService.userSubjectsMap);

    for (final subject in subjects.values) {
      final subjectVM = SubjectItemViewModel(
          subject, chooseSubject, deleteSubject, selectItemAction, isSelectionMode);

      currUserSubjects.add(subjectVM);
      currThinoutedSubject.remove(subjectVM);
    }

    userSubjects.assignAll(currUserSubjects);
    thinoutedPredefSubject.assignAll(currThinoutedSubject);

    searchSubjectsList.assignAll(thinoutedPredefSubject);
    searchSubjectsList.addAll(userSubjects);

    _updateSelectedSubject();
  }

  void _updateSelectedSubject() {
    if (!selectedSubject.value.isUnknown) return;

    userSubjects.isNotEmpty
      ? selectedSubject.value = userSubjects.first.subject
      : selectedSubject.value = Subject.unknown();
  }

  void chooseSubject(SubjectItemViewModel item) {
    selectedSubject.value = item.subject;
    appSession.account.preferences.recentSearchedSubjectList.add(item.subject);
    appSession.setAccountPref();
    sendEvent(SubjectGetResultEvent(selectedSubject.value));
  }

  Future<void> deleteSubject(SubjectItemViewModel item) async {
    final deletionResult = await tryDeleteSubject(item.subject);
    if (!deletionResult)
      sendEvent(SubjectDeleteErrorEvent());
  }

  void selectItemAction(SubjectItemViewModel item) {
    if (item.isSelected.value)
      selectedSubjects.add(item);
    else
      selectedSubjects.remove(item);
  }

  void selectAllSubjects() {
    allCustomSubjectsList.forEach((item) {
      if (!item.isSelected.value) {
        item.performSelectAction();
      }
    });
  }

  void clearSelectedSubjects() {
    selectedSubjects.forEach((item) {
      item.isSelected.value = false;
    });
    selectedSubjects.clear();
  }

  bool isSelectionMode() => _isSelectionMode.value;

  List<Subject> getSuggestionList(String query) {
    if (query.isEmpty)
      return appSession.account.preferences.recentSearchedSubjectList.toList();
    else
      return
        searchSubjectsList
          .map((item) => item.subject)
          .where(
            (element) => element.name.toLowerCase().contains(query.toLowerCase()),
        ).toList();
  }

  void setRecentSearchedSubject(Subject selectedResult) {
    appSession.account.preferences.recentSearchedSubjectList.add(selectedResult);
    appSession.setAccountPref();
  }
}

class SubjectDeleteErrorEvent extends Event {}
class SubjectGetResultEvent extends Event {
  final Subject subject;
  SubjectGetResultEvent(this.subject);
}
