import 'dart:async';

import 'package:bson/bson.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class ParticipantsListViewModel extends BaseViewModel {
  final _serverUpdatesService = Get.find<ServerUpdatesService>();
  final _userDataService = Get.find<UserDataService>();

  final List<StreamSubscription> _subscribes = [];

  final userGroupsItemList = <ParticipantListItemViewModel>[].obs;
  final userContactsItemList = <ParticipantListItemViewModel>[].obs;

  final selectedParticipants = <ContactViewModel>[].obs;

  final searchParticipantList = <ContactViewModel>[];

  RxBool get isGroupsUpdating => _userDataService.isUserGroupsUpdating;
  RxBool get isContactsUpdating =>  _userDataService.isUserContactsUpdating;

  bool get isNoContacts => userGroupsItemList.isEmpty && userContactsItemList.isEmpty;
  bool get isSomeSelected => selectedParticipants.isNotEmpty;

  var tempUserContacts;
  var tempUserGroups;

  // todo del after full group and contacts testing
  void testContactCreation() {
    User user0 = User(ObjectId.fromHexString('611a60445fdfea9fb79d1050'), 'Перец0', 'Фамилия0', 'Отчество');
    User user1 = User(ObjectId.fromHexString('611a60445fdfea9fb79d1051'), 'Перец1', 'Фамилия1', 'Отчество');
    User user2 = User(ObjectId.fromHexString('611a60445fdfea9fb79d1052'), 'Перец2', 'Фамилия2', 'Отчество');
    User user3 = User(ObjectId.fromHexString('611a60445fdfea9fb79d1053'), 'Перец3', 'Фамилия3', 'Отчество');
    User user4 = User(ObjectId.fromHexString('611a60445fdfea9fb79d1054'), 'Перец4', 'Фамилия4', 'Отчество');
    User user5 = User(ObjectId.fromHexString('611a60455fdfea9fb79d1055'), 'Перец5', 'Фамилия5', 'Отчество');
    User user6 = User(ObjectId.fromHexString('611a60455fdfea9fb79d1056'), 'Перец6', 'Фамилия6', 'Отчество');
    User user7 = User(ObjectId.fromHexString('611a60455fdfea9fb79d1057'), 'Перец7', 'Фамилия7', 'Отчество');

    tempUserContacts = <User>[ user0, user1, user2, user3, user4, user5, user6, user7];
    tempUserGroups = <Group>[
      Group(ObjectId.fromHexString('507c7f79bcf86cd7994f6c11'), 'Сборище1', user0, GroupPolicy.opened),
      Group(ObjectId.fromHexString('507c7f79bcf86cd7994f6c12'), 'Сборище2', user0, GroupPolicy.opened),
      Group(ObjectId.fromHexString('507c7f79bcf86cd7994f6c13'), 'Сборище3', user0, GroupPolicy.opened),
      Group(ObjectId.fromHexString('507c7f79bcf86cd7994f6c14'), 'Сборище4', user0, GroupPolicy.opened),
      Group(ObjectId.fromHexString('507c7f79bcf86cd7994f6c15'), 'Сборище5', user0, GroupPolicy.opened),
      Group(ObjectId.fromHexString('507c7f79bcf86cd7994f6c16'), 'Сборище6', user0, GroupPolicy.opened),
      Group(ObjectId.fromHexString('507c7f79bcf86cd7994f6c17'), 'Сборище7', user0, GroupPolicy.opened),
    ];

    Group group = tempUserGroups[0];
    group.addMembers(tempUserContacts);

    var tempTemp1 = <User>[ user0, user1, user2];
    var tempTemp2 = <User>[ user0, user3, user4];
    tempUserGroups[1].addMembers(tempTemp1);
    tempUserGroups[2].addMembers(tempTemp2);
  }

  @override
  void onInit() {
    super.onInit();

    testContactCreation(); // todo del after full group and contacts testing

    _serverUpdatesService.init();

    _userDataService
        .userGroupsMap
        .listen((_) => _updateUserGroupsList())
        .addTo(_subscribes);

    _userDataService
        .userContactsMap
        .listen((_) => _updateUserContactsList())
        .addTo(_subscribes);

    _updateAllUserContacts();
  }

  @override
  void onClose() {
    _subscribes.disposeSubscriptions();
    super.onClose();
  }

  Future<void> fetchUserContacts() async {
    await _userDataService.tryUpdateUserGroups();
    await _userDataService.tryUpdateUserContacts();
  }

  void _updateAllUserContacts() {
    _updateUserGroupsList();
    _updateUserContactsList();
    _updateSearchList();
  }

  void _updateUserGroupsList() {
    // region todo del this after second group task stage will be finished
    var userGroups2 = <UserGroupListItemViewModel>[];
    for (final group in tempUserGroups as List<Group>) {
      final groupVM = GroupViewModel(group);
      final needSelect = selectedParticipants.contains(groupVM);
      final groupItem = UserGroupListItemViewModel(
          groupVM, onSelectParticipant, onParticipantInfo, isSelected: needSelect);
      userGroups2.add(groupItem);
    }
    userGroupsItemList.assignAll(userGroups2);
    return;
    // endregion

    var userGroups = <UserGroupListItemViewModel>[];

    for (final group in _userDataService.userGroupsMap.values) {
      final groupVM = GroupViewModel(group);
      final needSelect = selectedParticipants.contains(groupVM);
      final groupItem = UserGroupListItemViewModel(
          groupVM, onSelectParticipant, onParticipantInfo, isSelected: needSelect);
      userGroups.add(groupItem);
    }

    userGroupsItemList.assignAll(userGroups);
  }

  void _updateUserContactsList() {
    // region todo del this after second group task stage will be finished
    var userContacts2 = <UserContactListItemViewModel>[];
    for (final friend in tempUserContacts as List<User>) {
      final friendVM = FriendViewModel(friend);
      final needSelect = selectedParticipants.contains(friendVM);
      final contactItem = UserContactListItemViewModel(
          friendVM, onSelectParticipant, isSelected: needSelect);
      userContacts2.add(contactItem);
    }
    userContactsItemList.assignAll(userContacts2);
    return;
    // endregion

    var userContacts = <UserContactListItemViewModel>[];

    for (final friend in _userDataService.userContactsMap.values) {
      final friendVM = FriendViewModel(friend);
      final needSelect = selectedParticipants.contains(friendVM);
      final contactItem = UserContactListItemViewModel(
          friendVM, onSelectParticipant, isSelected: needSelect);
      userContacts.add(contactItem);
    }

    userContactsItemList.assignAll(userContacts);
  }

  void _updateSearchList() {
    searchParticipantList.clear();
    userGroupsItemList
        .where((g) => !selectedParticipants.contains(g.participant))
        .forEach((g) => searchParticipantList.add(g.participant));
    userContactsItemList
        .where((c) => !selectedParticipants.contains(c.participant))
        .forEach((c) => searchParticipantList.add(c.participant));
  }

  void onSelectParticipant(ParticipantListItemViewModel item) {
    final participant = item.participant;
    item.isSelected.value
      ? addParticipant(participant)
      : removeParticipant(participant);

    Log.info(selectedParticipants.map((p) => p.name));
  }

  void addParticipant(ContactViewModel participant) {
    if (!selectedParticipants.contains(participant))
      selectedParticipants.add(participant);

    _updateAllUserContacts();
  }

  bool removeParticipant(ContactViewModel participant) {
    bool removeResult;
    while (selectedParticipants.contains(participant))
      removeResult = selectedParticipants.remove(participant);

    _updateAllUserContacts();

    return removeResult;
  }

  void clearSelectedParticipant() {
    selectedParticipants.clear();
    _updateAllUserContacts();
  }

  void onParticipantInfo(ParticipantListItemViewModel item) =>
      sendEvent(ShowParticipantInfoEvent(item.participant));
}

class ShowParticipantInfoEvent extends Event {
  final participant;
  ShowParticipantInfoEvent(this.participant);
}