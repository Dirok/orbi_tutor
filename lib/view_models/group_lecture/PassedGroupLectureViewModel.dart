import 'package:tutor/focused.dart';

abstract class PassedGroupLectureViewModel extends GroupLectureViewModel {
  @override
  void onInit() {
    super.onInit();
    drawingController.drawingEnabled.value = false;
    initializeFuture.value = _loadLecture();
  }

  Future<void> _loadLecture() async {
    isSelfWhiteboardSetActive.value = true;
    selfWhiteboardSet = await lectureStorageService.loadWhiteboardSet(lecture, userId);
    if (selfWhiteboardSet != null && selfWhiteboardSet.length > 0) {
      setCurrentWhiteboard(selfWhiteboardSet);
    } else {
      selfWhiteboardSet = WhiteboardSet(userId);
      insertWhiteboard(selfWhiteboardSet);
    }
  }
}