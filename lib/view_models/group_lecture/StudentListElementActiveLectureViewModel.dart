import 'package:bson/bson.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

typedef Future<void> SetUserAllowSpeak(ObjectId id, bool value);
typedef Future<void> LowerUserHand(ObjectId id);

class StudentListElementActiveGroupLectureViewModel extends StudentListElementViewModel {
  final isOnline = false.obs;
  final isHandRaised = false.obs;
  final allowSpeak = false.obs;
  final isMicEnabled = false.obs;
  final volumeLevel = Constants.minAudioVolume.obs;

  StudentListElementActiveGroupLectureViewModel(
      User student, [OpenUserWhiteboards goToStudentWhiteboards])
      : super(student, goToStudentWhiteboards);

  @override
  int compareTo(other) {
    var byHand = isHandRaised.value == other.isHandRaised.value;
    if (!byHand) return isHandRaised.value ? -1 : 1;

    var byOnline = isOnline.value == other.isOnline.value;
    if (!byOnline) return isOnline.value ? -1 : 1;

    return name.compareTo(other.name);
  }
}

class ControllableStudentListElementActiveGroupLectureViewModel
    extends StudentListElementActiveGroupLectureViewModel {

  final SetUserAllowSpeak _setAllowSpeak;
  final LowerUserHand _lowerHand;

  ControllableStudentListElementActiveGroupLectureViewModel(
      User student, [OpenUserWhiteboards goToStudentWhiteboards,
        SetUserAllowSpeak setStudentAllowSpeak, LowerUserHand lowerStudentHand])
      : _setAllowSpeak = setStudentAllowSpeak, _lowerHand = lowerStudentHand,
        super(student, goToStudentWhiteboards);

  Future<void> setAllowSpeak() => _setAllowSpeak?.call(id, !allowSpeak.value);
  Future<void> lowerHand() =>_lowerHand?.call(id);
}