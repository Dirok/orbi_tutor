import 'package:bson/bson.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

typedef Future<void> OpenUserWhiteboards(ObjectId id);

class StudentListElementViewModel extends GetxController implements Comparable {
  final User _student;
  final OpenUserWhiteboards _openWhiteboards;

  final hasWhiteboards = false.obs;

  ObjectId get id => _student.id;
  String get name => _student.surnameName;

  final isPresentedNow = false.obs;

  StudentListElementViewModel(User student, OpenUserWhiteboards goToStudentWhiteboards)
      : _student = student, _openWhiteboards = goToStudentWhiteboards;

  Future<void> openWhiteboards() => _openWhiteboards?.call(id);

  @override
  int compareTo(other) {
    var byWhiteboard = hasWhiteboards.value == other.hasWhiteboards.value;
    if (!byWhiteboard) return hasWhiteboards.value ? -1 : 1;

    return name.compareTo(other.name);
  }
}