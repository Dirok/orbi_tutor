import 'dart:async';

import 'package:bson/bson.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

abstract class ActiveGroupLectureViewModel extends GroupLectureViewModel
    with MultimediaLectureViewModelMixin
    implements IStudentListViewModel, IActiveLectureViewModel {
  static const _lectureServerReconnectInterval = Duration(seconds: 3);

  final List<StreamSubscription> subscribes = [];

  final _lectureServer = LectureServerClassicApi();

  bool get isActiveLecture => true;

  @override
  ILectureServerApi get lectureServer => _lectureServer;

  final studentViewModelById = <ObjectId, StudentListElementActiveGroupLectureViewModel>{};

  @override
  final studentViewModelList = RxList<StudentListElementActiveGroupLectureViewModel>();

  StudentListElementActiveGroupLectureViewModel teacherViewModel;

  final isLectureServerConnects = false.obs;
  final isLectureServerConnected = false.obs;

  bool get isConnects => isLectureServerConnects.value && isMultimediaServerConnects.value;

  final isOnline = false.obs;

  final isPinEnabled = true.obs;

  bool _isDisposed = false;

  Timer _lectureServerReconnectionTimer;

  @override
  void onInit() {
    super.onInit();

    subscribeLectureServerEvents();

    initMultimediaMixin(this);

    teacherViewModel = StudentListElementActiveGroupLectureViewModel(teacher);
  }

  @override
  void onClose() async {
    await disposeConnection();

    super.onClose();
  }

  Future<void> disposeConnection() async {
    if (_isDisposed) return;

    if (lecture.state == LectureState.planned)
      await lectureStorageService.saveWithWhiteboard(lecture, selfWhiteboardSet);

    subscribes.forEach((s) => s.cancel());

    await disposeMultimedia();

    await _disposeLectureServer();

    _isDisposed = true;
  }

  Future<void> _disposeLectureServer() async {
    unsubscribeLectureServerEvents();

    await lectureServer.close();

    _lectureServerReconnectionTimer?.cancel();
  }

  Future<List<ObjectId>> loadUsersInfo() async {
    final onlineUserIdList = await lectureServer.getOnlineUserList();

    // fill online students
    onlineUserIdList.forEach((id) {
      if (!studentViewModelById.containsKey(id)) return;

      studentViewModelById[id]
        ..isOnline.value = true
        ..hasWhiteboards.value = true;
    });

    studentViewModelList.sort();
    return onlineUserIdList;
  }

  subscribeLectureServerEvents() {
    lectureServer
      ..imageReceived = _onImageReceived
      ..userWhiteboardInserted = onUserWhiteboardInserted
      ..whiteboardImageChanged = onWhiteboardImageChanged
      ..drawingPathAdded = _onDrawingPathAdded
      ..drawingPointAdded = _onDrawingPointAdded
      ..lastDrawingPathRemoved = _onLastDrawingPathRemoved
      ..drawingCleared = _onDrawingCleaned
      ..connectionClosed = _onLectureServerConnectionClosed
      ..userMicStateChanged = userMicStateChanged
      ..userConnected = onUserConnected
      ..userDisconnected = onUserDisconnected;
  }

  unsubscribeLectureServerEvents() {
    lectureServer
      ..imageReceived = null
      ..userWhiteboardInserted = null
      ..whiteboardImageChanged = null
      ..drawingPathAdded = null
      ..drawingPointAdded = null
      ..lastDrawingPathRemoved = null
      ..drawingCleared = null
      ..connectionClosed = null
      ..userMicStateChanged = null
      ..userConnected = null
      ..userDisconnected = null;
  }

  Future<void> initConnection() async {
    subscribes.add(isConnectedToInternet
        .listen((_) async => await _tryConnectToServers()));

    await _tryConnectToServers();
  }

  Future<void> _tryConnectToServers() async {
    if (!isConnectedToInternet.value) return;

    if (!lectureServer.isConnected)
      await _tryConnectToLectureServer();

    if (!multimediaServer.isConnected)
      await tryConnectToMultimediaServer();
  }

  Future<void> _tryConnectToLectureServer() async {
    isLectureServerConnects.value = true;

    isLectureServerConnected.value = await lectureServer.connect(lecture);
    if (!isLectureServerConnected.value) {
      Log.error('Unable to connect to lecture server ${lecture.server}');

      _startLectureServerReconnectionTimer();

      isLectureServerConnects.value = false;
      return;
    }

    initializeFuture.value = onLectureServerConnected();

    isLectureServerConnects.value = false;
    return;
  }

  Future<void> onLectureServerConnected();

  @override
  Future<void> insertWhiteboard(WhiteboardSet whiteboardSet) async {
    super.insertWhiteboard(whiteboardSet);

    if (lecture.state == LectureState.active)
      await insertWhiteboardOnline(whiteboardSet);
  }

  Future<void> loadOrCreateSelfWhiteboards() async {
    try {
      selfWhiteboardSet = await lectureServer.getUserWhiteboardsInfo(userId);
    } catch (e) {
      Log.info('Self whiteboards are null - will insert one for you');
    }

    if (selfWhiteboardSet == null || selfWhiteboardSet.length == 0) {
      selfWhiteboardSet = WhiteboardSet(userId);
      await insertWhiteboard(selfWhiteboardSet);
    }
    else {
      if (isPinEnabled.value) {
        // load main whiteboard first
        final whiteboardInfo = await lectureServer.getMainWhiteboardInfo();
        final wb = selfWhiteboardSet.getWhiteboardById(whiteboardInfo.id);
        if (wb != null) selfWhiteboardSet.current = wb;
      }

      await loadWhiteboardSetData(selfWhiteboardSet);
    }
  }

  void _onLectureServerConnectionClosed() {
    isLectureServerConnected.value = false;
    _startLectureServerReconnectionTimer();
  }

  userMicStateChanged(ObjectId userId, bool isEnabled) {
    Log.info('Change mic state from lecture server');
    if (!studentViewModelById.containsKey(userId) && userId != teacher.id) {
      Log.error('Change mic state failed: no id');
      return;
    }

    final userViewModel = userId == teacher.id
        ? teacherViewModel
        : studentViewModelById[userId];

    userViewModel.isMicEnabled.value = isEnabled;

    if (!isEnabled)
      changeUserVolumeLevel(userId, Constants.minAudioVolume);
  }

  Future<UserState> onUserConnected(ObjectId userId) async {
    if (!studentViewModelById.containsKey(userId)) return null;
    if (!lectureServer.isConnected) return null;

    UserState state;
    try {
      state = await lectureServer.getUserState(userId);
    } on CommandFailedException catch (e) {
      Log.error('_onUserConnected($userId) error', exception: e);
      return null;
    }

    studentViewModelById[userId]
      ..isOnline.value = true
      ..hasWhiteboards.value = true
      ..allowSpeak.value = state.allowSpeak
      ..isHandRaised.value = state.isHandRaised
      ..isMicEnabled.value = state.isMicEnabled;

    studentViewModelList.sort();

    return state;
  }

  Future<void> onUserDisconnected(ObjectId userId) async {
    if (!studentViewModelById.containsKey(userId)) return;

    studentViewModelById[userId]
      ..isOnline.value = false
      ..allowSpeak.value = false
      ..isHandRaised.value = false
      ..isMicEnabled.value = false;

    studentViewModelList.sort();

    Log.info('User disconnected mic state from lecture server');
    changeUserVolumeLevel(userId, Constants.minAudioVolume);
  }

  void _startLectureServerReconnectionTimer() {
    _lectureServerReconnectionTimer?.cancel();
    if (lecture.state == LectureState.active)
      _lectureServerReconnectionTimer = Timer(_lectureServerReconnectInterval, _tryConnectToLectureServer);
  }

  Future<void> insertWhiteboardOnline(WhiteboardSet whiteboardSet) async {
    if (!lectureServer.isConnected) return;
    try {
      await lectureServer.sendInsertWhiteboard(
          whiteboardSet.currentIndex, whiteboardSet.current, whiteboardSet.id);

      if (whiteboardSet.current.ownerId == userId)
        await lectureServer.sendSetCurrentWhiteboard(whiteboardSet.current.id, whiteboardSet.id);

      await lectureServer.subscribeWhiteboard(whiteboardSet.current.id);
    } on CommandFailedException catch(e) {
      Log.error(e);
    }
  }

  Future<void> changeWhiteboard(int index) async {
    super.changeWhiteboard(index);

    if (lecture.state != LectureState.active) return;
    if (!lectureServer.isConnected) return;
    try {
      await onWhiteboardChanged(currentWhiteboard.value);
    } on CommandFailedException catch (e) {
      Log.error(e);
    }
  }

  onWhiteboardChanged(Whiteboard wb) async {
    if (isSelfWhiteboardSetActive.value)
      await lectureServer.sendSetCurrentWhiteboard(wb.id, selfWhiteboardSet.id);
  }

  onUserWhiteboardInserted(Whiteboard whiteboard, int index);

  onWhiteboardImageChanged(ObjectId whiteboardId, String imageName) async {
    // TODO change for any whiteboard
    final wb = currentWhiteboardSet.getWhiteboardById(whiteboardId);
    if (wb == null) return;

    final image =
        await lectureServer.downloadImage(lecture.key, lecture.id, imageName, lecture.imagesFolderPath);
    if (image == null) {
      Log.error('Downloaded image is null');
      return;
    }

    wb.images.add(image);

    if (wb == currentWhiteboard.value) currentWhiteboard.refresh();
  }

  Future<void> preloadOtherUserInsertedWhiteboard(Whiteboard wb, int index) async {
    wb.setImageFolderPath(imagesFolderPath);

    var whiteboardSet = wb.ownerId == userId ? selfWhiteboardSet : otherWhiteboardSet;
    whiteboardSet.insert(index, wb);

    if (whiteboardSet == currentWhiteboardSet)
      updateWhiteboardIndexTitle(whiteboardSet);

    await loadWhiteboardData(wb);
    await lectureServer.subscribeWhiteboard(wb.id);
  }

  void _onImageReceived(ImageMessage msg) async {
    var wb = _getWhiteboardById(msg.whiteboardId);
    if (wb == null) return;

    if (!wb.imageIds.contains(msg.imageInfo.id))
      wb.images.add(msg.imageInfo);

    if (wb == currentWhiteboard.value)
      currentWhiteboard.refresh();
  }

  Future<void> loadWhiteboardSetData(WhiteboardSet whiteboardSet) async {
    await loadWhiteboardDataAndSubscribe(whiteboardSet.current);
    setCurrentWhiteboard(whiteboardSet);
    preloadDataForNotActiveWhiteboards(whiteboardSet);
  }

  Future<void> loadWhiteboardDataAndSubscribe(Whiteboard wb) async {
    await loadWhiteboardData(wb);
    await lectureServer.subscribeWhiteboard(wb.id);
  }

  Future<void> loadWhiteboardData(Whiteboard wb) async {
    await _loadImage(wb);
    await _loadDrawingData(wb);
  }

  Future<void> preloadDataForNotActiveWhiteboards(WhiteboardSet whiteboardSet) async {
    for (var wb in whiteboardSet.whiteboards) {
      if (wb != whiteboardSet.current)
        await loadWhiteboardDataAndSubscribe(wb);
    }
  }

  Future<void> _loadDrawingData(Whiteboard wb) async =>
      wb.drawingSet = await lectureServer.getAllDrawingData(wb);

  Future<void> _loadImage(Whiteboard wb) async {
    if (wb.image == null) return;
    wb.setImageFolderPath(imagesFolderPath);
    if (!await File(wb.image.path).exists())
      lectureServer.requestWhiteboardImage(wb.id, wb.image);
  }

  @override
  Future<bool> addPhoto(Whiteboard wb, String imagePath, CameraResult result) async {
    final isSuccess = await super.addPhoto(wb, imagePath, result);
    if (!isSuccess) return false;

    if (lecture.state != LectureState.active) return true;
    return _trySendImage(wb);
  }

  @override
  Future<bool> addImage(Whiteboard wb) async {
    final isSuccess = await super.addImage(wb);
    if (!isSuccess) return false;
    if (lecture.state != LectureState.active) return true;
    return _trySendImage(wb);
  }

  Future<bool> _trySendImage(Whiteboard wb) async {
    if (wb.image == null) return false;

    if (lectureServer.isDisconnected) {
      Log.error('Image sending with no internet');
      sendEvent(SendImageFailedEvent());
      return false;
    }

    final result = await lectureServer.uploadImage(lecture.key, wb.image, lecture.id, wb.id);
    if (result == SendDataResult.success) return true;

    switch (result) {
      case SendDataResult.fail:
        sendEvent(SendImageFailedEvent());
        break;
      case SendDataResult.noData:
        sendEvent(SendImageFailedEvent()); // TODO no data message
        break;
      case SendDataResult.tooLarge:
        sendEvent(ImageTooLargeErrorEvent());
        break;
      default:
        Log.error('Unknown image sending result');
        break;
    }

    wb.images.removeLast();

    if (wb == currentWhiteboard.value)
      currentWhiteboard.refresh();

    return false;
  }

  _onDrawingPathAdded(ObjectId whiteboardId, DrawingData drawingData, ObjectId userId) {
    var wb = _getWhiteboardById(whiteboardId);
    if (wb == null) return;

    wb.addDrawingPath(drawingData, userId);

    if (wb == currentWhiteboard.value)
      currentWhiteboard.refresh();
  }

  _onDrawingPointAdded(ObjectId whiteboardId, DrawingPoint point, ObjectId userId) {
    var wb = _getWhiteboardById(whiteboardId);
    if (wb == null) return;

    wb.addDrawingPoint(point, userId);

    if (wb == currentWhiteboard.value)
      currentWhiteboard.refresh();
  }

  _onLastDrawingPathRemoved(ObjectId whiteboardId, ObjectId userId) {
    var wb = _getWhiteboardById(whiteboardId);
    if (wb == null) return;

    var removedId = wb.removeLastDrawingPath(userId);

    if (wb == currentWhiteboard.value && removedId != null)
      currentWhiteboard.refresh();
  }

  _onDrawingCleaned(ObjectId whiteboardId) async {
    var wb = _getWhiteboardById(whiteboardId);
    if (wb == null) return;

    wb.clearDrawing();

    if (wb == currentWhiteboard.value)
      currentWhiteboard.refresh();
  }

  undoDrawing() {
    var removedId = currentWhiteboard.value.removeLastDrawingPath(userId);
    if (removedId == null) return;

    currentWhiteboard.refresh();

    if(lectureServer.isConnected)
      lectureServer.undoDrawing(currentWhiteboard.value.id, removedId);
  }

  clearDrawing() {
    currentWhiteboard.value.clearDrawing();

    currentWhiteboard.refresh();

    if(lectureServer.isConnected)
      lectureServer.clearDrawing(currentWhiteboard.value.id);
  }

  Whiteboard _getWhiteboardById(ObjectId whiteboardId) {
    if (selfWhiteboardSet != null) {
      var wb = selfWhiteboardSet.getWhiteboardById(whiteboardId);
      if (wb != null) return wb;
    }

    if (otherWhiteboardSet != null) {
      var wb = otherWhiteboardSet.getWhiteboardById(whiteboardId);
      if (wb != null) return wb;
    }

    return null;
  }

  setPinState(bool enabled) { }

  Future<void> onMicStateChanged(bool isEnabled) => lectureServer.setMicState(isEnabled);
}

class SendImageFailedEvent extends Event {}
class ImageTooLargeErrorEvent extends Event {}
class LectureDestroyedEvent extends Event {}