import 'package:get/get.dart';
import 'package:tutor/focused.dart';

abstract class IStudentListViewModel {
  RxList<StudentListElementViewModel> get studentViewModelList;
  bool get isActiveLecture;
}
