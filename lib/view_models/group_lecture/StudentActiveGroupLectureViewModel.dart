import 'package:bson/bson.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class StudentActiveGroupLectureViewModel extends ActiveGroupLectureViewModel {
  final isHandRaised = false.obs;

  Future<void> raiseHand() async =>
      isHandRaised.value = await lectureServer.setHandState(!isHandRaised.value);

  @override
  void onInit() {
    super.onInit();

    isOnline.value = true;
    allowSpeak.value = false;

    initConnection();
    createStudentViewModels();
  }

  void createStudentViewModels() {
    userMap.forEach((id, student) {
      final studentViewModel = StudentListElementActiveGroupLectureViewModel(student);
      studentViewModelById[id] = studentViewModel;
      studentViewModelList.add(studentViewModel);
    });
  }

  @override
  Future<void> onLectureServerConnected() async {
    if (!lectureServer.isConnected) return;

    try {
      await loadUsersInfo();
    } on CommandFailedException catch (e) {
      Log.error(e);
    }

    try {
      var state = await lectureServer.getUserState(userId);
      if (state != null) {
        allowSpeak.value = state.allowSpeak;
        isHandRaised.value = state.isHandRaised;
      }

      isPinEnabled.value = await lectureServer.getPinState();

      if (isPinEnabled.value) {
        allowNavigateWorkspaces.value = allowNavigateWhiteboards.value = false;
        await _goToMainWhiteboard();
      }

      await loadOrCreateSelfWhiteboards();

      setCurrentWhiteboard(currentWhiteboardSet);
    } on CommandFailedException catch (e) {
      Log.error(e);
    }
  }

  @override
  setCurrentWhiteboard(WhiteboardSet whiteboardSet) {
    super.setCurrentWhiteboard(whiteboardSet);
    drawingController.drawingEnabled.value = whiteboardSet.ownerId == userId;
  }

  @override
  Future<void> changeWhiteboardsSource() =>
      isSelfWhiteboardSetActive.value ? _goToTeacherWhiteboards() : goToSelfWhiteboards();

  Future<void> insertSelfWhiteboard() => insertWhiteboard(selfWhiteboardSet);

  _goToMainWhiteboard() async {
    final whiteboardInfo = await lectureServer.getMainWhiteboardInfo();
    return _loadMainWhiteboard(whiteboardInfo);
  }

  _loadMainWhiteboard(MainWhiteboardInfo info) async {
    // previous main whiteboard belongs to another student
    if (currentWhiteboard.value != null &&
        _isOtherStudentWhiteboard(currentWhiteboard.value.ownerId))
      await lectureServer.unsubscribeWhiteboard(currentWhiteboard.value.id);

    var mainWhiteboard = await lectureServer.loadWhiteboardInfo(info.id);
    mainWhiteboard.setImageFolderPath(imagesFolderPath);

    // teacher can show our whiteboard as main
    if (mainWhiteboard.ownerId != userId) {
      isSelfWhiteboardSetActive.value = false;

      otherWhiteboardSet = WhiteboardSetStub(
          mainWhiteboard, info.index, info.containingSequenceLength);

      await loadWhiteboardData(otherWhiteboardSet.current);
      await lectureServer.subscribeWhiteboard(otherWhiteboardSet.current.id);

      setCurrentWhiteboard(otherWhiteboardSet);
    } else {
      isSelfWhiteboardSetActive.value = true;

      if (selfWhiteboardSet != null) {
        var wb = selfWhiteboardSet.getWhiteboardById(mainWhiteboard.id);
        if (wb != null) {
          selfWhiteboardSet.current = wb;
          setCurrentWhiteboard(selfWhiteboardSet);
          lectureServer.sendSetCurrentWhiteboard(wb.id, selfWhiteboardSet.id);
        }
      }
    }
  }

  bool _isOtherStudentWhiteboard(ObjectId ownerId) =>
      ownerId != userId && ownerId != lecture.owner.id;

  @override
  onUserWhiteboardInserted(Whiteboard whiteboard, int index) async {
    if (whiteboard.ownerId != userId
        && (isPinEnabled.value || whiteboard.ownerId != lecture.owner.id)) return;
    if (!lectureServer.isConnected) return;

    try {
      await preloadOtherUserInsertedWhiteboard(whiteboard, index);
    } on CommandFailedException catch(e) {
      Log.error(e);
    }
  }

  _onMainWhiteboardChanged(MainWhiteboardInfo info) {
    if (!isPinEnabled.value) return;
    if (!lectureServer.isConnected) return;
    try {
      initializeFuture.value = _loadMainWhiteboard(info);
    } on CommandFailedException catch (e) {
      Log.error(e);
    }
  }

  Future<void> _goToTeacherWhiteboards() async {
    isSelfWhiteboardSetActive.value = false;

    if (!lectureServer.isConnected) return;
    try {
      otherWhiteboardSet = await lectureServer.getUserWhiteboardsInfo(lecture.owner.id);
      await loadWhiteboardSetData(otherWhiteboardSet);
    } on CommandFailedException catch (e) {
      Log.error(e);
    }
  }

  @override
  goToSelfWhiteboards() async {
    super.goToSelfWhiteboards();

    if (!_isOtherStudentWhiteboard(currentWhiteboard.value.ownerId)) return;

    if (!lectureServer.isConnected) return;
    try {
      await lectureServer.unsubscribeWhiteboard(currentWhiteboard.value.id);
    } on CommandFailedException catch (e) {
      Log.error(e);
    }
  }

  _onPinStateChanged(bool enabled) {
    isPinEnabled.value = enabled;
    allowNavigateWorkspaces.value = allowNavigateWhiteboards.value = !isPinEnabled.value;
    if (!lectureServer.isConnected) return;
    try {
      if (isPinEnabled.value)
        initializeFuture.value = _goToMainWhiteboard();
      else if (currentWhiteboard.value.ownerId == lecture.owner.id)
        _goToTeacherWhiteboards();
    } on CommandFailedException catch (e) {
      Log.error(e);
    }
  }

  _onHandStateChanged(ObjectId userId, bool isRaised) {
    if (userId == this.userId)
      isHandRaised.value = isRaised;
  }

  @override
  subscribeLectureServerEvents() {
    super.subscribeLectureServerEvents();

    lectureServer
      ..mainWhiteboardChanged = _onMainWhiteboardChanged
      ..pinStateChanged = _onPinStateChanged
      ..allowSpeakChanged = onAllowSpeakChanged
      ..userHandStateChanged = _onHandStateChanged
      ..lectureCheckingMode = _onLectureFinished
      ..lectureFinished = _onLectureFinished;
  }

  @override
  unsubscribeLectureServerEvents() {
    super.unsubscribeLectureServerEvents();

    lectureServer
      ..mainWhiteboardChanged = null
      ..pinStateChanged = null
      ..allowSpeakChanged = null
      ..userHandStateChanged = null
      ..lectureCheckingMode = null
      ..lectureFinished = null;
  }

  _onLectureFinished() {
    isOnline.value = false;
    sendEvent(LectureFinishedEvent());
  }
}

class LectureFinishedEvent extends Event {}
