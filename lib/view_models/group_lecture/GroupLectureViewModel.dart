import 'package:bson/bson.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

abstract class GroupLectureViewModel extends LectureViewModel {
  WhiteboardSet selfWhiteboardSet;
  WhiteboardSet otherWhiteboardSet;

  Map<ObjectId, User> userMap = Map();

  User get teacher => lecture.owner;

  var isSelfWhiteboardSetActive = true.obs;

  @override
  WhiteboardSet get currentWhiteboardSet =>
      isSelfWhiteboardSetActive.value ? selfWhiteboardSet : otherWhiteboardSet;

  set currentWhiteboardSet(WhiteboardSet value) =>
      isSelfWhiteboardSetActive.value
          ? selfWhiteboardSet = value
          : otherWhiteboardSet = value;

  var ownerName = ''.obs;

  var allowNavigateWorkspaces = true.obs;

  @override
  String get imagesFolderPath => lecture.imagesFolderPath;

  @override
  void onInit() {
    super.onInit();
    userMap.addAll(Map.fromIterable(
        lecture.participants, key: (s) => s.id, value: (s) => s));
  }

  @override
  setCurrentWhiteboard(WhiteboardSet whiteboardSet) {
    super.setCurrentWhiteboard(whiteboardSet);
    ownerName.value = _getWhiteboardOwnerName(currentWhiteboard.value);
  }

  String _getWhiteboardOwnerName(Whiteboard wb) {
    if (wb == null) return '';
    if (wb.ownerId == lecture.owner.id)
      return lecture.owner.shortName;
    if (userMap.containsKey(wb.ownerId))
      return userMap[wb.ownerId].shortName;
    return "Unknown";
  }

  updateWhiteboardIndexTitle(WhiteboardSet whiteboardSet) {
    indexTitleWidthFactor.value = _getIndexTitleWidthFactor(whiteboardSet);
    whiteboardsCount.value = whiteboardSet?.length ?? 0;
    currentWhiteboardIndex.value = whiteboardSet?.currentIndex ?? 0;
    isFirstWhiteboard.value = currentWhiteboardIndex.value == 0;
    isLastWhiteboard.value = currentWhiteboardIndex.value == whiteboardsCount.value - 1;
  }

  int _getIndexTitleWidthFactor(WhiteboardSet whiteboardSet) {
    if (whiteboardSet == null || whiteboardSet.length < 10) return 1;
    if (whiteboardSet.length < 100) return 2;

    // 100+
    var number = whiteboardSet.length;
    var count = 0;
    while (number != 0) {
      count++;
      number = number~/10;
    }
    return count;
  }

  Future<void> changeWhiteboardsSource();

  goToSelfWhiteboards() {
    isSelfWhiteboardSetActive.value = true;
    setCurrentWhiteboard(selfWhiteboardSet);
  }
}