import 'package:tutor/focused.dart';

class StudentPassedGroupLectureViewModel extends PassedGroupLectureViewModel {
  @override
  Future<void> changeWhiteboardsSource() =>
      isSelfWhiteboardSetActive.value ? _goToTeacherWhiteboards() : goToSelfWhiteboards();

  Future<void> _goToTeacherWhiteboards() async {
    isSelfWhiteboardSetActive.value = false;
    otherWhiteboardSet ??= await lectureStorageService.loadWhiteboardSet(lecture, lecture.owner.id);
    setCurrentWhiteboard(otherWhiteboardSet);
  }
}