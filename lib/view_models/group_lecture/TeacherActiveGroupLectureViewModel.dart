import 'dart:async';
import 'dart:collection';

import 'package:bson/bson.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class TeacherActiveGroupLectureViewModel extends ActiveGroupLectureViewModel implements IStudentListViewModel {
  final lectureDataService = Get.find<LectureDataService>();

  final HashSet<ObjectId> _raisedHands = HashSet();

  final studentConnectionInfoViewModel = Rx<StudentConnectionInfoViewModel>(null);

  Timer studentTimer;

  bool _isFirstConnection = false;

  var raisedHandsCount = 0.obs;

  var showStudentState = false.obs;

  bool get isActiveLecture => lecture.state == LectureState.active;

  bool get canStartLecture => lectureDataService.isActiveLecturesLimitNotExceeded;

  @override
  void onInit() {
    super.onInit();

    hasAudio.value = true;
    createStudentViewModels();
    initializeFuture.value = _init();
  }

  @override
  setCurrentWhiteboard(WhiteboardSet whiteboardSet) {
    super.setCurrentWhiteboard(whiteboardSet);
    studentViewModelList.forEach((s) => s.isPresentedNow.value = s.id == whiteboardSet.ownerId);
  }

  Future<void> _init() async {
    if (lecture.state == LectureState.active
        || lecture.state == LectureState.checking) {
      isOnline.value = true;
      await initConnection();
    }
    else if (lecture.state == LectureState.planned) {
      await _initPlannedLecture();
    }
  }

  Future<void> _initPlannedLecture() async {
    isSelfWhiteboardSetActive.value = true;
    allowNavigateWorkspaces.value = false;

    selfWhiteboardSet = await lectureStorageService.loadWhiteboardSet(lecture, userId);
    if (selfWhiteboardSet != null && selfWhiteboardSet.length > 0) {
      setCurrentWhiteboard(selfWhiteboardSet);
    } else {
      selfWhiteboardSet = WhiteboardSet(userId);
      insertWhiteboard(selfWhiteboardSet);
    }
  }

  createStudentViewModels() {
    userMap.forEach((id, student) {
      final studentViewModel = ControllableStudentListElementActiveGroupLectureViewModel(
          student, _goToStudentWhiteboards, _setStudentAllowSpeak, _lowerStudentHand);
      studentViewModelById[id] = studentViewModel;
      studentViewModelList.add(studentViewModel);
    });

    studentViewModelList.sort();
  }

  @override
  Future<void> onLectureServerConnected() async {
    if (!lectureServer.isConnected) return;
    try {
      if (_isFirstConnection) {
        _isFirstConnection = false;
        await _uploadLectureData();
      } else {
        await _loadLectureData();
      }

      await loadUsersInfo();
    } on CommandFailedException catch (e) {
      Log.error(e);
    }
  }

  Future<void> _uploadLectureData() async {
    await _uploadLocalWhiteboards();
    await lectureServer.setMainWhiteboard(currentWhiteboard.value.id);
    await setPinState(true);
  }

  Future<void> _loadLectureData() async {
    isPinEnabled.value = await lectureServer.getPinState();

    final whiteboardInfo = await lectureServer.getMainWhiteboardInfo();

    final mainWhiteboard = await lectureServer.loadWhiteboardInfo(whiteboardInfo.id);
    mainWhiteboard.setImageFolderPath(imagesFolderPath);

    if (mainWhiteboard.ownerId == userId) {
      isSelfWhiteboardSetActive.value = true;
      allowNavigateWorkspaces.value = false;
      await loadOrCreateSelfWhiteboards();
      selfWhiteboardSet.current = mainWhiteboard;
      setCurrentWhiteboard(selfWhiteboardSet);
    } else {
      isSelfWhiteboardSetActive.value = false;
      allowNavigateWorkspaces.value = true;
      otherWhiteboardSet = await lectureServer.getUserWhiteboardsInfo(mainWhiteboard.ownerId);
      otherWhiteboardSet.current = mainWhiteboard;

      await loadWhiteboardSetData(otherWhiteboardSet);

      loadOrCreateSelfWhiteboards();
    }
  }

  @override
  Future<List<ObjectId>> loadUsersInfo() async {
    final studentStateMap = await lectureServer.getStudentStates();
    final onlineUsersInfo = await super.loadUsersInfo();

    // fill online students
    onlineUsersInfo.forEach((id) {
      if (!studentViewModelById.containsKey(id)) return;

      var state = studentStateMap.remove(id);
      if (state == null) return;

      studentViewModelById[id]
        ..allowSpeak.value = state.allowSpeak
        ..isHandRaised.value = state.isHandRaised
        ..isMicEnabled.value = state.isMicEnabled;

      _updateHandsInfo(id, state.isHandRaised);
    });

    // fill offline students
    studentStateMap.forEach((id, state) {
      if (!studentViewModelById.containsKey(id)) return;
      studentViewModelById[id]
        ..hasWhiteboards.value = state.hasWhiteboards
        ..allowSpeak.value = state.allowSpeak
        ..isHandRaised.value = state.isHandRaised
        ..isMicEnabled.value = state.isMicEnabled;
    });

    studentViewModelList.sort();
    return null;
  }

  Future<void> _uploadLocalWhiteboards() async {
    var index = 0;
    for (var wb in selfWhiteboardSet.whiteboards) {
      await lectureServer.sendInsertWhiteboard(index, wb, selfWhiteboardSet.id);
      index++;
    }

    await lectureServer.sendSetCurrentWhiteboard(selfWhiteboardSet.current.id, selfWhiteboardSet.id);
    await _uploadWhiteboardData(selfWhiteboardSet.current);
    await lectureServer.subscribeWhiteboard(selfWhiteboardSet.current.id);

    for (var wb in selfWhiteboardSet.whiteboards) {
      if (wb != selfWhiteboardSet.current) {
        await _uploadWhiteboardData(wb);
        await lectureServer.subscribeWhiteboard(wb.id);
      }
    }
  }

  Future<void> _uploadWhiteboardData(Whiteboard wb) async {
    _uploadDrawing(wb);
    _uploadImage(wb);
  }

  _uploadDrawing(Whiteboard wb) {
    if (wb.drawingSet.length > 0)
      lectureServer.uploadDrawing(wb);
  }

  Future<void> _uploadImage(Whiteboard wb) async {
    if (wb.image == null) return;
    var data = await getFileData(wb.image.path);
    if (data == null) return;
    await lectureServer.uploadImage(lecture.key, wb.image, lecture.id, wb.id);
  }

  @override
  Future<void> onWhiteboardChanged(Whiteboard wb) async {
    if (!lectureServer.isConnected) return;
    try {
      await super.onWhiteboardChanged(wb);
      await lectureServer.setMainWhiteboard(wb.id);
    } on CommandFailedException catch (e) {
      Log.error(e);
    }
  }

  @override
  Future<void> changeWhiteboardsSource() async {
    if (!isSelfWhiteboardSetActive.value)
      await goToSelfWhiteboards();
  }

  @override
  Future<void> goToSelfWhiteboards() async {
    super.goToSelfWhiteboards();
    allowNavigateWorkspaces.value = false;

    if (!lectureServer.isConnected) return;
    try {
      await lectureServer.setMainWhiteboard(selfWhiteboardSet.current.id);

      for (var wb in otherWhiteboardSet.whiteboards)
        await lectureServer.unsubscribeWhiteboard(wb.id);

      otherWhiteboardSet.clear();
    } on CommandFailedException catch (e) {
      Log.error(e);
    }
  }

  Future<void> addMainWhiteboard() async {
    await insertWhiteboard(currentWhiteboardSet);

    if (lecture.state != LectureState.active
      && lecture.state != LectureState.checking) return;

    if (!lectureServer.isConnected) return;
    try {
      await lectureServer.setMainWhiteboard(currentWhiteboard.value.id);
    } on CommandFailedException catch (e) {
      Log.error(e);
    }
  }

  Future<void> tryStartLecture() async {
    Lecture lectureInfo;
    try {
      lectureInfo = await lectureDataService.tryStartLecture(lecture.id);
    } on ServerOperationException catch (e) {
      Log.error(e);
      final error = e.errorType;
      final errorMessage = error == ServerOperationError.lectureAlreadyActive
          ? Translations.lecture_already_active_error
          : Translations.error;
      sendEvent(ServerOperationErrorEvent(errorMessage));
      return;
    }

    if (lectureInfo == null) {
      Log.error('Failed to start lecture');
      return;
    }

    lecture = lectureInfo;

    isOnline.value = true;

    _isFirstConnection = true;

    await startMultimediaServer();

    await initConnection();
  }

  Future<void> _goToStudentWhiteboards(ObjectId studentId) async {
    if (!lectureServer.isConnected) return;
    try {
      if (isPinEnabled.value)
        await setPinState(false);

      allowNavigateWorkspaces.value = true;

      if (isSelfWhiteboardSetActive.value) {
        isSelfWhiteboardSetActive.value = false;
      } else {
        for (var wb in otherWhiteboardSet.whiteboards)
          await lectureServer.unsubscribeWhiteboard(wb.id);
        otherWhiteboardSet.clear();
      }

      otherWhiteboardSet = await lectureServer.getUserWhiteboardsInfo(studentId);

      await lectureServer.setMainWhiteboard(otherWhiteboardSet.current.id);

      await loadWhiteboardSetData(otherWhiteboardSet);
    } on CommandFailedException catch(e) {
      Log.error(e);
    }
  }

  @override
  onUserWhiteboardInserted(Whiteboard whiteboard, int index) async {
    if (isSelfWhiteboardSetActive.value || whiteboard.ownerId != otherWhiteboardSet.ownerId) return;
    if (!lectureServer.isConnected) return;
    try {
      await preloadOtherUserInsertedWhiteboard(whiteboard, index);
    } on CommandFailedException catch(e) {
      Log.error(e);
    }
  }

  Future<void> _setStudentAllowSpeak(ObjectId studentId, bool allow) async {
    if (!lectureServer.isConnected) return;

    bool allowSpeak;
    try {
      allowSpeak = await lectureServer.sendStudentAllowSpeak(studentId, allow);
    } on CommandFailedException catch(e) {
      Log.error('sendStudentAllowSpeak() error', exception: e);
      return;
    }

    if (studentViewModelById.containsKey(studentId)) {
      var studentViewModel = studentViewModelById[studentId];
      studentViewModel.allowSpeak.value = allowSpeak;
    }
  }

  Future<void> _lowerStudentHand(ObjectId studentId) async {
    if (!lectureServer.isConnected) return;
    if (!studentViewModelById.containsKey(studentId)) return;

    bool isHandRaised;
    try {
      isHandRaised = await lectureServer.sendStudentHandState(studentId, false);
    } on CommandFailedException catch(e) {
      Log.error('_lowerStudentHand() error', exception: e);
      return;
    }

    var studentViewModel = studentViewModelById[studentId];
    studentViewModel.isHandRaised.value = isHandRaised;

    _updateHandsInfo(studentId, isHandRaised);
  }

  @override
  Future<UserState> onUserConnected(ObjectId userId) async {
    final state = await super.onUserConnected(userId);
    if (state == null) return null;

    studentTimer?.cancel();

    _updateHandsInfo(userId, state.isHandRaised);

    var studentName = _getStudentName(userId);
    if (studentName == null) return null;
    studentConnectionInfoViewModel.value =
        StudentConnectionInfoViewModel(studentName, isConnected: true);
    studentTimer = Timer(Duration(seconds: 3), () {
      studentConnectionInfoViewModel.value = null;
    });
    return state;
  }

  @override
  Future<void> onUserDisconnected(ObjectId userId) async {
    await super.onUserDisconnected(userId);

    if (!studentViewModelById.containsKey(userId)) return;

    studentTimer?.cancel();

    _updateHandsInfo(userId, false);

    var studentName = _getStudentName(userId);
    studentConnectionInfoViewModel.value =
        StudentConnectionInfoViewModel(studentName, isConnected: false);
    studentTimer = Timer(Duration(seconds: 3), () {
      studentConnectionInfoViewModel.value = null;
    });
  }

  _onUserHandStateChanged(ObjectId userId, bool isRaised) {
    if (!studentViewModelById.containsKey(userId)) return;

    var studentViewModel = studentViewModelById[userId];
    studentViewModel.isHandRaised.value = isRaised;

    _updateHandsInfo(userId, isRaised);
  }

  _updateHandsInfo(ObjectId userId, bool isRaised) {
    if (isRaised) _raisedHands.add(userId);
    else _raisedHands.remove(userId);

    raisedHandsCount.value = _raisedHands.length;
  }

  @override
  setPinState(bool enabled) async {
    if (!lectureServer.isConnected) return;
    try {
      isPinEnabled.value = await lectureServer.sendPinState(enabled);
    } on CommandFailedException catch(e) {
      Log.error('setPinState($enabled) failed', exception: e);
    }
  }

  @override
  subscribeLectureServerEvents() {
    super.subscribeLectureServerEvents();

    lectureServer
      ..userConnected = onUserConnected
      ..userDisconnected = onUserDisconnected
      ..userHandStateChanged = _onUserHandStateChanged
      ..userMicStateChanged = userMicStateChanged;
  }

  @override
  unsubscribeLectureServerEvents() {
    super.unsubscribeLectureServerEvents();

    lectureServer
      ..userConnected = null
      ..userDisconnected = null
      ..userHandStateChanged = null
      ..userMicStateChanged = null;
  }

  Future<void> exitLectureWithFinish() async {
    isOnline.value = false;

    await multimediaServer.leaveRoom();
    await stopMultimediaServer();

    final response = await lectureDataService.tryStopLecture(lecture);
    if (!response)
      Log.warning('Lecture not finished correctly');

    subscribes.forEach((s) => s.cancel());

    await lectureStorageService.removeLectureData(lecture);
  }

  Future<void> setLectureCheckingMode() async {
    await multimediaServer.leaveRoom();
    await stopMultimediaServer();

    final response = await lectureDataService.trySetCheckingMode(lecture);
    if (!response)
      Log.warning('Lecture not checking mode correctly');

    lecture.state = LectureState.checking;
  }

  Future<void> exitLectureWithoutFinish() async {
    isOnline.value = false;
    await multimediaServer.leaveRoom();
    subscribes.forEach((s) => s.cancel());
  }

  String _getStudentName(ObjectId studentId) =>
      userMap.containsKey(studentId) ? userMap[studentId].surnameName : '';
}
