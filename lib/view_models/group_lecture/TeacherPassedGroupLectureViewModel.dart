import 'package:bson/bson.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class TeacherPassedGroupLectureViewModel extends PassedGroupLectureViewModel implements IStudentListViewModel {
  final Map<ObjectId, StudentListElementViewModel> _studentViewModelById = Map();

  var studentViewModelList = RxList<StudentListElementViewModel>();

  bool get isActiveLecture => false;

  @override
  void onInit() {
    super.onInit();
    allowNavigateWorkspaces.value = false;
    _createStudentViewModels();
  }

  @override
  setCurrentWhiteboard(WhiteboardSet whiteboardSet) {
    super.setCurrentWhiteboard(whiteboardSet);
    studentViewModelList.forEach((s) => s.isPresentedNow.value = s.id == whiteboardSet.ownerId);
  }

  _createStudentViewModels() {
    userMap.forEach((id, student) {
      var studentViewModel = StudentListElementViewModel(student, _goToStudentWhiteboards);
      studentViewModel.hasWhiteboards.value = lectureStorageService.isWhiteboardSetExists(lecture, id);
      _studentViewModelById[id] = studentViewModel;
      studentViewModelList.add(studentViewModel);
    });

    studentViewModelList.sort();
  }

  Future<void> _goToStudentWhiteboards(ObjectId studentId) async {
    if (isSelfWhiteboardSetActive.value) {
      isSelfWhiteboardSetActive.value = false;
      allowNavigateWorkspaces.value = true;
    }

    otherWhiteboardSet = await lectureStorageService.loadWhiteboardSet(lecture, studentId);
    setCurrentWhiteboard(otherWhiteboardSet);
  }

  @override
  Future<void> changeWhiteboardsSource() async {
    if (!isSelfWhiteboardSetActive.value)
      await goToSelfWhiteboards();
  }

  @override
  Future<void> goToSelfWhiteboards() async {
    super.goToSelfWhiteboards();
    allowNavigateWorkspaces.value = false;
  }
}