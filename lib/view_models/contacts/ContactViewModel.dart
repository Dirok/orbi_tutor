import 'package:get/get.dart';
import 'package:tutor/focused.dart';

abstract class ContactViewModel extends GetxController {
  final IContact contact;
  String get tagName;

  String get name => contact.fullName;

  ContactViewModel(this.contact);

  @override
  bool operator ==(Object other) => identical(this, other)
      || (other is ContactViewModel && contact.id == other.contact.id);

  @override
  int get hashCode => contact.id.hashCode;
}

class GroupViewModel extends ContactViewModel {
  @override
  String get tagName => TagTrimmer.trimTagByLength(contact.fullName);

  @override
  GroupViewModel(Group group) : super(group);
}

class FriendViewModel extends ContactViewModel {
  @override
  String get tagName {
    final trimmed = TagTrimmer.trimTagBySpace(contact.fullName);
    return TagTrimmer.trimTagByLength(trimmed);
  }

  @override
  FriendViewModel(User friend) : super(friend);
}