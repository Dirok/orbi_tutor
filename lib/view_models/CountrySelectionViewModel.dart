import 'package:country_provider/country_provider.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class CountrySelectionViewModel extends BaseViewModel {
  final _mainServer = Get.find<MainServerApi>();

  final _countryProviderService = Get.find<CountryProviderService>();

  CountrySelectionViewModel() {
    _loadCountries();
    _initSelectedCountry();
  }

  List<Country> countries;

  final selectedCountry = Rx<Country>(null);

  _loadCountries() {
    countries = _countryProviderService.countries;
  }

  _initSelectedCountry() {
    selectedCountry.value = countries?.firstWhere(
        (element) => element?.alpha2Code == Get.deviceLocale.countryCode,
        orElse: () => null);
  }

  Future<void> confirmCountryRequest() async {
    try {
      final currUser = appSession.account.user;
      final result = await _mainServer.updateUserOld(currUser,
          countryCode: int.parse(selectedCountry.value.numericCode));
      if (!result.isSuccess) {
        sendEvent(UpdateAccountFailedEvent());
        return;
      }
      final isSuccess = result.payload;
      sendEvent(isSuccess ? AddCountrySuccessEvent() : AddCountryFailedEvent());
    } catch (e) {
      Log.error('confirmCountryRequest exception', exception: e);
      sendEvent(AddCountryFailedEvent());
    }
  }
}

class AddCountrySuccessEvent extends Event {}

class AddCountryFailedEvent extends Event {}
