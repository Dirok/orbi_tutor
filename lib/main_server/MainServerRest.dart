import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:tutor/focused.dart';

class MainServerRest with ServerRestMixin {
  String _url;

  MainServerRest(this._url);

  set uri(String uri) => _url = uri;

  String _token;

  Map<String, String> get _headers =>
      {'Content-Type': 'application/json', 'Authorization': 'Bearer $_token'};

  void setToken(String token) {
    if (token == null) return;
    _token = token;
  }

  // region authorization requests
  Future<Map> jwtAuth(String token, String deviceId) async {
    final uri = Uri.parse('$_url/auth?deviceID=$deviceId');
    final headers = {'Content-type': 'application/json', 'Authorization': 'Bearer ' + token};

    Log.info('------ jwtAuth request: $uri');

    final response = await http.get(uri, headers: headers);
    return parseResponse(response, 'jwtAuth');
  }

  Future<Map> authByKundelik(String token, String deviceId) async {
    final uri = Uri.parse('$_url/kundelik/auth?deviceID=$deviceId');
    final headers = {'Content-type': 'application/json', 'Authorization': 'Bearer ' + token};

    Log.info('------ authByKundelik request: $uri');

    final response = await http.get(uri, headers: headers);
    return parseResponse(response, 'authByKundelik');
  }

  Future<Map> authByLogin(String email, String password, String deviceId) async {
    final uri = Uri.parse('$_url/login?deviceID=$deviceId');
    final jsonBody = json.encode({'email': email, 'password': password});
    final headers = {'Content-type': 'application/json'};

    Log.info('------ authByLogin request: $uri');

    final response = await http.post(uri, headers: headers, body: jsonBody);
    return parseResponse(response, 'authByLogin');
  }

  Future<Map> authByLink(String hashLink, String deviceId) async {
    final uri = Uri.parse('$_url$hashLink?deviceID=$deviceId');
    final headers = {'Content-type': 'application/json'};

    Log.info('------ authByLink request: $uri');

    final response = await http.get(uri, headers: headers);
    return parseResponse(response, 'authByLink');
  }

  Future<Map> refreshToken(String deviceId, String refreshToken) async {
    final uri = Uri.parse('$_url/refresh?deviceID=$deviceId');
    final jsonBody = json.encode({'refreshToken': refreshToken});

    Log.info('------ refreshToken request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, 'refreshToken');
  }

  // endregion

  // region users
  @deprecated
  Future<bool> updateUserOld(
      String id, String name, String surname, String patronymic,
      {int countryCode}) async {
    final uri = Uri.parse('$_url/user/update');
    final jsonBody = json.encode({
      'userID': id,
      'name': name,
      'surname': surname,
      'patronymic': patronymic,
      'countryCode': countryCode,
    });

    Log.info('------ updateUser old request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    parseResponse(response, 'updateUser old');
    return true;
  }

  Future<bool> updateUser(Map<String, dynamic> user) async {
    final uri = Uri.parse('$_url/user/update');
    final jsonBody = json.encode({'user': user});

    Log.info('------ updateUser request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    parseResponse(response, 'updateUser');
    return true;
  }

  Future<Map> getUserInfo(String userId) async {
    final uri = Uri.parse('$_url/user/info');
    final jsonBody = json.encode({'user_id': userId});

    Log.info('------ getUserInfo request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, 'getUserInfo')['account'];
  }

  @deprecated
  Future<List> getUserClasses(String userId, String userRole) async {
    final uri = Uri.parse('$_url/user/classes');
    final jsonBody = json.encode({'user_id': userId, 'role': userRole});

    Log.info('------ getUserClasses request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, 'getUserClasses')['classes'];
  }

  Future<bool> deleteUser(String userId) async {
    final uri = Uri.parse('$_url/user/del');
    final jsonBody = json.encode({'user_id': userId});

    Log.info('------ deleteUser request: $uri id: $userId');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    parseResponse(response, 'deleteUser');
    return true;
  }

  Future<Map> getUserHashes() async {
    final uri = Uri.parse('$_url/user/hashes');

    final response = await http.get(uri, headers: _headers);
    return parseResponse(response, 'getUserHashes', showLogs: false)['hashes'];
  }
  // endregion

  // region schools
  Future<Map> createSchool(String name, String address) async {
    final uri = Uri.parse('$_url/school/new');
    final jsonBody = json.encode({'name': name, 'address': address});

    Log.info('------ addSchool request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, 'addSchool')['school'];
  }

  Future<List> getSchools() async {
    final uri = Uri.parse('$_url/schools');

    Log.info('------ getSchools request: $uri');

    final response = await http.get(uri, headers: _headers);
    return parseResponse(response, 'getSchools')['schools'];
  }
  // endregion

  // region subjects
  Future<List> getPredefinedSubjects(String countryCode) async {
    final uri = Uri.parse('$_url/subject/get/list');
    final jsonBody = json.encode({'countryCode': countryCode});

    Log.info('------ getPredefinedSubjects request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, 'getPredefinedSubjects')['subjects'];
  }

  Future<Map> createUserSubject(String subjectName) async {
    final uri = Uri.parse('$_url/user/subject/create');
    final jsonBody = json.encode({'name': subjectName});

    Log.info('------ createUserSubject request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, 'createUserSubject');
  }

  Future<List> getUserSubjects(String countryCode) async {
    final uri = Uri.parse('$_url/user/subject/list');
    final jsonBody = json.encode({'countryCode': countryCode});

    Log.info('------ getUserSubjects request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, 'getUserSubjects')['subjects'];
  }

  Future<bool> deleteUserSubject(String subjectName) async {
    final uri = Uri.parse('$_url/user/subject/delete');
    final jsonBody = json.encode({'name': subjectName});

    Log.info('------ deleteUserSubject request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    parseResponse(response, 'deleteUserSubject');
    return true;
  }
  // endregion

  // region groups
  Future<Map> createGroup(String name, String ownerId, String policy,
      {List<String> membersIds, String schoolId}) async {
    final uri = Uri.parse('$_url/group/create');
    final jsonBody = json.encode({
      'name': name,
      'ownerID': ownerId,
      'policy': policy,
      'schoolID': schoolId,
      'membersIDs': membersIds
    });

    Log.info('------ createGroup request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, 'createGroup')['group'];
  }

  Future<List> getGroupList(List<String> policies, {List<String> schoolIds}) async {
    final uri = Uri.parse('$_url/group/list');
    final jsonBody = json.encode({
      'policies': policies,
      'schoolIDs': schoolIds,
    });

    Log.info('------ getGroupList request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, 'getGroupList')['groups'];
  }

  Future<bool> updateGroup(String groupId, String name, String policy) async {
    final uri = Uri.parse('$_url/group/update');
    final jsonBody = json.encode({
      'id': groupId,
      'name': name,
      'policy': policy,
    });

    Log.info('------ updateGroup request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    parseResponse(response, 'updateGroup');
    return true;
  }

  Future<bool> deleteGroup(String groupId) async {
    final uri = Uri.parse('$_url/group/delete');
    final jsonBody = json.encode({'id': groupId});

    Log.info('------ deleteGroup request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    parseResponse(response, 'deleteGroup');
    return true;
  }

  Future<bool> addGroupMembers(String groupId, List<String> membersIds) async {
    final uri = Uri.parse('$_url/group/members/add');
    final jsonBody = json.encode({'id': groupId, 'membersIDs': membersIds});

    Log.info('------ addGroupMembers request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    parseResponse(response, 'addGroupMembers');
    return true;
  }

  Future<List> getGroupMembers(String groupId) async {
    final uri = Uri.parse('$_url/group/members');
    final jsonBody = json.encode({'id': groupId});

    Log.info('------ getGroupMembers request: $uri for groupId: $groupId');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, 'getGroupMembers')['members'];
  }

  Future<bool> deleteGroupMembers(String groupId, List<String> membersIds) async {
    final uri = Uri.parse('$_url/group/members/delete');
    final jsonBody = json.encode({'id': groupId, 'membersIDs': membersIds});

    Log.info('------ deleteGroupMembers request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    parseResponse(response, 'deleteGroupMembers');
    return true;
  }
  // endregion

  // region contacts
  Future<bool> addUserContacts(List<String> contactsIds) async {
    final uri = Uri.parse('$_url/user/contacts/add');
    final jsonBody = json.encode({'contactsIDs': contactsIds});

    Log.info('------ addUserContacts request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    parseResponse(response, 'addUserContacts');
    return true;
  }

  Future<List> getUserContactList() async {
    final uri = Uri.parse('$_url/user/contacts');

    Log.info('------ getUserContactList request: $uri');

    final response = await http.get(uri, headers: _headers);
    return parseResponse(response, 'getUserContactList')['contacts'];
  }

  Future<bool> deleteUserContacts(List<String> contactsIds) async {
    final uri = Uri.parse('$_url/user/contacts/delete');
    final jsonBody = json.encode({'contactsIDs': contactsIds});

    Log.info('------ deleteUserContacts request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    parseResponse(response, 'deleteUserContacts');
    return true;
  }
  // endregion

  // region links
  Future<bool> createUserLink(String userId) async {
    final uri = Uri.parse('$_url/user/enable-auth-link');
    final jsonBody = json.encode({'user_id': userId});

    Log.info('------ createUserLink request: $uri user_id: $userId');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    parseResponse(response, 'createUserLink');
    return true;
  }

  // endregion

  // region lectures
  Future<Map<String, dynamic>> createLecture(
      Map<String, dynamic> subject,
      String datetime,
      String policy,
      {List<String> participantsIds,
        String description = '',
        String serverType}) async {
    final uri = Uri.parse('$_url/lecture/new');
    final jsonBody = json.encode({
      'datetime': datetime,
      'description': description,
      'participantsIDs': participantsIds,
      'policy': policy,
      'subject': subject,
      'lectureServerType': serverType
    });

    Log.info('------ createLecture request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, 'createLecture')['lecture'];
  }

  Future<Map<String, dynamic>> getLecture(String lectureId) => _requestLecture(lectureId, 'get');
  Future<Map<String, dynamic>> joinLecture(String lectureId) => _requestLecture(lectureId, 'join');
  Future<Map<String, dynamic>> startLecture(String lectureId) => _requestLecture(lectureId, 'start');

  Future<Map<String, dynamic>> _requestLecture(String lectureId, String request) async {
    final uri = Uri.parse('$_url/lecture/$request');
    final jsonBody = json.encode({'lectureID': lectureId});

    Log.info('------ ${request}Lecture request: $uri id: $lectureId');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, '${request}Lecture')['lecture'];
  }

  Future<List> getLecturesList(List<String> states, String fromDatetime, String toDatetime,
      List<String> owners, List<String> subjects,
      {String countryCode = Constants.defaultCountryCode}) async {
    final uri = Uri.parse('$_url/lecture/list');
    final jsonBody = json.encode({
      'states': states,
      'fromDatetime': fromDatetime,
      'toDatetime': toDatetime,
      'owners': owners,
      'subjects': subjects,
      'countryCode': countryCode
    });

    Log.info('------ getLecturesList request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, 'getLecturesList')['lectures'];
  }

  Future<bool> finishLecture(String lectureId) async {
    final uri = Uri.parse('$_url/lecture/finish');
    final jsonBody = json.encode({'lectureID': lectureId});

    Log.info('------ finishLecture request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    parseResponse(response, 'finishLecture');
    return true;
  }

  Future<bool> deleteLecture(String lectureId) async {
    final uri = Uri.parse('$_url/lecture/delete');
    final jsonBody = json.encode({'lectureID': lectureId});

    Log.info('------ deleteLecture request: $uri id: $lectureId');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    parseResponse(response, 'deleteLecture');
    return true;
  }

  Future<List> getLectureParticipants(String lectureId) async {
    final uri = Uri.parse('$_url/lecture/participants');
    final jsonBody = json.encode({'lectureID': lectureId});

    Log.info('------ getLectureParticipants request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, 'getLectureParticipants')['participants'];
  }

  // TODO implement on server side
  Future<Map> getDownloadableLink(String lectureId) async {
    final uri = Uri.parse('$_url/lecture/download');
    final jsonBody = json.encode({'lectureID': lectureId});

    Log.info('------ getDownloadableLink request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, 'getDownloadableLink');
  }

  Future<Map<String, dynamic>> startMultimediaServer(String lectureId) async {
    final uri = Uri.parse('$_url/lecture/janus/start');
    final jsonBody = json.encode({'lectureID': lectureId});

    Log.info('------ startMultimediaServer request: $uri id: $lectureId');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, 'startMultimediaServer');
  }

  Future<Map<String, dynamic>> getMultimediaServer(String lectureId) async {
    final uri = Uri.parse('$_url/lecture/janus/get');
    final jsonBody = json.encode({'lectureID': lectureId});

    Log.info('------ getMultimediaServer request: $uri id: $lectureId');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    return parseResponse(response, 'getMultimediaServer');
  }

  Future<bool> stopMultimediaServer(String lectureId) async {
    final uri = Uri.parse('$_url/lecture/janus/stop');
    final jsonBody = json.encode({'lectureID': lectureId});

    Log.info('------ stopMultimediaServer request: $uri id: $lectureId');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    parseResponse(response, 'stopMultimediaServer');
    return true;
  }

  // endregion

  // region other
  Future<String> getVersion() async {
    final uri = Uri.parse('$_url/api/version');

    Log.info('------ getVersion request: $uri');

    final response = await http.get(uri);
    return parseResponse(response, 'getVersion')['version'];
  }

  Future<bool> updateFcmToken(String fcmToken) async {
    if (fcmToken == null || fcmToken.isEmpty) {
      Log.info('No saved fcm token');
      return false;
    }

    final uri = Uri.parse('$_url/user/notify/token');
    final jsonBody = json.encode({'FCMToken': fcmToken});

    Log.info('------ updateFcmToken request: $uri');

    final response = await http.post(uri, headers: _headers, body: jsonBody);
    parseResponse(response, 'updateFcmToken');
    return true;
  }

  /// temporary debug function to upload image on server
  Future<bool> uploadFiles(
      {@required originalPath,
      @required originalName,
      @required resultPath,
      @required resultName,
      @required errorCode}) async {
    // TODO: Update from dio to http or back
    /*
    
      const String debug_url = 'https://imfocused.io:8999/upload';

      final environment = PlatformUtils.environment;

      FormData formData = FormData.fromMap({
        'version': environment.appVersion,
        'platform': environment.platform,
        'device': environment.device,
        'original': await MultipartFile.fromFile(originalPath, filename: originalName),
        'result': await MultipartFile.fromFile(resultPath, filename: resultName),
        'errorCode': errorCode,
      });

      Log.info('------ uploadFiles request ------');
      var dio = Dio();
      //dio.options.contentType = Headers.formUrlEncodedContentType;
      dio.options.headers['Authorization'] = 'Bearer ' + token;
      final response = await dio.post(debug_url, data: formData);

      
      if (response.statusCode == 200) {
        Log.info('files uploaded: ' + response.statusMessage.toString());
        return true;
      } else {
        Log.info('uploadFiles error: ' + response.statusMessage.toString());
        return false;
      }
    
     */
  }
  // endregion
}
