import 'dart:async';
import 'dart:core';

import 'package:bson/bson.dart';
import 'package:tutor/focused.dart';

class MainServerApi with TranslatableMixin, EventMixin, ServerApiMixin {
  final MainServerRest _httpRest;

  MainServerApi(String url) : _httpRest = MainServerRest(url) {
    refreshTokenFunc = _refreshToken;
  }

  set uri(String uri) => _httpRest.uri = uri;

  set token(String token) => _httpRest.setToken(token);

  Future<ServerResult<String>> getVersion() async =>
      await checkRequest(_httpRest.getVersion, (r) => r as String);

  // region authorization requests
  Future<ServerResult<AuthorizationBundle>> jwtAuth(String token, ObjectId deviceId) =>
      checkRequest(() => _httpRest.jwtAuth(token, deviceId.toHexString()),
          (r) => AuthorizationBundle.fromJson(r));

  Future<ServerResult<AuthorizationBundle>> authByKundelik(String token, ObjectId deviceId) =>
      checkRequest(() => _httpRest.authByKundelik(token, deviceId.toHexString()),
          (r) => AuthorizationBundle.fromJson(r));

  Future<ServerResult<AuthorizationBundle>> authByLogin(
          String email, String password, ObjectId deviceId) =>
      checkRequest(() => _httpRest.authByLogin(email, password, deviceId.toHexString()),
          (r) => AuthorizationBundle.fromJson(r));

  Future<ServerResult<AuthorizationBundle>> authByLink(String hashLink, ObjectId deviceId) =>
      checkRequest(() => _httpRest.authByLink(hashLink, deviceId.toHexString()),
          (r) => AuthorizationBundle.fromJson(r));

  Future<bool> _refreshToken() async {
    final refreshResult =
        await refreshToken(Settings.deviceId.toHexString(), appSession.refreshToken);

    if (refreshResult.isSuccess) {
      Log.info('making second request after token request');

      token = refreshResult.payload.httpsToken;
      sendEvent(TokenRefreshEvent(refreshResult.payload));
      return true;
    } else {
      return false;
    }
  }

  Future<ServerResult<TokenBundle>> refreshToken(String deviceId, String refreshToken) =>
      checkRequest(
          () => _httpRest.refreshToken(deviceId, refreshToken), (r) => TokenBundle.fromJson(r));

  // endregion

  // region users requests
  @deprecated
  Future<ServerResult<bool>> updateUserOld(User user, {int countryCode}) => checkRequest(() =>
          _httpRest.updateUserOld(user.id.toHexString(), user.name, user.surname, user.patronymic, countryCode: countryCode),
      (r) => r as bool);

  Future<ServerResult<bool>> updateUser(User user) =>
      checkRequest(() => _httpRest.updateUser(user.toJson()), (r) => r as bool);

  Future<ServerResult<UserHashChanges>> getUserHashes() =>
      checkRequest(_httpRest.getUserHashes, (r) => UserHashChanges.fromJson(r), shouldSendEvents: false);
  // endregion

  // region subjects requests
  Future<ServerResult<List<Subject>>> getSubjects(String countryCode) => checkRequest(
      () => _httpRest.getPredefinedSubjects(countryCode),
      (r) => r.map<Subject>((s) => Subject.fromJson(s)).toList());

  Future<ServerResult<List<Subject>>> getUserSubjects(String countryCode) => checkRequest(
      () => _httpRest.getUserSubjects(countryCode),
      (r) => r.map<Subject>((s) => Subject.fromJson(s)).toList());

  Future<ServerResult<Subject>> createUserSubjects(String subjectName) =>
      checkRequest(() => _httpRest.createUserSubject(subjectName), (r) => Subject.fromJson(r));

  Future<ServerResult<bool>> deleteUserSubjects(String subjectName) =>
      checkRequest(() => _httpRest.deleteUserSubject(subjectName), (r) => r as bool);

  // endregion

  // region groups request
  Future<ServerResult<Group>> createGroup(String name, ObjectId ownerId, GroupPolicy policy,
          {List<ObjectId> membersIds = const <ObjectId>[], ObjectId schoolId}) =>
      checkRequest(
          () => _httpRest.createGroup(name, ownerId.toHexString(), policy.asString,
              membersIds: membersIds.map((id) => id.toHexString()).toList(),
              schoolId: schoolId.toHexString()),
          (r) => Group.fromJson(r));

  Future<ServerResult<List<Group>>> getGroupList({List<GroupPolicy> policies = const <GroupPolicy>[],
          List<ObjectId> schoolIds = const <ObjectId>[]}) =>
      checkRequest(
          () => _httpRest.getGroupList(policies.map((p) => p.asString).toList(),
              schoolIds: schoolIds.map((id) => id.toHexString()).toList()),
          (r) => r.map<Group>((g) => Group.fromJson(g)).toList());

  Future<ServerResult<bool>> updateGroup(ObjectId groupId, String name, GroupPolicy policy) =>
      checkRequest(
          () => _httpRest.updateGroup(groupId.toHexString(), name, policy.asString),
          (r) => r as bool);

  Future<ServerResult<bool>> deleteGroup(ObjectId groupId) =>
      checkRequest(() => _httpRest.deleteGroup(groupId.toHexString()), (r) => r as bool);

  Future<ServerResult<bool>> addGroupMembers(ObjectId groupId, List<ObjectId> membersIds) =>
      checkRequest(
          () => _httpRest.addGroupMembers(
              groupId.toHexString(), membersIds.map((id) => id.toHexString()).toList()),
          (r) => r as bool);

  Future<ServerResult<List<User>>> getGroupMembers(ObjectId groupId) =>
      checkRequest(
          () => _httpRest.getGroupMembers(groupId.toHexString()),
          (r) => List<User>.from(r.map((u) => User.fromJson(u))));

  Future<ServerResult<bool>> deleteGroupMembers(ObjectId groupId, List<ObjectId> membersIds) =>
      checkRequest(
          () => _httpRest.deleteGroupMembers(
              groupId.toHexString(), membersIds.map((id) => id.toHexString()).toList()),
          (r) => r as bool);
  // endregion

  // region contacts request
  Future<ServerResult<bool>> addUserContacts(List<ObjectId> contactsIds) =>
      checkRequest(
          () => _httpRest.addUserContacts(contactsIds.map((id) => id.toHexString()).toList()),
          (r) => r as bool);

  Future<ServerResult<List<User>>> getContactList() =>
      checkRequest(() => _httpRest.getUserContactList(),
        (r) => r.map<User>((u) => User.fromJson(u)).toList());

  Future<ServerResult<bool>> deleteUserContacts(List<ObjectId> contactsIds) =>
      checkRequest(() => _httpRest.deleteUserContacts(
          contactsIds.map((id) => id.toHexString()).toList()),
          (r) => r as bool);
  // endregion

  // region lectures requests
  Future<ServerResult<Lecture>> createLecture(
      Subject subject,
      DateTime datetime,
      LecturePolicy policy,
      LectureServerType serverType,
      {List<ObjectId> participantsIds = const <ObjectId>[],
        String description = ""}) =>
      checkRequest(
          () => _httpRest.createLecture(
              subject.toJson(), datetime.toIso8601String(), policy.asString,
              participantsIds: participantsIds.map((id) => id.toHexString()).toList(),
              description: description, serverType: serverType.asString),
              (r) => Lecture.fromJson(r));

  Future<ServerResult<Lecture>> startLecture(ObjectId lectureId) => checkRequest(
      () => _httpRest.startLecture(lectureId.toHexString()), (r) => Lecture.fromJson(r));

  Future<ServerResult<bool>> finishLecture(ObjectId lectureId) =>
      checkRequest(() => _httpRest.finishLecture(lectureId.toHexString()), (r) => r as bool);

  Future<ServerResult<Lecture>> joinLecture(ObjectId lectureId) =>
      checkRequest(() => _httpRest.joinLecture(lectureId.toHexString()), (r) => Lecture.fromJson(r));

  Future<ServerResult<Lecture>> getLecture(ObjectId lectureId) =>
      checkRequest(() => _httpRest.getLecture(lectureId.toHexString()), (r) => Lecture.fromJson(r));

  Future<ServerResult<List<Lecture>>> getLecturesList(List<String> states, DateTime fromDatetime,
          DateTime toDatetime, List<ObjectId> owners, List<String> subjects,
          {String countryCode = Constants.defaultCountryCode}) =>
      checkRequest(
          () => _httpRest.getLecturesList(states, fromDatetime.toIso8601String(),
              toDatetime.toIso8601String(), owners.map((id) => id.toHexString()).toList(), subjects,
              countryCode: countryCode),
          (r) => r.map<Lecture>((s) => Lecture.fromJson(s)).toList());

  Future<ServerResult<bool>> deleteLecture(ObjectId lectureId) =>
      checkRequest(() => _httpRest.deleteLecture(lectureId.toHexString()), (r) => r as bool);

  Future<ServerResult<List<User>>> getLectureParticipants(ObjectId lectureId) =>
      checkRequest(() => _httpRest.getLectureParticipants(lectureId.toHexString()),
          (r) => List<User>.from(r.map((u) => User.fromJson(u))));

  Future<ServerResult<MultimediaServerParams>> startMultimediaServer(ObjectId lectureId) =>
      checkRequest(() => _httpRest.startMultimediaServer(lectureId.toHexString()), (r) => MultimediaServerParams.fromJson(r));

  Future<ServerResult<MultimediaServerParams>> getMultimediaServer(ObjectId lectureId) =>
      checkRequest(() => _httpRest.getMultimediaServer(lectureId.toHexString()), (r) => MultimediaServerParams.fromJson(r));

  Future<ServerResult<bool>> stopMultimediaServer(ObjectId lectureId) =>
      checkRequest(() => _httpRest.stopMultimediaServer(lectureId.toHexString()), (r) => r as bool);

  // endregion

  Future<ServerResult<bool>> updateFcmToken(String fcmToken) =>
      checkRequest(() => _httpRest.updateFcmToken(fcmToken), (r) => r);

  // region upload/download requests
  @deprecated
  Future<ServerResult<ArchiveBundle>> getDownloadableLink(ObjectId lectureId) => checkRequest(
      () => _httpRest.getDownloadableLink(lectureId.toHexString()),
      (r) => r != null
          ? ArchiveBundle.fromJson(r)
          : null); // TODO add error failed to load info about lesson

  /// temporary debug function to upload image on server (null context)
  Future<ServerResult<bool>> uploadFiles(
      String originalFile, String resultFile, int errorCode) async {
    final originFileName = originalFile.split('/').last;
    final resultFileName = resultFile.split('/').last;

    final response = await checkRequest(
        () => _httpRest.uploadFiles(
            originalPath: originalFile,
            originalName: originFileName,
            resultPath: resultFile,
            resultName: resultFileName,
            errorCode: errorCode),
        (r) => (r as bool) ?? false);

    return response;
  }
  // endregion
}
