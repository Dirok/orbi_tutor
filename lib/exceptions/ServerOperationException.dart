enum ServerOperationError {
  lectureAlreadyActive,
  unknown
}


class ServerOperationErrorHelper {
  static final  _map = <int, ServerOperationError>{
    0 : ServerOperationError.unknown,
    418 : ServerOperationError.lectureAlreadyActive
  };

  static ServerOperationError fromInt(int value) =>
      _map.containsKey(value) ? _map[value] : ServerOperationError.unknown;
}


class ServerOperationException implements Exception {
  final ServerOperationError errorType;
  final String message;

  const ServerOperationException(this.errorType, {this.message});

  @override
  String toString() {
    final msgString = message != null && message.isNotEmpty ? "($message)" : "";
    return "server operation failed with $errorType $msgString";
  }
}

class ServerCodeException implements Exception {
  final String callingRequest;
  final String message;
  final int errorCode;

  const ServerCodeException(this.callingRequest, this.errorCode, this.message);

  @override
  String toString() =>
      '$callingRequest request failed with code $errorCode${message != null && message.isNotEmpty ? ' and msg $message' : ''}';
}