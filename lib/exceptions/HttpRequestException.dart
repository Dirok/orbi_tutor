class HttpRequestException implements Exception {
  HttpRequestException(this.message, {this.code, this.exception});

  final String message;
  final int code;
  final dynamic exception;

  @override
  toString() {
    return '$message with code: $code';
  }
}
