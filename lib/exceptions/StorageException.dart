class StorageException implements Exception {
  StorageException(this.message, {this.exception});

  final String message;
  final dynamic exception;

  @override
  toString() {
    return '$message';
  }
}
