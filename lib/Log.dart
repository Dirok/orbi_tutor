import 'package:tutor/focused.dart';

final Logger Log = Logger();

abstract class Logger {
  static Logger _instance;

  factory Logger() {
    _instance ??= getLogger();
    return _instance;
  }

  void init();

  Future<void> exportWeek(String path);

  Future<void> trace(Object object, {
      String className,
      String methodName,
      dynamic exception,
      String dataLogType,
      StackTrace stacktrace,
  });

  Future<void> debug(Object object, {
    String className,
    String methodName,
    dynamic exception,
    String dataLogType,
    StackTrace stacktrace,
  });

  Future<void> info(Object object, {
    String className,
    String methodName,
    dynamic exception,
    String dataLogType,
    StackTrace stacktrace,
  });

  Future<void> warning(Object object, {
    String className,
    String methodName,
    dynamic exception,
    String dataLogType,
    StackTrace stacktrace,
  });

  Future<void> error(Object object, {
    String className,
    String methodName,
    dynamic exception,
    String dataLogType,
    StackTrace stacktrace,
  });

  Future<void> fatal(Object object, {
    String className,
    String methodName,
    dynamic exception,
    String dataLogType,
    StackTrace stacktrace,
  });
}
