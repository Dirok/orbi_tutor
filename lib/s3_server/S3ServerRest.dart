import 'dart:typed_data';

import 'package:http/http.dart' as http;
import 'package:tutor/focused.dart';

class S3ServerRest with ServerRestMixin {
  String _url;

  set uri(String uri) => _url = uri;

  final _headers = Map<String, String>();

  void setToken(String token) {
    if (token == null) return;
    _headers['Authorization'] = 'Bearer ' + token;
  }

  Future<http.StreamedResponse> upload(
      String token, String parameters, Uint8List data, String filename) async {
    final postUri = Uri.parse('$_url/upload?$parameters');

    Log.info('------ uploadFile request ------');
    final request = http.MultipartRequest('POST', postUri);
    request.headers.addAll(_headers);

    final multipartFile = http.MultipartFile.fromBytes('data', data, filename: filename);

    request.files.add(multipartFile);

    final response = await request.send();
    parseStreamedResponse(response, 'uploadFile');
    return response;
  }

  Future<Uint8List> download(String token, String parameters) async {
    final uri = Uri.parse('$_url/download?$parameters');

    Log.info('------ downloadFile request ------');
    final response = await http.get(uri, headers: _headers);

    return parseResponse(response, 'downloadFile', parseJson: false);
  }
}
