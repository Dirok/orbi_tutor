import 'dart:async';
import 'dart:core';
import 'dart:typed_data';

import 'package:tutor/focused.dart';

class S3ServerApi with TranslatableMixin, EventMixin, ServerApiMixin {
  final S3ServerRest _s3Rest;

  S3ServerApi() : _s3Rest = S3ServerRest();

  set uri(String uri) => _s3Rest.uri = uri;

  set token(String token) => _s3Rest.setToken(token);

  Future<ServerResult<void>> upload(
          String token, Uint8List data, String parameters, String filename) =>
      checkRequest(() => _s3Rest.upload(token, parameters, data, filename), (r) => r,
          timeout: Settings.httpsFileRequestTimeout);

  Future<ServerResult<Uint8List>> download(String token, String parameters, String filename) =>
      checkRequest(() => _s3Rest.download(token, parameters), (r) => r,
          timeout: Settings.httpsFileRequestTimeout);
}
