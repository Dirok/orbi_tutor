import 'package:country_provider/country_provider.dart';
import 'package:tutor/focused.dart';

class CountryProviderService {
  List<Country> _countries;

  List<Country> get countries => _countries;

  Future<void> init() async {
    try {
      _countries = await CountryProvider.getAllCountries();
    } catch(e) {
      Log.error('Error while getting the list of countries', exception: e);
    }
  }
}