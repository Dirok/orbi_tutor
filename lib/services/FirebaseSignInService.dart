import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:crypto/crypto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:tutor/focused.dart';

class FirebaseSignInService extends GetxService with EventMixin {
  StreamSubscription _tokenChangesSubscription;

  Future<String> signInAnonymously() async {
    Log.info('Try signIn as Firebase Anonymous');
    final userCredential = await FirebaseAuth.instance.signInAnonymously();
    final jwt = await userCredential?.user?.getIdToken(true);
    if (jwt == null)
      Log.error('Failed Firebase Anonymous authorization');
    await _monitorFirebaseTokenChanges();
    return jwt;
  }

  Future<String> signInWithGoogle() async {
    Log.info('Try signIn by Google');

    final googleUser = await GoogleSignIn().signIn();
    if (googleUser == null) {
      Log.error('Failed Google auth');
      return null;
    }

    final googleAuth = await googleUser.authentication;

    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    final jwt = await _firebaseAuth(credential);
    return jwt;
  }

  Future<FirebaseAuthResult<String>> registerWithEmail(String email, String password) async {
    Log.info('Try create user as Firebase Email');
    String jwt;
    try {
      final userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: email,
          password: password
      );
      jwt = await userCredential?.user?.getIdToken(true);
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case 'weak-password':
          return FirebaseAuthResult.weakPassword();
        case 'email-already-in-use':
          return FirebaseAuthResult.accountAlreadyExist();
        case 'operation-not-allowed':
          return FirebaseAuthResult.operationNotAllowed();
        default:
          return FirebaseAuthResult.fail();
      }
    } catch (e) {
      Log.error(e);
    }

    if (jwt == null) {
      Log.error('Failed Firebase Email authorization');
      return FirebaseAuthResult.fail();
    }

    await _monitorFirebaseTokenChanges();
    return FirebaseAuthResult.success(jwt);
  }

  Future<void> _monitorFirebaseTokenChanges() async {
    await _tokenChangesSubscription?.cancel();
    _tokenChangesSubscription = FirebaseAuth.instance
        .idTokenChanges()
        .listen((user) async {
      if (user == null) {
        Log.info('Firebase user signed out');
        sendEvent(SignedOutEvent());
      } else {
        Log.info('Firebase token updated');
        final jwt = await user.getIdToken();
        sendEvent(FirebaseJwtUpdatedEvent(jwt));
      }
    });
  }

  Future<String> signInWithFacebook() async {
    Log.info('Try signIn by Facebook');

    final loginResult = await FacebookAuth.instance.login();
    if (loginResult == null) {
      Log.error('Failed Facebook auth with loginResult is null');
      return null;
    }

    if (loginResult.status == LoginStatus.cancelled) return '';

    if (loginResult.status != LoginStatus.success) {
      Log.error('Failed Facebook auth with ${loginResult.status}');
      Log.error(loginResult.message);
      return null;
    }

    final credential = FacebookAuthProvider.credential(
        loginResult.accessToken?.token);

    final jwt = await _firebaseAuth(credential);
    return jwt;
  }

  Future<String> signInWithApple() async {
    Log.info('Try signIn by AppleID');

    final rawNonce = _generateNonce();
    final nonce = _sha256ofString(rawNonce);

    final appleCredential = await SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ],
      nonce: nonce,
    );

    final credential = OAuthProvider('apple.com').credential(
      idToken: appleCredential?.identityToken,
      rawNonce: rawNonce,
    );

    final jwt = await _firebaseAuth(credential);
    return jwt;
  }

  Future<String> _firebaseAuth(OAuthCredential credential) async {
    final userCredential = await FirebaseAuth.instance.signInWithCredential(credential);
    final jwt = await userCredential?.user?.getIdToken(true);
    if (jwt == null)
      Log.error('Failed Firebase authorization');
    await _monitorFirebaseTokenChanges();
    return jwt;
  }

  String _generateNonce([int length = 32]) {
    final charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._';
    final random = Random.secure();
    return List.generate(length, (_) => charset[random.nextInt(charset.length)])
        .join();
  }

  /// Returns the sha256 hash of [input] in hex notation.
  String _sha256ofString(String input) {
    final bytes = utf8.encode(input);
    final digest = sha256.convert(bytes);
    return digest.toString();
  }

  @override
  void onClose() {
    _tokenChangesSubscription?.cancel();
    super.onClose();
  }
}

class SignedOutEvent extends Event {}
class FirebaseJwtUpdatedEvent extends Event {
  FirebaseJwtUpdatedEvent(this.jwt);
  final String jwt;
}