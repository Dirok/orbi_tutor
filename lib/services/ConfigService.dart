import 'dart:async';

import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class ConfigService extends NetworkDependentService with EventMixin {
  /// To get value you need to know its type
  /// Get value as bool type: config['key'].asBool()
  Map<String, RemoteConfigValue> config;

  final _remoteConfigSettings = RemoteConfigSettings(
    fetchTimeout: Duration(seconds: 10),
    // How often to fetch new values from the server
    minimumFetchInterval: Duration(minutes: 0),
  );

  @override
  Future<bool> initialize() async {
    if (!await super.initialize()) return false;

    // TODO: [WEB] Implement analogue for config
    if (GetPlatform.isWeb) return false;

    Log.info('Init config');
    final remoteConfig = RemoteConfig.instance;
    await remoteConfig.setConfigSettings(_remoteConfigSettings);

    try {
      await remoteConfig.fetchAndActivate();
      config = remoteConfig.getAll();
      Log.info('Config received');
    } catch (_) {
      Log.info('Cannot fetch remote config');
      await Future.delayed(Duration(seconds: Constants.configFetchTimeout));
      initialize();
      return false;
    }

    try {
      sendEvent(ServersFetchedEvent());

      if (!PlatformUtils.isAppVersionLatest(config['version'].asString()))
        sendEvent(UpdateNeededEvent());

      Log.info('All events sent');
    } catch (e) {
      Log.error('Error when parsing remote config', exception: e);
    }

    return true;
  }
}
