import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/widgets.dart';
import 'package:tutor/focused.dart';

abstract class CameraUIProvider {
  Widget createPreviewWidget();
  VisualizerController get visualizerController;
}

abstract class Camera {
  static Camera _instance;

  factory Camera() {
    _instance ??= getCamera();
    return _instance;
  }

  Size get imageSize;

  bool get isInitialized;

  bool get isDetectsHands;

  double get aspectRatio;

  Future<bool> init();

  Future<void> startHandDetection();

  Future<void> stopHandDetection();

  Future<void> startPaperDetection();

  Future<void> stopPaperDetection();

  Future<CameraResult> takePicture(
      String targetImagePath, bool resetPaperCache);

  Uint8List flipImageDataVertically(Uint8List pixels, int width, int height);

  Future<void> dispose();
}

enum CameraResult {
  Success,
  Cancelled,
  CapturingError,
  RecognitionError,
  RecognitionCrash
}

enum CameraPreviewResult {
  Cancel,
  Retake,
  Accept,
}
