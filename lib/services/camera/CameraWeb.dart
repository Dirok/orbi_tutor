import 'dart:typed_data';
import 'dart:ui';

import 'package:tutor/focused.dart';

Camera getCamera() => CameraWeb();

class CameraWeb implements Camera {
  Size get imageSize => throw UnimplementedError('Camera not available on web');

  bool get isInitialized =>
      throw UnimplementedError('Camera not available on web');

  bool get isDetectsHands =>
      throw UnimplementedError('Camera not available on web');

  double get aspectRatio =>
      throw UnimplementedError('Camera not available on web');

  Future<bool> init() =>
      throw UnimplementedError('Camera not available on web');

  Future<void> startHandDetection() =>
      throw UnimplementedError('Camera not available on web');

  Future<void> stopHandDetection() =>
      throw UnimplementedError('Camera not available on web');

  Future<void> startPaperDetection() =>
      throw UnimplementedError('Camera not available on web');

  Future<void> stopPaperDetection() =>
      throw UnimplementedError('Camera not available on web');

  // Use image package for compressing images after taking them
  Future<CameraResult> takePicture(
          String targetImagePath, bool resetPaperCache) =>
      throw UnimplementedError('Camera not available on web');

  Uint8List flipImageDataVertically(Uint8List pixels, int width, int height) =>
      throw UnimplementedError('Camera not available on web');

  Future<void> dispose() =>
      throw UnimplementedError('Camera not available on web');
}
