import 'dart:async';
import 'dart:ffi';
import 'package:universal_io/io.dart';
import 'dart:typed_data';
import 'dart:ui';

import 'package:camera/camera.dart';
import 'package:ffi/ffi.dart';
import 'package:flutter/material.dart' show Widget;
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:paper_scanner_plugin/paper_scanner_plugin.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:tutor/focused.dart';
import 'package:vector_math/vector_math.dart';


Camera getCamera() => CameraMobile._();

class CameraMobile implements Camera, CameraUIProvider {
  final _mainServer = Get.find<MainServerApi>();

  static const String _handNetModelDataPath = 'net_hand/yolov4_tiny_hand';

  static var _resolutionPresets = {
    CameraResolution.high: ResolutionPreset.high,
    CameraResolution.veryHigh: ResolutionPreset.veryHigh,
    CameraResolution.ultraHigh: ResolutionPreset.ultraHigh,
  };

  static var _imageFormats = {
    CameraResolution.high: Size(1280, 720),
    CameraResolution.veryHigh: Size(1920, 1080),
    CameraResolution.ultraHigh: Size(3840, 2160)
  };

  static double _minSideLength = 720.0;

  final _imageService = Get.find<ImageService>();

  int _handFrameCounter = 0;
  int _lastHandDetectedFrameNumber = 0;
  bool _isDetectsHand = false;
  int _lastHandDetectionResult = 0;

  // int _paperFrameCounter = 0;
  // int _lastPaperDetectedFrameNumber = 0;
  bool _isDetectsPaper = false;
  CameraController _cameraController;
  final _visualizerController = VisualizerController();

  CameraMobile._();

  CameraController get controller => _cameraController;

  Size get imageSize => _imageFormats[Settings.cameraResolution];

  bool get isInitialized => _cameraController?.value?.isInitialized ?? false;

  bool get isDetectsHands => _cameraController?.value?.isStreamingImages ?? false;

  double get aspectRatio => _cameraController?.value?.aspectRatio ?? 0;


  Future<bool> init() async {
    if (isInitialized && _cameraController.resolutionPreset ==
        _resolutionPresets[Settings.cameraResolution]) return true;

    var cameras = await availableCameras();
    var backCamera = cameras.firstWhere((c) => c.lensDirection == CameraLensDirection.back);
//    var resolution =  ResolutionPreset.low; // - for aspect ratio 4:3
    var resolution = _resolutionPresets[Settings.cameraResolution];
    _cameraController = CameraController(backCamera, resolution, enableAudio: false);

    // try {
    //   paperStreamSetDetectionMode(0);
    // } catch (e) {
    //   Log.error('Paper stream set detection mode error:', exception: e);
    // }

    try {
      await _cameraController.initialize();
      await _cameraController.lockCaptureOrientation(DeviceOrientation.landscapeRight);
      return true;
    } catch (e) {
      Log.error('Camera initialization failed', exception: e);
      return false;
    }

  }

  Future<void> startHandDetection() async {
    if (isDetectsHands) return;

    var isHandNetModelLoaded = await _loadHandNetModelData();
    if (isHandNetModelLoaded)
      await _cameraController.startImageStream(_tryDetectHand);
  }

  Future<void> _tryDetectHand(CameraImage image) async {
    var frameNumber = _handFrameCounter++;

    if (_isDetectsHand) return;
    _isDetectsHand = true;

    var sw = Stopwatch()..start();

    //var result = await compute(_detectHand, image);
    // var result = await compute(_detectHandOld, image);
    var result = _detectHands(image);
    if (result >= 0)
      _lastHandDetectionResult = result;

    var duration = sw.elapsedMilliseconds;
    var droppedFrames = frameNumber - _lastHandDetectedFrameNumber;

    if (result >= 0)
      Log.info('Hand detection on frame $frameNumber: $_lastHandDetectionResult, '
          '$duration ms, $droppedFrames frames dropped');

    _lastHandDetectedFrameNumber = frameNumber;
    _isDetectsHand = false;
  }

  static int _detectHands(CameraImage image) {
    // var sw = Stopwatch()..start();

    // print('Copy YUV data to unmanaged: ${sw.elapsedMilliseconds} ms');

    int numberOfHands = -1;
    try {
      if (asyncHaveOutputHandDetectionResults() > 0) {
        numberOfHands = asyncWaitOutputHandDetectionResults();
      }
    } catch (e) {
      print('Get hand detection result error' + e.toString());
    }

    Pointer<Uint8> p0, p1, p2;
    try {
      //var result = detectHandsYuvImmediate(p0, p1, p2, image.width, image.height,
      //    image.planes[1].bytesPerRow, image.planes[2].bytesPerPixel);
      //return result > 0;
      if (asyncHaveInputFrameForHandDetection() == 0) {

        final bytes0 = image.planes[0].bytes;
        final bytes1 = image.planes[1].bytes;
        final bytes2 = image.planes[2].bytes;

        p0 = calloc<Uint8>(bytes0.length);
        p1 = calloc<Uint8>(bytes1.length);
        p2 = calloc<Uint8>(bytes2.length);

        p0.asTypedList(bytes0.length).setRange(0, bytes0.length, bytes0);
        p1.asTypedList(bytes1.length).setRange(0, bytes1.length, bytes1);
        p2.asTypedList(bytes2.length).setRange(0, bytes2.length, bytes2);

        asyncStartHandDetectionYuv(
            p0,
            p1,
            p2,
            image.width,
            image.height,
            image.planes[1].bytesPerRow,
            image.planes[2].bytesPerPixel);
      }
    } catch (e) {
      print('Hand detection error' + e.toString());
    } finally {
      if (p0 != null)
        calloc.free(p0);
      if (p1 != null)
        calloc.free(p1);
      if (p2 != null)
        calloc.free(p2);
    }
    return numberOfHands;
  }

  // static bool _detectHandOld(CameraImage image) {
  //   Pointer<Uint8> imageBufferPtr;
  //   try {
  //     var sw = Stopwatch()..start();
  //     imageBufferPtr = _convertYUV420toRGBA(image);
  //     print('Convert YUV420 to RGBA: ${sw.elapsedMilliseconds} ms');
  //     var result = detectHandsImmediate(imageBufferPtr, image.width, image.height);
  //     return result > 0;
  //   } catch (e) {
  //     print('Hand detection error' + e.toString());
  //     return false;
  //   } finally {
  //     if (imageBufferPtr != null)
  //       free(imageBufferPtr);
  //   }
  // }

  Future<void> stopHandDetection() async {
    if (isDetectsHands)
      await _cameraController.stopImageStream();
  }

  Future<void> startPaperDetection() async {
    if (_isDetectsPaper) return;
    try {
      asyncStartPaperDetectionProcessing();
    } catch (e) {
      Log.error('Paper stream set detection mode error:', exception: e);
    }
    await _cameraController.startImageStream(_tryDetectPaper);
  }

  Future<void> _tryDetectPaper(CameraImage image) async {
    // final frameNumber = _paperFrameCounter++;

    if (_isDetectsPaper) return;
    _isDetectsPaper = true;

    // final sw = Stopwatch()..start();

    //var result = await compute(_detectPaper, image);
    // var result = await compute(_detectHandOld, image);
    //_visualizerController.setTargetPoints(result, image.width, image.height);

    final outCorners = calloc<Double>(8 * 4);
    try {
      if (asyncHaveOutputPaperDetectionCorners() > 0) {
        final r = asyncWaitOutputPaperDetectionCorners(outCorners);
        final points = r > 0
          ? List.generate(4, (i) => Vector2(outCorners[i * 2], outCorners[i * 2 + 1]), growable: false)
          : null;

        _visualizerController.setTargetPoints(points, image.width, image.height);
      }
    } catch (e) {
      print('Error on get paper detection result: ' + e.toString());
      return null;
    } finally {
      calloc.free(outCorners);
    }
    Pointer<Uint8> imageData;
    try {
      if (asyncHaveInputFrameForPaperDetection() == 0) {
        var bytes0 = image.planes[0].bytes;
        imageData = calloc<Uint8>(bytes0.length);

        imageData.asTypedList(bytes0.length).setRange(0, bytes0.length, bytes0);
        var successFlag = asyncStartPaperDetectionCorners(imageData, image.width, image.height);
        if (successFlag != 1) {
          print('Problem with sending image to paper detection.');
        }
      }
    } catch (e) {
      print('Error on sending image to paper detection: ' + e.toString());
      return null;
    } finally {
      if (imageData != null)
        calloc.free(imageData);
    }
    // var duration = sw.elapsedMilliseconds;
    // var droppedFrames = frameNumber - _lastPaperDetectedFrameNumber;
    //Log.info('Hand detection on frame $frameNumber: $result, $duration ms, $droppedFrames frames dropped');

    // _lastPaperDetectedFrameNumber = frameNumber;
    _isDetectsPaper = false;
  }

  // static List<Vector2> _detectPaper(CameraImage image) {
  //   // var sw = Stopwatch()..start();
  //
  //   var bytes0 = image.planes[0].bytes;
  //   //var bytes1 = image.planes[1].bytes;
  //   //var bytes2 = image.planes[2].bytes;
  //
  //   Pointer<Uint8> p0 = allocate(count: bytes0.length);
  //   //Pointer<Uint8> p1 = allocate(count: bytes1.length);
  //   //Pointer<Uint8> p2 = allocate(count: bytes2.length);
  //
  //   p0.asTypedList(bytes0.length).setRange(0, bytes0.length, bytes0);
  //   //p1.asTypedList(bytes1.length).setRange(0, bytes1.length, bytes1);
  //   //p2.asTypedList(bytes2.length).setRange(0, bytes2.length, bytes2);
  //
  //   // print('Copy YUV data to unmanaged: ${sw.elapsedMilliseconds} ms');
  //
  //   List<Vector2> points;
  //   Pointer<Double> out_corners = allocate(count: 8 * 4);
  //   try {
  //     var result = paperDetectGrayImmediate(out_corners,
  //         p0, image.width, image.height);
  //     if (result > 0) {
  //       points = new List<Vector2>(4);
  //       for (int i = 0; i < 4; ++i) {
  //         points[i] = new Vector2(out_corners[i * 2], out_corners[i * 2 + 1]);
  //       }
  //     }
  //   } catch (e) {
  //     print('Hand detection error' + e.toString());
  //     return null;
  //   } finally {
  //     free(p0);
  //     //free(p1);
  //     //free(p2);
  //     free(out_corners);
  //   }
  //   return points;
  // }

  Future<void> stopPaperDetection() async {
    // (_isDetectsPaper)
    await _cameraController.stopImageStream();
  }

  Future<CameraResult> takePicture(
      String targetImagePath, bool resetPaperCache) async {
    if (Settings.isPaperRecognitionEnabled && resetPaperCache) _resetPaper();

    final capturedImagePath = Settings.isPaperRecognitionEnabled
        ? join(Settings.tmpFolderPath, '${DateTime.now()}_captured.png') : targetImagePath;

    try {
      final xfile = await _cameraController.takePicture();
      await xfile.saveTo(capturedImagePath);
    } catch (e) {
      Log.error(e);
    }

    if (!await File(capturedImagePath).exists())
      return CameraResult.CapturingError;

    if (Settings.isNeedToSaveImages)
      await _saveImageToGallery(capturedImagePath);

    if (!Settings.isPaperRecognitionEnabled) {
      if (Settings.isCameraVerticalMirroringEnabled) {
        Log.error('Flip image');
        await _flipImage(targetImagePath, targetImagePath);
      } else {
        await _imageService.compressFromFileAndSave(targetImagePath);
      }

      return CameraResult.Success;
    }

    var result = await _recognizePaper(capturedImagePath, targetImagePath);

    var errorCode = -1;

    if (!await File(targetImagePath).exists()) {
      Log.error('Recognition result: $result, resulting file not found');

      if (Settings.isCameraVerticalMirroringEnabled) {
        Log.error('Emergency image flip and copy');
        await _flipImage(capturedImagePath, targetImagePath);
      } else {
        Log.error('Emergency image copy');
        await File(capturedImagePath).copy(targetImagePath);
      }

      return CameraResult.RecognitionCrash;
    } else {
      if (!result) Log.error('Recognition result: $result, but file is found');

      errorCode = _getLastRecognizedError();
      Log.info('Recognition errorCode: ' + errorCode.toString());
    }

    if (Settings.isNeedToSaveImages)
      await _saveImageToGallery(targetImagePath);

    /// send images to server if recognition is failed
    if ((errorCode != 0) && (errorCode != -1)) {
      final uploadResult =
          await _mainServer.uploadFiles(capturedImagePath, targetImagePath, errorCode);
      Log.info('uploadResult: ${uploadResult.payload}');
    }

    await File(capturedImagePath).delete();

    await _imageService.compressFromFileAndSave(targetImagePath);
    return result ? CameraResult.Success : CameraResult.RecognitionError;
  }

  Future<void> _saveImageToGallery(String imagePath) async {
    try {
      String galleryPath = await ImageGallerySaver.saveFile(imagePath);
      if (galleryPath != null && galleryPath != '')
        Log.info('Image saved to $galleryPath');
      else
        Log.error('Failed to save image to gallery');
    } catch (e) {
      Log.error(e);
    }
  }

  bool _setRecognizedPaperSize(Size size) {
    double h, w, pw, ph;

    if (size.width > size.height) {
      h = _minSideLength;
      var k = size.height/h;
      w = size.width/k;
      pw = Settings.paperWidth.toDouble();
      ph = Settings.paperHeight.toDouble();
    } else {
      w = _minSideLength;
      var k = size.width/w;
      h = size.height/k;
      pw = Settings.paperHeight.toDouble();
      ph = Settings.paperWidth.toDouble();
    }

    try {
      paperStreamSetPaperSize(pw, ph, w.toInt(), h.toInt());
      return true;
    } catch (e) {
      Log.error(e);
      return false;
    }
  }

  bool _setPostTransform(double angle, bool horizontalFlip, bool verticalFlip) {
    try {
      paperStreamSetPostTransform(angle, horizontalFlip ? 1 : 0, verticalFlip ? 1 : 0);
      return true;
    } catch (e) {
      Log.error(e);
      return false;
    }
  }

  int _getLastRecognizedError() {
    var errorCode = -1;
    try {
      errorCode = paperLastProcessResultCode();
    } catch (e) {
      Log.info(e);
    }
    return errorCode;
  }

  _resetPaper() {
    try {
      paperStreamReset();
    } catch (e) {
      Log.info(e);
    }
  }

  Future<bool> _recognizePaper(String sourceImagePath, String targetImagePath) async {
    var sw = Stopwatch();
    sw.start();

    var res = _setRecognizedPaperSize(imageSize);
    if (!res) return false;

    res = _setPostTransform(0, false, Settings.isCameraVerticalMirroringEnabled);
    if (!res) return false;

    bool result = false;

    final Pointer<Utf8> sourceImagePathPtr = sourceImagePath.toNativeUtf8();
    final Pointer<Utf8> targetImagePathPtr = targetImagePath.toNativeUtf8();

    try {
      var intResult = paperProcess(sourceImagePathPtr, targetImagePathPtr);
      result = intResult != 0;
    } catch (e) {
      Log.info(e);
    }

    if (_getLastRecognizedError() == 5) {
      Log.info("Hands are detected!");
    }

    Log.info('${sw.elapsedMilliseconds} ms - !Paper recognition process');

    calloc.free(sourceImagePathPtr);
    calloc.free(targetImagePathPtr);

    return result;
  }

  Future<void> _flipImage(String sourceImagePath, String targetImagePath) async {
    var imageBytes = await File(sourceImagePath).readAsBytes();
    var completer = Completer<Image>();
    decodeImageFromList(imageBytes, (Image image) => completer.complete(image));
    var image = await completer.future;
    var imageData = await image.toByteData(format: ImageByteFormat.rawRgba);
    var pixels = imageData.buffer.asUint8List();

    pixels = flipImageDataVertically(pixels, image.width, image.height);

    completer = Completer<Image>();
    decodeImageFromPixels(pixels, image.width, image.height,
        PixelFormat.rgba8888, (Image img) => completer.complete(img));
    image = await completer.future;
    imageData = await image.toByteData(format: ImageByteFormat.png);
    imageBytes = imageData.buffer.asUint8List();
    try {
      await _imageService.compressFromBytesAndSave(imageBytes, targetImagePath);
    } catch (e) {
      Log.error('Compressing image failed', exception: e);
    }
  }

  Uint8List flipImageDataVertically(Uint8List pixels, int width, int height) {
    const int components = 4;

    var newPixels = Uint8List(pixels.lengthInBytes);

    for (int j = 0; j < height; ++j) {
      for (int i = 0; i < width; ++i) {
        var fpi = (i + j * width) * components;
        var pi = (i + (height - 1 - j) * width) * components;
        for (int k = 0; k < components; ++k)
          newPixels[fpi + k] = pixels[pi + k];
      }
    }

    return newPixels;
  }

  Future<bool> _loadHandNetModelData() async {
    final dataFolder = await getExternalStorageDirectory();
    final path = join(dataFolder.path, _handNetModelDataPath);
    final pathPtr = path.toNativeUtf8();
    try {
      //return handNetModelLoad(pathPtr, 0) > 0;
      asyncStartHandDetectionProcessing(pathPtr, 1);
      asyncWaitHandModelLoading();
      final ls = asyncHandModelLoadState();
      return ls == 1;
    } catch (e) {
      Log.error('Loading hand net model data failed', exception: e);
      try {
        asyncStopHandDetectionProcessing();
      } catch (e) {
        Log.error('Stop async hand detection processing error: ', exception: e);
      }
      return false;
    } finally {
      calloc.free(pathPtr);
    }
  }

  // static Pointer<Uint8> _convertYUV420toRGBA(CameraImage image) {
  //   var width = image.width;
  //   var height = image.height;
  //
  //   var plane0 = image.planes[0];
  //   var plane1 = image.planes[1];
  //   var plane2 = image.planes[2];
  //
  //   var uvRowStride = plane1.bytesPerRow;
  //   var uvPixelStride = plane1.bytesPerPixel;
  //
  //   int tmpUvIndex, uvIndex, index, yp, up, vp, outIndex;
  //
  //   var imgPtr = allocate<Uint8>(count: image.width * image.height * 4);
  //
  //   // Fill image buffer with plane[0] from YUV420_888
  //   for (var x = 0; x < width; x++) {
  //     tmpUvIndex = uvPixelStride * (x / 2).floor();
  //     for (var y = 0; y < height; y++) {
  //       index = y * width + x;
  //       uvIndex = tmpUvIndex + uvRowStride * (y / 2).floor();
  //
  //       yp = plane0.bytes[index];
  //       up = plane1.bytes[uvIndex];
  //       vp = plane2.bytes[uvIndex];
  //
  //       outIndex = index * 4;
  //       imgPtr[outIndex] = (yp + up * 1814 / 1024 - 227).round().clamp(0, 255); // B
  //       imgPtr[outIndex + 1] = (yp - up * 46549 / 131072 + 44 - vp * 93604 / 131072 + 91).round().clamp(0, 255); // G
  //       imgPtr[outIndex + 2] = (yp + vp * 1436 / 1024 - 179).round().clamp(0, 255); // R
  //       imgPtr[outIndex + 3] = 255; // A
  //     }
  //   }
  //
  //   return imgPtr;
  // }

  Future<void> dispose() async {
    if (!isInitialized) return;
    // await stopHandDetection();
    await _cameraController?.dispose();
    _cameraController = null;
  }


  @override
  VisualizerController get visualizerController => _visualizerController;

  @override
  Widget createPreviewWidget() => CameraPreview(controller);
}
