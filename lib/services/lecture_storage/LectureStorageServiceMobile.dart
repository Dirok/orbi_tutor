import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'package:universal_io/io.dart';

import 'package:bson/bson.dart';
import 'package:path/path.dart' show join;
import 'package:tutor/focused.dart';

class LectureStorageServiceMobile extends LectureStorageService {
  static const String _whiteboardFileName = 'WhiteboardSet.dat';

  LectureStorageServiceMobile() {
    Log.info("LectureStorageServiceMobile ctor");
  }

  @override
  void initLectureData(Lecture lecture) {
    final dir = Directory(lecture.imagesFolderPath);
    if (!dir.existsSync())
      dir.create(recursive: true);
  }

  @override
  Future<void> saveToStorage(Lecture lecture) async {
    final file = File(lecture.dataFilePath);
    if (!await file.exists()) await file.create(recursive: true);
    final str = JsonEncoder().convert(lecture);
    await file.writeAsString(str, flush: true);
  }

  @override
  Future<void> saveWithWhiteboard(Lecture lecture, WhiteboardSet whiteboardSet) async {
    await saveToStorage(lecture);

    final folderPath = join(lecture.whiteboardsFolderPath, whiteboardSet.ownerId.toHexString());
    final filePath = join(folderPath, _whiteboardFileName);

    final file = File(filePath);
    if (!await file.exists())
      await file.create(recursive: true);

    final str = JsonEncoder().convert(whiteboardSet);
    await file.writeAsString(str, flush: true);

    for(final wb in whiteboardSet.whiteboards)
      await wb.saveDrawing(folderPath);
  }

  @override
  Future<WhiteboardSet> loadWhiteboardSet(Lecture lecture, ObjectId userId) async {
    if (!isWhiteboardSetExists(lecture, userId)) {
      return null;
    }

    final folderPath = join(lecture.whiteboardsFolderPath, userId.toHexString());
    final filePath = join(folderPath, _whiteboardFileName);
    final imagesFolderPath = lecture.imagesFolderPath;

    final file = File(filePath);
    if (!await file.exists()) return null;

    final str = await file.readAsString();
    final json = JsonDecoder().convert(str);
    final whiteboardSet = WhiteboardSet.fromJson(json);

    for (var wb in whiteboardSet.whiteboards) {
      await wb.loadDrawing(folderPath);
      wb.setImageFolderPath(imagesFolderPath);
    }

    return whiteboardSet;
  }

  @override
  Future<void> removeLectureData(Lecture lecture) async {
    final dir = Directory(lecture.folderPath);
    if (await dir.exists())
      await dir.delete(recursive: true);

    final downloadFile = File(lecture.archivePath);
    if (await downloadFile.exists())
      await downloadFile.delete();

    lecture.resetDownloadState();
  }

  @override
  bool isWhiteboardSetExists(Lecture lecture, ObjectId userId) {
    return Directory(join(lecture.whiteboardsFolderPath, userId.toHexString())).existsSync();
  }
}
