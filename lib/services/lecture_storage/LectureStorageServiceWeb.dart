import 'dart:async';
import 'dart:convert';
import 'dart:core';

import 'package:bson/bson.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tutor/focused.dart';

class LectureStorageServiceWeb extends LectureStorageService {
  SharedPreferences _preferences;
  
  static const String baseKey = "l";

  String _subKey(String parentKey, ObjectId id) => "$parentKey/${id.toHexString()}";

  String _lectureKey(Lecture lecture) => "$baseKey/${lecture.id.toHexString()}";

  String _whiteboardSetKey(Lecture lecture, ObjectId id) => _subKey(_lectureKey(lecture), id);

  LectureStorageServiceWeb() {
    Log.info("LectureDataServiceWeb ctor");
  }

  Future<void> init() async {
    _preferences = await SharedPreferences.getInstance();
  }

  @override
  void initLectureData(Lecture lecture) {
    //TODO init images storage
  }

  @override
  Future<void> saveToStorage(Lecture lecture) async {
    // Log.info("can not save lecture to storage on web");
    final key = _lectureKey(lecture);
    final value = JsonEncoder().convert(lecture);
    await _preferences.setString(key, value);
  }

  @override
  Future<void> saveWithWhiteboard(Lecture lecture, WhiteboardSet whiteboardSet) async {
    // Log.info("can not save lecture+whiteboard to storage on web");
    await saveToStorage(lecture);

    final key = _whiteboardSetKey(lecture, whiteboardSet.ownerId);
    final value = JsonEncoder().convert(whiteboardSet);
    await _preferences.setString(key, value);

    for (final wb in whiteboardSet.whiteboards) {
      final k = _subKey(key, wb.id);
      final v = JsonEncoder().convert(wb.drawingSet);
      await _preferences.setString(k, v);
    }
  }

  @override
  Future<WhiteboardSet> loadWhiteboardSet(Lecture lecture, ObjectId userId) async {
    final key = _whiteboardSetKey(lecture, userId);
    final value = _preferences.getString(key);

    if (value == null) {
      Log.warning("no whiteboardSet in prefs for key $key");
      return null;
    }

    final json = JsonDecoder().convert(value);
    final whiteboardSet = WhiteboardSet.fromJson(json);
    final imagesFolderPath = lecture.imagesFolderPath;

    for (final wb in whiteboardSet.whiteboards) {
      wb.setImageFolderPath(imagesFolderPath);

      final k = _subKey(key, wb.id);
      final v = _preferences.getString(k);

      if (v == null) {
        Log.warning("no whiteboard in prefs for key $k");
        continue;
      }

      try {
        final json = JsonDecoder().convert(v);
        wb.drawingSet = DrawingSet.fromJson(json);
      } catch (e) {
        Log.error(e);
      }
    }

    return whiteboardSet;
  }

  @override
  Future<void> removeLectureData(Lecture lecture) async {

    final prefix = "${_lectureKey(lecture)}";
    _preferences
        .getKeys()
        .where((e) => e.startsWith(prefix))
        .toSet()
        .forEach((e) => _preferences.remove(e));
    
    // var downloadFile = File(lecture.archivePath);
    // if (await downloadFile.exists())
    //   await downloadFile.delete();

    lecture.resetDownloadState();
  }

  @override
  bool isWhiteboardSetExists(Lecture lecture, ObjectId userId) {
    final key = _whiteboardSetKey(lecture, userId);
    return _preferences.containsKey(key);
  }
}
