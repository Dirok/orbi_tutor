import 'dart:async';
import 'dart:core';

import 'package:bson/bson.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

abstract class LectureStorageService extends GetxService {
  void initLectureData(Lecture lecture);
  Future<void> saveToStorage(Lecture lecture);
  Future<void> saveWithWhiteboard(Lecture lecture, WhiteboardSet whiteboardSet);
  Future<WhiteboardSet> loadWhiteboardSet(Lecture lecture, ObjectId userId);
  Future<void> removeLectureData(Lecture lecture);
  bool isWhiteboardSetExists(Lecture lecture, ObjectId userId);
}