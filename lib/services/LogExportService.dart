import 'package:intl/intl.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

class LogExportService {
  static final logNameFormat = DateFormat('d_M__HH-mm-ss');

  static Future<List<File>> exportLogs(String prefix) async {
    List<File> logArchives = [];
    try {
      var tmpFolder = await getTemporaryDirectory();
      var tmpFolderPath = tmpFolder.path;
      var logsTempPath = path.join(tmpFolderPath, 'logs');

      var filename = '${prefix}__${logNameFormat.format(DateTime.now())}';

      var rawFolder = path.join(logsTempPath, 'raw');
      var sourcePath = path.join(rawFolder, filename + Constants.logsExt);
      var targetPath = path.join(logsTempPath, filename + Constants.archiveExt);

      final fileService = FileService();
      await fileService.removeFiles(logsTempPath);

      await Log.exportWeek(sourcePath);

      await fileService.createZip(rawFolder, targetPath);

      logArchives.add(File(targetPath));
    }
    catch(e) {
      rethrow;
    }
    return logArchives;
  }
}