import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:stack_trace/stack_trace.dart';
import 'package:tutor/focused.dart';

enum LogLevel { ALL, TRACE, DEBUG, INFO, WARNING, ERROR, SEVERE, FATAL, OFF }

Logger getLogger() => LoggerWeb._();

class LoggerWeb implements Logger {

  LoggerWeb._();

  void init() {}

  Future<void> exportWeek(String path) async
  {
    print("exportWeek is not implemented for web");
  }

  clearLogs() async
  {
    print("clearLogs is not implemented for web");
  }


  trace(
      Object object, {
        String className,
        String methodName,
        dynamic exception,
        String dataLogType,
        StackTrace stacktrace,
      }) async {
    _logThis(className, methodName, object, LogLevel.TRACE, exception,
        dataLogType, stacktrace);
  }

  debug(
      Object object, {
        String className,
        String methodName,
        dynamic exception,
        String dataLogType,
        StackTrace stacktrace,
      }) async {
    _logThis(className, methodName, object, LogLevel.DEBUG, exception,
        dataLogType, stacktrace);
  }

  info(
      Object object, {
        String className,
        String methodName,
        dynamic exception,
        String dataLogType,
        StackTrace stacktrace,
      }) async {
    _logThis(className, methodName, object, LogLevel.INFO, exception,
        dataLogType, stacktrace);
  }

  warning(
      Object object, {
        String className,
        String methodName,
        dynamic exception,
        String dataLogType,
        StackTrace stacktrace,
      }) async {
    _logThis(className, methodName, object, LogLevel.WARNING, exception,
        dataLogType, stacktrace);
  }

  error(
      Object object, {
        String className,
        String methodName,
        dynamic exception,
        String dataLogType,
        StackTrace stacktrace,
      }) async {
    _logThis(className, methodName, object, LogLevel.ERROR, exception,
        dataLogType, stacktrace);
  }

  fatal(
      Object object, {
        String className,
        String methodName,
        dynamic exception,
        String dataLogType,
        StackTrace stacktrace,
      }) async {
    _logThis(className, methodName, object, LogLevel.FATAL, exception,
        dataLogType, stacktrace);
  }

  void _logThis(
      String className,
      String methodName,
      Object object,
      LogLevel type,
      dynamic exception,
      String dataLogType,
      StackTrace stacktrace) {
    assert(object != null);
    assert(type != null);
    final text = object.toString();

    if (methodName == null) {
      methodName = Trace.current().frames[2].member ?? "<fn>";
    }
    final timestamp = DateTime.now().toString();

    if (type == LogLevel.FATAL || type == LogLevel.ERROR)
      Sentry.captureException(
        exception,
        stackTrace: stacktrace,
      );
    print("$timestamp|$methodName|$text");
  }
}
