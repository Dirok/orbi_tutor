import 'dart:io';

import 'package:f_logs/f_logs.dart';
import 'package:get/get.dart';
import 'package:stack_trace/stack_trace.dart';
import 'package:tutor/focused.dart';

Logger getLogger() => LoggerMobile._();

class LoggerMobile implements Logger {

  LoggerMobile._();

  final _consoleConfig = LogsConfig()
    ..isDebuggable = true
    ..isWeb = GetPlatform.isWeb
    ..customClosingDivider = " "
    ..customOpeningDivider = ""
    ..isLogsEnabled = true
    ..encryptionEnabled = false
    ..formatType = FormatType.FORMAT_CUSTOM
    ..fieldOrderFormatCustom = [
      FieldName.METHOD_NAME,
      FieldName.TEXT,
      FieldName.EXCEPTION,
      FieldName.STACKTRACE
    ] // Field order for output
    ..timestampFormat = TimestampFormat.TIME_FORMAT_24_FULL;

  final _exportConfig = LogsConfig()
    ..isDebuggable = false
    ..customClosingDivider = ""
    ..customOpeningDivider = "| "
    ..isLogsEnabled = true
    ..encryptionEnabled = false
    ..formatType = FormatType.FORMAT_CUSTOM
    ..fieldOrderFormatCustom = [
      FieldName.LOG_LEVEL,
      FieldName.TIMESTAMP,
      FieldName.METHOD_NAME,
      FieldName.TEXT,
      FieldName.EXCEPTION,
      FieldName.STACKTRACE
    ] // Field order for output
    ..timestampFormat = TimestampFormat.TIME_FORMAT_24_FULL;

  void init()
  {
    FLog.applyConfigurations(_consoleConfig);
  }

  Future<void> exportWeek(String path) async
  {
    try {
      var now = DateTime.now();
      info("EXPORT WEEK LOG AT ${now.subtract(Duration(days: 7))} - $now");
      await Future.delayed(Duration(milliseconds: 500));
      FLog.applyConfigurations(_exportConfig);
      final logs = await FLog.getAllLogsByFilter(filterType: FilterType.WEEK);

      var buffer = StringBuffer();
      logs.forEach((log) {
        buffer.write(Formatter.format(log, _exportConfig));
      });

      var file = File(path);
      if (!await file.exists()) await file.create(recursive: true);

      await file.writeAsString(buffer.toString());
      buffer.clear();
    }
    catch (e) {
      rethrow;
    }
    finally {
      FLog.applyConfigurations(_consoleConfig);
    }
  }

  clearLogs() async
  {
    FLog.clearLogs();
  }


  trace(
      Object object, {
        String className,
        String methodName,
        dynamic exception,
        String dataLogType,
        StackTrace stacktrace,
      }) async {
    _logThis(className, methodName, object, LogLevel.TRACE, exception,
        dataLogType, stacktrace);
  }

  debug(
      Object object, {
        String className,
        String methodName,
        dynamic exception,
        String dataLogType,
        StackTrace stacktrace,
      }) async {
    _logThis(className, methodName, object, LogLevel.DEBUG, exception,
        dataLogType, stacktrace);
  }

  info(
      Object object, {
        String className,
        String methodName,
        dynamic exception,
        String dataLogType,
        StackTrace stacktrace,
      }) async {
    _logThis(className, methodName, object, LogLevel.INFO, exception,
        dataLogType, stacktrace);
  }

  warning(
      Object object, {
        String className,
        String methodName,
        dynamic exception,
        String dataLogType,
        StackTrace stacktrace,
      }) async {
    _logThis(className, methodName, object, LogLevel.WARNING, exception,
        dataLogType, stacktrace);
  }

  error(
      Object object, {
        String className,
        String methodName,
        dynamic exception,
        String dataLogType,
        StackTrace stacktrace,
      }) async {
    _logThis(className, methodName, object, LogLevel.ERROR, exception,
        dataLogType, stacktrace);
  }

  fatal(
      Object object, {
        String className,
        String methodName,
        dynamic exception,
        String dataLogType,
        StackTrace stacktrace,
      }) async {
    _logThis(className, methodName, object, LogLevel.FATAL, exception,
        dataLogType, stacktrace);
  }

  void _logThis(
      String className,
      String methodName,
      Object object,
      LogLevel type,
      dynamic exception,
      String dataLogType,
      StackTrace stacktrace) {
    assert(object != null);
    assert(type != null);
    final text = object.toString();

    if (methodName == null) {
      methodName = Trace.current().frames[2].member ?? "<fn>";
    }

    //check to see if className is not provided
    //then its already been taken from calling class
    // if (className == null) {
    //   className = Trace.current().frames[2].member.split(".")[0];
    // }
    //
    // //check to see if methodName is not provided
    // //then its already been taken from calling class
    // if (methodName == null) {
    //   methodName = Trace.current().frames[2].member.split(".")[1];
    // }

    FLog.logThis(
        text: text,
        type: type,
        methodName: methodName,
        className: className ?? "",
        exception: exception,
        dataLogType: dataLogType,
        stacktrace: stacktrace);
  }
}
