import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class AuthorizationService extends GetxService with EventMixin {
  final _userDataService = Get.find<UserDataService>();
  final _mainServer = Get.find<MainServerApi>();
  final _countryProviderService = Get.find<CountryProviderService>();

  Future<AuthorizationResult> authorizeWithEmail(String email, String password) async {
    final result = await _mainServer.authByLogin(email, password, Settings.deviceId);
    return _handleMainServerResult(result);
  }

  Future<AuthorizationResult> authorizeWithJwt(String jwt, {bool isAnonymous}) async {
    final result = await _mainServer.jwtAuth(jwt, Settings.deviceId);
    final authResult = await _handleMainServerResult(result, isAnonymous: isAnonymous);

    if (authResult == AuthorizationResult.success)
      appSession?.setFirebaseJwt(jwt);

    return authResult;
  }

  Future<AuthorizationResult> authorizeWithLink(String link) async {
    final result = await _mainServer.authByLink(link, Settings.deviceId);
    return _handleMainServerResult(result);
  }

  Future<AuthorizationResult> authorizeWithKundelik(String token) async {
    final result = await _mainServer.authByKundelik(token, Settings.deviceId);
    final authBundle = result?.payload;
    authBundle?.partner = Partners.kundelik;
    return _handleMainServerResult(result);
  }

  Future<AuthorizationResult> _handleMainServerResult(ServerResult<AuthorizationBundle> result,
      {bool isAnonymous = false}) async {
    if (result.isSuccess) {
      final isSuccess = await _createSession(result.payload, isAnonymous: isAnonymous);
      return isSuccess ? AuthorizationResult.success : AuthorizationResult.fail;
    } else if (result.isFail) {
      return AuthorizationResult.fail;
    } else {
      return AuthorizationResult.error;
    }
  }

  Future<bool> _createSession(AuthorizationBundle authBundle, {bool isAnonymous}) async {
    if (authBundle == null ||
        authBundle.tokenBundle == null ||
        authBundle.tokenBundle.httpsToken == null ||
        authBundle.tokenBundle.refreshToken == null ||
        authBundle.account == null) {
      Log.error('Invalid authorization bundle');
      return false;
    }

    appSession = AppSession.fromAuthorizationBundle(authBundle);

    _mainServer.token = appSession.httpToken;

    if(appSession.account.countryCode == 0) {
      await _countryProviderService.init();
    }

    // TODO check license AppSession?.user?.licenseType
    // TODO load info about linked tutors

    await appSession.save();

    appSession.isAuthorized = true;
    appSession.isAnonymous = isAnonymous;

    Log.info('${appSession.account.permissions?.currPermissions}');

    return true;
  }

  Future<bool> tryAutoLogin() async {
    if (appSession == null || appSession.httpToken == null || appSession.account == null) {
      Log.info('No authorized session for check');
      return false;
    }

    // TODO check license AppSession?.user?.licenseType
    // TODO load info about linked tutors

    appSession.isAuthorized = true;

    return true;
  }
}

enum AuthorizationResult { success, fail, error }
