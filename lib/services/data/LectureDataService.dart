import 'package:bson/bson.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

class LectureDataService extends GetxService {
  static const Duration connectionTimeout = Duration(seconds: 10);

  final _mainServer = Get.find<MainServerApi>();
  final _storageService = Get.find<LectureStorageService>();
  final _s3Server = Get.find<S3ServerApi>();

  final isLectureUpdating = false.obs;
  final allLecturesMap = <ObjectId, Lecture>{}.obs;
  final downloadsMap = <ObjectId, Download<Lecture>>{}.obs;

  final finishedStates = [
    LectureState.checking.asString,
    LectureState.passed.asString,
    LectureState.completed.asString
  ];

  final scheduledStates = [
    LectureState.active.asString,
    LectureState.checking.asString,
    LectureState.planned.asString
  ];

  final _countryCode = Settings.country;

  var _passedLecturesFilter = LectureListFilter();
  var _scheduledLecturesFilter = LectureListFilter();

  LectureListFilter get passedLecturesFilter => _passedLecturesFilter;
  LectureListFilter get scheduledLecturesFilter => _scheduledLecturesFilter;

  Lecture currentLecture;

  bool get isActiveLecturesLimitNotExceeded => allLecturesMap.values
      .where((element) => element.state == LectureState.active)
      .length < Settings.allowedNumOfActiveLectures;

  LectureDataService() {
    Log.info("LectureDataService ctor");
  }

  init() {
    _initLecturesFilters();
  }

  deInit() {
    allLecturesMap?.clear();
    downloadsMap?.clear();
  }

  // region Lecture
  Future<bool> deleteLecture(ObjectId lectureId, {bool removeFromServer = false}) async {
    final lecture = allLecturesMap[lectureId];

    if (removeFromServer) {
      final deletionResult = await _mainServer.deleteLecture(lectureId);
      if (!deletionResult.isSuccess) {
        Log.warning('Failed to delete lecture');
        return false;
      }
      lecture.removeDataLocation(Location.server);
    }
    // todo lecture deletion through realm
    // await _storageService.removeLectureData(lecture);
    // lecture.removeDataLocation(Location.local);

    await updateLectureList();
    allLecturesMap.refresh();

    return true;
  }
  // endregion

  // region Lectures list
  Future<bool> updateLectureList() => _updateLectureList.callWithLock(isLectureUpdating);

  Future<bool> _updateLectureList() async {
    try {
      await _updateFinishedLecturesList();
      await _updateScheduledLecturesList();
    } catch (e) {
      return false;
    }
    return true;
  }

  Future<void> _updateFinishedLecturesList() async {
    Log.info('Try update finished lectures list');
    try {
      // await scanForDownloadedLectures();
      await fetchFinishedLecturesList();
    } catch (e) {
      Log.error("_updateFinishedLecturesList failed with $e");
      rethrow;
    }
  }

  Future<void> _updateScheduledLecturesList() async {
    Log.info('Try update scheduled lectures list');
    try {
      await fetchScheduledLecturesList();
    } catch (e) {
      Log.error("_updateScheduledLecturesList failed with $e");
      rethrow;
    }
  }

  Future<void> fetchFinishedLecturesList() async {
    final currentLectures = Map.of(allLecturesMap);
    currentLectures.forEach((key, value) => value.removeDataLocation(Location.server));
    currentLectures.removeWhere((key, value) =>
      value.dataLocation() == Location.nowhere || value.isPlanned || value.isActive);

    final ownersIds = <ObjectId>[];
    final subjectsIds = <String>[];
    final groupsIds = <ObjectId>[];
    _passedLecturesFilter.ownersFilterList.where((f) => f.value).forEach((f) => ownersIds.add(f.item.id));
    _passedLecturesFilter.subjectsFilterList.where((f) => f.value).forEach((f) => subjectsIds.add(f.item.id));
    _passedLecturesFilter.groupsFilterList.where((f) => f.value).forEach((f) => groupsIds.add(f.item.id));

    final lecturesResult = await _mainServer.getLecturesList(
        finishedStates,
        _passedLecturesFilter.fromDateTimeFilter.value.toUtc(),
        _passedLecturesFilter.toDateTimeFilter.value.add(Duration(hours: 24)).toUtc(),
        ownersIds,
        subjectsIds,
        countryCode: _countryCode);

    if (!lecturesResult.isSuccess) {
      Log.info('finishedLecture list fetching failed');
      allLecturesMap.assignAll(currentLectures);
      return;
    }
    final lectures = lecturesResult.payload;

    if (lectures.isEmpty) {
      Log.info('finishedLessons list is empty');
      allLecturesMap.assignAll(currentLectures);
      return;
    }
    Log.info('fetched ${lectures.length} finished lessons');

    for (final lecture in lectures) {
      // заменяем модель на скаченную с сервера, так как локально у нас состояние пока не меняется,
      // а на сервере состояние могло измениться
      final exists = currentLectures.containsKey(lecture.id);
      lecture.dataLocation.value = exists ? Location.everywhere : Location.server;
      currentLectures[lecture.id] = lecture;
      Log.debug('updated lecture with id ${lecture.id}: $lecture ');
    }
    allLecturesMap.assignAll(currentLectures);
    Log.info('current allLectures amount ${allLecturesMap.length}');
  }

  Future<void> fetchScheduledLecturesList() async {
    final currentLectures = Map.of(allLecturesMap);
    currentLectures.removeWhere((key, value) => value.isPlanned || value.isActive);

    final ownersIds = <ObjectId>[];
    final subjectsIds = <String>[];
    final groupsIds = <ObjectId>[];
    _scheduledLecturesFilter.ownersFilterList.where((f) => f.value).forEach((f) => ownersIds.add(f.item.id));
    _scheduledLecturesFilter.subjectsFilterList.where((f) => f.value).forEach((f) => subjectsIds.add(f.item.id));
    _scheduledLecturesFilter.groupsFilterList.where((f) => f.value).forEach((f) => groupsIds.add(f.item.id));

    final lecturesResult = await _mainServer.getLecturesList(
        scheduledStates,
        _scheduledLecturesFilter.fromDateTimeFilter.value.toUtc(),
        _scheduledLecturesFilter.toDateTimeFilter.value.add(Duration(hours: 24)).toUtc(),
        ownersIds,
        subjectsIds,
        countryCode: _countryCode);

    if (!lecturesResult.isSuccess) {
      Log.info('scheduledLecture list fetching failed');
      allLecturesMap.assignAll(currentLectures);
      return;
    }
    final lectures = lecturesResult.payload;

    if (lectures.isEmpty) {
      Log.info('scheduledLecture list is empty');
      allLecturesMap.assignAll(currentLectures);
      return;
    }
    Log.info('fetched ${lectures.length} scheduled lessons');

    for (final lecture in lectures) {
      lecture.dataLocation.value = Location.nowhere;
      currentLectures[lecture.id] = lecture;

      if (allLecturesMap.containsKey(lecture.id)
          && allLecturesMap[lecture.id].key != null
          && allLecturesMap[lecture.id].key != '') {
        currentLectures[lecture.id].key = allLecturesMap[lecture.id].key;
      }
    }
    allLecturesMap.assignAll(currentLectures);
    Log.info('current allLectures amount ${allLecturesMap.length}');
  }
  // endregion

  // region Lectures filters
  void _initLecturesFilters() {
    _scheduledLecturesFilter = LectureListFilter();
    _passedLecturesFilter = LectureListFilter();
  }

  void updateLecturesFilters() {
    _scheduledLecturesFilter.updateSubjectsFilterList();
    _passedLecturesFilter.updateSubjectsFilterList();
  }

  Future<Lecture> getLecture(ObjectId id, {bool needRefresh = false}) async {
    if (!needRefresh && allLecturesMap.containsKey(id))
      return allLecturesMap[id];

    final result = await _mainServer.getLecture(id);
    if (!result.isSuccess) return null;

    return allLecturesMap[id] = result.payload;
  }

  Future<Lecture> joinLecture(ObjectId id) async {
    final result = await _mainServer.joinLecture(id);
    if (!result.isSuccess) {
      Log.warning('Failed join to lecture: $id');
      return null;
    }

    return allLecturesMap[id] = result.payload;
  }

  Future<Lecture> tryStartLecture(ObjectId lectureId) async {
    final result = await _mainServer.startLecture(lectureId);
    if (!result.isSuccess) return null;

    final lecture = result.payload;
    if (lecture == null) return null;

    // TODO remove when lecture.server will be removed
    if (lecture.serverType == LectureServerType.lectureServer
        && lecture.server.isEmpty) return null;

    _updateLectureState(lecture);

    return lecture;
  }

  Future<bool> trySetCheckingMode(Lecture lecture) {
    // TODO implement when server implements
    return Future.value(false);
  }

  Future<bool> tryStopLecture(Lecture lecture) async {
    final result = await _mainServer.finishLecture(lecture.id);
    if (!result.isSuccess) return false;

    final isFinished = result.payload;
    if (isFinished) {
      _updateLectureState(lecture);
      updateLectureList();
    }
    return isFinished;
  }

  void _updateLectureState(Lecture lecture) {
    allLecturesMap.update(lecture.id, (Lecture val) => lecture, ifAbsent: () => null);
    allLecturesMap.refresh();
  }
  // endregion

  // region Participants list
  Future<void> fetchParticipantsToLecture(Lecture lecture) async {
    final studentsResult = await _mainServer.getLectureParticipants(lecture.id);
    if (!studentsResult.isSuccess) {
      Log.warning('Participants list failed to load');
      return;
    }

    final participants = studentsResult.payload;

    if (participants.isEmpty)
      Log.warning('Participants list is empty');

    lecture.participants.addAll(participants);
  }
  // endregion


  // region Lessons downloader
  @deprecated
  Future<Download> getOrCreateLessonDownload(ObjectId id) async {
    var download = downloadsMap[id];
    if (download != null) return download;

    final archiveResult = await _mainServer.getDownloadableLink(id);
    if (!archiveResult.isSuccess || archiveResult.payload == null)
      throw LectureDownloadException(id, LectureDownloadErrorType.noArchive);

    final downloadDirectory = Directory(Settings.downloadsFolderPath);
    if (!downloadDirectory.existsSync())
      Directory(Settings.downloadsFolderPath).createSync();

    final archive = archiveResult.payload;
    final isEnough = await FileService().isDiskSpaceEnough(archive.size);
    if (!isEnough)
      throw LectureDownloadException(id, LectureDownloadErrorType.notEnoughSpace);

    final isGranted = await PermissionsHelper().checkStoragePermission(Get.context);
    if (!isGranted)
      throw LectureDownloadException(id, LectureDownloadErrorType.storagePermissionDenied);

    final lesson = allLecturesMap[id];
    if (lesson == null)
      throw LectureDownloadException(id, LectureDownloadErrorType.noSuchLecture);

    final downloadPath = lesson.archivePath;
    final extractPath = lesson.folderPath;

    Log.info("Create download for lessonID: $id");

    download = Download(
        lesson, archive.url, downloadPath, extractPath, connectionTimeout);

    // FIXME: need to dispose worker?
    once(download.stateX, _downloadFinishedHandler(download, id),
        condition: _downloadFinishedCondition(download));
    once(download.stateX, _downloadSucceededHandler(download, id),
        condition: _downloadSucceededCondition(download));

    downloadsMap[id] = download;

    return download;
  }

  @deprecated
  _downloadFinishedHandler(Download<Lecture> download, ObjectId id) {
    return (_) {
      final state = download.state;
      Log.info("download $id finished with $state");
      downloadsMap.remove(id);
    };
  }

  @deprecated
  _downloadFinishedCondition(Download<Lecture> download) {
    return () => download.state.isFinished;
  }

  @deprecated
  _downloadSucceededHandler(Download<Lecture> download, ObjectId id) {
    return (_) async {
      Log.info("download $id succeeded");
      await _storageService.saveToStorage(allLecturesMap[id]);
      allLecturesMap[id].addDataLocation(Location.local);

      allLecturesMap.refresh();
    };
  }

  @deprecated
  bool Function() _downloadSucceededCondition(Download<Lecture> download) {
    return () => download.state.isDownloaded;
  }
  // endregion
}

enum LectureDownloadErrorType {
  notEnoughSpace,
  storagePermissionDenied,
  noSuchLecture,
  noArchive,
}

@deprecated
class LectureDownloadException implements Exception {
  final ObjectId id;
  final LectureDownloadErrorType errorType;
  final String message;

  const LectureDownloadException(this.id, this.errorType, {this.message});

  @override
  String toString() {
    final msgString = message?.isNotEmpty ?? false ? '($message)' : '';
    return 'Lecture $id download failed with $errorType $msgString';
  }
}