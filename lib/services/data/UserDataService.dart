import 'package:bson/bson.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class UserDataService extends GetxService with EventMixin {
  final _mainServer = Get.find<MainServerApi>();
  final _lectureDataService = Get.find<LectureDataService>();

  final _customSubjectsMap = <String, Subject>{}.obs;

  final isUserSubjectsUpdating = false.obs;
  final userSubjectsMap = <String, Subject>{}.obs;

  final isPredefinedSubjectsUpdating = false.obs;
  final predefinedSubjectsMap = <String, Subject>{}.obs;

  final isUserGroupsUpdating = false.obs;
  final userGroupsMap = <ObjectId, Group>{}.obs;

  final isUserContactsUpdating = false.obs;
  final userContactsMap = <ObjectId, User>{}.obs;

  final _countryCode = Settings.country;

  final legacyClassesMap = <ObjectId, Class>{}.obs;
  final legacyDisciplinesMap = <ObjectId, Discipline>{}.obs;

  UserDataService() {
    Log.info("UserDataService ctor");
  }

  init() {
    // todo after full migrate to lectures del all and save only _tryUpdatePredefinedSubjects()
    final account = appSession?.account;
    if (account == null) {
      Log.info('data service will work for known account');
      return;
    }

    if (account.permissions.hasPersonalLicense)
      _tryUpdatePredefinedSubjects();
  }

  deInit() {
    userSubjectsMap?.clear();
    predefinedSubjectsMap?.clear();
    userGroupsMap?.clear();
    userContactsMap?.clear();
  }
  
  // region Subjects cruds
  Future<void> tryUpdateUserSubjects() => _updateUserSubjects.callWithLock(isUserSubjectsUpdating);
  Future<void> _tryUpdatePredefinedSubjects() =>
      _updatePredefinedSubjects.callWithLock(isPredefinedSubjectsUpdating);

  Future<void> _updateUserSubjects() async {
    Log.info('Try update user subjects list');
    try {
      final subjectsResult = await _mainServer.getUserSubjects(_countryCode);
      if (!subjectsResult.isSuccess) {
        Log.info('userSubjects list fetching failed [last saved values would used]');
        return;
      }
      final subjects = subjectsResult.payload;
      Log.info('fetched ${subjects.length} user subjects');

      appSession.account.userSubjects = subjects;
      _lectureDataService.updateLecturesFilters();

      userSubjectsMap.assignAll({for (var subject in subjects) subject.id: subject});
      Log.info('current userSubjects amount ${userSubjectsMap.length}');

      await _lectureDataService.updateLectureList();
    } catch (e) {
      Log.error("_updateUserSubjects failed with $e");
      sendEvent(FetchingSubjectsErrorEvent());
    }
  }

  Future<void> _updatePredefinedSubjects() async {
    Log.info('Try update predefined subjects list');
    try {
      final subjectsResult = await _mainServer.getSubjects(_countryCode);
      if (!subjectsResult.isSuccess) {
        Log.info('predefinedSubjects list fetching failed [last saved values would used]');
        return;
      }
      final subjects = subjectsResult.payload;
      Log.info('fetched ${subjects.length} predefined subjects');

      predefinedSubjectsMap.assignAll({for (var subject in subjects) subject.id: subject});
      Log.info('current predefinedSubjects amount ${predefinedSubjectsMap.length}');

      appSession.account.predefinedSubjects = subjects;
    } catch (e) {
      Log.error("_loadPredefinedSubjectList failed with $e");
      sendEvent(FetchingSubjectsErrorEvent());
    }
  }

  Future<Subject> tryCreateSubject(String subjectName) async {
    final createResult = await _mainServer.createUserSubjects(subjectName);
    if (!createResult.isSuccess) return null;

    await tryUpdateUserSubjects();
    userSubjectsMap.refresh();

    Log.info('Subject: $subjectName was created successfully');
    return createResult.payload;
  }

  Future<bool> tryDeleteSubject(Subject subject) async {
    final deletionResult = await _mainServer.deleteUserSubjects(subject.name);
    if (!deletionResult.isSuccess) return false;

    if (appSession.account.preferences.recentSearchedSubjectList.contains(subject)) {
      appSession.account.preferences.recentSearchedSubjectList.remove(subject);
      appSession.setAccountPref();
    }

    await tryUpdateUserSubjects();
    userSubjectsMap.refresh();

    Log.info('Subject: ${subject.name} was deleted successfully');
    return true;
  }

  Subject getSubjectById(String id) {
    if (predefinedSubjectsMap.containsKey(id))
      return predefinedSubjectsMap[id];
    else if (userSubjectsMap.containsKey(id))
      return userSubjectsMap[id];
    else if (_customSubjectsMap.containsKey(id))
      return _customSubjectsMap[id];
    else
      return _customSubjectsMap[id] = Subject.custom(id);
  }

  // endregion

  // region Groups cruds
  Future<void> tryUpdateAllUserContacts() async {
    await tryUpdateUserGroups();
    await tryUpdateUserContacts();
  }

  Future<void> tryUpdateUserGroups() => _updateUserGroups.callWithLock(isUserGroupsUpdating);

  Future<void> _updateUserGroups() async {
    Log.info('Try update user groups list');
    try {
      final groupsResult = await _mainServer.getGroupList();
      if (!groupsResult.isSuccess) {
        Log.info('userGroups list fetching failed [last saved values would used]');
        return;
      }
      final groups = groupsResult.payload;
      Log.info('fetched ${groups.length} user groups');

      groups.forEach((g) async => await fetchMembersListToGroup(g));

      userGroupsMap.assignAll({for (var group in groups) group.id: group});
      Log.info('current userGroups amount ${userGroupsMap.length}');

    } catch (e) {
      Log.error("_updateUserGroups failed with $e");
      sendEvent(FetchingUserContactsErrorEvent());
    }
  }

  Future<void> fetchMembersListToGroup(Group group) async {
    final membersResult = await _mainServer.getGroupMembers(group.id);
    if (!membersResult.isSuccess) {
      Log.warning('Members list failed to load');
      return;
    }

    final members = membersResult.payload;

    if (members.isEmpty)
      Log.warning('Members list is empty');

    group.assignMembers(members);
  }
  // endregion

  // region Contacts
  Future<void> tryUpdateUserContacts() => _updateUserContacts.callWithLock(isUserContactsUpdating);

  Future<void> _updateUserContacts() async {
    Log.info('Try update user contacts list');
    try {
      final contactsResult = await _mainServer.getContactList();
      if (!contactsResult.isSuccess) {
        Log.info('userContacts list fetching failed [last saved values would used]');
        return;
      }
      final contacts = contactsResult.payload;
      Log.info('fetched ${contacts.length} user contacts');

      userContactsMap.assignAll({for (var user in contacts) user.id: user});
      Log.info('current userContacts amount ${userContactsMap.length}');
    } catch (e) {
      Log.error("_updateUserContacts failed with $e");
      sendEvent(FetchingUserContactsErrorEvent());
    }
  }
  // endregion
}

class FetchingSubjectsErrorEvent extends Event {}
class FetchingUserContactsErrorEvent extends Event {}