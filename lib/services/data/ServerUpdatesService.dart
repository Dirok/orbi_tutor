import 'dart:async';

import 'package:get/get.dart';
import 'package:rxdart/transformers.dart';
import 'package:tutor/focused.dart';

enum CheckingHashStatus {
  actual,
  needUpdate,
  error,
}

class ServerUpdatesService extends GetxService {
  static const Duration fetchInterval = Duration(seconds: 5);

  final _mainServer = Get.find<MainServerApi>();
  final _userDataService = Get.find<UserDataService>();
  final _lectureDataService = Get.find<LectureDataService>();

  var _isInitialized = false;

  var _isCheckingUpdates = false;
  var _subscriptions = <StreamSubscription>[];
  StreamSubscription _fetchSubscription;

  init() {
    if (_isInitialized) return;
    _isInitialized = true;

    initAllDataServices();

    final connectivity = Get.find<ConnectivityService>();
    connectivity.isConnectedToInternet
        .listen(onConnectedChanged)
        .addTo(_subscriptions);
    onConnectedChanged(connectivity.isConnectedToInternet.value);

    final account = appSession?.account;
    if (account == null) {
      Log.info('data service will work for known account');
      return;
    }
    
    account.hashes.initUserHashes();
  }

  deInit() {
    if(!_isInitialized) return;

    _subscriptions.disposeSubscriptions();
    _unsubscribeMainServerUpdates();

    deInitAllDataServices();

    _isInitialized = false;
  }

  reInit() {
    deInit();
    init();
  }

  void initAllDataServices() {
    _userDataService.init();
    _lectureDataService.init();
  }

  void deInitAllDataServices() {
    _userDataService.deInit();
    _lectureDataService.deInit();
  }

  void onConnectedChanged(bool isConnected) {
    if (isConnected) {
      _subscribeMainServerUpdates();
    } else {
      _unsubscribeMainServerUpdates();
    }
  }

  void _subscribeMainServerUpdates() {
    Log.info("start MainServer update timer");
    _fetchSubscription = Stream.periodic(fetchInterval)
        .startWith(0)   // to call immediately before the first period
        .listen((_) => checkUserHashesStatus());
  }

  void _unsubscribeMainServerUpdates() {
    Log.info("stop MainServer update timer");
    _fetchSubscription?.cancel();
  }

  // check MainServer has updates for current user
  Future<bool> checkUserHashesStatus() async {
    if (_isCheckingUpdates) return false;

    if (appSession?.account == null) {
      Log.info('hashes cant be updated for unknown user');
      return false;
    }

    _isCheckingUpdates = true;
    final checkResult = await _checkUserHashes();
    _isCheckingUpdates = false;

    if (checkResult == CheckingHashStatus.error)
      Log.warning('hash checking error');

    return true;
  }

  Future<CheckingHashStatus> _checkUserHashes() async {
    final result = await _mainServer.getUserHashes();

    final currHashes = result?.payload;
    if (currHashes == null) return CheckingHashStatus.error;

    if (currHashes == appSession.account.hashes) return CheckingHashStatus.actual;

    Log.info('Some hash was changed');
    Log.info('curr: ${appSession.account.hashes.toJson().toString()}');
    Log.info('new:  ${currHashes.toJson().toString()}');

    if (currHashes.accountHash != appSession.account.hashes.accountHash) {
      appSession.account.hashes.accountHash = currHashes.accountHash;
      // await _userDataService.updateUserAccount();  // todo add account update
    }
    if (currHashes.groupsHash != appSession.account.hashes.groupsHash) {
      appSession.account.hashes.groupsHash = currHashes.groupsHash;
      await _userDataService.tryUpdateAllUserContacts();
    }
    if (currHashes.subjectsHash != appSession.account.hashes.subjectsHash) {
      appSession.account.hashes.subjectsHash = currHashes.subjectsHash;
      await _userDataService.tryUpdateUserSubjects();
    }
    if (currHashes.lecturesHash != appSession.account.hashes.lecturesHash) {
      appSession.account.hashes.lecturesHash = currHashes.lecturesHash;
      await _lectureDataService.updateLectureList();
    }
    return CheckingHashStatus.needUpdate;
  }
}