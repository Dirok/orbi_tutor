import 'dart:async';
import 'dart:convert';

import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:fcm_config/fcm_config.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart' hide Translations;
import 'package:tutor/focused.dart';

class PushService extends GetxService with EventMixin, TranslatableMixin {
  final _mainServer = Get.find<MainServerApi>();

  static const platform = const MethodChannel('com.orbi.tutor/pushTrigger');

  final _cloudMessaging = FirebaseMessaging.instance;
  final _fcmConfig = FCMConfig.instance;
  final _localNotifications = FlutterLocalNotificationsPlugin();
  final _notifications = AwesomeNotifications();
  final _initializationSettingsAndroid = AndroidInitializationSettings(
    '@drawable/ic_notification',
  );
  final _initializationSettingsIOS = IOSInitializationSettings();

  String fcmToken;

  StreamSubscription _pushStream;
  Stream<ReceivedAction> _actionsStream;

  @override
  void onInit() {
    super.onInit();
    final initializationSettings = InitializationSettings(
        android: _initializationSettingsAndroid, iOS: _initializationSettingsIOS);
    _localNotifications.initialize(initializationSettings, onSelectNotification: (payload) async {
      if (payload == null) return;

      try {
        final payloadMap = json.decode(payload);

        if (!payloadMap.containsKey('data')) return;

        final notification = NotificationModel.fromJson(payloadMap['data']);

        NotificationHelper.handleNotificationAction(notification, OpenDestination.Background);
      } catch (e) {
        Log.error('Error while parsing payload json', exception: e);
      }
    });
    _initializePermissions();
  }

  Future<String> refreshToken() async {
    try {
      await _cloudMessaging.deleteToken();
      Log.info('FCM token updated');
    } catch(e) {
      Log.error('Failed to delete token. FCM token $fcmToken', exception: e);
      return fcmToken;
    }
    try {
      fcmToken = await _cloudMessaging.getToken();
      Log.info('FCM token $fcmToken');
      return fcmToken;
    } catch (e) {
      Log.error('Failed to get FCM token. FCM token $fcmToken', exception: e);
      return fcmToken;
    }
  }

  @override
  void onClose() => unsubscribeFromEvents();

  Future<void> _subscribeToEvents() async {
    if (_pushStream != null) return;

    _pushStream = _actionsStream.listen((receivedNotification) {
      final event = NotificationEventHelper.fromString(receivedNotification.buttonKeyPressed);

      switch (event) {
        case NotificationEvent.mute:
          sendEvent(MuteMicEvent());
          break;
        case NotificationEvent.unmute:
          sendEvent(UnmuteMicEvent());
          break;
        default:
          break;
      }
    });

    Log.info('subscribe to push actions');
  }

  Future<void> unsubscribeFromEvents() async {
    await _pushStream?.cancel();
    Log.info('unsubscribe from lifecycle');
  }

  Future<void> _initializePermissions() async {
    final settings = await _cloudMessaging.requestPermission(
      badge: false,
      sound: true,
    );

    switch (settings.authorizationStatus) {
      case AuthorizationStatus.authorized:
      case AuthorizationStatus.provisional:
        if (GetPlatform.isAndroid)
          await _initActionNotifications();
        await _initializeCallbacks();
        break;
      case AuthorizationStatus.denied:
      case AuthorizationStatus.notDetermined:
        Log.warning('Notifications access denied');
        break;
    }
  }

  Future<void> _initActionNotifications() async {
    await _notifications.initialize(
      'resource://drawable/ic_notification',
      [
        NotificationChannel(
          channelName: 'General',
          locked: true,
          playSound: false,
          enableVibration: false,
          channelKey: 'general',
          defaultColor: UIConstants.purpleIconColor,
          importance: NotificationImportance.Max,
        )
      ],
    );
    _actionsStream = _notifications.actionStream.asBroadcastStream();
  }

  Future<void> _initializeCallbacks() async {
    Log.info('Notifications access granted');
    try {
      final fcmToken = await _cloudMessaging.getToken();
      Log.info('FCM token $fcmToken');
      this.fcmToken = fcmToken;
      appSession?.fcmToken = fcmToken;
    } catch (_) {
      Log.info('Cannot fetch FCM token without World Wide Web © connection');
    }

    if (appSession?.account?.id != null && appSession?.fcmToken != null) {
      final result = await _mainServer.updateFcmToken(appSession.fcmToken);
      if (result.isSuccess)
        await appSession.save();
    }

    RemoteMessage initialMessage;
    try {
      initialMessage = await _fcmConfig.getInitialMessage();
    } catch (e) {
      Log.error('Initial message failed', exception: e);
    }

    FirebaseMessaging.onBackgroundMessage(cloudMessagingHandler);

    if (initialMessage != null && initialMessage.data != null) {
      final notification = NotificationModel.fromJson(initialMessage.data);

      NotificationHelper.handleNotificationAction(notification, OpenDestination.Killed);
    }

    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      if (message != null && message.data != null) {
        final notification = NotificationModel.fromJson(message.data);

        NotificationHelper.handleNotificationAction(notification, OpenDestination.Background);
      }
    });

    FirebaseMessaging.onMessage.listen((event) {
      if (event.data == null) return;

      final notification = NotificationModel.fromJson(event.data);
      sendEvent(ShowSnackEvent(notification));
    });
  }

  Future<void> showActiveLectureNotification(
      {@required bool hasAudio, @required bool allowSpeak}) async {
    if (GetPlatform.isWeb) return;

    _subscribeToEvents();
    try {
      await platform.invokeMethod('startKiller');
    } on PlatformException catch (e) {
      Log.error('Failed to trigger push killer service', exception: e);
    }
    await showPersistentNotification(
      Settings.persistentNotificationId,
      translate(Translations.call_lecture),
      translate(Translations.call_lecture_description),
      translate(Translations.call_notification),
      actionButtons: [
        NotificationActionButton(
          buttonType: ActionButtonType.KeepOnTop,
          label: translate(hasAudio ? Translations.mute : Translations.unmute),
          key: hasAudio ? NotificationEvent.mute.asString : NotificationEvent.unmute.asString,
          enabled: true,
          autoCancel: false,
        ),
      ],
      needShowButtons: allowSpeak,
    );
  }

  Future<void> hideActiveLectureNotification() async {
    if (GetPlatform.isWeb) return;

    try {
      await platform.invokeMethod('stopKiller');
    } on PlatformException catch (e) {
      Log.error('Failed to stop push killer service', exception: e);
    }
    await cancelNotification(Settings.persistentNotificationId);
  }

  Future<void> showPersistentNotification(int id, String title, String body, String channelName,
          {List<NotificationActionButton> actionButtons,
          Map<String, String> payload,
          bool needShowButtons = true}) =>
      _notifications.createNotification(
        content: NotificationContent(
          id: id,
          channelKey: 'general',
          title: title,
          body: body,
          autoCancel: false,
          showWhen: false,
          payload: payload,
        ),
        actionButtons: needShowButtons ? actionButtons : null,
      );

  Future<void> showNotification(int id, String title, String body, String channelName,
      [String payload]) async {
    final androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'general',
      channelName,
      '',
      importance: Importance.max,
      priority: Priority.max,
    );
    final iosPlatformChannelSpecifics = IOSNotificationDetails();
    final platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics, iOS: iosPlatformChannelSpecifics);
    await _localNotifications.show(id, title, body, platformChannelSpecifics, payload: payload);
  }

  Future<void> cancelNotification(int id) async => _notifications.cancel(id);
}

Future<void> cloudMessagingHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  if (message.data == null) return;
  final notification = NotificationModel.fromJson(message.data);
  await Settings.load();
  final _localization = Settings.localization;

  if (notification.formattedTitle(_localization) == null ||
      notification.formattedBody(_localization) == null) return;

  FCMConfig.instance.displayNotification(
    title: notification.formattedTitle(_localization),
    body: notification.formattedBody(_localization),
    data: message.data,
  );
}

enum OpenDestination { Killed, Background, Foreground }
