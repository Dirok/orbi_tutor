import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:tutor/focused.dart';

class StorageService {
  Future<bool> saveImageToGallery(String imagePath) async {
    try {
      final galleryPath = await ImageGallerySaver.saveFile(imagePath);
      if (galleryPath != null) {
        Log.info('Image saved to $galleryPath');
        return true;
      } else
        throw StorageException('Getting gallery path didn\'t succeed');
    } catch (e) {
      Log.error('Failed to save image to gallery', exception: e);
    }
    return false;
  }
}
