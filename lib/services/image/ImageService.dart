import 'dart:typed_data';

import 'package:get/get.dart';
import 'package:tutor/focused.dart';

abstract class ImageService extends GetxService {
  Uint8List getImageBytesFromLocalStorage(ImageInfo image);

  Future<bool> saveImageBytesToLocalStorage(ImageInfo image, [Uint8List data]);

  Future<void> compressFromBytesAndSave(Uint8List bytes, String targetPath);

  Future<void> compressFromFileAndSave(String sourcePath, {String imageName, String targetPath});

  Future<Uint8List> compressBytes(Uint8List bytes);
}
