import 'dart:typed_data';

import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

class ImageServiceMobile extends ImageService {
  @override
  Uint8List getImageBytesFromLocalStorage(ImageInfo image) {
    try {
      return File(image.path).readAsBytesSync();
    } catch (e) {
      Log.error('Image read error', exception: e);
      return null;
    }
  }

  @override
  Future<bool> saveImageBytesToLocalStorage(ImageInfo image, [Uint8List data]) async {
    try {
      final file = File(image.path);
      if (!await file.exists()) {
        Log.info('Writing image as it doesn\'t  exist');
        await file.writeAsBytes(data);
      }
      return true;
    } catch (e) {
      Log.error('Image write error', exception: e);
      return false;
    }
  }

  @override
  Future<void> compressFromBytesAndSave(Uint8List bytes, String targetPath) async {
    Log.info('Uncompressed file size in bytes: ${bytes.length}');
    if (bytes.length > Settings.minImageSizeToCompress)
      bytes = await compressBytes(bytes);
    await File(targetPath).writeAsBytes(bytes);
  }

  @override
  Future<void> compressFromFileAndSave(String sourcePath,
      {String imageName, String targetPath}) async {
    final file = File(sourcePath);

    if (!await file.exists())
      throw FileSystemException('File doesn\'t exist');
    final fileSize = await file.length();
    Log.info('Uncompressed file size in bytes: $fileSize');
    if (fileSize < Settings.minImageSizeToCompress) {
      if (targetPath != null)
        file.copy(targetPath);
      return;
    }
    final bytes = await file.readAsBytes();
    final compressedBytes = await compressBytes(bytes);

    await File(targetPath ?? sourcePath).writeAsBytes(compressedBytes);
  }

  @override
  Future<Uint8List> compressBytes(Uint8List bytes) async {
    final compressedBytes = await FlutterImageCompress.compressWithList(bytes,
        quality: Settings.imageCompressionQuality);
    Log.info('Compressed file size in bytes: ${compressedBytes.length}');
    return compressedBytes;
  }
}
