import 'dart:typed_data';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:image/image.dart' as img;
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

class ImageServiceWeb extends ImageService {
  final _cacheService = Get.find<CacheService>();

  @override
  Uint8List getImageBytesFromLocalStorage(ImageInfo image) {
    try {
      return _cacheService.getFile(image.name);
    } catch (e) {
      Log.error('Image read error', exception: e);
      return null;
    }
  }

  @override
  Future<bool> saveImageBytesToLocalStorage(ImageInfo image, [Uint8List data]) async {
    try {
      _cacheService.cacheFile(image.name, data);
      return true;
    } catch (e) {
      Log.error('Image write error', exception: e);
      return false;
    }
  }

  @override
  Future<void> compressFromBytesAndSave(Uint8List bytes, String targetPath) async {
    throw UnimplementedError('Unimplemented for web error');
  }

  @override
  Future<void> compressFromFileAndSave(String sourcePath,
      {String imageName, String targetPath}) async {
    final file = await http.get(Uri.parse(sourcePath));

    final fileSize = file.bodyBytes.length;
    Log.info('Uncompressed file size in bytes: $fileSize');
    if (fileSize < Settings.minImageSizeToCompress) {
      final result = _cacheService.cacheFile(imageName, file.bodyBytes);
      if (!result) throw FileSystemException('Failed to write file to local storage');

      return;
    }
    final bytes = file.bodyBytes;
    final compressedBytes = await compressBytes(bytes);

    final result = _cacheService.cacheFile(imageName, compressedBytes);
    if (!result) throw FileSystemException('Failed to write file to local storage');
  }

  @override
  Future<Uint8List> compressBytes(Uint8List bytes) async {
    final image = img.decodeJpg(bytes);
    final compressedBytes = img.encodeJpg(image, quality: Settings.imageCompressionQuality);

    Log.info('Compressed file size in bytes: ${compressedBytes.length}');
    return bytes;
  }
}
