import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:tutor/focused.dart';
import 'package:universal_io/io.dart';

typedef void OnUploadProgressCallback(int sentBytes, int totalBytes);

class ReportService with TranslatableMixin {
  static final ReportService _reportService = ReportService._internal();

  factory ReportService() {
    return _reportService;
  }

  ReportService._internal();

  static const String _authority  = 'orbi.azurewebsites.net';
  static const String _path = '/api/RequestSupport';
  static const String _key = 'jIUrqk9KyuTuammgA6HaVTllzYcK9DJPmGom5cUYBw6TGuVrNHRveA==';
  static const String _version = '1.0';

  static const Duration connectionTimeout = Duration(seconds: 10);

  Future<void> sendReport({
    @required IssueReport report,
    @required OnUploadProgressCallback onUploadProgress}) async {
    Map<String, String> queryParameters = {
      "code": _key,
      "version": _version
    };
    final uri = Uri.https(_authority, _path, queryParameters);
    final body = report.issueDescription + '\n\n' + PlatformUtils.environment.systemInfo;

    try {
      Log.info('------ uploadFiles request ------');

      final httpClient = new HttpClient()
        ..connectionTimeout = connectionTimeout;

      final request = await httpClient.postUrl(uri);
      int byteCount = 0;

      var multiRequest = http.MultipartRequest('POST', uri);
      multiRequest.fields['subject'] = report.contacts;
      multiRequest.fields['body'] = body;

      for (var file in report.files) {
        assert(file != null);
        Log.info(file.path);

        final multiFile = await http.MultipartFile.fromPath(
            'package', file.path,
            filename: file.path.split('/').last,
            contentType: new MediaType('application', 'zip'));
        multiRequest.files.add(multiFile);
      }

      var msStream = multiRequest.finalize();
      var totalBytes = multiRequest.contentLength;
      
      request.contentLength = totalBytes;
      request.headers.set(
          HttpHeaders.contentTypeHeader,
          multiRequest.headers[HttpHeaders.contentTypeHeader]);

      Stream<List<int>> streamUpload = msStream.transform(
        new StreamTransformer.fromHandlers(
          handleData: (data, sink) {
            sink.add(data);
            byteCount += data.length;

            if (onUploadProgress != null) {
              onUploadProgress(byteCount, totalBytes);
              Log.info('Sent: ' + byteCount.toString() + '/' + totalBytes.toString());
            }
          },
          handleError: (error, stack, sink) {
            throw error;
          },
          handleDone: (sink) {
            sink.close();
            Log.info('Uploading done');
          }
        ),
      );

      await request.addStream(streamUpload);

      final httpResponse = await request.close();

      var statusCode = httpResponse.statusCode;

      if (statusCode ~/ 100 != 2) {
        Log.warning('Upload error: ${httpResponse.statusCode}');
        throw HttpRequestException('Report upload error',
            code: httpResponse.statusCode);
      } else {
        await checkResponseSuccess(httpResponse).catchError((error) {
          Log.warning('Reading response error');
          throw HttpRequestException('Reading response error',
              code: httpResponse.statusCode);
        });
      }
    }
    catch (e) {
      Log.info(e.toString());
      throw e;
    }
  }

  Future<String> readResponseAsString(HttpClientResponse response) {
    var completer = new Completer<String>();
    var contents = new StringBuffer();
    response.transform(utf8.decoder).listen((String data) {
      contents.write(data);
    }, onDone: () => completer.complete(contents.toString()),
       onError: (error) => completer.completeError(error)
      );
    return completer.future;
  }

  Future<bool> checkResponseSuccess(HttpClientResponse response) {
    var completer = Completer<bool>();
    var contents = StringBuffer();
    response.transform(utf8.decoder).listen((String data) {
      contents.write(data);
    },
        onDone: () => completer.complete(contents != null),
        onError: (error) => completer.completeError(false));
    return completer.future;
  }
}