import 'package:tutor/focused.dart';

abstract class FileService {
  static FileService _instance;

  factory FileService() {
    _instance ??= getFileService();
    return _instance;
  }

  Future<void> removeFiles(String folderPath);
  Future<void> createZip(String sourcePath, String targetPath);
  Future<bool> extractZip(String sourcePath, String targetPath);
  Future<bool> isDiskSpaceEnough(int fileSizeInBytes);
}