import 'dart:async';
import 'package:universal_io/io.dart';

import 'package:disk_space/disk_space.dart';
import 'package:flutter_archive/flutter_archive.dart';
import 'package:tutor/focused.dart';

FileService getFileService() => FileServiceMobile._();

class FileServiceMobile implements FileService {

  FileServiceMobile._();

  Future<void> removeFiles(String folderPath) async {
    var dir = Directory(folderPath);
    if (await dir.exists())
      await dir.delete(recursive: true);
  }

  Future<void> createZip(String sourcePath, String targetPath) async {
    final sourceDir = Directory(sourcePath);
    final zipFile = File(targetPath);
    try {
      await ZipFile.createFromDirectory(
          sourceDir: sourceDir, zipFile: zipFile, recurseSubDirs: false);
    } catch (e) {
      Log.error('Zip failed', exception: e);
      rethrow;
    }
  }

  Future<bool> extractZip(String sourcePath, String targetPath) async {
    final zipFile = File(sourcePath);
    if (!await zipFile.exists()) {
      Log.error('File archive $sourcePath not found');
      return false;
    }

    final targetDir = Directory(targetPath);
    if (!await targetDir.exists()) await targetDir.create(recursive: true);

    final targetFile = File(targetDir.path + '/WhiteboardSet.dat');

    try {
      if (await targetFile.exists())
        await targetFile.delete();

      await ZipFile.extractToDirectory(
          zipFile: zipFile,
          destinationDir: targetDir,
          onExtracting: (zipEntry, progress) {
            Log.info('Unzip file: ${zipEntry.name}');
            return ExtractOperation.extract;
          });
      return true;
    } catch (e) {
      Log.error('Unzip failed', exception: e);
      return false;
    }
  }

  Future<bool> isDiskSpaceEnough(int fileSizeInBytes) async {
    final fileSizeMB = fileSizeInBytes / (1024 * 1024);
    final freeMB = await DiskSpace.getFreeDiskSpace;
    if (fileSizeMB >= freeMB) {
      Log.warning('File size: ' + fileSizeMB.toStringAsFixed(2) + ' MB');
      Log.warning('Free space on device: ' + freeMB.toStringAsFixed(2) + ' MB');
      return false;
    }
    return true;
  }
}
