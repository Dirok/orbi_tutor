// import 'dart:html';

import 'dart:async';

import 'package:tutor/focused.dart';

FileService getFileService() => FileServiceWeb._();

class FileServiceWeb implements FileService {

  FileServiceWeb._();

  Future<void> removeFiles(String folderPath) => Future.value();
  Future<void> createZip(String sourcePath, String targetPath) async {
    throw UnsupportedError("zip creation is unavailable on web");
  }

  Future<bool> extractZip(String sourcePath, String targetPath) async {
    throw UnsupportedError("zip extraction is unavailable on web");
  }

  Future<bool> isDiskSpaceEnough(int fileSizeInBytes) async => false;
}
