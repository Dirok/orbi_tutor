import 'dart:convert';
import 'dart:typed_data';

import 'package:get/get.dart';
import 'package:universal_html/html.dart' as html;

class CacheService extends GetxService {
  final _localStorage = html.window.localStorage;

  bool cacheFile(String name, Uint8List data) {
    if (_localStorage.containsKey(name)) return false;
    final fileEncoded = base64.encode(data);
    _localStorage[name] = fileEncoded;
    return true;
  }

  Uint8List getFile(String name) {
    if (!_localStorage.containsKey(name)) return null;
    final data = _localStorage[name];
    final imageData = base64.decode(data);
    return imageData;
  }
}
