import 'dart:async';

import 'package:bson/bson.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';
import 'package:uni_links/uni_links.dart';

class UriLinkService extends GetxService with EventMixin {
  static const String _loginLinkKey = 'login';
  static const String _lectureLinkKey = 'lecture';
  static const String _idLinkKey = 'id';
  static const String _partnerLinkKey = 'partner';

  StreamSubscription _subscription;

  @override
  void onInit() async {
    super.onInit();
    await _initPlatformStateForUriUniLinks();
  }

  @override
  void onClose() async {
    await _subscription?.cancel();
    cancelEvents();

    super.onClose();
  }

  Future<void> _initPlatformStateForUriUniLinks() async {
    if (GetPlatform.isMobile)
      _subscription = getUriLinksStream().listen((Uri uri) async =>
          await _handleUri(uri), onError: (Object err) => Log.info(err));

    try {
      var uri = await getInitialUri();
      await _handleUri(uri);
    } catch(e) {
      Log.error('Handle URI link failed', exception: e);
    }
  }

  Future<void> _handleUri(Uri uri) async {
    if (uri == null) return;

    Log.info('Open link ${uri.toString()}');

    // TODO remove when Kundelik fixes links
    uri = Uri.parse(uri.toString().replaceAll(RegExp('&amp;'), '&'));
    Log.info('Corrected link ${uri.toString()}');

    if (!await _tryParseUri(uri))
      sendEvent(ParseFailedLinkEvent());
  }

  Future<bool> _tryParseUri(Uri uri) async {
    if (!_isCurrentServerLink(uri)) {
      Log.error('Link has wrong host');
      return false;
    }

    if (uri.hasEmptyPath) {
      Log.error('Link has empty path');
      return false;
    }

    final command = uri.pathSegments[0];
    switch(command) {
      case _loginLinkKey:
        Log.info('Link has login command');
        sendEvent(SignInLinkEvent(uri.path));
        break;
      case _lectureLinkKey:
        Log.info('Link has open lecture command');
        final params = uri.queryParameters;

        if (!params.containsKey(_idLinkKey)) {
          Log.error('Link has no lecture id');
          return false;
        }

        final lectureId = ObjectId.fromHexString(params[_idLinkKey]);
        final partner = params[_partnerLinkKey];
        sendEvent(OpenLectureLinkEvent(lectureId, partner));
        break;
      default:
        Log.error('Unknown command in link: $command');
        return false;
    }

    return true;
  }

  bool _isCurrentServerLink(Uri uri) =>
      uri.origin == Settings.server.url ||
      (uri.origin == Servers.link && Settings.server.url == Servers.release);
}

class ParseFailedLinkEvent extends Event {}

class SignInLinkEvent extends Event {
  final String link;
  SignInLinkEvent(this.link);
}

class OpenLectureLinkEvent extends Event {
  final ObjectId lectureId;
  final String partner;
  OpenLectureLinkEvent(this.lectureId, this.partner);
}