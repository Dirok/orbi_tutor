import 'dart:async';

import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class NetworkDependentService extends GetxService {
  final _connectivity = Get.find<ConnectivityService>();

  final subscriptions = <StreamSubscription>[];
  StreamSubscription _connectivitySubscription;

  @override
  void onInit() {
    super.onInit();
    initialize();
  }

  Future<bool> initialize() async {
    if (!_connectivity.isConnectedToInternet.value) {
      Log.info('Cannot fetch sth without World Wide Web © connection');
      _connectivitySubscription = _connectivity.isConnectedToInternet.listen((bool isConnected) {
        if (isConnected) {
          Log.info('Run reinit service with connection');
          initialize();
        }
      })
        ..addTo(subscriptions);
      return false;
    }
    _connectivitySubscription?.cancel();
    return true;
  }

  @override
  void onClose() {
    disposeAllSubscriptions();
    super.onClose();
  }

  void disposeAllSubscriptions() {
    subscriptions.disposeSubscriptions();
  }
}
