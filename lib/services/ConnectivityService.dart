import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:tutor/focused.dart';

class ConnectivityService extends GetxService {
  StreamSubscription _connectivitySubscription;

  Timer _connectionCheckTimer;

  final isConnectedToInternet = true.obs;

  @override
  void onInit() async {
    super.onInit();

    _connectivitySubscription = Connectivity()
        .onConnectivityChanged
        .listen(_onHardwareConnectionChanged);

    // First event from listener goes with none status
    // so need to check connectivity after callback is called
    // TODO: remove after connectivity plugin fix
    Timer(Duration(milliseconds: 100), () async {
      final result = await Connectivity().checkConnectivity();
      await _onHardwareConnectionChanged(result);
    });
  }

  @override
  void onClose() async {
    await _connectivitySubscription.cancel();
    _connectionCheckTimer?.cancel();
    super.onClose();
  }

  Future<void> _onHardwareConnectionChanged(ConnectivityResult result) async {
    _connectionCheckTimer?.cancel();
    Log.info('Hardware network connection: ${describeEnum(result)}');
    if (result == ConnectivityResult.none) {
      isConnectedToInternet.value = false;
      return;
    }

    final isConnected = await _checkServerConnection();
    Log.info('Internet connection: $isConnected');
    isConnectedToInternet.value = isConnected;
    // TODO: Split for 2 interfaces for platforms
    if (GetPlatform.isWeb) return;
    setupTimer();
  }

  void setupTimer() {
    _connectionCheckTimer = Timer(Duration(seconds: 3), () async {
      final isConnected = await _checkServerConnection();
      if (isConnected != isConnectedToInternet.value) {
        Log.info('Internet connection: ${isConnectedToInternet.value}');
        isConnectedToInternet.value = isConnected;
      }
      setupTimer();
    });
  }

  Future<bool> _checkServerConnection() async {
    // TODO: Split for 2 interfaces for platforms
    if (GetPlatform.isWeb) return true;
    try {
      final result = await http.get(Uri.parse('http://example.com'));
      return result.statusCode == 200;
    } catch (_) {
      return false;
    }
  }
}
