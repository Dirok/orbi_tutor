import 'dart:async';

import 'package:get/get.dart';

abstract class Event {}

mixin EventMixin {
  RxNotifier get eventNotifier => _eventNotifier;
  final _eventNotifier = RxNotifier<Event>();

  final _subscriptions = <StreamSubscription<Event>>[];

  Event lastEvent;

  void sendEvent(Event event) {
    if (_eventNotifier.subject.isClosed) return;

    lastEvent = event;
    _eventNotifier.subject.add(event);
  }

  StreamSubscription<Event> listenEvents(void Function(Event) onData) {
    final sub = _eventNotifier.listen(onData);
    _subscriptions.add(sub);
    return sub;
  }

  void clearSubscriptions() => _subscriptions..forEach((s) => s.cancel())..clear();

  void cancelEvents() {
    if (!_eventNotifier.subject.isClosed)
      _eventNotifier.close();
  }
}