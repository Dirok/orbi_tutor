import 'package:tutor/focused.dart';

class ShowSnackEvent extends Event {
  final notification;

  ShowSnackEvent(NotificationModel this.notification);
}

class MuteMicEvent extends Event {}

class UnmuteMicEvent extends Event {}
