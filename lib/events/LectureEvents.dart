import 'package:bson/bson.dart';
import 'package:flutter/foundation.dart';
import 'package:tutor/focused.dart';

class OpenLectureEvent extends Event {
  final bool isOwner;
  OpenLectureEvent(this.isOwner);
}

class OpenGroupLectureEvent extends OpenLectureEvent {
  OpenGroupLectureEvent(bool isOwner) : super(isOwner);
}

class OpenPersonalLectureEvent extends OpenLectureEvent {
  OpenPersonalLectureEvent(bool isOwner) : super(isOwner);
}

class OpenLectureEventFactory {
  static OpenLectureEvent create({@required bool isGroupLecture, @required bool isOwner}) =>
      isGroupLecture ? OpenGroupLectureEvent(isOwner) : OpenPersonalLectureEvent(isOwner);
}

class CantOpenLectureEvent extends Event {}

class NavigateToLectureEvent extends Event {
  final ObjectId lectureId;
  NavigateToLectureEvent(this.lectureId);
}

class ChangeLectureRequestEvent extends Event {
  final ObjectId id;
  ChangeLectureRequestEvent(this.id);
}

class LectureInactiveEvent extends Event {}
