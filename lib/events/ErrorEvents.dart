import 'package:tutor/focused.dart';

class ServerOperationErrorEvent extends Event {
  final String errorMessage;

  ServerOperationErrorEvent(this.errorMessage);
}

class OpenLinkErrorEvent extends Event {}
