class LectureAction {
  // {"Data":{"UserID":"5f6cb23087524f43b210daee"},"ElapsedTimeMs":118,"MessageType":"UserConnected","TransactionID":""}

  final String transactionId;
  final int elapsedMs;
  final String messageType;
  final Map<String, dynamic> data;

  LectureAction.fromJson(Map<String, dynamic> json)
      : transactionId = json['TransactionID'],
        elapsedMs = json['ElapsedTimeMs'],
        messageType = json['MessageType'],
        data = json['Data'];
}
