import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class UserNameFormsController extends GetxController {
  final nameController = TextEditingController();
  final surnameController = TextEditingController();

  final nameFocusNode = FocusNode();
  final surnameFocusNode = FocusNode();

  User get user => appSession.account.user;

  @override
  void onInit() {
    super.onInit();

    nameController.text = user.name;
    surnameController.text = user.surname;
    nameFocusNode.requestFocus();
  }

  @override
  void onClose() {
    nameFocusNode.dispose();
    surnameFocusNode.dispose();

    super.onClose();
  }
}
