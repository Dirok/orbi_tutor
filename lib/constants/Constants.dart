class Constants {
  // static const String realmAppId = 'focusednow-hsnax';
  static const String realmAppId = 'focused-now-nxwqd'; // for tests
  static const String androidStoreUrl = 'https://play.google.com/store/apps/details?id=com.orbi.focused';
  static const String iosStoreUrl = 'https://apps.apple.com/us/app/focused-now/id1529311912';
  static const String archiveExt = '.zip';
  static const String logsExt = '.log';
  static const String defaultCountryCode = 'US';
  static const double minAudioVolume = 0.0;
  static const double drawerWidth = 0.26;
  static const int noSessionErrorCode = 458;
  static const int snackbarDurationSeconds = 3;
  static const int retryMicStateCount = 2;
  static const int micStateTimeout = 5;
  static const int configFetchTimeout = 15;
}
