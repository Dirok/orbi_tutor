import 'dart:ui';

import 'package:flutter/material.dart';

class UIConstants {
  static const Color primaryLightColor = Color.fromARGB(255, 222, 232, 247);
  static const Color primaryDarkColor = Color.fromARGB(255, 111, 160, 213);
  static const Color yellowColor = Color.fromARGB(255, 249, 233, 70);
  static const Color redLogoColor = Color.fromARGB(255, 230, 71, 40);
  static const Color purpleIconColor = Color.fromARGB(255, 122, 93, 166);
  static const Color lightGrayColor = Color.fromRGBO(192, 192, 192, 0.9);
  static const Color darkGrayColor = Color.fromRGBO(0, 0, 0, 0.38);
  static const Color defaultTextColor = Colors.black;
  static const Color buttonTextColor = Colors.white;
  static const Color backgroundColor = Colors.white;
  static const String fontFamily = 'OpenSans';
  static const double defaultFontSize = 14.0;
  static const double buttonFontSize = 15.0;
  static const double tabBarFontSize = 16.0;
  static const double appBarFontSize = 20.0;
  static const double appBarPreferredHeight = 56.0;
  static const double buttonSize = 36.0;
  static const double iconSize = 24.0;
  static const double speakingUserWidthFactor = 0.28;
  static const double speakingUserMarginWidthFactor = 0.11;
  static const double microIconWidth = 70;
  static const double userListItemWidthFactor = 0.7;
  static const double mainMenuButtonWidthFactor = 0.4;
  static const double mainMenuButtonHeightFactor = 0.6;
  static const int minTabBarItemsCount = 2;
  static const int maxTabBarItemsCount = 5;
  static const int maxTagTextSize = 15;
}