class Translations {
  static const welcome_to = "welcome_to";
  static const login = "login";
  static const password = "password";
  static const password_repeat = "password_repeat";
  static const password_mismatch = "password_mismatch";
  static const password_forgot = "password_forgot";
  static const password_where_get = "password_where_get";
  static const log_in = "log_in";
  static const log_out = "log_out";
  static const you_invited_to_lecture = "you_invited_to_lecture";
  static const want_to_sign_in = "want_to_sign_in";
  static const continue_as_anonymous = "continue_as_anonymous";
  static const register = "register";
  static const register_to_continue = "register_to_continue";
  static const register_success = "register_success";
  static const fill_to_complete_registration = "fill_to_complete_registration";
  static const cancel_register = "cancel_register";
  static const skip = "skip";
  static const failed_to_update_user_account = "failed_to_update_user_account";
  static const cant_update_account = "cant_update_account";
  static const continue_with_apple = "continue_with_apple";
  static const continue_with_google = "continue_with_google";
  static const continue_with_facebook = "continue_with_facebook";
  static const continue_with_kundelik = "continue_with_kundelik";
  static const warning = "warning";
  static const error = "error";
  static const unknown_error = "unknown_error";
  static const bad_credentials = "bad_credentials";
  static const enter_valid_credentials = "enter_valid_credentials";
  static const i_am_student = "i_am_student";
  static const in_developing = "in_developing";
  static const double_back_exit = "double_back_exit";
  static const show_password = "show_password";
  static const lecture_not_exist = "lesson_not_exist";
  static const lecture_not_available = "lesson_not_available";
  static const lecture_not_started = "lesson_not_started";
  static const lecture_is_over = "lesson_is_over";
  static const change_lecture = "change_lesson";
  static const invalid_link = "invalid_link";
  static const cant_open_link = "cant_open_link";
  static const not_signed_in = "not_signed_in";
  static const create_lecture_kundelik = "create_lesson_kundelik";
  static const auto_login_fail = "auto_login_fail";
  static const create_lesson_no_classes = "create_lesson_no_classes";
  static const create_lesson_no_discipline = "create_lesson_no_discipline";
  static const choose_country = "choose_country";
  static const country_selection_tooltip = "country_selection_tooltip";
  static const country_selection_error = "country_selection_error";

  static const auth_field_empty = "auth_field_empty";
  static const auth_field_with_space = "auth_field_with_space";
  static const password_unsafe = "password_unsafe";
  static const email_invalid = "email_invalid";
  static const contact_admin = "contact_admin";
  static const get_auth_link = "get_auth_link";
  static const account_errors = "account_errors";
  static const try_again = "try_again";
  static const change_login_data = "change_login_data";
  static const have_not_used_for_while = "have_not_used_for_while";
  static const have_logged_out = "have_logged_out";

  static const failed_to_authorization = "failed_to_authorization";
  static const failed_to_registration = "failed_to_registration";
  static const failed_to_registration_account_exists = "failed_to_registration_account_exists";
  static const failed_to_registration_weak_password = "failed_to_registration_weak_password";
  static const failed_to_registration_operation_not_allowed = "failed_to_registration_operation_not_allowed";
  static const failed_to_delete_lesson = "failed_to_delete_lesson";
  static const failed_authorization_by_kundelik = "failed_authorization_by_kundelik";
  static const failed_authorization_in_outside_service = "failed_authorization_in_outside_service";

  static const my_lectures = "my_lessons";
  static const create_new = "create_new";
  static const finished = "finished";
  static const scheduled = "scheduled";
  static const create_new_lesson = "create_new_lesson";
  static const finished_lessons = "finished_lessons";
  static const scheduled_lessons = "scheduled_lessons";
  static const no_finished_lessons = "no_finished_lessons";
  static const no_scheduled_lessons = "no_scheduled_lesson";
  static const active = "active";
  static const downloaded = "downloaded";
  static const delete_lesson = "delete_lesson";
  static const want_to_delete_lesson = "want_to_delete_lesson";
  static const remove_from_device = "remove_from_device";
  static const remove_completely = "remove_completely";

  static const create_new_lecture = "create_new_lecture";
  static const lecture_date = "lecture_date";
  static const lecture_time = "lecture_time";
  static const lecture_description = "lecture_description";
  static const not_chosen = "not_chosen";
  static const delete_lecture = "delete_lecture";
  static const want_to_delete_lecture = "want_to_delete_lecture";
  static const cant_delete_lecture = "cant_delete_lecture";
  static const scheduled_lectures = "scheduled_lectures";
  static const finished_lectures = "finished_lectures";
  static const no_scheduled_lectures = "no_scheduled_lectures";
  static const no_finished_lectures = "no_finished_lectures";
  static const failed_to_update_lecture_list = "failed_to_update_lecture_list";
  static const add_description = "add_description";
  static const optional = "optional";
  static const cant_join_lecture = "cant_join_lecture";

  static const select_subject = "select_subject";
  static const your_subjects = "your_subjects";
  static const enter_new_subject_name = "enter_new_subject_name";
  static const subject_already_exist = "subject_already_exist";
  static const cant_create_subject = "cant_create_subject";
  static const want_to_delete_subject = "want_to_delete_subject";
  static const cant_delete_subject = "cant_delete_subject";
  static const failed_to_update_subjects_list = "failed_to_update_subjects_list";
  static const show_all = "show_all";

  static const participants = "participants";
  static const participants_list = "participants_list";
  static const participants_not_group_members = "participants_not_group_members";
  static const create_group_for_fast_lesson_creation = "create_group_for_fast_lesson_creation";
  static const add_participants = "add_participants";
  static const select_participants = "select_participants";
  static const no_participants = "no_participants";
  static const no_contacts = "no_contacts";
  static const easy_to_fix_this = "easy_to_fix_this";
  static const create_lecture_without_participants = "create_lecture_without_participants";
  static const send_invite_link = "send_invite_link";
  static const can_see_contacts_after_lecture = "can_see_contacts_after_lecture";

  static const contacts = "contacts";
  static const groups = "groups";
  static const group_name = "group_name";
  static const rename_group = "rename_group";
  static const delete_group = "delete_group";
  static const want_to_delete_group = "want_to_delete_group";
  static const members = "members";
  static const add_members = "add_members";
  static const change_members_list = "change_members_list";
  static const cant_create_group = "cant_create_group";
  static const cant_delete_group = "cant_delete_group";
  static const failed_to_update_contacts_list = "failed_to_update_contacts_list";

  static const next = "next";
  static const apply = "apply";
  static const create = "create";

  static const owners = "owners";
  static const teachers = "teachers";

  static const lecture_was_created = "lesson_was_created";
  static const how_to_continue = "how_to_continue";
  static const open_lecture = "open_lesson";
  static const schedule_new_lecture = "schedule_new_lesson";
  static const return_to_scheduled = "return_to_scheduled";
  static const cant_create_lecture = "cant_create_lesson";
  static const cant_start_lecture = "cant_start_lesson";
  static const schedule_lecture_in_past = "schedule_lesson_in_past";
  static const want_to_schedule_lecture = "want_to_schedule_lesson";
  static const current_time = "current_time";

  static const play_the_lecture = "play_the_lesson";
  static const view_boards = "view_boards";
  static const on_checking = "on_checking";
  static const processing = "processing";
  static const local_copy_only = "local_copy_only";
  static const not_enough_space = "not_enough_space";
  static const please_wait = "please_wait";
  static const busy_downloading = "busy_downloading";
  static const failed_to_download_lecture = "failed_to_download_lesson";

  static const call_notification = "call_notification";
  static const call_lecture = "call_lesson";
  static const call_lecture_description = "call_lesson_description";
  static const mute = "mute";
  static const unmute = "unmute";

  static const install_on_holder = "install_on_holder";
  static const settings = "settings";
  static const about = "about";

  static const enter_your_name = "enter_your_name";
  static const surname = "surname";
  static const name = "name";
  static const patronymic = "patronymic";
  static const confirm = "confirm";
  static const cancel = "cancel";
  static const continue_ = "continue";

  static const enter_as_student = "enter_as_student";
  static const not_me = "not_me";

  static const lesson_date = "lesson_date";
  static const lesson_time = "lesson_time";
  static const chosen_class = "chosen_class";
  static const chosen_discipline = "chosen_discipline";
  static const classes = "classes";
  static const disciplines = "disciplines";
  static const ready = "ready";
  static const reset = "reset";
  static const from = "from";
  static const to = "to";

  static const share_snapshot = "share_snapshot";
  static const cant_share_snapshot = "cant_share_snapshot";

  static const share_link = "share_link";
  static const save = "save";
  static const load = "load";
  static const exit = "exit";

  static const lecture_finished = "lesson_finished";
  static const teacher_finished_lesson = "teacher_finished_lesson";
  static const lesson_auto_finished_limit = "lesson_auto_finished_limit";
  static const lecture_auto_finished = "lesson_auto_finished";

  static const ok = "ok";
  static const yes = "yes";
  static const no = "no";
  static const or = "or";

  static const failure = "failure";
  static const photo_capturing_error = "photo_capturing_error";
  static const image_export_failed = "image_export_failed";
  static const paper_not_recognized = "paper_not_recognized";
  static const want_to_exit = "want_to_exit";
  static const no_students = "no_students";
  static const image_is_too_large = "image_is_too_large";
  static const send_image_failed = "send_image_failed";

  static const ask_student = "ask_student";
  static const cancel_question_student = "cancel_question_student";
  static const show_student_whiteboards = "show_student_whiteboards";

  static const no_internet_connection = "no_internet_connection";
  static const no_main_server_connection = "no_main_server_connection";
  static const no_lecture_server_connection = "no_lesson_server_connection";
  static const no_multimedia_server_connection = "no_multimedia_server_connection";
  static const failed_to_load_data = "failed_to_load_data";
  static const please_try_later = "please_try_later";
  static const lecture_already_active_error = "lesson_already_active_error";
  static const server_invalid_result = "server_invalid_result";
  static const server_error = "server_error";

  static const want_start_lecture = "want_start_lesson";
  static const finish_lecture_exit = "finish_lesson_exit";
  static const finish_lecture_for_students = "finish_lesson_for_students";
  static const want_to_finish = "want_to_finish";
  static const exit_lecture_without_finish = "exit_lesson_without_finish";
  static const you_cannot_teach_more_than_two_lecture = "you_cannot_teach_more_than_two_lesson";

  static const select_audio_device = "select_audio_device";
  static const unknown = "unknown";
  static const receiver = "receiver";
  static const speaker = "speaker";
  static const headphones = "headphones";
  static const bluetooth = "bluetooth";

  static const share_logs = "share_logs";
  static const clear_logs = "clear_logs";

  static const image_will_saved_to_gallery = "image_will_saved_to_gallery";
  static const camera_resolution = "camera_resolution";
  static const paper_width = "paper_width";
  static const paper_height = "paper_height";
  static const camera_delay = "camera_delay";
  static const sec = "sec";
  static const paper_size = "paper_size";
  static const camera_mirroring = "camera_mirroring";
  static const paper_recognition = "paper_recognition";
  static const save_images = "save_images";
  static const stylus_mode = "stylus_mode";

  static const need_camera_permission = "need_camera_permission";
  static const need_mic_permission = "need_mic_permission";
  static const need_storage_permission = "need_storage_permission";
  static const media_access_permission = "media_access_permission";

  static const report_an_issue = "report_an_issue";
  static const report_description = "report_description";
  static const contact_details = "contact_details";
  static const enter_email = "enter_email";
  static const describe_issue = "describe_issue";
  static const send = "send";
  static const preparing = "preparing";
  static const sending = "sending";
  static const finishing = "finishing";
  static const success = "success";
  static const image_saved = "image_saved";
  static const image_not_saved = "image_not_saved";
  static const report_sent_successfully = "report_sent_successfully";
  static const failed_to_create_report = "failed_to_create_report";
  static const failed_to_send_report = "failed_to_send_report";
  static const no_report_server_connection = "no_report_server_connection";
  static const platform_web_error = "platform_web_error";

  static const connected = "connected";
  static const disconnected = "disconnected";

  static const we_got_better = "we_got_better";
  static const new_version_available = "new_version_available";
  static const update = "update";

  static const whats_new = "whats_new";
  static const get_started = "get_started";
}