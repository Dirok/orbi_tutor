import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class Pages {
  static final loginPage = GetPage(
      name: Routes.login,
      page: () => LoginWidget());

  static final studentAuthInfoPage = GetPage(
      name: Routes.studentAuthInfo,
      page: () => StudentAuthInfoWidget());

  static final kundelikAuthPage = GetPage(
      name: Routes.kundelikAuth,
      page: () => KundelikAuthWidget(),
      binding: KundelikAuthBindings());

  static final wantToSignInPage = GetPage(
      name: Routes.wantToSignIn,
      page: () => WantToSignInWidget(),
      binding: WantToSignInBindings());

  static final registrationFirebasePage = GetPage(
      name: Routes.registrationSignIn,
      page: () => RegistrationSignInWidget(),
      binding: RegistrationSignInBindings());

  static final registrationUserNamePage = GetPage(
      name: Routes.registrationUserName,
      page: () => RegistrationUserNameWidget(),
      binding: RegistrationUserNameBindings());

  static final registrationWelcomePage = GetPage(
      name: Routes.registrationWelcome,
      page: () => RegistrationWelcomeWidget(),
      binding: RegistrationWelcomeBindings());

  static final mainMenuPage = GetPage(
      name: Routes.mainMenu,
      page: () => MainMenuWidget(),
      binding: MainMenuBindings());

  static final scheduledLecturesPage = GetPage(
      name: Routes.scheduledLectures,
      page: () => ScheduledLecturesListWidget(),
      binding: ScheduledLecturesListBindings());

  static final passedLecturesPage = GetPage(
      name: Routes.passedLectures,
      page: () => PassedLecturesListWidget(),
      binding: PassedLecturesListBindings());

  static final cameraPage = GetPage(
      name: Routes.camera,
      page: () => CameraWidget());

  static final photoPreviewPage = GetPage(
      name: Routes.photoPreview,
      page: () => PhotoPreviewScreen(),
      binding: PhotoPreviewBindings());

  static final settingsPage = GetPage(
      name: Routes.settings,
      page: () => SettingsWidget(),
      binding: SettingsBindings());

  static final enterFullNamePage = GetPage(
      name: Routes.enterFullName,
      page: () => EnterFullNameWidget());

  static final studentAuthPage = GetPage(
      name: Routes.studentAuth,
      page: () => StudentAuthWidget(), binding: StudentAuthBindings());

  static final teacherActiveLecturePage = GetPage(
      name: Routes.teacherActiveGroupLecture,
      page: () => TeacherActiveGroupLectureWidget(),
      binding: TeacherActiveGroupLectureBindings());

  static final studentActiveLecturePage = GetPage(
      name: Routes.studentActiveGroupLecture,
      page: () => StudentActiveGroupLectureWidget(),
      binding: StudentActiveGroupLectureBindings());

  static final teacherPassedLecturePage = GetPage(
      name: Routes.teacherPassedGroupLecture,
      page: () => TeacherPassedGroupLectureWidget(),
      binding: TeacherPassedGroupLectureBindings());

  static final studentPassedLecturePage = GetPage(
      name: Routes.studentPassedGroupLecture,
      page: () => StudentPassedGroupLectureWidget(),
      binding: StudentPassedGroupLectureBindings());

  static final personalLecturePage = GetPage(
      name: Routes.personalLecture,
      page: () => PersonalLectureWidget(),
      binding: PersonalLectureBindings());

  static final createLecturePage = GetPage(
      name: Routes.createLecture,
      page: () => CreateLectureWidget(),
      binding: CreateLectureBindings());

  static final selectSubjectPage = GetPage(
      name: Routes.selectSubject,
      page: () => SelectSubjectWidget(),
      binding: SelectSubjectBindings());

  static final participantsListPage = GetPage(
      name: Routes.participantsList,
      page: () => ParticipantsListWidget(),
      binding: ParticipantsListBindings());

  static final sendRequestPage = GetPage(
      name: Routes.sendRequest,
      page: () => ReportWidget(),
      binding: SendRequestBindings());

  static final lecturePlayerPage = GetPage(
      name: Routes.lecturePlayer,
      page: () => LecturePlayerWidget());

  static final notificationPage = GetPage(
      name: Routes.notification,
      page: () => NotificationWidget());

  static final countrySelectionPage = GetPage(
      name: Routes.countrySelection,
      page: () => CountrySelectionWidget(),
      binding: CountrySelectionBindings(),);

  static final List<GetPage> list = [
    loginPage,
    studentAuthInfoPage,
    kundelikAuthPage,
    wantToSignInPage,
    registrationUserNamePage,
    registrationWelcomePage,
    mainMenuPage,
    scheduledLecturesPage,
    passedLecturesPage,
    cameraPage,
    photoPreviewPage,
    settingsPage,
    enterFullNamePage,
    studentAuthPage,
    teacherActiveLecturePage,
    studentActiveLecturePage,
    teacherPassedLecturePage,
    studentPassedLecturePage,
    personalLecturePage,
    createLecturePage,
    selectSubjectPage,
    participantsListPage,
    sendRequestPage,
    lecturePlayerPage,
    notificationPage,
    countrySelectionPage,
  ];
}
