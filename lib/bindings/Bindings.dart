import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:get/get.dart';
import 'package:tutor/focused.dart';

class MainBindings implements Bindings {
  @override
  void dependencies() {
    _initServicesBindings();
    _initWidgetsBindings();
  }

  void _initServicesBindings() {
    Get.lazyPut(() => ConnectivityService());
    Get.lazyPut(() => UriLinkService());
    Get.lazyPut(() => PushService(), fenix: true);
    Get.lazyPut(() => ConfigService(), fenix: true);
    Get.lazyPut(() => MainServerApi(Settings.server.url)..token = appSession?.httpToken,
        fenix: true);
    Get.lazyPut(() => S3ServerApi(), fenix: true);
    Get.lazyPut(() => CacheService(), fenix: true);

    Get.lazyPut<ImageService>(() => kIsWeb ? ImageServiceWeb() : ImageServiceMobile());

    if (kIsWeb) {
      Get.putAsync<LectureStorageService>(() async {
        final s = LectureStorageServiceWeb();
        await s.init();
        return s;
      });
    } else {
      Get.lazyPut<LectureStorageService>(() => LectureStorageServiceMobile());
    }

    Get.lazyPut(() => ServerUpdatesService(), fenix: true);
    Get.lazyPut(() => UserDataService());
    Get.lazyPut(() => LectureDataService());
    Get.lazyPut(() => CountryProviderService(), fenix: true);

    Get.lazyPut(() => AuthorizationService(), fenix: true);
    Get.lazyPut(() => FirebaseSignInService());
    Log.info("services inited");
  }

  void _initWidgetsBindings() {
    Get.lazyPut(() => LoginViewModel(), fenix: true);
    Get.lazyPut(() => ReportViewModel(), fenix: true);
    Log.info("widgets bindings inited");
  }
}

// region authorizations bindings
class KundelikAuthBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => KundelikAuthViewModel());
}

class StudentAuthBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => StudentAuthViewModel());
}

class SelectCountryBinding extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => CountrySelectionViewModel());
}

class RegistrationSignInBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => RegistrationSignInViewModel());
}

class RegistrationUserNameBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => RegistrationUserNameViewModel());
    Get.lazyPut(() => UserNameFormsController());
  }
}

class RegistrationWelcomeBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => RegistrationWelcomeViewModel());
}

class WantToSignInBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => WantToSignInViewModel());
}
// endregion


// region lectures process bindings
class StudentActiveGroupLectureBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => StudentActiveGroupLectureViewModel());
}

class TeacherActiveGroupLectureBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => TeacherActiveGroupLectureViewModel());
}

class StudentPassedGroupLectureBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => StudentPassedGroupLectureViewModel());
}

class TeacherPassedGroupLectureBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => TeacherPassedGroupLectureViewModel());
}

class PersonalLectureBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => PersonalLectureViewModel());
}

class PhotoPreviewBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => StorageService());
}
// endregion

// region lectures  menu / creation / lists / subjects bindings
class MainMenuBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => MainMenuViewModel());
}

class CreateLectureBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CreateLectureViewModel());
    Get.lazyPut(() => SelectSubjectViewModel());
    Get.lazyPut(() => ParticipantsListViewModel());
  }
}

class ScheduledLecturesListBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ScheduledLecturesListViewModel());
    Get.lazyPut<LecturesFilterViewModel>(() => ScheduledLecturesFilterViewModel());
  }
}

class PassedLecturesListBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PassedLecturesListViewModel());
    Get.lazyPut<LecturesFilterViewModel>(() => PassedLecturesFilterViewModel());
  }
}

class SelectSubjectBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => SelectSubjectViewModel());
}

class ParticipantsListBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => ParticipantsListViewModel());
}
// endregion

// region basic bindings
class SendRequestBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => ReportViewModel());
}

class SettingsBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => SettingsViewModel());
}
// endregion

class CountrySelectionBindings extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => CountrySelectionViewModel());
}