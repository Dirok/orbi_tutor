import 'dart:async';

import 'package:fcm_config/fcm_config.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:mongo_realm/mongo_realm.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:tutor/focused.dart';

Future<void> main() async {
  Log.init();

  WidgetsFlutterBinding.ensureInitialized();

  if (GetPlatform.isMobile) {
    // TODO: Remove to enable vertical orientation
    await SystemChrome.setPreferredOrientations([
      GetPlatform.isAndroid ? DeviceOrientation.landscapeLeft : DeviceOrientation.landscapeRight
    ]);
    await SystemChrome.setEnabledSystemUIOverlays([]);
  }

  await PlatformUtils.init();
  final environment = PlatformUtils.environment;

  Log.info("---------------------------------------------------");
  Log.info("INIT SESSION === DATE: ${DateTime.now()}"
      + " = APP: " + environment.appVersion
      + " = DEVICE: " + environment.device
      + " = OS: " + environment.platform);
  Log.info("---------------------------------------------------");

  await Firebase.initializeApp();

  appSession = await AppSession.load();

  await FCMConfig.instance.init();

  MainBindings().dependencies();

  await Settings.load();

  // TODO: [WEB] Implement for web
  if (!GetPlatform.isWeb)
    await RealmApp.init(Constants.realmAppId);

  if (kIsWeb)
    await SentryFlutter.init(
      (options) {
        options.dsn = 'https://5c4522025b874d648a46d7e73e639d32@o998131.ingest.sentry.io/5956613';
      },
      appRunner: () => runApp(FocusedApp()),
    );
  else
    runApp(FocusedApp());
}