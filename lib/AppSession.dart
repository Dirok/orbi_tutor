import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:tutor/focused.dart';

AppSession appSession;

class AppSession {
  static const String _httpTokenKey = 'http_token';
  static const String _refreshTokenKey = 'refresh_token';
  static const String _userKey = 'user';
  static const String _partnerKey = 'partner';
  static const String _firebaseJwtKey = 'firebase_jwt';
  static const String _fcmKey = 'fcm_token';

  final Account account;
  final String partner;

  bool isAuthorized = false;
  bool isAnonymous = false;
  String fcmToken;

  String _httpToken;
  String get httpToken => _httpToken;

  String _refreshToken;
  String get refreshToken => _refreshToken;

  String _firebaseJwt;
  String get firebaseJwt => _firebaseJwt;

  AppSession(this._httpToken, this._refreshToken, this.account, this.partner, {String firebaseJwt})
      : _firebaseJwt = firebaseJwt;

  AppSession.fromAuthorizationBundle(AuthorizationBundle authInfo)
      : _httpToken = authInfo.tokenBundle.httpsToken,
        _refreshToken = authInfo.tokenBundle.refreshToken,
        account = authInfo.account,
        partner = authInfo.partner;

  Future<void> setFirebaseJwt(String jwt) async {
    _firebaseJwt = jwt;
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(_firebaseJwtKey, _firebaseJwt);
  }

  Future<void> setHttpToken(String token) async {
    _httpToken = token;
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(_httpTokenKey, httpToken);
  }

  Future<void> setRefreshToken(String token) async {
    _refreshToken = token;
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(_refreshTokenKey, refreshToken);
  }

  Future<void> setAccountPref() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(_userKey, JsonEncoder().convert(account));
  }

  Future<void> save() async {
    var prefs = await SharedPreferences.getInstance();
    await prefs.setString(_userKey, JsonEncoder().convert(account));
    await prefs.setString(_httpTokenKey, httpToken);
    await prefs.setString(_refreshTokenKey, refreshToken);

    if (fcmToken != null)
      await prefs.setString(_fcmKey, fcmToken);

    if (partner != null && partner.isNotEmpty)
      await prefs.setString(_partnerKey, partner);
    else
      await prefs.remove(_partnerKey);
  }

  static Future<AppSession> load() async {
    final prefs = await SharedPreferences.getInstance();

    final httpToken = prefs.getString(_httpTokenKey);
    if (httpToken == null) return null;

    final refreshToken = prefs.getString(_refreshTokenKey);
    if (refreshToken == null) return null;

    final userData = prefs.getString(_userKey);
    if (userData == null) return null;

    final account = Account.fromJson(JsonDecoder().convert(userData));

    final partner = prefs.getString(_partnerKey);

    final firebaseJwt = prefs.getString(_firebaseJwtKey);

    return AppSession(httpToken, refreshToken, account, partner, firebaseJwt: firebaseJwt);
  }

  Future<void> reset() async {
    isAuthorized = false;
    isAnonymous = false;

    final prefs = await SharedPreferences.getInstance();
    await prefs.remove(_httpTokenKey);
    await prefs.remove(_userKey);
    await prefs.remove(_partnerKey);
    await prefs.remove(_firebaseJwtKey);
    await prefs.remove(_fcmKey);

    appSession = null;
  }
}
