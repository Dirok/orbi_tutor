import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tutor/focused.dart';

Future<Map> getTurnCredential(String host, int port) async {
  final url = Uri.parse('https://$host:$port/api/turn?service=turn&username=flutter-webrtc');
  final res = await http.get(url);
  if (res.statusCode == 200) {
    final data = json.decode(res.body);
    Log.info('getTurnCredential:response => $data.');
    return data;
  }
  return {};
}