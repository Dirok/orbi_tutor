import 'package:web_socket_channel/html.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

WebSocketChannel connectToUri(uri,
    {Iterable<String> protocols,
      Map<String, dynamic> headers,
      Duration pingInterval}) {
  return HtmlWebSocketChannel.connect(uri, protocols: protocols);
}