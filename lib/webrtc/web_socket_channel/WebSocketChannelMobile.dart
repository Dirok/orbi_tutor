import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

WebSocketChannel connectToUri(uri,
    {Iterable<String> protocols,
    Map<String, dynamic> headers,
    Duration pingInterval}) {
  return IOWebSocketChannel.connect(uri,
      protocols: protocols, headers: headers, pingInterval: pingInterval);
}
