import 'package:web_socket_channel/web_socket_channel.dart';

WebSocketChannel connectToUri(uri,
    {Iterable<String> protocols,
    Duration pingInterval}) {
  throw UnsupportedError('No implementation of the connect web socket channel provided');
}
