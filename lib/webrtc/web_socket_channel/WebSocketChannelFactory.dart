import 'package:tutor/focused.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class WebSocketChannelFactory {
  static WebSocketChannel connect(uri, {Iterable<String> protocols, Duration pingInterval}) {
    return connectToUri(uri, protocols: protocols, pingInterval: pingInterval);
  }
}
