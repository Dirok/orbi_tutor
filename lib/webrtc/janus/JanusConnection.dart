import 'dart:async';

import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:tutor/focused.dart';

typedef void OnAddStreamCallback(
    JanusConnection connection, MediaStream stream);
typedef void OnIceCandidateCallback(
    JanusConnection connection, RTCIceCandidate candidate);
typedef void OnIceGatheringCompleted(JanusConnection connection);

class JanusConnection {
  int handleId;
  RTCPeerConnection _connection;

  OnIceCandidateCallback onIceCandidate;
  OnAddStreamCallback onAddStream;

  OnIceGatheringCompleted onIceGatheringCompleted;

  MediaStream _remoteStream;
  MediaStream get remoteStream => _remoteStream;

  JanusConnection(this.handleId);

  Future<void> init() async {
    _connection = await createPeerConnection(
        RTCSettingsConstants.configuration, RTCSettingsConstants.config);

    _connection.onSignalingState =
        (state) => Log.info(state, className: 'SignalingState');

    _connection.onIceGatheringState = (RTCIceGatheringState state) {
      Log.info(state, methodName: 'IceGatheringState');

      if (state == RTCIceGatheringState.RTCIceGatheringStateComplete)
        onIceGatheringCompleted?.call(this);
    };

    _connection.onIceConnectionState = (RTCIceConnectionState state) =>
        Log.info(state, methodName: 'IceConnectionState');

    _connection.onIceCandidate = (candidate) {
      this.onIceCandidate(this, candidate);
    };

    _connection.onIceConnectionState = (state) {};

    _connection.onAddStream = (stream) {
      Log.info("Connection on add stream $stream");
      _remoteStream = stream;
      this.onAddStream(this, stream);
    };

    _connection.onRemoveStream = (stream) {
      _remoteStream = null;
    };

    // _connection.onTrack = (RTCTrackEvent event) {
    //   if (event.streams.isNotEmpty)
    //     this.onAddStream(this, event.streams[0]);
    // };
  }

  Future<RTCSessionDescription> setLocalDescription() async {
    var sdp = await _connection.createOffer(RTCSettingsConstants.constraints);
    _connection.setLocalDescription(sdp);
    return sdp;
  }

  Future<RTCSessionDescription> setRemoteDescription(Map jsep) async {
    var sdp = RTCSessionDescription(jsep['sdp'], jsep['type']);
    try {
      await _connection.setRemoteDescription(sdp);
    } catch (e) {
      print(e);
    }
    return sdp;
  }

  Future<void> addLocalStream(MediaStream localStream) =>
      _connection.addStream(localStream);
  Future<void> removeLocalStream(MediaStream localStream) =>
      _connection.removeStream(localStream);

  // Future<void> removeTracks() async {
  //   var senders = await _connection.getSenders();
  //   for (var sender in senders)
  //     await _connection.removeTrack(sender);
  // }

  // addLocalStreamTracks(MediaStream localStream) =>
  //     localStream.getTracks().forEach((track) => _connection.addTrack(track, localStream));

  Future<RTCSessionDescription> createAnswer() async {
    var sdp = await _connection.createAnswer(RTCSettingsConstants.constraints);
    _connection.setLocalDescription(sdp);
    return sdp;
  }

  Future<void> close() => _connection?.close();
}
