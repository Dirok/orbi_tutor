import 'package:flutter/foundation.dart';

typedef void TransactionSuccess(Map data);
typedef void TransactionError(Map data);
typedef void TransactionDestroyed();

class JanusTransaction {
  String transId;

  TransactionSuccess success;
  TransactionError error;
  TransactionDestroyed destroyed;

  JanusTransaction({@required this.transId});
}