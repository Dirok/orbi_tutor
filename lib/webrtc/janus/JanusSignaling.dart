import 'dart:async';
import 'dart:collection';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:tutor/focused.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

typedef void OnPluginMessage(int handleId, Map msg, Map jsep);
typedef void OnJanusConnectionClosed();
typedef void OnJanusConnectionError();
typedef void OnJanusError(int code);

class JanusSignaling {
  static const _keyJanus = 'janus';
  static const _protocols = ['janus-protocol'];
  static const _pingInterval = Duration(seconds: 15);

  static const _keepAliveInterval = Duration(seconds: 30);

  Timer _keepAliveTimer;
  
  OnPluginMessage onPluginMessage;
  OnJanusConnectionClosed connectionClosed;
  OnJanusConnectionError onConnectionError;
  OnJanusError onError;

  WebSocketChannel _channel;
  StreamSubscription _channelSubscription;

  final _transMap = <String, JanusTransaction>{};
  final _handleMap = HashSet<int>();

  int sessionId = -1;

  final _encoder = JsonEncoder();
  final _decoder = JsonDecoder();

  String _token;

  void connect(String url, String token) {
    _token = token;

    _channel = WebSocketChannelFactory.connect(url, protocols: _protocols, pingInterval: _pingInterval);
    _channelSubscription = _channel.stream.listen((message) {
      Log.info('receive $message');
      this.handleMessage(_decoder.convert(message));
    });

    _channelSubscription.onDone(() {
      Log.info('closed by server');
      connectionClosed?.call();
    });

    _channelSubscription.onError((err) {
      Log.info('connection error: ' + err.runtimeType.toString());
      onConnectionError?.call();
    });
  }

  void disconnect() {
    Log.info('disconnect');

    _keepAliveTimer?.cancel();
    _keepAliveTimer = null;

    _channelSubscription?.cancel();
    _channel?.sink?.close();

    sessionId = -1;
  }

  void send(Map map) {
    final json = _encoder.convert(map);
    Log.info('send $json');
    _channel?.sink?.add(json);
  }

  void ping({String plugin, String opaqueId,
    TransactionSuccess success, TransactionError error}) {
    final transaction = randomNumeric(12);

    final jt = JanusTransaction(transId: transaction);
    jt.success = success;
    jt.error = error;
    _transMap[transaction] = jt;

    final ping = {
      'janus': 'ping',
      'transaction': transaction,
      'token': _token,
    };

    send(ping);
  }

  void createSession({TransactionSuccess success, TransactionError error}) {
    final transaction = randomNumeric(12);

    final jt = JanusTransaction(transId: transaction);

    jt.success = (Map data) {
      Log.info('createSession');
      sessionId = data['data']['id'];
      _keepAlive();
      success(data);
    };

    jt.error = (Map data) {
      _keepAliveTimer?.cancel();
      _keepAliveTimer = null;
      sessionId = -1;
      error(data);
    };

    _transMap[transaction] = jt;

    final createMessage = {
      'janus': 'create',
      'transaction': transaction,
      'token': _token,
    };

    send(createMessage);
  }

  void attach({String plugin, String opaqueId,
    TransactionSuccess success, TransactionError error}) {
    final transaction = randomNumeric(12);

    final jt = JanusTransaction(transId: transaction);
    jt.success = success;
    jt.error = error;
    _transMap[transaction] = jt;

    final attachMessage = {
      'janus': 'attach',
      'plugin': plugin,
      'transaction': transaction,
      'session_id': sessionId,
      'opaque_id': opaqueId,
      'token': _token,
    };

    send(attachMessage);
  }

  void trickleCandidate({int handleId, Map candidate}) {
    final trickleMessage = {
      'janus': 'trickle',
      'candidate': candidate,
      'transaction': randomNumeric(12),
      'session_id': sessionId,
      'handle_id': handleId,
      'token': _token,
    };

    send(trickleMessage);
  }

  void _keepAlive() {
    if (sessionId == -1) {
      _keepAliveTimer?.cancel();
      _keepAliveTimer = null;
      return;
    }

    final alive = {
      'janus': 'keepalive',
      'session_id': sessionId,
      'transaction': randomNumeric(12),
      'token': _token,
    };

    send(alive);

    _keepAliveTimer = Timer(_keepAliveInterval, _keepAlive);
  }

  void sendMessage({Map body, Map jsep, int handleId}) {
    final transaction = randomNumeric(12);
    final msg = {
      'janus': 'message',
      'body': body,
      'transaction': transaction,
      'session_id': sessionId,
      'handle_id': handleId,
      'token': _token,
    };
    
    if (jsep != null)
      msg['jsep'] = jsep;

    _handleMap.add(handleId);
    
    send(msg);
  }

  void handleMessage(Map message) {
    var janus = message[_keyJanus];
    if (janus == null) return;

    switch (janus) {
      case 'pong':
      case 'success':
        var transaction = message['transaction'];
        var jt = _transMap[transaction];
        jt?.success(message);
        _transMap.remove(transaction);
        break;
      case 'error':
        var transaction = message['transaction'];
        var jt = _transMap[transaction];
        jt?.error(message);
        _transMap.remove(transaction);
        final error = message['error'];
        onError?.call(error['code']);
        break;
      case 'ack':
      case 'event':
      case 'detached':
        break;
    }

    if (message.containsKey('plugindata'))
      _onPluginDataReceived(message);
  }

  void _onPluginDataReceived(Map message) {
    var handleId = message['sender'];
    if (!_handleMap.contains(handleId)) {
      Log.info('missing handle?');
     return;
    }
    var pluginMsg = message['plugindata']['data'];
    var jsep = message['jsep'];
    this.onPluginMessage(handleId, pluginMsg, jsep);
  }
}