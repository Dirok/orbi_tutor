class RTCSettingsConstants {
  static const Map<String, dynamic> configuration = {
    // 'sdpSemantics': 'plan-b',
    // 'sdpSemantics': 'unified-plan',
    'iceServers': [
      {'url':'stun:stun1.l.google.com:19302'},
      {'url':'stun:stun2.l.google.com:19302'},
      {'url':'stun:stun3.l.google.com:19302'},
      {'url':'stun:stun4.l.google.com:19302'},
      // {
      //   'url': 'turn:numb.viagenie.ca',
      //   'credential': 'muazkh',
      //   'username': 'webrtc@live.com'
      // },
    ],
    'tcpCandidatePolicy': 'disabled'
  };

  static const Map<String, dynamic> config = {
    'mandatory': {},
    'optional': [
      {'DtlsSrtpKeyAgreement': true},
    ],
  };

  static const Map<String, dynamic> constraints = {
    'mandatory': {
      'OfferToReceiveAudio': true,
      'OfferToReceiveVideo': true,
    },
    'optional': [],
  };

  static final Map<String, dynamic> mediaConstraintsAudioOnly = {
    'audio': {
      'channelCount': '1',
      'echoCancellation': true,
      'autoGainControl': true,
      'noiseSuppression': true,
      'sampleRate': '44000',
      'sampleSize': '16',
    },
    'video': false
  };

  static final Map<String, dynamic> mediaConstraintsVideo = {
    'audio': {
      'channelCount': '1',
      'echoCancellation': true,
      'autoGainControl': true,
      'noiseSuppression': true,
      'sampleRate': '44000',
      'sampleSize': '16',
    },
    'video': {
      'mandatory': {
        'minWidth': '640',
        'minHeight': '480',
        'minFrameRate': '30',
      },
      'facingMode': 'user',
      'optional': [],
    }
  };
}
