import 'dart:async';

import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:tutor/focused.dart';
import 'package:uuid/uuid.dart';

typedef void StreamStateCallback(MediaStream stream);

class VideoRoomSignaling {
  static const _pluginName = 'janus.plugin.videoroom';
  static const _connectionTimeoutSeconds = 10;

  final _signaling = JanusSignaling();

  bool _hasAudio;
  bool _hasVideo;

  MediaStream _localStream;
  MediaStream _currentRemoteStream;

  bool _isMicEnabled = true;

  final _opaqueId = Uuid().v4();

  JanusConnection _selfJanusConnection;

  final _janusConnectionMap = <int, JanusConnection>{};
  final _userIdMap = <int, String>{};

  int _selfHandleId;

  int _roomId;
  String _userId;
  String _name;

  MediaStream get localStream => _localStream;
  MediaStream get currentRemoteStream => _currentRemoteStream;

  StreamStateCallback onAddRemoteStream;
  StreamStateCallback onRemoveRemoteStream;

  bool _isConnected = false;
  bool get isConnected => _isConnected;

  bool get isMicEnabled => _isMicEnabled;
  set isMicEnabled(bool value) {
    setMicState(value);
    _isMicEnabled = value;
  }

  CallState _currentCallState = CallState.Not;
  CallState get callState => _currentCallState;

  final userVolumeChangedController = StreamController<UserVolume>.broadcast();
  Stream<UserVolume> get onVolumeChange => userVolumeChangedController.stream;

  final connectionStateChangeController = StreamController<ConnectionStatus>.broadcast();
  Stream<ConnectionStatus> get onConnectionStateChange => connectionStateChangeController.stream;

  final callStateChangeController = StreamController<CallState>.broadcast();
  Stream<CallState> get onCallStateChange => callStateChangeController.stream;

  final eventController = StreamController<MediaEvent>.broadcast();
  Stream<MediaEvent> get onEventReceived => eventController.stream;

  bool recordingEnabled = false;

  close() async {
    Log.info('close');

    await _closePeerConnections();

    _signaling.disconnect();

    await connectionStateChangeController.close();
    await callStateChangeController.close();
  }

  setMicState(bool enabled) {
    if(_localStream == null) return;
    for (var track in _localStream.getAudioTracks())
      track.enabled = enabled;
  }

  Future<bool> connect(MultimediaServerParams serverParams,
      String name, String userId, bool hasAudio, bool hasVideo) {
    _roomId = serverParams.id;
    _userId = userId;
    _name = name;
    _hasAudio = hasAudio;
    _hasVideo = hasVideo;

    _signaling.connectionClosed = _onConnectionClosed;
    _signaling.onConnectionError = _onConnectionError;
    _signaling.onPluginMessage = _onMessage;
    _signaling.onError = _onError;

    final completer = Completer<bool>();

    _signaling.connect(serverParams.address, serverParams.token);

    _signaling.createSession(
        success: (Map data) {
          _signaling.attach(
              plugin: _pluginName,
              opaqueId: _opaqueId,
              success: (Map data) {
                _selfHandleId = data['data']['id'];
                _joinRoom();

                _isConnected = true;
                connectionStateChangeController.add(ConnectionStatus.Opened);
                completer.complete(true);
              });
        },
        error: (Map data) {
          _isConnected = false;
          connectionStateChangeController.add(ConnectionStatus.Error);
          completer.complete(false);
        });

    try {
      return completer.future.timeout(Duration(seconds: _connectionTimeoutSeconds));
    } on TimeoutException catch(e) {
      Log.error(e);
      return Future.value(false);
    }
  }

  Completer _leaveCompleter;

  Future<void> leaveRoom() async {
    _leaveCompleter = Completer();
    _leave();
    try {
      await _leaveCompleter.future.timeout(Duration(seconds: _connectionTimeoutSeconds));
    } on TimeoutException catch(e) {
      Log.error(e);
    }
    _leaveCompleter = null;
    _closePeerConnections();
  }

//   void _createRoom(int roomId) {
//     var body = {
//       "request": "create",
//       "room": _roomId,
//       "permanent": true,
//       "description": "Classroom_" + roomId.toString(),
// //      "secret" : "<password required to edit/destroy the room, optional>",
// //      "pin" : "<password required to join the room, optional>",
//       "is_private": true,
// //      "allowed" : [ array of string tokens users can use to join this room, optional],
//     };
//
//     _signaling.sendMessage(body: body, handleId: _selfHandleId);
//   }

  void _joinRoom() {
    var body = {
      "request": "join",
      "room": _roomId,
      "ptype": "publisher",
      "display": '$_userId|$_name',
    };

    _signaling.sendMessage(body: body, handleId: _selfHandleId);
  }

  // void _checkRoomExists(int handleId, int roomId) {
  //   var body = {
  //     "request": "exists",
  //     "room": roomId
  //   };
  //
  //   _signaling.sendMessage(body: body, handleId: handleId);
  // }

  Future<void> _configure() async {
    if (_selfJanusConnection == null) {
      _selfJanusConnection = await _createJanusConnection(_selfHandleId);
    }
    else {
      // _selfJanusConnection.removeTracks();
      await removeLocalStream();
    }

    await _createLocalStream(_hasVideo);

    setMicState(_isMicEnabled);

    if (_selfJanusConnection == null) return;

    // await _selfJanusConnection.addLocalStreamTracks(_localStream);
    await _selfJanusConnection.addLocalStream(_localStream);

    var body = {
      "request": "configure",
      "audio": _hasAudio,
      "video": _hasVideo,
      "data": false,
      // "record": recordingEnabled
    };

    if (_selfJanusConnection == null) return;

    var sdp = await _selfJanusConnection.setLocalDescription();
    var jsep = {"type": sdp.type, "sdp": sdp.sdp};
    _signaling.sendMessage(handleId: _selfHandleId, body: body, jsep: jsep);
  }

 Future<void> _unpublish() async {
   await removeLocalStream();

   // var body = {'request': 'unpublish'};

   var body = {
     "request": "configure",
     "audio": false,
     "video": false,
     "data": false,
   };

   _signaling.sendMessage(body: body, handleId: _selfHandleId);
 }

  void _leave() {
    var body = {"request": "leave"};
    _signaling.sendMessage(handleId: _selfHandleId, body: body);
  }

  void _onConnectionClosed() {
    _isConnected = false;
    connectionStateChangeController.add(ConnectionStatus.Closed);
    _closePeerConnections();
  }

  void _onConnectionError() {
    _isConnected = false;
    connectionStateChangeController.add(ConnectionStatus.Error);
    _closePeerConnections();
  }
  
  void _onMessage (int handleId, Map msg, Map jsep) {
    var videoroom = msg['videoroom'];
    if (videoroom == null) return; // it is no videoroom plugin message

    switch (videoroom) {
      case 'created':
        _onCreated(msg['room']);
        break;
      case 'talking':
        _onTalking(msg);
        break;
      case 'stopped-talking':
        _onStoppedTalking(msg);
        break;
      case 'edited':
      case 'participants':
      case 'rtp_forward':
      case 'stop_rtp_forward':
      case 'forwarders':
        break;
      case 'destroyed':
        eventController.add(MediaEvent.Destroyed);
        break;
      case 'joined':
        _onJoined(msg);
        break;
      case 'attached':
        _setRemoteJsepForPeer(handleId, jsep);
        break;
      case 'success':
        _onSuccess(msg);
        break;
      case 'event':
        _onEvent(handleId, msg, jsep);
        break;
    }
  }

  void _onError(int code) {
    switch (code) {
      case Constants.noSessionErrorCode:
        eventController.add(MediaEvent.NoSession);
        break;
    }
  }

  void _onJoined(Map<dynamic, dynamic> msg) async {
    callStateChangeController.add(_currentCallState = CallState.Connected);

    if (_hasAudio || _hasVideo) _configure();

    _userIdMap[msg['id']] = _userId;
    _subscribeToPublishers(msg);
  }

  void _onTalking(Map<dynamic, dynamic> msg) async =>
      userVolumeChangedController.add(UserVolume(_userIdMap[msg['id']],
          MathUtils.convertVolumeRange(msg['audio-level-dBov-avg'])));

  void _onStoppedTalking(Map<dynamic, dynamic> msg) async =>
      userVolumeChangedController
          .add(UserVolume(_userIdMap[msg['id']], Constants.minAudioVolume));

  void _onSuccess(Map msg) {
    if (msg.containsKey('exists')) {
    } else if (msg.containsKey('allowed')) {
    } else if (msg.containsKey('rooms')) { // on 'list' requested
    } else { // on 'kick' requested
    }
  }

  void _onEvent(int handleId, Map msg, Map jsep) {
    if (msg.containsKey('started')) {
    } else if (msg.containsKey('paused')) {
    } else if (msg.containsKey('joining')) {
    } else if (msg.containsKey('configured')) {
      handleId == _selfHandleId
          ? _setRemoteJsepForSelf(jsep)
          : _setRemoteJsepForPeer(handleId, jsep);
    } else if (msg.containsKey('publishers')) {
      _subscribeToPublishers(msg);
    } else if (msg.containsKey('unpublished')) {
        msg['unpublished'] == 'ok' ? onSelfUnpublished() : onRemoteUnpublished(msg['unpublished']);
    } else if (msg.containsKey('leaving')) {
      if (msg['leaving'] == 'ok') {
        _leaveCompleter?.complete();
      } else {
        Log.info('leaving ' + msg['leaving'].toString());
        userVolumeChangedController.add(UserVolume(_userIdMap[msg['leaving']], Constants.minAudioVolume));
      }
      _userIdMap.remove(msg['leaving']);
    } else if (msg.containsKey('switched')) {
    } else if (msg.containsKey('left')) {
    } else if (msg.containsKey('error')) {
    }
  }

  void onSelfUnpublished() => _selfJanusConnection = null;

  void onRemoteUnpublished(int id) => _janusConnectionMap.remove(id);

  void _subscribeToPublishers(Map<dynamic, dynamic> msg) {
    final publishers = msg['publishers'];
    if (publishers == null) return;
    publishers.forEach((publisher) {
      _userIdMap[publisher['id']] =
          publisher['display'].toString().split('|').first;
      var feed = publisher['id'];
      _subscribeToFeed(msg['private_id'], feed);
    });
  }

  void _subscribeToFeed(int privateId, int feed) {
    _signaling.attach(
        plugin: _pluginName,
        opaqueId: _opaqueId,
        success: (data) {
          var body = {
            "request": "join",
            "room": _roomId,
            "ptype": "subscriber",
            "feed": feed
          };

          if (privateId != null)
            body['private_id'] = privateId;

          var handleId = data['data']['id'];
          _signaling.sendMessage(body: body, handleId: handleId);
        });
  }

  Future<void> _closePeerConnections() async {
    Log.info('close peer connections');

    await _disposeStreams();

    await _selfJanusConnection?.close();
    _selfJanusConnection = null;

    _janusConnectionMap.forEach((handleId, jc) async { await jc.close(); });
    _janusConnectionMap.clear();
  }

  Completer<void> _changeModeCompleter;

  Future<void> changeMode(bool hasAudio, hasVideo) async {
    if (hasAudio == _hasAudio && hasVideo == _hasVideo) return;

    _hasAudio = hasAudio;
    _hasVideo = hasVideo;

    _changeModeCompleter = Completer<void>();

    _hasAudio || _hasVideo
        ? await _configure()
        : await _unpublish();

    await _changeModeCompleter.future.timeout(Duration(seconds: 5));
    _changeModeCompleter = null;
  }

  Future<void> _disposeStreams() async {
    await removeLocalStream();
    await _currentRemoteStream?.dispose();
    _currentRemoteStream = null;
  }

  _createLocalStream(bool withVideo) async {
    Log.info("Creating local stream with video enabled = $withVideo");

    _localStream = await navigator.mediaDevices.getUserMedia(withVideo
        ? RTCSettingsConstants.mediaConstraintsVideo
        : RTCSettingsConstants.mediaConstraintsAudioOnly);
  }

  removeLocalStream() async {
    if (_localStream == null) return;
    await _selfJanusConnection?.removeLocalStream(_localStream);
    await _localStream.dispose();
    _localStream = null;
  }

  Future<JanusConnection> _createJanusConnection(int handleId) async {
    var jc = JanusConnection(handleId);
    await jc.init();

    jc.onAddStream = (connection, stream) => _onAddStream(stream);

    jc.onIceCandidate = (connection, candidate) {
      if (candidate == null) return;

      var candidateMap = {
        "candidate": candidate.candidate,
        "sdpMid": candidate.sdpMid,
        "sdpMLineIndex": candidate.sdpMlineIndex
      };

      _signaling.trickleCandidate(
          handleId: connection.handleId, candidate: candidateMap);
    };

    jc.onIceGatheringCompleted = (connection) {
      var candidateMap = {"completed": true};
      _signaling.trickleCandidate(
          handleId: connection.handleId, candidate: candidateMap);
    };

    return jc;
  }

  void _onAddStream(MediaStream stream) {
    _currentRemoteStream = stream;
    onAddRemoteStream?.call(stream);
  }

  void _onCreated(int roomId) async {
    _roomId = roomId;
    _joinRoom();
  }
  
  void _setRemoteJsepForSelf(Map jsep) async {
    if (jsep != null)
      await _selfJanusConnection?.setRemoteDescription(jsep);

    _changeModeCompleter?.complete();
  }

  void _setRemoteJsepForPeer(int handleId, Map jsep) async {
    JanusConnection jc;
    if (_janusConnectionMap.containsKey(handleId)){
      jc = _janusConnectionMap[handleId];
    } else {
      jc = await _createJanusConnection(handleId);
      _janusConnectionMap[handleId] = jc;
    }

    await jc.setRemoteDescription(jsep);

    if (jc.remoteStream != null)
      _onAddStream(jc.remoteStream);

    var body = {'request': 'start', 'room': _roomId};
    var sdp = await jc.createAnswer();
    var jsepMap = {'type': sdp.type, 'sdp': sdp.sdp};

    _signaling.sendMessage(body: body, handleId: handleId, jsep: jsepMap);
  }
}

enum ConnectionStatus {
  Opened,
  Closed,
  Error
}

enum CallState {
  Ringing,
  Invite,
  Connected,
  Not
}

enum MediaEvent {
  NoSession,
  Destroyed
}