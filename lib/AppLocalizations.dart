import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class AppLocalizations {
  final Locale locale;

  AppLocalizations(this.locale);

  // Helper method to keep the code in the widgets concise
  // Localizations are accessed using an InheritedWidget "of" syntax
  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  // static member to have a simple access to the delegate from MaterialApp
  static const LocalizationsDelegate<AppLocalizations> delegate =
    _AppLocalizationsDelegate();

  Map<String, String> _localizedString;

  Future<bool> load() async {
    String jsonString = await rootBundle.loadString(
        'assets/translations/${locale.languageCode}.json');  //_${locale.countryCode}  translations/

    Map<String, dynamic> jsonMap = json.decode(jsonString);

    _localizedString = jsonMap.map((key, value) {
      return MapEntry(key, value.toString());
    });
    return true;
  }
  
  String translate(String key) {
    return _localizedString[key];
  }
}

// LocalizationsDelegate is a factory for a set of localized resources
// In this case, the localized strings will be gotten in a AppLocalizations object
class _AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  // this delegate instance will never change, it can provide const constructor
  const _AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['en', 'ru', 'kk'].contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) async {
    AppLocalizations localizations = new AppLocalizations(locale);
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}

abstract class TranslatedState<T extends StatefulWidget> extends State<T> {
  String translate(String key) => AppLocalizations.of(context).translate(key);
}

abstract class TranslatedStatelessWidget extends StatelessWidget with TranslatableMixin { }

mixin TranslatableMixin {
  String translate(String key) => AppLocalizations.of(Get.context).translate(key);
}