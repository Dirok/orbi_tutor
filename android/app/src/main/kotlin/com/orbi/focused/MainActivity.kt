package com.orbi.focused

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.media.AudioFocusRequest
import android.media.AudioManager
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.*
import android.util.Log
import androidx.annotation.NonNull
import androidx.core.app.NotificationManagerCompat
import com.orbi.focused.KillNotificationsService.KillBinder
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.EventChannel.EventSink
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant

enum class ActiveNetwork { Wifi, Cellular }

class MainActivity : FlutterActivity() {
    private val _pushStream = "com.orbi.tutor/pushTrigger"
    private val _connectivityStream = "dev.fluttercommunity.plus/connectivity_status"

    private var _connectivityEvents: EventSink? = null
    private var _serviceConnection: ServiceConnection? = null
    private val _mainHandler = Handler(Looper.getMainLooper())
    private val _cellularNetworkRequest = getCellularNetworkRequest()
    private val _wifiNetworkRequest = getWifiNetworkRequest()
    private val _cellularNetworkCallback = getCellularNetworkCallBack()
    private val _wifiNetworkCallback = getWifiNetworkCallBack()
    private var _activeNetwork = "none"
    private val _activeNetworks = mutableSetOf<ActiveNetwork>()

    private var _killerActive = false

    /// Notification ID for persistent notification - same as in android flutter part
    private val NOTIFICATION_ID = 112231

    private fun getConnectivityManager() =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine)

        EventChannel(
            flutterEngine.dartExecutor.binaryMessenger,
            _connectivityStream
        ).setStreamHandler(object :
            EventChannel.StreamHandler {
            override fun onListen(args: Any?, events: EventSink?) {
                events?.success("adding connectivity listener")
                _connectivityEvents = events
            }

            override fun onCancel(args: Any?) {
                _connectivityEvents = null
                Log.d("TAG", "cancelling connectivity listener")
            }
        })

        checkInternetAccess()

        MethodChannel(
            flutterEngine.dartExecutor.binaryMessenger,
            _pushStream
        ).setMethodCallHandler { call, result ->
            when (call.method) {
                "startKiller" -> {
                    Log.d("TAG", "starting killer")
                    startKiller()
                    result.success(true)
                }
                "stopKiller" -> {
                    Log.d("TAG", "cancelling killer")
                    stopKiller()
                    result.success(true)
                }
                else -> {
                    result.notImplemented()
                }
            }
        }
    }

    private fun startKiller() {
        if (_killerActive) return
        _killerActive = true
        _serviceConnection = object : ServiceConnection {
            override fun onServiceConnected(
                className: ComponentName?,
                binder: IBinder
            ) {
                (binder as KillBinder).service.startService(
                    Intent(
                        this@MainActivity,
                        KillNotificationsService::class.java
                    )
                )
            }

            override fun onServiceDisconnected(className: ComponentName?) {}
        }
        val intent = Intent(context, KillNotificationsService::class.java)
        if (_serviceConnection != null)
            bindService(intent, _serviceConnection as ServiceConnection, Context.BIND_AUTO_CREATE)
    }

    private fun stopKiller() {
        if (!_killerActive) return
        _killerActive = false
        if (_serviceConnection != null)
            unbindService(_serviceConnection as ServiceConnection)
    }

    private fun checkInternetAccess() {
        val capabilities =
            getConnectivityManager().getNetworkCapabilities(getConnectivityManager().activeNetwork)
        if (capabilities != null) {
            if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                _activeNetworks.add(ActiveNetwork.Cellular)
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                _activeNetworks.add(ActiveNetwork.Wifi)
            }
        }
        sendEvent()
    }

    private fun getCellularNetworkRequest(): NetworkRequest {
        return NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .build()
    }

    private fun getWifiNetworkRequest(): NetworkRequest {
        return NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .build()
    }

    private fun getCellularNetworkCallBack(): ConnectivityManager.NetworkCallback {
        return object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)

                _activeNetworks.add(ActiveNetwork.Cellular)
                sendEvent()
            }

            override fun onLost(network: Network) {
                super.onLost(network)

                _activeNetworks.remove(ActiveNetwork.Cellular)
                sendEvent()
            }
        }
    }

    private fun getWifiNetworkCallBack(): ConnectivityManager.NetworkCallback {
        return object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)

                _activeNetworks.add(ActiveNetwork.Wifi)
                sendEvent()
            }

            override fun onLost(network: Network) {
                super.onLost(network)

                _activeNetworks.remove(ActiveNetwork.Wifi)
                sendEvent()
            }
        }
    }

    private fun sendEvent() {
        if (_connectivityEvents == null) return
        _activeNetwork = when {
            _activeNetworks.contains(ActiveNetwork.Wifi) -> "wifi"
            _activeNetworks.contains(ActiveNetwork.Cellular) -> "mobile"
            else -> "none"
        }
        val runnable = Runnable { _connectivityEvents!!.success(_activeNetwork) }
        _mainHandler.post(runnable)
    }

    override fun startActivity(intent: Intent?) {
        intent?.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_TASK_ON_HOME
        super.startActivity(intent)
    }

    override fun startActivity(intent: Intent?, options: Bundle?) {
        intent?.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_TASK_ON_HOME
        super.startActivity(intent, options)
    }

    override fun setIntent(newIntent: Intent?) {
        newIntent?.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_TASK_ON_HOME
        super.setIntent(newIntent)
    }

    override fun onDestroy() {
        // Another method if first didn't kill notification
        NotificationManagerCompat.from(context).cancel(NOTIFICATION_ID)
        if (_serviceConnection != null)
            unbindService(_serviceConnection as ServiceConnection)
        super.onDestroy()
    }

    override fun onPause() {
        getConnectivityManager().unregisterNetworkCallback(_cellularNetworkCallback)
        getConnectivityManager().unregisterNetworkCallback(_wifiNetworkCallback)
        super.onPause()
    }

    override fun onResume() {
        checkInternetAccess()
        getConnectivityManager().registerNetworkCallback(
            _cellularNetworkRequest,
            _cellularNetworkCallback
        )
        getConnectivityManager().registerNetworkCallback(
            _wifiNetworkRequest,
            _wifiNetworkCallback
        )
        super.onResume()
    }
}
