package com.orbi.focused

import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder

/// Class for killing background notification after killing app
class KillNotificationsService : Service() {
    class KillBinder(val service: Service) : Binder()

    private var _notificationManager: NotificationManager? = null
    private val _binder: IBinder = KillBinder(this)

    override fun onBind(intent: Intent?): IBinder {
        return _binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    override fun onCreate() {
        _notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager?
        _notificationManager?.cancel(NOTIFICATION_ID)
    }

    companion object {
        /// Notification ID for persistent notification - same as in android flutter part
        private const val NOTIFICATION_ID = 112231
    }
}